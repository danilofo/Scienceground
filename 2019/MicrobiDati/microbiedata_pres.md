---
title: Microbi e dati
---

## Cosa c'entra l'influenza con le statistiche di Wikipedia?

### 
![](./images/journal.pcbi.1003892.png)
(da Plos CompBio, Nicholas Generous et al. 2014 )

### Letture in rete
* [Historical predictions of GFT](https://ai.googleblog.com/2015/08/the-next-chapter-for-flu-trends.html)
* [What can we learn from Google Flu Trend epic fail?](https://www.wired.com/2015/10/can-learn-epic-failure-google-flu-trends/)

## Chiederesti a un computer di misurarti la febbre?

### DeepMind: Health
<img src="./images/nn.png" width="50%" style="background:none; border:none; box-shadow:none;"> 

## Resistenze antibiotiche e machine learning

###
![](./images/resistance.gif)

### Creare nuovi antibiotici usando il machine learning 

### 
* [ML per capire i meccanismi di azione degli antibiotici noti](https://news.mit.edu/2019/how-antibiotics-kill-bacteria-0509)
* [ML per scoprire nuovi farmaci]( https://www.nature.com/articles/d41586-018-05267-x)

### “AI is going to lead to the full understanding of human biology and give us the means to fully address human disease” (T. Chittenden, Wuxi NextCODE - Contract Genomics Organization, 2018)

### Paster's hygiene "makes it possible to prevent the morbid causes, to remove diseases so as not to have to cure them"(E. Alix, 1882, Revue Scientifique)

## Fin qui tutto bene?

## Equità algoritmica e salute pubblica <img src="./images/salut-public-resized.JPG" width="90%" style="background:none;">

## Sicurezza e dati biomedici

###  [Deanonimizzazione dei dati](https://cpg.doc.ic.ac.uk/individual-risk/)


## Consigli di lettura

### Libri sull'intelligenza artificiale
* _Computer e cervello_, John Von Neumann
* _Macchine che pensano_, a cura di Douglas Heaven, Edizioni Dedalo 2018

### Articoli su rivista
The Lab's Quarterly, 2018,  n. 4 (ottobre-dicembre):

* Mattia Galeotti, _Discriminazione e algoritmi. Incontri e scontri tra diverse idee di fairness_
* Chiara Visentin, _Il potere razionale degli algoritmi tra burocrazia e nuovi idealtipi_
