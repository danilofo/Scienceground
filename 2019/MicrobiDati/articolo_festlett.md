# 
---
author: ExTemporanea
---

## Scienceground
[Scienceground][scienceground] è uno spazio di comunicazione scientifica orizzontale, gestito da ExTemporanea, una piccola comunità scientifica di studenti universitari, ricercatori e cittadini con l'occhio attento al ruolo della scienza nella società. Quest'anno il tema intorno a cui ruotano le attività è "Microbi"; dopo una [discussione][latour] col filosofo Bruno Latour, intervistato alla scorsa edizione di Scienceground, siamo rimasti affascinati da quanti "misteri" ci siano in "quattro metri quadri di terreno". Ecosistemi altrettanto complessi si nascondono nelle acque del Rio di Mantova, nel pane che mangiamo ogni giorno, addirittura sulla punta delle nostre dita. Per osservare più da vicino questo mondo microscopico, i nostri laboratori portano i più curiosi a contatto con le tecniche sperimentali della ricerca biologica. Nei gruppi di lettura e nelle discussioni cerchiamo di avvicinare tutti agli aspetti più sottili e problematici della ricerca scientifica. Per esporci alle idee più innovative abbiamo pensato di "contaminarci" con altri temi grazie agli ospiti già presenti a Festivaletteratura; alcune di queste _contaminazioni_ ci permettono di dare continuità ai discorsi che sono stati toccati dalla precedente edizione di Scienceground, dedicata al vasto tema dei "dati".

[scienceground]: www.scienceground.it
[latour]: https://www.iltascabile.com/scienze/latour-scienze-intervista

## Microbi e dati
Giovedì 5, alle 17;30, il chiostro di Isabella d'Este ha ospitato la _contaminazione_ "Microbi e big data", con la partecipazione di Mattia Galeotti. La discussione col pubblico, attraverso gli esempi delle ricerche su come prevedere i picchi influenzali, aiutare i medici nella diagnosi di malattie asintomatiche e tanto altro ancora, ha indagato cosa si intende con _machine learning_ e _Big data_. La seconda parte dell'evento ha affrontato i risvolti sociali della diffusione del machine learning medico e della raccolta di dati sensibili su larga scala: cosa può succedere quando diventa semplice reidentificare qualcuno in un database "anonimizzato" e quali sfide tecniche ed epistemologiche si aprono di fronte all'idea di equità algoritmica (_fairness_)?

## Machine learning a Festivaletteratura
Alle 19:15 l'equità algoritmica è stata il tema della lavagna "Anche gli algoritmi hanno pregiudizi?", in cui Mattia ha ampliato la prospettiva introdotta nella contaminazione. Le altre lavagne della serie "algoritmi" saranno venerdì alle 11:00, "Di algoritmi, _machine learning_ e altri demoni", di Carlotta Orsenigo; sabato alle 18:00, "Il ruolo del tempo nell'apprendimento umano e delle macchine", di Marco Gori e domenica alle 10:00, "Intelligenza artificiale umana", di Dino Pedreschi. 

Cercateci in giro per il Festivaletteratura e venite a trovarci a Scienceground per discutere con noi di questo e tanto altro.
