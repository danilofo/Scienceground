# Coordinamento Microbi e Dati

Ciao a tutti, queste sono le note per il coordinamento dell'evento su microbi e dati. Siete tutti liberi di contribuire e modificare. Se siete in disaccordo con qualche cosa aggiungete una riga di commento tipo 
[danilo]: # commento

## Materiale 
Trovate il materiale raccolto in due cartelle, a seconda della tipologia di articoli. Ovviamente ogni altro contributo è più che apprezzato.
La cartella images raccoglie le immagini che si possono inserire nella presentazione.
La cartella reveal.js serve per ragioni tecniche documentate nel file readme.md
La presentazione microbiedata_pres non è ancora completa, e sarebbe meglio ultimarla a Mantova insieme.

## Obiettivi della discussione
Introduzione:
-------------
* Cosa si intende con ML/AI ("I computer possono pensare? Ci sostituiranno?")
* Cosa significa lavorare con correlazioni statistiche
* Quali sono le applicazioni biomediche più comuni o più probabili in futuro

Casi di studio: 
---------------
* Predizione dei picchi epidemici usando dei proxy (Wikipedia): come funziona e quali limiti ha 
articoli generalisti: 
- wired_gft.pdf  

articoli specialistici 
- journal.pcbi.1003892.pdf 

* DeepMind Health e diagnostica con reti neurali
  Fonti: Tesi Luisa
  
* Bacterial computing: che cos'è e a cosa può servire
Articoli specialistici
- fmicb-05-00101.pdf

Riflessioni generali:
---------------------
* Definizione della fairness di un algoritmo e come questo influisce sulla sanità pubblica
Articoli specialistici
- labsquarterly2018galeotti.pdf
- revpublhealth2018Mooney.pdf

* Dati personali di tipo biomedico e valore dell'anonimato
articoli specialistici 
- s41467-019-10933-3.pdf
