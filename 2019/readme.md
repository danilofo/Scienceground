# How to create a presentation with pandoc and reveal.js (in a UNIX-like environment, sorry)
1. download and extract [reveal.js][https://revealjs.com]
2. download and install [pandoc][https://pandoc.org]
3. write the slides in markdown, a title like # or ## creates a new slide
4. in a terminal, launch
```
pandoc -t revealjs -s -o mypres.html mysource.md -V revealjs-url=https://revealjs.com
```
This should work even without downloading reveal.js, but needs an internet connection during the presentation.

5. open `mypres.html` in the browser, and use F11 to visualize.  


Alternative options:  
* to link the local version of reveal.js (should be available in the local directory)
**mandatory when including external markdown**
```-V revealjs-url=./reveal.js```
* to choose a different theme 
```-V theme:$theme``` 
For available options see https://github.com/jgm/pandoc/wiki/Using-pandoc-to-produce-reveal.js-slides
