ScienceGround 1.5
-----------------

1. Citazioni non relative ai microbi
------------------------------------

- Charles Lamb, Streghe e altri terrori della notte (Witches and other night-fears, citato in H.P. Lovecraft.Il dominatore delle tenebre. Feltrinelli 2012)  
"Gorgoni e Idra e Chimere - storie sinistre di Celeno e delle Arpie - possono replicare loro stesse nella mente della superstizione, ma esistevano da prima. Sono trascrizioni, tipologie, sono archetipi all'interno di noi, e sono eterni. Come altro potrebbe, la pantomima del considerare falso ciò che conosciamo nello stato di veglia, avere effetti su tutti noi? È forse perché proviamo un naturale terrore verso simili entità, considerata la loro capacità di infliggere danni materiali al nostro corpo? Oh, assolutamente no! Questi terrori provengono da un passato molto più antico. Risalgono a un tempo antecedente il tempo del corpo - e anche senza il corpo, sarebbero esistiti comunque..." 

- William Shakespeare, Macbeth (Sansoni 1912)
Atto I. Scena I. Le tre streghe nei pressi di un insalubre campo di battaglia:
"Il bello è brutto, il brutto è bello. Libriamoci attraverso la nebbia e l'aria immonda."
(Fair is foul, and foul is fair: Hover through the fog and filthy air.)

- J.L. Borges, There Are More Things
"Il selvaggio non può percepire la Bibbia del missionario; il passeggero non vede lo stesso cordame che vede l'equipaggio. Se vedessimo realmente l'universo, forse lo capiremmo"

"La curiosità fu più forte della paura e non chiusi gli occhi."

2. Spiriti e microbi nel Nuovo Testamento e oltre
-------------------------------------------------

La Bibbia di Gerusalemme, Edizioni Dehoniane Bologna

- Matteo 8,29 nota:
"in attesa del giorno del giudizio, i demoni godono di una certa libertà nella loro azione sulla terra; lo fanno di preferenza prendendo possesso degli uomini. Questa possessione è accompagnata spesso da una malattia, poiché questa, a titolo di conseguenza del peccato, è un'altra manifestazione dell'azione di Satana. Così gli esorcismi del Vangelo, che a volte, come qui, appaiono allo stato puro, avvengono spesso in forma di guarigione. [...]" 

- Matteo 12,22
"In quel tempo gli fu portato un indemoniato, cieco e muto, ed egli lo guarì, sicché il muto parlava e vedeva."

- Luca 13,10
"C'era là una donna che aveva da diciotto anni uno spirito che la teneva inferma; era curva e non poteva drizzarsi in nessun modo"

- Nell'AT, il Salmo 91,6 offre un passaggio lovecraftiano in cui la pestilenza è associata all'aleggiare di orrori notturni
"La sua fedeltà ti sarà scudo e corazza; non temerai i terrori della notte, né la freccia che vola di giorno, la peste che vaga nelle tenebre, lo sterminio che devasta a mezzogiorno"
cfr. CCCP, "Maciste contro TUTTI" per una realizzazione contemporanea di questi terrori arcaici, con stilisti milanesi nei panni di entità aliene

3. Microbi e dati
-----------------

### Appunti vari
Vergogna prometeica come opposto dell'orgoglio del selfmade man (Gunther Anders): l'eccessiva complessità nega l'esistenza di un atto di creazione

Similitudine biologica: sistema nervoso e architetture computazionali (Von Neumann)

Bacterial computing: using bacteria for solving problems that today are solved by computers
https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3971165/


### Discussione

* Prima parte: usi del ML in biologia

* Seconda parte: 

4. Filosofia della biologia
---------------------------

Microbi 'sintetici' e speciazione:
http://www.raiscuola.rai.it/programma-unita/telmo-pievani-dal-silicio-al-microbo-la-biologia-sintetica/273/39143/default.aspx
https://science.sciencemag.org/content/351/6280/aad6253.full

5. Altri temi ScienceGroundiani
-------------------------------

- Video:
1. Resistenze antibiotiche e fagi: https://youtu.be/YI3tsmFsrOg (Kurzgesagt)
2. Microbioma e condividui: 
   https://youtu.be/VzPD009qTN4 (Kurzgesagt)
   Luther Blisset

