---
title: Reti neurali
---

# Come fa il cervello a pensare? 


## 
<img src="images/cani_gatti.jpg"> 

## 
<img src="images/brain.jpg">


## 
<img src="images/n_signals.jpg">

## 

<img src="images/neuron.png">


# Possiamo far pensare un computer? 

## 
<img src="images/perceptron-picture.png">

## 
<img src="images/perceptron_decision_function.png">

##
<img src="images/artificial_nn.png">


# Supervised / Unsupervised learning

## Getti del geyser Old Faithful
<img src="images/old_faithful.gif">
