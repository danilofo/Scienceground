#! /bin/bash

# view the results
echo ""
echo "RESULTS:"
echo ""

echo "Storia della colonna infame:"
echo "==========================="
ps_sdci=$(stat --printf="%s" ps+sdci.md.gz)
tln_sdci=$(stat --printf="%s" tln+sdci.md.gz)
ps=$(stat --printf="%s" i_promessi_sposi.md.gz)
m=$(stat --printf="%s" tutte_le_novelle.md.gz)
diff_manzoni=$(echo "${ps_sdci} - ${ps}" | bc -l)
diff_verga=$(echo "${tln_sdci} - ${m}" | bc -l)
if (( ${diff_manzoni} < ${diff_verga} )); then
    attr_auth="Manzoni"
else
    attr_auth="Verga"
fi
echo "Compressed size using Manzoni's style: ${diff_manzoni}"
echo "Compressed size using Verga's style: ${diff_verga}"
echo "The Lempel-Ziv algorithm attributes the unknown text to ${attr_auth}"
echo ""

echo "I Malavoglia:"
echo "============"
ps_m=$(stat --printf="%s" ps+m.md.gz)
tln_m=$(stat --printf="%s" tln+m.md.gz)
ps=$(stat --printf="%s" i_promessi_sposi.md.gz)
m=$(stat --printf="%s" tutte_le_novelle.md.gz)
diff_manzoni=$(echo "${ps_m} - ${ps}" | bc -l)
diff_verga=$(echo "${tln_m} - ${m}" | bc -l)
if (( ${diff_manzoni} < ${diff_verga} )); then
    attr_auth="Manzoni"
else
    attr_auth="Verga"
fi
echo "Compressed size using Manzoni's style: ${diff_manzoni}"
echo "Compressed size using Verga's style: ${diff_verga}"
echo "The Lempel-Ziv algorithm attributes the unknown text to ${attr_auth}"
