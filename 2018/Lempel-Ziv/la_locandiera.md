[]{#Section0001.xhtml}

::: {.immagine_copertina}
<svg xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink" height="100%" width="100%" preserveaspectratio="xMidYMid meet" version="1.1" viewbox="0 0 1057 1500">
<desc>Copertina di La Locandiera, di Carlo Goldoni</desc>
<image height="1500" width="1057" href="../Images/copertina.jpg"></image>
</svg>
Copertina {#Section0001.xhtml#testo_copertina}
=========
:::

[]{#Section0002.xhtml}

Informazioni {.sigil_not_in_toc}
============

Questo e-book è stato realizzato anche grazie al sostegno di:

::: {.box_sponsor}
[![E-text](Images/e-text.png)](http://www.e-text.it/)\
**E-text**\
Editoria, Web design, Multimedia

[Pubblica il tuo libro, o crea il tuo sito con
E-text!](http://www.e-text.it/)
:::

QUESTO E-BOOK:

TITOLO: La locandiera\
AUTORE: Goldoni, Carlo\
TRADUTTORE:\
CURATORE: Antonucci, Giovanni\
NOTE: Note critiche a cura di Laura Barberi

CODICE ISBN E-BOOK: 9788828100522

DIRITTI D\'AUTORE: sì, sulle note critiche

LICENZA: questo testo è distribuito con la licenza specificata al
seguente indirizzo Internet:
http://www.liberliber.it/online/opere/libri/licenze/

COPERTINA: \[elaborazione da\] \"Madame de Sorquainville (1749)\" di
Jean-Baptiste Perronneau (1715--1783) - Musée du Louvre, Paris -
https://commons.wikimedia.org/wiki/File:Perronneau\_Madame\_de\_Sorquainville.jpg
- Pubblico Dominio.

TRATTO DA: La locandiera / Carlo Goldoni ; a cura di Giovanni Antonucci.
- Roma : Tascabili Economici Newton, 1993. - 93 p. ; 20 cm. -
(Centopaginemillelire 71).

CODICE ISBN FONTE: 88-7983-064-3

1a EDIZIONE ELETTRONICA DEL: 2 febbraio 1996\
2a EDIZIONE ELETTRONICA DEL: 2 febbraio 2014

INDICE DI AFFIDABILITÀ: 1\
0: affidabilità bassa\
1: affidabilità standard\
2: affidabilità buona\
3: affidabilità ottima

SOGGETTO:\
PER015000 ARTI RAPPRESENTATIVE / Commedia

DIGITALIZZAZIONE:\
Stefano D'Urso

REVISIONE:\
Susanna Corona\
Ugo Santamaria

IMPAGINAZIONE:\
Catia Righi, catia\_righi\@tin.it (ODT)\
Mariano Piscopo (ePub)\
Rosario Di Mauro (revisione ePub)

PUBBLICAZIONE:\
Catia Righi, catia\_righi\@tin.it\
Ugo Santamaria

[]{#Section0003.xhtml}

Liber Liber
===========

[![Fai una
donazione](Images/2_euro.png)](http://www.liberliber.it/online/aiuta/)

Se questo libro ti è piaciuto, aiutaci a realizzarne altri. Fai una
donazione: <http://www.liberliber.it/online/aiuta/>.

Scopri sul sito Internet di Liber Liber ciò che stiamo realizzando:
migliaia di ebook gratuiti in edizione integrale, audiolibri, brani
musicali con licenza libera, video e tanto altro:
<http://www.liberliber.it/>.

[]{#Section0004.xhtml}

Indice generale

-   [Copertina](../Text/Section0001.xhtml#testo_copertina)
-   [Informazioni](../Text/Section0002.xhtml)
-   [Liber Liber](../Text/Section0003.xhtml)
-   [Note critiche](../Text/Section0005.xhtml)
-   [La locandiera](../Text/Section0006.xhtml)
-   [PERSONAGGI](../Text/Section0007.xhtml)
-   [L\'autore a chi legge](../Text/Section0008.xhtml)
-   [ATTO PRIMO](../Text/Section0009.xhtml)
    -   [SCENA PRIMA](../Text/Section0009.xhtml#sigil_toc_id_39)
    -   [SCENA SECONDA](../Text/Section0009.xhtml#sigil_toc_id_40)
    -   [SCENA TERZA](../Text/Section0009.xhtml#sigil_toc_id_41)
    -   [SCENA QUARTA](../Text/Section0009.xhtml#sigil_toc_id_42)
    -   [SCENA QUINTA](../Text/Section0009.xhtml#sigil_toc_id_43)
    -   [SCENA SESTA](../Text/Section0009.xhtml#sigil_toc_id_44)
    -   [SCENA SETTIMA](../Text/Section0009.xhtml#sigil_toc_id_45)
    -   [SCENA OTTAVA](../Text/Section0009.xhtml#sigil_toc_id_46)
    -   [SCENA NONA](../Text/Section0009.xhtml#sigil_toc_id_47)
    -   [SCENA DECIMA](../Text/Section0009.xhtml#sigil_toc_id_48)
    -   [SCENA UNDICESIMA](../Text/Section0009.xhtml#sigil_toc_id_49)
    -   [SCENA DODICESIMA](../Text/Section0009.xhtml#sigil_toc_id_50)
    -   [SCENA TREDICESIMA](../Text/Section0009.xhtml#sigil_toc_id_51)
    -   [SCENA
        QUATTORDICESIMA](../Text/Section0009.xhtml#sigil_toc_id_52)
    -   [SCENA QUINDICESIMA](../Text/Section0009.xhtml#sigil_toc_id_53)
    -   [SCENA SEDICESIMA](../Text/Section0009.xhtml#sigil_toc_id_54)
    -   [SCENA
        DICIASSETTESIMA](../Text/Section0009.xhtml#sigil_toc_id_55)
    -   [SCENA DICIOTTESIMA](../Text/Section0009.xhtml#sigil_toc_id_56)
    -   [SCENA
        DICIANNOVESIMA](../Text/Section0009.xhtml#sigil_toc_id_57)
    -   [SCENA VENTESIMA](../Text/Section0009.xhtml#sigil_toc_id_58)
    -   [SCENA VENTUNESIMA](../Text/Section0009.xhtml#sigil_toc_id_59)
    -   [SCENA VENTIDUESIMA](../Text/Section0009.xhtml#sigil_toc_id_60)
    -   [SCENA VENTITREESIMA](../Text/Section0009.xhtml#sigil_toc_id_61)
-   [ATTO SECONDO](../Text/Section0010.xhtml)
    -   [SCENA PRIMA](../Text/Section0010.xhtml#sigil_toc_id_62)
    -   [SCENA SECONDA](../Text/Section0010.xhtml#sigil_toc_id_63)
    -   [SCENA TERZA](../Text/Section0010.xhtml#sigil_toc_id_64)
    -   [SCENA QUARTA](../Text/Section0010.xhtml#sigil_toc_id_65)
    -   [SCENA QUINTA](../Text/Section0010.xhtml#sigil_toc_id_66)
    -   [SCENA SESTA](../Text/Section0010.xhtml#sigil_toc_id_67)
    -   [SCENA SETTIMA](../Text/Section0010.xhtml#sigil_toc_id_68)
    -   [SCENA OTTAVA](../Text/Section0010.xhtml#sigil_toc_id_69)
    -   [SCENA NONA](../Text/Section0010.xhtml#sigil_toc_id_70)
    -   [SCENA DECIMA](../Text/Section0010.xhtml#sigil_toc_id_71)
    -   [SCENA UNDICESIMA](../Text/Section0010.xhtml#sigil_toc_id_72)
    -   [SCENA DODICESIMA](../Text/Section0010.xhtml#sigil_toc_id_73)
    -   [SCENA TREDICESIMA](../Text/Section0010.xhtml#sigil_toc_id_74)
    -   [SCENA
        QUATTORDICESIMA](../Text/Section0010.xhtml#sigil_toc_id_75)
    -   [SCENA QUINDICESIMA](../Text/Section0010.xhtml#sigil_toc_id_76)
    -   [SCENA SEDICESIMA](../Text/Section0010.xhtml#sigil_toc_id_77)
    -   [SCENA
        DICIASSETTESIMA](../Text/Section0010.xhtml#sigil_toc_id_78)
    -   [SCENA DICIOTTESIMA](../Text/Section0010.xhtml#sigil_toc_id_79)
    -   [SCENA
        DICIANNOVESIMA](../Text/Section0010.xhtml#sigil_toc_id_80)
-   [ATTO TERZO](../Text/Section0011.xhtml)
    -   [SCENA PRIMA](../Text/Section0011.xhtml#sigil_toc_id_81)
    -   [SCENA SECONDA](../Text/Section0011.xhtml#sigil_toc_id_82)
    -   [SCENA TERZA](../Text/Section0011.xhtml#sigil_toc_id_83)
    -   [SCENA QUARTA](../Text/Section0011.xhtml#sigil_toc_id_84)
    -   [SCENA QUINTA](../Text/Section0011.xhtml#sigil_toc_id_85)
    -   [SCENA SESTA](../Text/Section0011.xhtml#sigil_toc_id_86)
    -   [SCENA SETTIMA](../Text/Section0011.xhtml#sigil_toc_id_87)
    -   [SCENA OTTAVA](../Text/Section0011.xhtml#sigil_toc_id_88)
    -   [SCENA NONA](../Text/Section0011.xhtml#sigil_toc_id_89)
    -   [SCENA DECIMA](../Text/Section0011.xhtml#sigil_toc_id_90)
    -   [SCENA UNDICESIMA](../Text/Section0011.xhtml#sigil_toc_id_91)
    -   [SCENA DODICESIMA](../Text/Section0011.xhtml#sigil_toc_id_92)
    -   [SCENA TREDICESIMA](../Text/Section0011.xhtml#sigil_toc_id_93)
    -   [SCENA
        QUATTORDICESIMA](../Text/Section0011.xhtml#sigil_toc_id_94)
    -   [SCENA QUINDICESIMA](../Text/Section0011.xhtml#sigil_toc_id_95)
    -   [SCENA SEDICESIMA](../Text/Section0011.xhtml#sigil_toc_id_96)
    -   [SCENA
        DICIASSETTESIMA](../Text/Section0011.xhtml#sigil_toc_id_97)
    -   [SCENA DICIOTTESIMA](../Text/Section0011.xhtml#sigil_toc_id_98)
    -   [SCENA
        DICIANNOVESIMA](../Text/Section0011.xhtml#sigil_toc_id_99)
    -   [SCENA ULTIMA](../Text/Section0011.xhtml#sigil_toc_id_100)

[]{#Section0005.xhtml}

Note critiche
=============

a cura di Laura Barberi

Commedia in tre atti rappresentata per la prima volta nel 1753, è una
delle più famose e stilisticamente riuscite tra le numerose scritte da
Carlo Goldoni (1707- 1793). In breve la trama: il conte di Albafiorita e
il marchese di Forlipopoli, ospiti nella locanda di Mirandolina a
Firenze, si contendono il suo amore, il primo con doni che può
facilmente permettersi grazie alla sua buona posizione economica, il
secondo, appartenente a quella parte di nobiltà decaduta e ormai senza
mezzi, tenta di conquistarla con promesse di protezione. Nella locanda
viene ospitato anche il cavaliere di Ripafratta, un convinto misogino
che si vanta di essere immune al fascino femminile ed anzi sostiene di
disprezzare l\'intero sesso \"debole\". Mirandolina, risentita del suo
atteggiamento e della sua insensibilità, decide di provarsi nel farlo
innamorare di sé e senza troppe difficoltà ci riesce, causando gelosie e
liti tra i tre pretendenti. Riuscita nell\'intento di far capitolare il
cavaliere, ella però ne rifiuta l\'amore così come prima aveva rifiutato
la corte dei due nobiluomini e concede la propria mano al cameriere
della locanda Fabrizio.

Questa commedia, che non fu mai tra le preferite del Goldoni (a cui
piacevano di più le sue commedie d\'insieme, corali), fotografa però
chiaramente l\'ormai raggiunta maturità artistica del suo autore. Non
solo l\'assoluta padronanza del mezzo scenico: la sapiente
organizzazione delle varie scene, l\'attenta caratterizzazione di tutti
i personaggi, i dialoghi ben calibrati, il chiaro delineamento
dell\'ambiente sociale all\'interno del quale si svolge l\'azione; ma
anche il raggiungimento di una \"forma\" teatrale originale e a lungo
ricercata negli anni, la materializzazione scenica di quella riforma
teatrale goldoniana che è storicamente il principale merito attribuito
dai critici al grande commediografo veneziano.

L\'opera di Carlo Goldoni, infatti, si colloca in un periodo di acceso
dibattito, in Italia ma anche nel resto d\'Europa, sulle modalità e
sugli intenti del teatro: sempre di più prende piede, sulla spinta del
pensiero illuminista e dell\'opera di [Diderot]{lang="fr"} in Francia o
di [Lessing]{lang="de"} in Germania, il cosiddetto dramma borghese, un
tipo di teatro che doveva ispirarsi alla società contemporanea, i cui
intrecci dovevano risultare plausibili e che doveva avere fini educativi
e di denuncia. In Italia, in particolare, Goldoni si trovò a dover fare
i conti con l\'ingombrante eredità della Commedia dell\'Arte: il
successo che continuava ad arridere alle maschere ed ai canovacci,
seppur usurati, di questa tradizione teatrale rendeva più difficile
qualsiasi innovazione per l\'opposizione sia del pubblico, appunto, sia
degli attori poco propensi a lasciare il loro ruolo di istrioni, maghi
dell\'improvvisazione, per piegarsi alla fedeltà ad un testo scritto;
abituati a recitare sempre solo la stessa parte, ad indossare la stessa
maschera - Arlecchino, Pantalone, Colombina - il cui comportamento e
linguaggio si erano \"sclerotizzati\" da tempo, erano riluttanti a
doversi calare ogni volta in un nuovo personaggio a seconda della
commedia.

In questo contesto la \"riforma\" goldoniana dovette procedere
gradualmente, tenendo conto sia dei gusti del pubblico che delle
necessità degli attori e degli impresari, ma passo passo Goldoni riuscì
a modificare le convenzioni drammaturgiche. Furono sostanzialmente due i
punti fermi di questa riforma: innanzitutto l\'introduzione di un testo
scritto fisso, non modificabile dagli attori, in sostituzione dei
canovacci della Commedia dell\'Arte, più che altro delle semplici
situazioni base, dei punti di partenza che lasciavano ampio spazio
all\'improvvisazione degli attori; e poi l\'eliminazione delle maschere,
vale a dire il passaggio dai tipi ai caratteri, a degli individui ben
precisi. Tutto ciò per rendere il suo teatro realistico, raffigurazione
sulla scena di un mondo reale. Così Mirandolina ne La locandiera è un
personaggio ben definito non solo caratterialmente, ma anche
socialmente; non è più la \"servetta\" intrigante della commedia
precedente, il tipo della \"donnina brillante e capricciosa\", ma è una
locandiera con i suoi affari, i suoi interessi. È un personaggio
radicato in una precisa realtà sociale, lontano da generalizzazioni e
stereotipi, il cui comportamento è dettato non da caratteristiche fisse
ed immutabili, ma dalle sue esperienze e dalla sua posizione sociale ed
è significativo che a conclusione della commedia Mirandolina sposi un
borghese come lei, il cameriere Fabrizio.

Una conclusione che conferma come, aldilà del piacere per una storia ben
congeniata, molti siano i temi di quest\'opera. L\'irrompere sul
palcoscenico di valori borghesi, quali l\'operosità, il senso della
misura, l\'attenzione al guadagno, non rappresenta il solo motivo di
interesse, per così dire, sociologico che questa commedia propone: non
va tralasciata infatti la parte di \"eroe positivo\" che la donna
ricopre all\'interno de La locandiera come pure in altre commedie di
Goldoni. La pubblicistica illuministica di quegli anni dibatteva
ampiamente sulla necessità di un nuovo e più importante ruolo per la
donna nella società e l\'autore veneziano ne rende testimonianza fedele
nel suo teatro, aderendo anche in questo caso a quelle spinte
modernizzatrici che stavano scuotendo l\'[ancien régime]{lang="fr"} in
tutta Europa. Tanti elementi diversi che però non fanno che confermare
la validità e la godibilità di questa commedia ancora oggi a più di due
secoli di distanza dalla sua stesura.

[]{#Section0006.xhtml}

La locandiera {.testo_frontespizio_titolo}
=============

di Carlo Goldoni

[]{#Section0007.xhtml}

PERSONAGGI
==========

Il Cavaliere di Ripafratta\
Il Marchese di Forlipopoli\
Il Conte d\'Albafiorita\
Mirandolina, [locandiera]{.testo_corsivo}\
Ortensia [comica]{.testo_corsivo}\
Dejanira [comica]{.testo_corsivo}\
Fabrizio, [cameriere di locanda]{.testo_corsivo}\
Servitore, [del Cavaliere]{.testo_corsivo}\
Servitore, [del Conte]{.testo_corsivo}

La scena si rappresenta in Firenze, nella locanda di Mirandolina.

[]{#Section0008.xhtml}

L\'autore a chi legge
=====================

Fra tutte le Commedie da me sinora composte, starei per dire essere
questa la più morale, la più utile, la più istruttiva. Sembrerà ciò
essere un paradosso a chi soltanto vorrà fermarsi a considerare il
carattere della [Locandiera]{.testo_corsivo}, e dirà anzi non aver io
dipinto altrove una donna più lusinghiera, più pericolosa di questa. Ma
chi rifletterà al carattere e agli avvenimenti del Cavaliere, troverà un
esempio vivissimo della presunzione avvilita, ed una scuola che insegna
a fuggire i pericoli, per non soccombere alle cadute.

Mirandolina fa altrui vedere come s\'innamorano gli uomini. Principia a
entrar in grazia del disprezzator delle donne, secondandolo nel modo suo
di pensare, lodandolo in quelle cose che lo compiacciono, ed eccitandolo
perfino a biasimare le donne istesse. Superata con ciò l\'avversione che
aveva il Cavaliere per essa, principia a usargli delle attenzioni, gli
fa delle finezze studiate, mostrandosi lontana dal volerlo obbligare
alla gratitudine. Lo visita, lo serve in tavola, gli parla con umiltà e
con rispetto, e in lui vedendo scemare la ruvidezza, in lei s\'aumenta
l\'ardire. Dice delle tronche parole, avanza degli sguardi, e senza
ch\'ei se ne avveda, gli dà delle ferite mortali. Il pover\'uomo conosce
il pericolo, e lo vorrebbe fuggire, ma la femmina accorta con due
lagrimette l\'arresta, e con uno svenimento l\'atterra, lo precipita,
l\'avvilisce. Pare impossibile, che in poche ore un uomo possa
innamorarsi a tal segno: un uomo, aggiungasi, disprezzator delle donne,
che mai ha seco loro trattato; ma appunto per questo più facilmente egli
cade, perché sprezzandole senza conoscerle, e non sapendo quali sieno le
arti loro, e dove fondino la speranza de\' loro trionfi, ha creduto che
bastar gli dovesse a difendersi la sua avversione, ed ha offerto il
petto ignudo ai colpi dell\'inimico.

Io medesimo diffidava quasi a principio di vederlo innamorato
ragionevolmente sul fine della Commedia, e pure, condotto dalla natura,
di passo in passo, come nella Commedia si vede, mi è riuscito di darlo
vinto alla fine dell\'Atto secondo.

Io non sapeva quasi cosa mi fare nel terzo, ma venutomi in mente, che
sogliono coteste lusinghiere donne, quando vedono ne\' loro lacci gli
amanti, aspramente trattarli, ho voluto dar un esempio di questa barbara
crudeltà, di questo ingiurioso disprezzo con cui si burlano dei
miserabili che hanno vinti, per mettere in orrore la schiavitù che si
procurano gli sciagurati, e rendere odioso il carattere delle
incantatrici Sirene. La Scena dello [stirare]{.testo_corsivo} allora
quando la Locandiera si burla del Cavaliere che languisce, non muove gli
animi a sdegno contro colei, che dopo averlo innamorato l\'insulta? Oh
bello specchio agli occhi della gioventù! Dio volesse che io medesimo
cotale specchio avessi avuto per tempo, che non avrei veduto ridere del
mio pianto qualche barbara Locandiera. Oh di quante Scene mi hanno
provveduto le mie vicende medesime!\... Ma non è il luogo questo né di
vantarmi delle mie follie, né di pentirmi delle mie debolezze. Bastami
che alcun mi sia grato della lezione che gli offerisco. Le donne che
oneste sono, giubileranno anch\'esse che si smentiscano codeste
simulatrici, che disonorano il loro sesso, ed esse femmine lusinghiere
arrossiranno in guardarmi, e non importa che mi dicano
nell\'incontrarmi: che tu sia maledetto!

Deggio avvisarvi, Lettor carissimo, di una picciola mutazione, che alla
presente Commedia ho fatto. Fabrizio, il cameriere della Locanda,
parlava in veneziano, quando si recitò la prima volta; l\'ho fatto
allora per comodo del personaggio, solito a favellar da Brighella; ove
l\'ho convertito in toscano, sendo disdicevole cosa introdurre senza
necessità in una Commedia un linguaggio straniero. Ciò ho voluto
avvertire, perché non so come la stamperà il Bettinelli; può essere
ch\'ei si serva di questo mio originale, e Dio lo voglia, perché almeno
sarà a dover penneggiato. Ma lo scrupolo ch\'ei si è fatto di stampare
le cose mie come io le ho abbozzate, lo farà trascurare anche questa
comodità.

[]{#Section0009.xhtml}

ATTO PRIMO
==========

SCENA PRIMA {#Section0009.xhtml#sigil_toc_id_39}
-----------

Sala di locanda.\
Il Marchese di Forlipopoli ed il Conte d\'Albafiorita

MARCHESE:

Fra voi e me vi è qualche differenza.

CONTE:

Sulla locanda tanto vale il vostro denaro, quanto vale il mio.

MARCHESE:

Ma se la locandiera usa a me delle distinzioni, mi si convengono più che
a voi.

CONTE:

Per qual ragione?

MARCHESE:

Io sono il Marchese di Forlipopoli.

CONTE:

Ed io sono il Conte d\'Albafiorita.

MARCHESE:

Sì, Conte! Contea comprata.

CONTE:

Io ho comprata la contea, quando voi avete venduto il marchesato.

MARCHESE:

Oh basta: son chi sono, e mi si deve portar rispetto.

CONTE:

Chi ve lo perde il rispetto? Voi siete quello, che con troppa libertà
parlando\...

MARCHESE:

Io sono in questa locanda, perché amo la locandiera. Tutti lo sanno, e
tutti devono rispettare una giovane che piace a me.

CONTE:

Oh, questa è bella! Voi mi vorreste impedire ch\'io amassi Mirandolina?
Perché credete ch\'io sia in Firenze? Perché credete ch\'io sia in
questa locanda?

MARCHESE:

Oh bene. Voi non farete niente.

CONTE:

Io no, e voi sì?

MARCHESE:

Io sì, e voi no. Io son chi sono. Mirandolina ha bisogno della mia
protezione.

CONTE:

Mirandolina ha bisogno di denari, e non di protezione.

MARCHESE:

Denari?\... non ne mancano.

CONTE:

Io spendo uno zecchino il giorno, signor Marchese, e la regalo
continuamente.

MARCHESE:

Ed io quel che fo non lo dico.

CONTE:

Voi non lo dite, ma già si sa.

MARCHESE:

Non si sa tutto.

CONTE:

Sì! caro signor Marchese, si sa. I camerieri lo dicono. Tre paoletti il
giorno.

MARCHESE:

A proposito di camerieri; vi è quel cameriere che ha nome Fabrizio, mi
piace poco. Parmi che la locandiera lo guardi assai di buon occhio.

CONTE:

Può essere che lo voglia sposare. Non sarebbe cosa mal fatta. Sono sei
mesi che è morto il di lei padre. Sola una giovane alla testa di una
locanda si troverà imbrogliata. Per me, se si marita, le ho promesso
trecento scudi.

MARCHESE:

Se si mariterà, io sono il suo protettore, e farò io\... E so io quello
che farò.

CONTE:

Venite qui: facciamola da buoni amici. Diamole trecento scudi per uno.

MARCHESE:

Quel ch\'io faccio, lo faccio segretamente, e non me ne vanto. Son chi
sono. Chi è di là?

(Chiama.)

CONTE:

(Spiantato! Povero e superbo!).

(Da sé.)

SCENA SECONDA {#Section0009.xhtml#sigil_toc_id_40}
-------------

Fabrizio e detti.

FABRIZIO:

Mi comandi, signore.

(Al Marchese.)

MARCHESE:

Signore? Chi ti ha insegnato la creanza?

FABRIZIO:

La perdoni.

CONTE:

Ditemi: come sta la padroncina?

(A Fabrizio.)

FABRIZIO:

Sta bene, illustrissimo.

MARCHESE:

È alzata dal letto?

FABRIZIO:

Illustrissimo sì.

MARCHESE:

Asino.

FABRIZIO:

Perché, illustrissimo signore?

MARCHESE:

Che cos\'è questo illustrissimo?

FABRIZIO:

È il titolo che ho dato anche a quell\'altro Cavaliere.

MARCHESE:

Tra lui e me vi è qualche differenza.

CONTE:

Sentite?

(A Fabrizio.)

FABRIZIO:

(Dice la verità. Ci è differenza: me ne accorgo nei conti).

(Piano al Conte.)

MARCHESE:

Di\' alla padrona che venga da me, che le ho da parlare.

FABRIZIO:

Eccellenza sì. Ho fallato questa volta?

MARCHESE:

Va bene. Sono tre mesi che lo sai; ma sei un impertinente.

FABRIZIO:

Come comanda, Eccellenza.

CONTE:

Vuoi vedere la differenza che passa fra il Marchese e me?

MARCHESE:

Che vorreste dire?

CONTE:

Tieni. Ti dono uno zecchino. Fa che anch\'egli te ne doni un altro.

FABRIZIO:

Grazie, illustrissimo.

(Al Conte.)

Eccellenza\...

(Al Marchese.)

MARCHESE:

Non getto il mio, come i pazzi. Vattene.

FABRIZIO:

Illustrissimo signore, il cielo la benedica.

(Al Conte.)

Eccellenza. (Rifinito. Fuor del suo paese non vogliono esser titoli per
farsi stimare, vogliono esser quattrini).

(Da sé, parte.)

SCENA TERZA {#Section0009.xhtml#sigil_toc_id_41}
-----------

Il Marchese ed il Conte.

MARCHESE:

Voi credete di soverchiarmi con i regali, ma non farete niente. Il mio
grado val più di tutte le vostre monete.

CONTE:

Io non apprezzo quel che vale, ma quello che si può spendere.

MARCHESE:

Spendete pure a rotta di collo. Mirandolina non fa stima di voi.

CONTE:

Con tutta la vostra gran nobiltà, credete voi di essere da lei stimato?
Vogliono esser denari.

MARCHESE:

Che denari? Vuol esser protezione. Esser buono in un incontro di far un
piacere.

CONTE:

Sì, esser buono in un incontro di prestar cento doppie.

MARCHESE:

Farsi portar rispetto bisogna.

CONTE:

Quando non mancano denari, tutti rispettano.

MARCHESE:

Voi non sapete quel che vi dite.

CONTE:

L\'intendo meglio di voi.

SCENA QUARTA {#Section0009.xhtml#sigil_toc_id_42}
------------

Il Cavaliere di Ripafratta dalla sua camera, e detti.

CAVALIERE:

Amici, che cos\'è questo romore? Vi è qualche dissensione fra di voi
altri?

CONTE:

Si disputava sopra un bellissimo punto.

MARCHESE:

II Conte disputa meco sul merito della nobiltà.

(Ironico.)

CONTE:

Io non levo il merito alla nobiltà: ma sostengo, che per cavarsi dei
capricci, vogliono esser denari.

CAVALIERE:

Veramente, Marchese mio\...

MARCHESE:

Orsù, parliamo d\'altro.

CAVALIERE:

Perché siete venuti a simil contesa?

CONTE:

Per un motivo il più ridicolo della terra.

MARCHESE:

Sì, bravo! il Conte mette tutto in ridicolo.

CONTE:

Il signor Marchese ama la nostra locandiera. Io l\'amo ancor più di lui.
Egli pretende corrispondenza, come un tributo alla sua nobiltà. Io la
spero, come una ricompensa alle mie attenzioni. Pare a voi che la
questione non sia ridicola?

MARCHESE:

Bisogna sapere con quanto impegno io la proteggo.

CONTE:

Egli la protegge, ed io spendo.

(Al Cavaliere.)

CAVALIERE:

In verità non si può contendere per ragione alcuna che io meriti meno.
Una donna vi altera? vi scompone? Una donna? che cosa mai mi convien
sentire? Una donna? Io certamente non vi è pericolo che per le donne
abbia che dir con nessuno. Non le ho mai amate, non le ho mai stimate, e
ho sempre creduto che sia la donna per l\'uomo una infermità
insopportabile.

MARCHESE:

In quanto a questo poi, Mirandolina ha un merito estraordinario.

CONTE:

Sin qua il signor Marchese ha ragione. La nostra padroncina della
locanda è veramente amabile.

MARCHESE:

Quando l\'amo io, potete credere che in lei vi sia qualche cosa di
grande.

CAVALIERE:

In verità mi fate ridere. Che mai può avere di stravagante costei, che
non sia comune all\'altre donne?

MARCHESE:

Ha un tratto nobile, che incatena.

CONTE:

È bella, parla bene, veste con pulizia, è di un ottimo gusto.

CAVALIERE:

Tutte cose che non vagliono un fico. Sono tre giorni ch\'io sono in
questa locanda, e non mi ha fatto specie veruna.

CONTE:

Guardatela, e forse ci troverete del buono.

CAVALIERE:

Eh, pazzia! L\'ho veduta benissimo. È una donna come l\'altre.

MARCHESE:

Non è come l\'altre, ha qualche cosa di più. Io che ho praticate le
prime dame, non ho trovato una donna che sappia unire, come questa, la
gentilezza e il decoro.

CONTE:

Cospetto di bacco! Io son sempre stato solito trattar donne: ne conosco
li difetti ed il loro debole. Pure con costei, non ostante il mio lungo
corteggio e le tante spese per essa fatte, non ho potuto toccarle un
dito.

CAVALIERE:

Arte, arte sopraffina. Poveri gonzi! Le credete, eh? A me non la
farebbe. Donne? Alla larga tutte quante elle sono.

CONTE:

Non siete mai stato innamorato?

CAVALIERE:

Mai, né mai lo sarò. Hanno fatto il diavolo per darmi moglie, né mai
l\'ho voluta.

MARCHESE:

Ma siete unico della vostra casa: non volete pensare alla successione?

CAVALIERE:

Ci ho pensato più volte ma quando considero che per aver figliuoli mi
converrebbe soffrire una donna, mi passa subito la volontà.

CONTE:

Che volete voi fare delle vostre ricchezze?

CAVALIERE:

Godermi quel poco che ho con i miei amici.

MARCHESE:

Bravo, Cavaliere, bravo; ci goderemo.

CONTE:

E alle donne non volete dar nulla?

CAVALIERE:

Niente affatto. A me non ne mangiano sicuramente.

CONTE:

Ecco la nostra padrona. Guardatela, se non è adorabile.

CAVALIERE:

Oh la bella cosa! Per me stimo più di lei quattro volte un bravo cane da
caccia.

MARCHESE:

Se non la stimate voi, la stimo io.

CAVALIERE:

Ve la lascio, se fosse più bella di Venere.

SCENA QUINTA {#Section0009.xhtml#sigil_toc_id_43}
------------

Mirandolina e detti.

MIRANDOLINA:

M\'inchino a questi cavalieri. Chi mi domanda di lor signori?

MARCHESE:

Io vi domando, ma non qui.

MIRANDOLINA:

Dove mi vuole, Eccellenza?

MARCHESE:

Nella mia camera.

MIRANDOLINA:

Nella sua camera? Se ha bisogno di qualche cosa verra il cameriere a
servirla.

MARCHESE:

(Che dite di quel contegno?).

(Al Cavaliere.)

CAVALIERE:

(Quello che voi chiamate contegno, io lo chiamerei temerità,
impertinenza).

(Al Marchese.)

CONTE:

Cara Mirandolina, io vi parlerò in pubblico, non vi darò l\'incomodo di
venire nella mia camera. Osservate questi orecchini. Vi piacciono?

MIRANDOLINA:

Belli.

CONTE:

Sono diamanti, sapete?

MIRANDOLINA:

Oh, gli conosco. Me ne intendo anch\'io dei diamanti.

CONTE:

E sono al vostro comando.

CAVALIERE:

(Caro amico, voi li buttate via).

(Piano al Conte.)

MIRANDOLINA:

Perché mi vuol ella donare quegli orecchini?

MARCHESE:

Veramente sarebbe un gran regalo! Ella ne ha de\' più belli al doppio.

CONTE:

Questi sono legati alla moda. Vi prego riceverli per amor mio.

CAVALIERE:

(Oh che pazzo!).

(Da sé.)

MIRANDOLINA:

No, davvero, signore\...

CONTE:

Se non li prendete, mi disgustate.

MIRANDOLINA:

Non so che dire\... mi preme tenermi amici gli avventori della mia
locanda. Per non disgustare il signor Conte, li prenderò.

CAVALIERE:

(Oh che forca!).

(Da sé.)

CONTE:

(Che dite di quella prontezza di spirito?).

(Al Cavaliere.)

CAVALIERE:

(Bella prontezza! Ve li mangia, e non vi ringrazia nemmeno).

(Al Conte.)

MARCHESE:

Veramente, signor Conte, vi siete acquistato gran merito. Regalare una
donna in pubblico, per vanità! Mirandolina, vi ho da parlare a
quattr\'occhi, fra voi e me: son Cavaliere.

MIRANDOLINA:

(Che arsura! Non gliene cascano).

(Da sé.)

Se altro non mi comandano, io me n\'anderò.

CAVALIERE:

Ehi! padrona. La biancheria che mi avete dato, non mi gusta. Se non ne
avete di meglio, mi provvederò.

(Con disprezzo.)

MIRANDOLINA:

Signore, ve ne sarà di meglio. Sarà servita, ma mi pare che la potrebbe
chiedere con un poco di gentilezza.

CAVALIERE:

Dove spendo il mio denaro, non ho bisogno di far complimenti.

CONTE:

Compatitelo. Egli è nemico capitale delle donne.

(A Mirandolina.)

CAVALIERE:

Eh, che non ho bisogno d\'essere da lei compatito.

MIRANDOLINA:

Povere donne! che cosa le hanno fatto? Perché così crudele con noi,
signor Cavaliere?

CAVALIERE:

Basta così. Con me non vi prendete maggior confidenza. Cambiatemi la
biancheria. La manderò a prender pel servitore. Amici, vi sono schiavo.

(Parte.)

SCENA SESTA {#Section0009.xhtml#sigil_toc_id_44}
-----------

Il Marchese, il Conte e Mirandolina.

MIRANDOLINA:

Che uomo salvatico! Non ho veduto il compagno.

CONTE:

Cara Mirandolina, tutti non conoscono il vostro merito.

MIRANDOLINA:

In verità, son così stomacata del suo mal procedere, che or ora lo
licenzio a dirittura.

MARCHESE:

Sì; e se non vuol andarsene, ditelo a me, che lo farò partire
immediatamente. Fate pur uso della mia protezione.

CONTE:

E per il denaro che aveste a perdere, io supplirò e pagherò tutto.
(Sentite, mandate via anche il Marchese, che pagherò io).

(Piano a Mirandolina.)

MIRANDOLINA:

Grazie, signori miei, grazie. Ho tanto spirito che basta, per dire ad un
forestiere ch\'io non lo voglio, e circa all\'utile, la mia locanda non
ha mai camere in ozio.

SCENA SETTIMA {#Section0009.xhtml#sigil_toc_id_45}
-------------

Fabrizio e detti.

FABRIZIO:

Illustrissimo, c\'è uno che la domanda.

(Al Conte.)

CONTE:

Sai chi sia?

FABRIZIO:

Credo ch\'egli sia un legatore di gioje. (Mirandolina, giudizio; qui non
istate bene).

(Piano a Mirandolina, e parte.)

CONTE:

Oh sì, mi ha da mostrare un gioiello. Mirandolina, quegli orecchini,
voglio che li accompagniamo.

MIRANDOLINA:

Eh no, signor Conte\...

CONTE:

Voi meritate molto, ed io i denari non li stimo niente. Vado a vedere
questo gioiello. Addio, Mirandolina; signor Marchese, la riverisco!

(Parte.)

SCENA OTTAVA {#Section0009.xhtml#sigil_toc_id_46}
------------

Il Marchese e Mirandolina.

MARCHESE:

(Maledetto Conte! Con questi suoi denari mi ammazza).

(Da sé.)

MIRANDOLINA:

In verità il signor Conte s\'incomoda troppo.

MARCHESE:

Costoro hanno quattro soldi, e li spendono per vanità, per albagia. Io
li conosco, so il viver del mondo.

MIRANDOLINA:

Eh, il viver del mondo lo so ancor io.

MARCHESE:

Pensano che le donne della vostra sorta si vincano con i regali.

MIRANDOLINA:

I regali non fanno male allo stomaco.

MARCHESE:

Io crederei di farvi un\'ingiuria, cercando di obbligarvi con i
donativi.

MIRANDOLINA:

Oh, certamente il signor Marchese non mi ha ingiuriato mai.

MARCHESE:

E tali ingiurie non ve le farò.

MIRANDOLINA:

Lo credo sicurissimamente.

MARCHESE:

Ma dove posso, comandatemi.

MIRANDOLINA:

Bisognerebbe ch\'io sapessi, in che cosa può Vostra Eccellenza.

MARCHESE:

In tutto. Provatemi.

MIRANDOLINA:

Ma verbigrazia, in che?

MARCHESE:

Per bacco! Avete un merito che sorprende.

MIRANDOLINA:

Troppe grazie, Eccellenza.

MARCHESE:

Ah! direi quasi uno sproposito. Maledirei quasi la mia Eccellenza.

MIRANDOLINA:

Perché, signore?

MARCHESE:

Qualche volta mi auguro di essere nello stato del Conte.

MIRANDOLINA:

Per ragione forse de\' suoi denari?

MARCHESE:

Eh! Che denari! Non li stimo un fico. Se fossi un Conte ridicolo come
lui\...

MIRANDOLINA:

Che cosa farebbe?

MARCHESE:

Cospetto del diavolo\... vi sposerei.

(Parte.)

SCENA NONA {#Section0009.xhtml#sigil_toc_id_47}
----------

MIRANDOLINA:

(sola)

Uh, che mai ha detto! L\'eccellentissimo signor Marchese Arsura mi
sposerebbe? Eppure, se mi volesse sposare, vi sarebbe una piccola
difficoltà. Io non lo vorrei. Mi piace l\'arrosto, e del fumo non so che
farne. Se avessi sposati tutti quelli che hanno detto volermi, oh, avrei
pure tanti mariti! Quanti arrivano a questa locanda, tutti di me
s\'innamorano, tutti mi fanno i cascamorti; e tanti e tanti mi
esibiscono di sposarmi a dirittura. E questo signor Cavaliere, rustico
come un orso, mi tratta sì bruscamente? Questi è il primo forestiere
capitato alla mia locanda, il quale non abbia avuto piacere di trattare
con me. Non dico che tutti in un salto s\'abbiano a innamorare: ma
disprezzarmi così? è una cosa che mi muove la bile terribilmente. È
nemico delle donne? Non le può vedere? Povero pazzo! Non avrà ancora
trovato quella che sappia fare. Ma la troverà. La troverà. E chi sa che
non l\'abbia trovata? Con questi per l\'appunto mi ci metto di picca.
Quei che mi corrono dietro, presto presto mi annoiano. La nobiltà non fa
per me. La ricchezza la stimo e non la stimo. Tutto il mio piacere
consiste in vedermi servita, vagheggiata, adorata. Questa è la mia
debolezza, e questa è la debolezza di quasi tutte le donne. A maritarmi
non ci penso nemmeno; non ho bisogno di nessuno; vivo onestamente, e
godo la mia libertà. Tratto con tutti, ma non m\'innamoro mai di
nessuno. Voglio burlarmi di tante caricature di amanti spasimati; e
voglio usar tutta l\'arte per vincere, abbattere e conquassare quei
cuori barbari e duri che son nemici di noi, che siamo la miglior cosa
che abbia prodotto al mondo la bella madre natura.

SCENA DECIMA {#Section0009.xhtml#sigil_toc_id_48}
------------

Fabrizio e detta.

FABRIZIO:

Ehi, padrona.

MIRANDOLINA:

Che cosa c\'è?

FABRIZIO:

Quel forestiere che è alloggiato nella camera di mezzo, grida della
biancheria; dice che è ordinaria, e che non la vuole.

MIRANDOLINA:

Lo so, lo so. Lo ha detto anche a me, e lo voglio servire.

FABRIZIO:

Benissimo. Venitemi dunque a metter fuori la roba, che gliela possa
portare.

MIRANDOLINA:

Andate, andate, gliela porterò io.

FABRIZIO:

Voi gliela volete portare?

MIRANDOLINA:

Sì, io.

FABRIZIO:

Bisogna che vi prema molto questo forestiere.

MIRANDOLINA:

Tutti mi premono. Badate a voi.

FABRIZIO:

(Già me n\'avvedo. Non faremo niente. Ella mi lusinga; ma non faremo
niente).

(Da sé.)

MIRANDOLINA:

(Povero sciocco! Ha delle pretensioni. Voglio tenerlo in isperanza,
perché mi serva con fedeltà).

(Da sé.)

FABRIZIO:

Si è sempre costumato, che i forestieri li serva io.

MIRANDOLINA:

Voi con i forestieri siete un poco troppo ruvido.

FABRIZIO:

E voi siete un poco troppo gentile.

MIRANDOLINA:

So quel quel che fo, non ho bisogno di correttori.

FABRIZIO:

Bene, bene. Provvedetevi di cameriere.

MIRANDOLINA:

Perché, signor Fabrizio? è disgustato di me?

FABRIZIO:

Vi ricordate voi che cosa ha detto a noi due vostro padre, prima
ch\'egli morisse?

MIRANDOLINA:

Sì; quando mi vorrò maritare, mi ricorderò di quel che ha detto mio
padre.

FABRIZIO:

Ma io son delicato di pelle, certe cose non le posso soffrire.

MIRANDOLINA:

Ma che credi tu ch\'io mi sia? Una frasca? Una civetta? Una pazza? Mi
maraviglio di te. Che voglio fare io dei forestieri che vanno e vengono?
Se il tratto bene, lo fo per mio interesse, per tener in credito la mia
locanda. De\' regali non ne ho bisogno. Per far all\'amore? Uno mi
basta: e questo non mi manca; e so chi merita, e so quello che mi
conviene. E quando vorrò maritarmi\... mi ricorderò di mio padre. E chi
mi averà servito bene, non potrà lagnarsi di me. Son grata. Conosco il
merito\... Ma io non son conosciuta. Basta, Fabrizio, intendetemi, se
potete.

(Parte.)

FABRIZIO:

Chi può intenderla, è bravo davvero. Ora pare che la mi voglia, ora che
la non mi voglia. Dice che non è una frasca, ma vuol far a suo modo. Non
so che dire. Staremo a vedere. Ella mi piace, le voglio bene,
accomoderei con essa i miei interessi per tutto il tempo di vita mia.
Ah! bisognerà chiuder un occhio, e lasciar correre qualche cosa.
Finalmente i forestieri vanno e vengono. Io resto sempre. Il meglio sarà
sempre per me.

(Parte.)

SCENA UNDICESIMA {#Section0009.xhtml#sigil_toc_id_49}
----------------

Camera del Cavaliere.\
Il Cavaliere ed un Servitore.

SERVITORE:

Illustrissimo, hanno portato questa lettera.

CAVALIERE:

Portami la cioccolata.

(Il Servitore parte.)\
(Il Cavaliere apre la lettera.)

Siena, primo Gennaio 1753.

(Chi scrive?)

Orazio Taccagni. Amico carissimo. La tenera amicizia che a voi mi lega,
mi rende sollecito ad avvisarvi essere necessario il vostro ritorno in
patria. È morto il Conte Manna\...

(Povero Cavaliere! Me ne dispiace).

Ha lasciato la sua unica figlia nubile erede di centocinquanta mila
scudi. Tutti gli amici vostri vorrebbero che toccasse a voi una tal
fortuna, e vanno maneggiando\... Non s\'affatichino per me, che non
voglio saper nulla. Lo sanno pure ch\'io non voglio donne per i piedi. E
questo mio caro amico, che lo sa più d\'ogni altro, mi secca peggio di
tutti.

(Straccia la lettera.)

Che importa a me di centocinquanta mila scudi? Finché son solo, mi basta
meno. Se fossi accompagnato, non mi basterebbe assai più. Moglie a me!
Piuttosto una febbre quartana.

SCENA DODICESIMA {#Section0009.xhtml#sigil_toc_id_50}
----------------

Il Marchese e detto.

MARCHESE:

Amico, vi contentate ch\'io venga a stare un poco con voi?

CAVALIERE:

Mi fate onore.

MARCHESE:

Almeno fra me e voi possiamo trattarci con confidenza; ma quel somaro
del Conte non è degno di stare in conversazione con noi.

CAVALIERE:

Caro Marchese, compatitemi; rispettate gli altri, se volete essere
rispettato voi pure.

MARCHESE:

Sapete il mio naturale. Io fo le cortesie a tutti, ma colui non lo posso
soffrire.

CAVALIERE:

Non lo potete soffrire, perché vi è rivale in amore! Vergogna! Un
cavaliere della vostra sorta innamorarsi d\'una locandiera! Un uomo
savio, come siete voi, correr dietro a una donna!

MARCHESE:

Cavaliere mio, costei mi ha stregato.

CAVALIERE:

Oh! pazzie! debolezze! Che stregamenti! Che vuol dire che le donne non
mi stregheranno? Le loro fattucchierie consistono nei loro vezzi, nelle
loro lusinghe, e chi ne sta lontano, come fo io, non ci è pericolo che
si lasci ammaliare.

MARCHESE:

Basta! ci penso e non ci penso: quel che mi dà fastidio e che
m\'inquieta, è il mio fattor di campagna.

CAVALIERE:

Vi ha fatto qualche porcheria?

MARCHESE:

Mi ha mancato di parola.

SCENA TREDICESIMA {#Section0009.xhtml#sigil_toc_id_51}
-----------------

Il Servitore con una cioccolata e detti.

CAVALIERE:

Oh mi dispiace\... Fanne subito un\'altra.

(Al Servitore.)

SERVITORE:

In casa per oggi non ce n\'è altra, illustrissimo.

CAVALIERE:

Bisogna che ne provveda. Se vi degnate di questa\...

(Al Marchese.)

MARCHESE:

(prende la cioccolata, e si mette a berla senza complimenti, seguitando
poi a discorrere e bere, come segue)

Questo mio fattore, come io vi diceva\...

(Beve.)

CAVALIERE:

(Ed io resterò senza).

(Da sé.)

MARCHESE:

Mi aveva promesso mandarmi con l\'ordinario\...

(Beve.)

venti zecchini\...

(Beve.)

CAVALIERE:

(Ora viene con una seconda stoccata).

(Da sé.)

MARCHESE:

E non me li ha mandati\...

(Beve.)

CAVALIERE:

Li manderà un\'altra volta.

MARCHESE:

Il punto sta\... il punto sta\...

(Finisce di bere.)

Tenete.

(Dà la chicchera al Servitore.)

Il punto sta che sono in un grande impegno, e non so come fare.

CAVALIERE:

Otto giorni più, otto giorni meno\...

MARCHESE:

Ma voi che siete Cavaliere, sapete quel che vuol dire il mantener la
parola. Sono in impegno; e\... corpo di bacco! Darei della pugna in
cielo.

CAVALIERE:

Mi dispiace di vedervi scontento. (Se sapessi come uscirne con
riputazione!)

(Da sé.)

MARCHESE:

Voi avreste difficoltà per otto giorni di farmi il piacere?

CAVALIERE:

Caro Marchese, se potessi, vi servirei di cuore; se ne avessi, ve li
avrei esibiti a dirittura. Ne aspetto, e non ne ho.

MARCHESE:

Non mi darete ad intendere d\'esser senza denari.

CAVALIERE:

Osservate. Ecco tutta la mia ricchezza. Non arrivano a due zecchini.

(Mostra uno zecchino e varie monete.)

MARCHESE:

Quello è uno zecchino d\'oro.

CAVALIERE:

Sì; l\'ultimo, non ne ho più.

MARCHESE:

Prestatemi quello, che vedrò intanto\...

CAVALIERE:

Ma io poi\...

MARCHESE:

Di che avete paura? Ve lo renderò.

CAVALIERE:

Non so che dire; servitevi.

(Gli dà lo zecchino.)

MARCHESE:

Ho un affare di premura\... amico: obbligato per ora: ci rivedremo a
pranzo.

(Prende lo zecchino, e parte.)

SCENA QUATTORDICESIMA {#Section0009.xhtml#sigil_toc_id_52}
---------------------

CAVALIERE:

(solo)

Bravo! Il signor Marchese mi voleva frecciare venti zecchini, e poi si è
contentato di uno. Finalmente uno zecchino non mi preme di perderlo, e
se non me lo rende, non mi verrà più a seccare. Mi dispiace più, che mi
ha bevuto la mia cioccolata. Che indiscretezza! E poi: son chi sono. Son
Cavaliere. Oh garbatissimo Cavaliere!

SCENA QUINDICESIMA {#Section0009.xhtml#sigil_toc_id_53}
------------------

Mirandolina colla biancheria, e detto.

MIRANDOLINA:

Permette, illustrissimo?

(Entrando con qualche soggezione.)

CAVALIERE:

Che cosa volete?

(Con asprezza.)

MIRANDOLINA:

Ecco qui della biancheria migliore.

(S\'avanza un poco.)

CAVALIERE:

Bene. Mettetela lì.

(Accenna il tavolino.)

MIRANDOLINA:

La supplico almeno degnarsi vedere se è di suo genio.

CAVALIERE:

Che roba è?

MIRANDOLINA:

Le lenzuola son di rensa.

(S\'avanza ancor più.)

CAVALIERE:

Rensa?

MIRANDOLINA:

Sì signore, di dieci paoli al braccio. Osservi.

CAVALIERE:

Non pretendevo tanto. Bastavami qualche cosa meglio di quel che mi avete
dato.

MIRANDOLINA:

Questa biancheria l\'ho fatta per personaggi di merito: per quelli che
la sanno conoscere; e in verità, illustrissimo, la do per esser lei, ad
un altro non la darei.

CAVALIERE:

Per esser lei! Solito complimento.

MIRANDOLINA:

Osservi il servizio di tavola.

CAVALIERE:

Oh! Queste tele di Fiandra, quando si lavano, perdono assai. Non vi è
bisogno che le insudiciate per me.

MIRANDOLINA:

Per un Cavaliere della sua qualità, non guardo a queste piccole cose. Di
queste salviette ne ho parecchie, e le serberò per V.S. illustrissima.

CAVALIERE:

(Non si può però negare, che costei non sia una donna obbligante).

(Da sé.)

MIRANDOLINA:

(Veramente ha una faccia burbera da non piacergli le donne).

(Da sé.)

CAVALIERE:

Date la mia biancheria al mio cameriere, o ponetela lì, in qualche
luogo. Non vi è bisogno che v\'incomodiate per questo.

MIRANDOLINA:

Oh, io non m\'incomodo mai, quando servo Cavaliere di sì alto merito.

CAVALIERE:

Bene, bene, non occorr\'altro. (Costei vorrebbe adularmi. Donne! Tutte
così).

(Da sé.)

MIRANDOLINA:

La metterò nell\'arcova.

CAVALIERE:

Sì, dove volete.

(Con serietà.)

MIRANDOLINA:

(Oh! vi è del duro. Ho paura di non far niente).

(Da sé, va a riporre la biancheria.)

CAVALIERE:

(I gonzi sentono queste belle parole, credono a chi le dice, e cascano).

(Da sè.)

MIRANDOLINA:

A pranzo, che cosa comanda?

(Ritornando senza la biancheria.)

CAVALIERE:

Mangerò quello che vi sarà.

MIRANDOLINA:

Vorrei pur sapere il suo genio. Se le piace una cosa più dell\'altra, lo
dica con libertà.

CAVALIERE:

Se vorrò qualche cosa, lo dirò al cameriere.

MIRANDOLINA:

Ma in queste cose gli uomini non hanno l\'attenzione e la pazienza che
abbiamo noi donne. Se le piacesse qualche intingoletto, qualche
salsetta, favorisca di dirlo a me.

CAVALIERE:

Vi ringrazio: ma né anche per questo verso vi riuscirà di far con me
quello che avete fatto col Conte e col Marchese.

MIRANDOLINA:

Che dice della debolezza di quei due cavalieri? Vengono alla locanda per
alloggiare, e pretendono poi di voler fare all\'amore colla locandiera.
Abbiamo altro in testa noi, che dar retta alle loro ciarle. Cerchiamo di
fare il nostro interesse; se diamo loro delle buone parole, lo facciamo
per tenerli a bottega; e poi, io principalmente, quando vedo che si
lusingano, rido come una pazza.

CAVALIERE:

Brava! Mi piace la vostra sincerità.

MIRANDOLINA:

Oh! non ho altro di buono, che la sincerità.

CAVALIERE:

Ma però, con chi vi fa la corte, sapete fingere.

MIRANDOLINA:

Io fingere? Guardimi il cielo. Domandi un poco a quei due signori che
fanno gli spasimati per me, se ho mai dato loro un segno d\'affetto. Se
ho mai scherzato con loro in maniera che si potessero lusingare con
fondamento. Non li strapazzo, perché il mio interesse non lo vuole, ma
poco meno. Questi uomini effeminati non li posso vedere. Sì come
abborrisco anche le donne che corrono dietro agli uomini. Vede? Io non
sono una ragazza. Ho qualche annetto; non sono bella, ma ho avute delle
buone occasioni; eppure non ho mai voluto maritarmi, perché stimo
infinitamente la mia libertà.

CAVALIERE:

Oh sì, la libertà è un gran tesoro.

MIRANDOLINA:

E tanti la perdono scioccamente.

CAVALIERE:

So io ben quel che faccio. Alla larga.

MIRANDOLINA:

Ha moglie V.S. illustrissima?

CAVALIERE:

Il cielo me ne liberi. Non voglio donne.

MIRANDOLINA:

Bravissimo. Si conservi sempre così. Le donne, signore\... Basta, a me
non tocca a dirne male.

CAVALIERE:

Voi siete per altro la prima donna, ch\'io senta parlar così.

MIRANDOLINA:

Le dirò: noi altre locandiere vediamo e sentiamo delle cose assai; e in
verità compatisco quegli uomini, che hanno paura del nostro sesso.

CAVALIERE:

(È curiosa costei).

(Da sé.)

MIRANDOLINA:

Con permissione di V.S. illustrissima.

(Finge voler partire.)

CAVALIERE:

Avete premura di partire?

MIRANDOLINA:

Non vorrei esserle importuna.

CAVALIERE:

No, mi fate piacere; mi divertite.

MIRANDOLINA:

Vede, signore? Così fo con gli altri. Mi trattengo qualche momento; sono
piuttosto allegra, dico delle barzellette per divertirli, ed essi subito
credono\... Se la m\'intende, e mi fanno i cascamorti.

CAVALIERE:

Questo accade, perché avete buona maniera.

MIRANDOLINA:

Troppa bontà, illustrissimo.

(Con una riverenza.)

CAVALIERE:

Ed essi s\'innamorano.

MIRANDOLINA:

Guardi che debolezza! Innamorarsi subito di una donna!

CAVALIERE:

Questa io non l\'ho mai potuta capire.

MIRANDOLINA:

Bella fortezza! Bella virilità!

CAVALIERE:

Debolezze! Miserie umane!

MIRANDOLINA:

Questo è il vero pensare degli uomini. Signor Cavaliere, mi porga la
mano.

CAVALIERE:

Perché volete ch\'io vi porga la mano?

MIRANDOLINA:

Favorisca; si degni; osservi, sono pulita.

CAVALIERE:

Ecco la mano.

MIRANDOLINA:

Questa è la prima volta, che ho l\'onore d\'aver per la mano un uomo,
che pensa veramente da uomo.

CAVALIERE:

Via, basta così.

(Ritira la mano.)

MIRANDOLINA:

Ecco. Se io avessi preso per la mano uno di que\' due signori sguaiati,
avrebbe tosto creduto ch\'io spasimassi per lui. Sarebbe andato in
deliquio. Non darei loro una semplice libertà, per tutto l\'oro del
mondo. Non sanno vivere. Oh benedetto in conversare alla libera! senza
attacchi, senza malizia, senza tante ridicole scioccherie.
Illustrissimo, perdoni la mia impertinenza. Dove posso servirla, mi
comandi con autorità, e avrò per lei quell\'attenzione, che non ho mai
avuto per alcuna persona di questo mondo.

CAVALIERE:

Per quale motivo avete tanta parzialità per me?

MIRANDOLINA:

Perché, oltre il suo merito, oltre la sua condizione, sono almeno sicura
che con lei posso trattare con libertà, senza sospetto che voglia fare
cattivo uso delle mie attenzioni, e che mi tenga in qualità di serva,
senza tormentarmi con pretensioni ridicole, con caricature affettate.

CAVALIERE:

(Che diavolo ha costei di stravagante, ch\'io non capisco!).

(Da sé.)

MIRANDOLINA:

(Il satiro si anderà a poco a poco addomesticando).

(Da sé.)

CAVALIERE:

Orsù, se avete da badare alle cose vostre, non restate per me.

MIRANDOLINA:

Sì signore, vado ad attendere alle faccende di casa. Queste sono i miei
amori, i miei passatempi. Se comanderà qualche cosa, manderò il
cameriere.

CAVALIERE:

Bene\... Se qualche volta verrete anche voi, vi vedrò volentieri.

MIRANDOLINA:

Io veramente non vado mai nelle camere dei forestieri, ma da lei ci
verrò qualche volta.

CAVALIERE:

Da me\... Perché?

MIRANDOLINA:

Perché, illustrissimo signore, ella mi piace assaissimo.

CAVALIERE:

Vi piaccio io?

MIRANDOLINA:

Mi piace, perché non è effeminato, perché non è di quelli che
s\'innamorano. (Mi caschi il naso, se avanti domani non l\'innamoro).

(Da sé.)

SCENA SEDICESIMA {#Section0009.xhtml#sigil_toc_id_54}
----------------

CAVALIERE:

(solo)

Eh! So io quel che fo. Colle donne? Alla larga. Costei sarebbe una di
quelle che potrebbero farmi cascare più delle altre. Quella verità,
quella scioltezza di dire, è cosa poco comune. Ha un non so che di
estraordinario; ma non per questo mi lascerei innamorare. Per un poco di
divertimento, mi fermerei più tosto con questa che con un\'altra. Ma per
fare all\'amore? Per perdere la libertà? Non vi è pericolo. Pazzi, pazzi
quelli che s\'innamorano delle donne.

(Parte.)

SCENA DICIASSETTESIMA {#Section0009.xhtml#sigil_toc_id_55}
---------------------

Altra camera di locanda.\
Ortensia, Dejanira, Fabrizio.

FABRIZIO:

Che restino servite qui, illustrissime. Osservino quest\'altra camera.
Quella per dormire, e questa per mangiare, per ricevere, per servirsene
come comandano.

ORTENSIA:

Va bene, va bene. Siete voi padrone, o cameriere?

FABRIZIO:

Cameriere, ai comandi di V.S. illustrissima.

DEJANIRA:

(Ci dà delle illustrissime).

(Piano a Ortensia, ridendo.)

ORTENSIA:

(Bisogna secondare il lazzo).

Cameriere?

FABRIZIO:

Illustrissima.

ORTENSIA:

Dite al padrone che venga qui, voglio parlar con lui per il trattamento.

FABRIZIO:

Verrà la padrona; la servo subito. (Chi diamine saranno queste due
signore così sole? All\'aria, all\'abito, paiono dame).

(Da sé, parte.)

SCENA DICIOTTESIMA {#Section0009.xhtml#sigil_toc_id_56}
------------------

Dejanira e Ortensia.

DEJANIRA:

Ci dà dell\'illustrissime. Ci ha creduto due dame.

ORTENSIA:

Bene. Così ci tratterà meglio.

DEJANIRA:

Ma ci farà pagare di più.

ORTENSIA:

Eh, circa i conti, avrà da fare con me. Sono degli anni assai, che
cammino il mondo.

DEJANIRA:

Non vorrei che con questi titoli entrassimo in qualche impegno.

ORTENSIA:

Cara amica, siete di poco spirito. Due commedianti avvezze a far sulla
scena da contesse, da marchese e da principesse, avranno difficoltà a
sostenere un carattere sopra di una locanda?

DEJANIRA:

Verranno i nostri compagni, e subito ci sbianchiranno.

ORTENSIA:

Per oggi non possono arrivare a Firenze. Da Pisa a qui in navicello vi
vogliono almeno tre giorni.

DEJANIRA:

Guardate che bestialità! Venire in navicello!

ORTENSIA:

Per mancanza di lugagni. È assai che siamo venute noi in calesse.

DEJANIRA:

È stata buona quella recita di più che abbiamo fatto.

ORTENSIA:

Sì, ma se non istavo io alla porta, non si faceva niente.

SCENA DICIANNOVESIMA {#Section0009.xhtml#sigil_toc_id_57}
--------------------

Fabrizio e dette.

FABRIZIO:

La padrona or ora sarà a servirle.

ORTENSIA:

Bene.

FABRIZIO:

Ed io le supplico a comandarmi. Ho servito altre dame: mi darò l\'onor
di servir con tutta l\'attenzione anche le signorie loro illustrissime.

ORTENSIA:

Occorrendo, mi varrò di voi.

DEJANIRA:

(Ortensia queste parti le fa benissimo).

(Da sé.)

FABRIZIO:

Intanto le supplico, illustrissime signore, favorirmi il loro riverito
nome per la consegna.

(Tira fuori un calamaio ed un libriccino.)

DEJANIRA:

(Ora viene il buono).

ORTENSIA:

Perché ho da dar il mio nome?

FABRIZIO:

Noialtri locandieri siamo obbligati a dar il nome, il casato, la patria
e la condizione di tutti i passeggeri che alloggiano alla nostra
locanda. E se non lo facessimo, meschini noi.

DEJANIRA:

(Amica, i titoli sono finiti).

(Piano ad Ortensia.)

ORTENSIA:

Molti daranno anche il nome finto.

FABRIZIO:

In quanto a questo poi, noialtri scriviamo il nome che ci dettano, e non
cerchiamo di più.

ORTENSIA:

Scrivete. La Baronessa Ortensia del Poggio, palermitana.

FABRIZIO:

(Siciliana? Sangue caldo).

(Scrivendo.)

Ella, illustrissima?

(A Dejanira.)

DEJANIRA:

Ed io\... (Non so che mi dire).

ORTENSIA:

Via, Contessa Dejanira, dategli il vostro nome.

FABRIZIO:

Vi supplico.

(A Dejanira.)

DEJANIRA:

Non l\'avete sentito?

(A Fabrizio.)

FABRIZIO:

L\'illustrissima signora Contessa Dejanira\...

(Scrivendo.)

Il cognome?

DEJANIRA:

Anche il cognome?

(A Fabrizio.)

ORTENSIA:

Sì, dal Sole, romana.

(A Fabrizio.)

FABRIZIO:

Non occorr\'altro. Perdonino l\'incomodo. Ora verrà la padrona. (L\'ho
io detto, che erano due dame? Spero che farò de\' buoni negozi. Mancie
non ne mancheranno).

(Parte.)

DEJANIRA:

Serva umilissima della signora Baronessa.

ORTENSIA:

Contessa, a voi m\'inchino.

(Si burlano vicendevolmente.)

DEJANIRA:

Qual fortuna mi offre la felicissima congiuntura di rassegnarvi il mio
profondo rispetto?

ORTENSIA:

Dalla fontana del vostro cuore scaturir non possono che torrenti di
grazie.

SCENA VENTESIMA {#Section0009.xhtml#sigil_toc_id_58}
---------------

Mirandolina e dette.

DEJANIRA:

Madama, voi mi adulate.

(Ad Ortensia, con caricatura.)

ORTENSIA:

Contessa, al vostro merito ci converrebbe assai più.

(Fa lo stesso.)

MIRANDOLINA:

(Oh che dame cerimoniose).

(Da sé, in disparte.)

DEJANIRA:

(Oh quanto mi vien da ridere!).

(Da sé.)

ORTENSIA:

Zitto: è qui la padrona.

(Piano a Dejanira.)

MIRANDOLINA:

M\'inchino a queste dame.

ORTENSIA:

Buon giorno, quella giovane.

DEJANIRA:

Signora padrona, vi riverisco.

(A Mirandolina.)

ORTENSIA:

Ehi!

(Fa cenno a Dejanira, che si sostenga.)

MIRANDOLINA:

Permetta ch\'io le baci la mano.

(Ad Ortensia.)

ORTENSIA:

Siete obbligante.

(Le dà la mano.)

DEJANIRA:

(ride da sé.)

MIRANDOLINA:

Anche ella, illustrissima.

(Chiede la mano a Dejanira.)

DEJANIRA:

Eh, non importa\...

ORTENSIA:

Via, gradite le finezze di questa giovane. Datele la mano.

MIRANDOLINA:

La supplico.

DEJANIRA:

Tenete.

(Le dà la mano, si volta, e ride.)

MIRANDOLINA:

Ride, illustrissima? Di che?

ORTENSIA:

Che cara Contessa! Ride ancora di me. Ho detto uno sproposito, che l\'ha
fatta ridere.

MIRANDOLINA:

(Io giuocherei che non sono dame. Se fossero dame, non sarebbero sole).

(Da sé.)

ORTENSIA:

Circa il trattamento, converrà poi discorrere.

(A Mirandolina.)

MIRANDOLINA:

Ma! Sono sole? Non hanno cavalieri, non hanno servitori, non hanno
nessuno?

ORTENSIA:

Il Barone mio marito\...

DEJANIRA:

(ride forte).

MIRANDOLINA:

Perché ride, signora?

(A Dejanira.)

ORTENSIA:

Via, perché ridete?

DEJANIRA:

Rido del Barone di vostro marito.

ORTENSIA:

Sì, è un Cavaliere giocoso: dice sempre delle barzellette; verrà quanto
prima col Conte Orazio, marito della Contessina.

DEJANIRA

(fa forza per trattenersi dal ridere).

MIRANDOLINA:

La fa ridere anche il signor Conte?

(A Dejanira.)

ORTENSIA:

Ma via, Contessina, tenetevi un poco nel vostro decoro.

MIRANDOLINA:

Signore mie, favoriscano in grazia. Siamo sole, nessuno ci sente. Questa
contea, questa baronia, sarebbe mai\...

ORTENSIA:

Che cosa vorreste voi dire? Mettereste in dubbio la nostra nobiltà?

MIRANDOLINA:

Perdoni, illustrissima, non si riscaldi, perché farà ridere la signora
Contessa.

DEJANIRA:

Eh via, che serve?

ORTENSIA:

Contessa, Contessa!

(Minacciandola.)

MIRANDOLINA:

Io so che cosa voleva dire, illustrissima.

(A Dejanira.)

DEJANIRA:

Se l\'indovinate, vi stimo assai.

MIRANDOLINA:

Volevate dire: Che serve che fingiamo d\'esser due dame, se siamo due
pedine? Ah! non è vero?

DEJANIRA:

E che sì che ci conoscete?

(A Mirandolina.)

ORTENSIA:

Che brava commediante! Non è buona da sostenere un carattere.

DEJANIRA:

Fuori di scena io non so fingere.

MIRANDOLINA:

Brava, signora Baronessa; mi piace il di lei spirito. Lodo la sua
franchezza.

ORTENSIA:

Qualche volta mi prendo un poco di spasso.

MIRANDOLINA:

Ed io amo infinitamente le persone di spirito. Servitevi pure nella mia
locanda, che siete padrone; ma vi prego bene, se mi capitassero persone
di rango, cedermi quest\'appartamento, ch\'io vi darò dei camerini assai
comodi.

DEJANIRA:

Sì, volentieri.

ORTENSIA:

Ma io, quando spendo il mio denaro, intendo volere esser servita come
una dama, e in questo appartamento ci sono, e non me ne anderò.

MIRANDOLINA:

Via, signora Baronessa, sia buona\... Oh! Ecco un cavaliere che è
alloggiato in questa locanda. Quando vede donne, sempre si caccia
avanti.

ORTENSIA:

È ricco?

MIRANDOLINA:

Io non so i fatti suoi.

SCENA VENTUNESIMA {#Section0009.xhtml#sigil_toc_id_59}
-----------------

Il Marchese e dette.

MARCHESE:

È permesso? Si può entrare?

ORTENSIA:

Per me è padrone.

MARCHESE:

Servo di lor signore.

DEJANIRA:

Serva umilissima.

ORTENSIA:

La riverisco divotamente.

MARCHESE:

Sono forestiere?

(A Mirandolina.)

MIRANDOLINA:

Eccellenza sì. Sono venute ad onorare la mia locanda.

ORTENSIA:

(È un\'Eccellenza! Capperi!).

(Da sé.)

DEJANIRA:

(Già Ortensia lo vorrà per sé).

(Da sé.)

MARCHESE:

E chi sono queste signore?

(A Mirandolina.)

MIRANDOLINA:

Questa è la Baronessa Ortensia del Poggio, e questa la Contessa Dejanira
dal Sole.

MARCHESE:

Oh compitissime dame!

ORTENSIA:

E ella chi è, signore?

MARCHESE:

Io sono il Marchese di Forlipopoli.

DEJANIRA:

(La locandiera vuol seguitare a far la commedia).

(Da sé.)

ORTENSIA:

Godo aver l\'onore di conoscere un cavaliere così compito.

MARCHESE:

Se vi potessi servire, comandatemi. Ho piacere che siate venute ad
alloggiare in questa locanda. Troverete una padrona di garbo.

MIRANDOLINA:

Questo cavaliere è pieno di bontà. Mi onora della sua protezione.

MARCHESE:

Sì, certamente. Io la proteggo, e proteggo tutti quelli che vengono
nella sua locanda; e se vi occorre nulla, comandate.

ORTENSIA:

Occorrendo, mi prevarrò delle sue finezze.

MARCHESE:

Anche voi, signora Contessa, fate capitale di me.

DEJANIRA:

Potrò ben chiamarmi felice, se avrò l\'alto onore di essere annoverata
nel ruolo delle sue umilissime serve.

MIRANDOLINA:

(Ha detto un concetto da commedia).

(Ad Ortensia.)

ORTENSIA:

(Il titolo di Contessa l\'ha posta in soggezione).

(A Mirandolina.)\
(Il Marchese tira fuori di tasca un bel fazzoletto di seta, lo spiega, e
finge volersi asciugar la fronte.)

MIRANDOLINA:

Un gran fazzoletto, signor Marchese!

MARCHESE:

Ah! Che ne dite? È bello? Sono di buon gusto io?

(A Mirandolina.)

MIRANDOLINA:

Certamente è di ottimo gusto.

MARCHESE:

Ne avete più veduti di così belli?

(Ad Ortensia.)

ORTENSIA:

È superbo. Non ho veduto il compagno. (Se me lo donasse, lo prenderei).

(Da sé.)

MARCHESE:

Questo viene da Londra.

(A Dejanira.)

DEJANIRA:

È bello, mi piace assai.

MARCHESE:

Son di buon gusto io?

DEJANIRA:

(E non dice a\' vostri comandi).

(Da sé.)

MARCHESE:

M\'impegno che il Conte non sa spendere. Getta via il denaro, e non
compra mai una galanteria di buon gusto.

MIRANDOLINA:

Il signor Marchese conosce, distingue, sa, vede, intende.

MARCHESE:

(piega il fazzoletto con attenzione)

Bisogna piegarlo bene, acciò non si guasti. Questa sorta di roba bisogna
custodirla con attenzione. Tenete.

(Lo presenta a Mirandolina.)

MIRANDOLINA:

Vuole ch\'io lo faccia mettere nella sua camera?

MARCHESE:

No. Mettetelo nella vostra.

MIRANDOLINA:

Perché\... nella mia?

MARCHESE:

Perché\... ve lo dono.

MIRANDOLINA:

Oh, Eccellenza, perdoni\...

MARCHESE:

Tant\'è. Ve lo dono.

MIRANDOLINA:

Ma io non voglio.

MARCHESE:

Non mi fate andar in collera.

MIRANDOLINA:

Oh, in quanto a questo poi, il signor Marchese lo sa, io non voglio
disgustar nessuno. Acciò non vada in collera, lo prenderò.

DEJANIRA:

(Oh che bel lazzo!).

(Ad Ortensia.)

ORTENSIA:

(E poi dicono delle commedianti).

(A Dejanira.)

MARCHESE:

Ah! Che dite? Un fazzoletto di quella sorta, l\'ho donato alla mia
padrona di casa.

(Ad Ortensia.)

ORTENSIA:

È un cavaliere generoso.

MARCHESE:

Sempre così.

MIRANDOLINA:

(Questo è il primo regalo che mi ha fatto, e non so come abbia avuto
quel fazzoletto).

(Da sé.)

DEJANIRA:

Signor Marchese, se ne trovano di quei fazzoletti in Firenze? Avrei
volontà d\'averne uno compagno.

MARCHESE:

Compagno di questo sarà difficile; ma vedremo.

MIRANDOLINA:

(Brava la signora Contessina).

(Da sé.)

ORTENSIA:

Signor Marchese, voi che siete pratico della città, fatemi il piacere di
mandarmi un bravo calzolaro, perché ho bisogno di scarpe.

MARCHESE:

Sì, vi manderò il mio.

MIRANDOLINA:

(Tutte alla vita; ma non ce n\'è uno per la rabbia).

(Da sé.)

ORTENSIA:

Caro signor Marchese, favorirà tenerci un poco di compagnia.

DEJANIRA:

Favorirà a pranzo con noi.

MARCHESE:

Sì, volentieri. (Ehi Mirandolina, non abbiate gelosia, son vostro, già
lo sapete).

MIRANDOLINA:

(S\'accomodi pure: ho piacere che si diverta).

(Al Marchese.)

ORTENSIA:

Voi sarete la nostra conversazione.

DEJANIRA:

Non conosciamo nessuno. Non abbiamo altri che voi.

MARCHESE:

Oh care le mie damine! Vi servirò di cuore.

SCENA VENTIDUESIMA {#Section0009.xhtml#sigil_toc_id_60}
------------------

Il Conte e detti.

CONTE:

Mirandolina, io cercava voi.

MIRANDOLINA:

Son qui con queste dame.

CONTE:

Dame? M\'inchino umilmente.

ORTENSIA:

Serva divota. (Questo è un guasco più badia! di quell\'altro).

(Piano a Dejanira.)

DEJANIRA:

(Ma io non sono buona per miccheggiare).

(Piano ad Ortensia.)

MARCHESE:

(Ehi! Mostrate al Conte il fazzoletto).

(Piano a Mirandolina.)

MIRANDOLINA:

Osservi signor Conte, il bel regalo che mi ha fatto il signor Marchese.

(Mostra il fazzoletto al Conte.)

CONTE:

Oh, me ne rallegro! Bravo, signor Marchese.

MARCHESE:

Eh niente, niente. Bagattelle. Riponetelo via; non voglio che lo
diciate. Quel che fo, non s\'ha da sapere.

MIRANDOLINA:

(Non s\'ha da sapere, e me lo fa mostrare. La superbia contrasta con la
povertà).

(Da sé.)

CONTE:

Con licenza di queste dame, vorrei dirvi una parola.

(A Mirandolina.)

ORTENSIA:

S\'accomodi con libertà.

MARCHESE:

Quel fazzoletto in tasca lo manderete a male.

(A Mirandolina.)

MIRANDOLINA:

Eh, lo riporrò nella bambagia, perché non si ammacchi!

CONTE:

Osservate questo piccolo gioiello di diamanti.

(A Mirandolina.)

MIRANDOLINA:

Bello assai.

CONTE:

È compagno degli orecchini che vi ho donato.

(Ortensia e Dejanira osservano, e parlano piano fra loro.)

MIRANDOLINA:

Certo è compagno, ma è ancora più bello.

MARCHESE:

(Sia maledetto il Conte, i suoi diamanti, i suoi denari, e il suo
diavolo che se lo porti).

(Da sé.)

CONTE:

Ora, perché abbiate il fornimento compagno, ecco ch\'io vi dono il
gioiello.

(A Mirandolina.)

MIRANDOLINA:

Non lo prendo assolutamente.

CONTE:

Non mi farete questa male creanza.

MIRANDOLINA:

Oh! delle male creanze non ne faccio mai. Per non disgustarla, lo
prenderò.

(Ortensia e Dejanira parlano come sopra, osservando la generosità del
Conte.)

MIRANDOLINA:

Ah! Che ne dice, signor Marchese? Questo gioiello non è galante?

MARCHESE:

Nel suo genere il fazzoletto è più di buon gusto.

CONTE:

Sì, ma da genere a genere vi è una bella distanza.

MARCHESE:

Bella cosa! Vantarsi in pubblico di una grande spesa.

CONTE:

Sì, sì, voi fate i vostri regali in segreto.

MIRANDOLINA:

(Posso ben dire con verità questa volta, che fra due litiganti il terzo
gode).

(Da sé.)

MARCHESE:

E così, damine mie, sarò a pranzo con voi.

ORTENSIA:

Quest\'altro signore chi è?

(Al Conte.)

CONTE:

Sono il Conte d\'Albafiorita, per obbedirvi.

DEJANIRA:

Capperi! È una famiglia illustre, io la conosco.

(Anch\'ella s\'accosta al Conte.)

CONTE:

Sono a\' vostri comandi.

(A Dejanira.)

ORTENSIA:

È qui alloggiato?

(Al Conte.)

CONTE:

Sì, signora.

DEJANIRA:

Si trattiene molto?

(Al Conte.)

CONTE:

Credo di sì.

MARCHESE:

Signore mie, sarete stanche di stare in piedi, volete ch\'io vi serva
nella vostra camera?

ORTENSIA:

Obbligatissima.

(Con disprezzo.)

Di che paese è, signor Conte?

CONTE:

Napolitano.

ORTENSIA:

Oh! Siamo mezzi patrioti. Io sono palermitana.

DEJANIRA:

Io son romana; ma sono stata a Napoli, e appunto per un mio interesse
desiderava parlare con un cavaliere napolitano.

CONTE:

Vi servirò, signore. Siete sole? Non avete uomini?

MARCHESE:

Ci sono io, signore: e non hanno bisogno di voi.

ORTENSIA:

Siamo sole, signor Conte. Poi vi diremo il perché.

CONTE:

Mirandolina.

MIRANDOLINA:

Signore.

CONTE:

Fate preparare nella mia camera per tre. Vi degnerete di favorirmi?

(Ad Ortensia e Dejanira.)

ORTENSIA:

Riceveremo le vostre finezze.

MARCHESE:

Ma io sono stato invitato da queste dame.

CONTE:

Esse sono padrone di servirsi come comandano, ma alla mia piccola tavola
in più di tre non ci si sta.

MARCHESE:

Vorrei veder anche questa\...

ORTENSIA:

Andiamo, andiamo, signor Conte. Il signor Marchese ci favorirà un\'altra
volta.

(Parte.)

DEJANIRA:

Signor Marchese, se trova il fazzoletto, mi raccomando.

(Parte.)

MARCHESE:

Conte, Conte, voi me la pagherete.

CONTE:

Di che vi lagnate?

MARCHESE:

Son chi sono, e non si tratta così. Basta\... Colei vorrebbe un
fazzoletto? Un fazzoletto di quella sorta? Non l\'avrà. Mirandolina,
tenetelo caro. Fazzoletti di quella sorta non se ne trovano. Dei
diamanti se ne trovano, ma dei fazzoletti di quella sorta non se ne
trovano.

(Parte.)

MIRANDOLINA:

(Oh che bel pazzo!).

(Da sé.)

CONTE:

Cara Mirandolina, avrete voi dispiacere ch\'io serva queste due dame?

MIRANDOLINA:

Niente affatto, signore.

CONTE:

Lo faccio per voi. Lo faccio per accrescer utile ed avventori alla
vostra locanda; per altro io son vostro, è vostro il mio cuore, e vostre
son le mie ricchezze, delle quali disponetene liberamente, che io vi
faccio padrona.

(Parte.)

SCENA VENTITREESIMA {#Section0009.xhtml#sigil_toc_id_61}
-------------------

MIRANDOLINA:

(sola)

Con tutte le sue ricchezze, con tutti li suoi regali, non arriverà mai
ad innamorarmi; e molto meno lo farà il Marchese colla sua ridicola
protezione. Se dovessi attaccarmi ad uno di questi due, certamente lo
farei con quello che spende più. Ma non mi preme né dell\'uno, né
dell\'altro. Sono in impegno d\'innamorar il Cavaliere di Ripafratta, e
non darei un tal piacere per un gioiello il doppio più grande di questo.
Mi proverò; non so se avrò l\'abilità che hanno quelle due brave
comiche, ma mi proverò. Il Conte ed il Marchese, frattanto che con
quelle si vanno trattenendo, mi lasceranno in pace; e potrò a mio
bell\'agio trattar col Cavaliere. Possibile ch\'ei non ceda? Chi è
quello che possa resistere ad una donna, quando le dà tempo di poter far
uso dell\'arte sua? Chi fugge non può temer d\'esser vinto, ma chi si
ferma, chi ascolta, e se ne compiace, deve o presto o tardi a suo
dispetto cadere.

(Parte.)

[]{#Section0010.xhtml}

ATTO SECONDO
============

SCENA PRIMA {#Section0010.xhtml#sigil_toc_id_62}
-----------

Camera del Cavaliere, con tavola apparecchiata per il pranzo e sedie.\
Il Cavaliere ed il suo Servitore, poi Fabrizio.\
Il Cavaliere passeggia con un libro.\
Fabrizio mette la zuppa in tavola.

FABRIZIO:

Dite al vostro padrone, se vuol restare servito, che la zuppa è in
tavola.

(Al Servitore.)

SERVITORE:

Glielo potete dire anche voi.

(A Fabrizio.)

FABRIZIO:

È tanto stravagante, che non gli parlo niente volentieri.

SERVITORE:

Eppure non è cattivo. Non può veder le donne, per altro cogli uomini è
dolcissimo.

FABRIZIO:

(Non può veder le donne? Povero sciocco! Non conosce il buono).

(Da sé, parte.)

SERVITORE:

Illustrissimo, se comoda, è in tavola.

(Il Cavaliere mette giù il libro, e va a sedere a tavola.)

CAVALIERE:

Questa mattina parmi che si pranzi prima del solito.

(Al Servitore, mangiando.)\
(Il Servitore dietro la sedia del Cavaliere, col tondo sotto il
braccio.)

SERVITORE:

Questa camera è stata servita prima di tutte. Il signor Conte
d\'Albafiorita strepitava che voleva essere servito il primo, ma la
padrona ha voluto che si desse in tavola prima a V.S. illustrissima.

CAVALIERE:

Sono obbligato a costei per l\'attenzione che mi dimostra.

SERVITORE:

È una assai compita donna, illustrissimo. In tanto mondo che ho veduto,
non ho trovato una locandiera più garbata di questa.

CAVALIERE:

Ti piace, eh?

(Voltandosi un poco indietro.)

SERVITORE:

Se non fosse per far torto al mio padrone, vorrei venire a stare con
Mirandolina per cameriere.

CAVALIERE:

Povero sciocco! Che cosa vorresti ch\'ella facesse di te?

(Gli dà il tondo, ed egli lo muta.)

SERVITORE:

Una donna di questa sorta, la vorrei servir come un cagnolino.

(Va per un piatto.)

CAVALIERE:

Per bacco! Costei incanta tutti. Sarebbe da ridere che incantasse anche
me. Orsù, domani me ne vado a Livorno. S\'ingegni per oggi, se può, ma
si assicuri che non sono sì debole. Avanti ch\'io superi l\'avversion
per le donne, ci vuol altro.

SCENA SECONDA {#Section0010.xhtml#sigil_toc_id_63}
-------------

Il Servitore col lesso ed un altro piatto, e detto.

SERVITORE:

Ha detto la padrona, che se non le piacesse il pollastro, le manderà un
piccione.

CAVALIERE:

Mi piace tutto. E questo che cos\'è?

SERVITORE:

Disse la padrona, ch\'io le sappia dire se a V.S. illustrissima piace
questa salsa, che l\'ha fatta ella colle sue mani.

CAVALIERE:

Costei mi obbliga sempre più.

(L\'assaggia.)

È preziosa. Dille che mi piace, che la ringrazio.

SERVITORE:

Glielo dirò, illustrissimo.

CAVALIERE:

Vaglielo a dir subito.

SERVITORE:

Subito. (Oh che prodigio! Manda un complimento a una donna!).

(Da sé, parte.)

CAVALIERE:

È una salsa squisita. Non ho sentita la meglio.

(Va mangiando.)

Certamente, se Mirandolina farà così, avrà sempre de\' forestieri. Buona
tavola, buona biancheria. E poi non si può negare che non sia gentile;
ma quel che più stimo in lei, è la sincerità. Oh, quella sincerità è
pure la bella cosa! Perché non posso io vedere le donne? Perché sono
finte, bugiarde, lusinghiere. Ma quella bella sincerità\...

SCENA TERZA {#Section0010.xhtml#sigil_toc_id_64}
-----------

Il servitore e detto.

SERVITORE:

Ringrazia V.S. illustrissima della bontà che ha d\'aggradire le sue
debolezze.

CAVALIERE:

Bravo, signor cerimoniere, bravo.

SERVITORE:

Ora sta facendo colle sue mani un altro piatto; non so dire che cosa
sia.

CAVALIERE:

Sta facendo?

SERVITORE:

Sì signore.

CAVALIERE:

Dammi da bere.

SERVITORE:

La servo.

(Va a prendere da bere.)

CAVALIERE:

Orsù, con costei bisognerà corrispondere con generosità. È troppo
compita; bisogna pagare il doppio. Trattarla bene, ma andar via presto.

(Il Servitore gli presenta da bere.)

CAVALIERE:

Il Conte è andato a pranzo?

(Beve.)

SERVITORE:

Illustrissimo sì, in questo momento. Oggi fa trattamento. Ha due dame a
tavola con lui.

CAVALIERE:

Due dame? Chi sono?

SERVITORE:

Sono arrivate a questa locanda poche ore sono. Non so chi sieno.

CAVALIERE:

Le conosceva il Conte?

SERVITORE:

Credo di no; ma appena le ha vedute, le ha invitate a pranzo seco.

CAVALIERE:

Che debolezza! Appena vede due donne, subito si attacca. Ed esse
accettano. E sa il cielo chi sono; ma sieno quali esser vogliono, sono
donne, e tanto basta. Il Conte si rovinerà certamente. Dimmi: il
Marchese è a tavola?

SERVITORE:

È uscito di casa, e non si è ancora veduto.

CAVALIERE:

In tavola.

(Fa mutare il tondo.)

SERVITORE:

La servo.

CAVALIERE:

A tavola con due dame! Oh che bella compagnia! Colle loro smorfie mi
farebbero passar l\'appetito.

SCENA QUARTA {#Section0010.xhtml#sigil_toc_id_65}
------------

Mirandolina con un tondo in mano, ed il Servitore, e detto.

MIRANDOLINA:

È permesso?

CAVALIERE:

Chi è di là?

SERVITORE:

Comandi.

CAVALIERE:

Leva là quel tondo di mano.

MIRANDOLINA:

Perdoni. Lasci ch\'io abbia l\'onore di metterlo in tavola colle mie
mani. (Mette in tavola la vivanda.)

CAVALIERE:

Questo non è offizio vostro.

MIRANDOLINA:

Oh signore, chi son io? Una qualche signora? Sono una serva di chi
favorisce venire alla mia locanda.

CAVALIERE:

(Che umiltà!).

(Da sé.)

MIRANDOLINA:

In verità, non avrei difficoltà di servire in tavola tutti, ma non lo
faccio per certi riguardi: non so s\'ella mi capisca. Da lei vengo senza
scrupoli, con franchezza.

CAVALIERE:

Vi ringrazio. Che vivanda è questa?

MIRANDOLINA:

Egli è un intingoletto fatto colle mie mani.

CAVALIERE:

Sarà buono. Quando lo avete fatto voi, sarà buono.

MIRANDOLINA:

Oh! troppa bontà, signore. Io non so far niente di bene; ma bramerei
saper fare, per dar nel genio ad un Cavalier sì compìto.

CAVALIERE:

(Domani a Livorno).

(Da sé.)

Se avete che fare, non istate a disagio per me.

MIRANDOLINA:

Niente, signore: la casa è ben provveduta di cuochi e servitori. Avrei
piacere di sentire, se quel piatto le dà nel genio.

CAVALIERE:

Volentieri, subito.

(Lo assaggia.)

Buono, prezioso. Oh che sapore! Non conosco che cosa sia.

MIRANDOLINA:

Eh, io, signore, ho de\' secreti particolari. Queste mani sanno far
delle belle cose!

CAVALIERE:

Dammi da bere.

(Al Servitore, con qualche passione.)

MIRANDOLINA:

Dietro questo piatto, signore, bisogna beverlo buono.

CAVALIERE:

Dammi del vino di Borgogna.

(Al Servitore.)

MIRANDOLINA:

Bravissimo. Il vino di Borgogna è prezioso. Secondo me, per pasteggiare
è il miglior vino che si possa bere.

(Il Servitore presenta la bottiglia in tavola, con un bicchiere.)

CAVALIERE:

Voi siete di buon gusto in tutto.

MIRANDOLINA:

In verità, che poche volte m\'inganno.

CAVALIERE:

Eppure questa volta voi v\'ingannate.

MIRANDOLINA:

In che, signore?

CAVALIERE:

In credere ch\'io meriti d\'essere da voi distinto.

MIRANDOLINA:

Eh, signor Cavaliere\...

(Sospirando.)

CAVALIERE:

Che cosa c\'è? Che cosa sono questi sospiri?

(Alterato.)

MIRANDOLINA:

Le dirò: delle attenzioni ne uso a tutti, e mi rattristo quando penso
che non vi sono che ingrati.

CAVALIERE:

Io non vi sarò ingrato.

(Con placidezza.)

MIRANDOLINA:

Con lei non pretendo di acquistar merito, facendo unicamente il mio
dovere.

CAVALIERE:

No, no, conosco benissimo\... Non sono cotanto rozzo quanto voi mi
credete. Di me non avrete a dolervi.

(Versa il vino nel bicchiere.)

MIRANDOLINA:

Ma\... signore\... io non l\'intendo.

CAVALIERE:

Alla vostra salute.

(Beve.)

MIRANDOLINA:

Obbligatissima; mi onora troppo.

CAVALIERE:

Questo vino è prezioso.

MIRANDOLINA:

Il Borgogna è la mia passione.

CAVALIERE:

Se volete, siete padrona.

(Le offerisce il vino.)

MIRANDOLINA:

Oh! Grazie, signore.

CAVALIERE:

Avete pranzato?

MIRANDOLINA:

Illustrissimo sì.

CAVALIERE:

Ne volete un bicchierino?

MIRANDOLINA:

Io non merito queste grazie.

CAVALIERE:

Davvero, ve lo do volentieri.

MIRANDOLINA:

Non so che dire. Riceverò le sue finezze.

CAVALIERE:

Porta un bicchiere.

(Al Servitore.)

MIRANDOLINA:

No, no, se mi permette: prenderò questo.

(Prende il bicchiere del Cavaliere.)

CAVALIERE:

Oibò. Me ne sono servito io.

MIRANDOLINA:

Beverò le sue bellezze.

(Ridendo.)\
(Il Servitore mette l\'altro bicchiere nella sottocoppa.)

CAVALIERE:

Eh galeotta!

(Versa il vino.)

MIRANDOLINA:

Ma è qualche tempo che ho mangiato: ho timore che mi faccia male.

CAVALIERE:

Non vi è pericolo.

MIRANDOLINA:

Se mi favorisse un bocconcino di pane\...

CAVALIERE:

Volentieri. Tenete.

(Le dà un pezzo di pane.)\
(Mirandolina col bicchiere in una mano, e nell\'altra il pane, mostra di
stare a disagio, e non saper come fare la zuppa.)

CAVALIERE:

Voi state in disagio. Volete sedere?

MIRANDOLINA:

Oh! Non son degna di tanto, signore.

CAVALIERE:

Via, via, siamo soli. Portale una sedia.

(Al Servitore.)

SERVITORE:

(Il mio padrone vuol morire: non ha mai fatto altrettanto.)

(Da sé; va a prendere la sedia.)

MIRANDOLINA:

Se lo sapessero il signor Conte ed il signor Marchese, povera me!

CAVALIERE:

Perché?

MIRANDOLINA:

Cento volte mi hanno voluto obbligare a bere qualche cosa, o a mangiare,
e non ho mai voluto farlo.

CAVALIERE:

Via, accomodatevi.

MIRANDOLINA:

Per obbedirla.

(Siede, e fa la zuppa nel vino.)

CAVALIERE:

Senti.

(Al Servitore, piano.)

(Non lo dire a nessuno, che la padrona sia stata a sedere alla mia
tavola).

SERVITORE:

(Non dubiti).

(Piano.)

(Questa novità mi sorprende).

(Da sé.)

MIRANDOLINA:

Alla salute di tutto quello che dà piacere al signor Cavaliere.

CAVALIERE:

Vi ringrazio, padroncina garbata.

MIRANDOLINA:

Di questo brindisi alle donne non ne tocca.

CAVALIERE:

No? Perché?

MIRANDOLINA:

Perché so che le donne non le può vedere.

CAVALIERE:

È vero, non le ho mai potute vedere.

MIRANDOLINA:

Si conservi sempre così.

CAVALIERE:

Non vorrei\...

(Si guarda dal Servitore.)

MIRANDOLINA:

Che cosa, signore?

CAVALIERE:

Sentite.

(Le parla nell\'orecchio.)

(Non vorrei che voi mi faceste mutar natura).

MIRANDOLINA:

Io, signore? Come?

CAVALIERE:

Va via.

(Al Servitore.)

SERVITORE:

Comanda in tavola?

CAVALIERE:

Fammi cucinare due uova, e quando son cotte, portale.

SERVITORE:

Come le comanda le uova?

CAVALIERE:

Come vuoi, spicciati.

SERVITORE:

Ho inteso. (Il padrone si va riscaldando).

(Da sé, parte.)

CAVALIERE:

Mirandolina, voi siete una garbata giovine.

MIRANDOLINA:

Oh signore, mi burla.

CAVALIERE:

Sentite. Voglio dirvi una cosa vera, verissima, che ritornerà in vostra
gloria.

MIRANDOLINA:

La sentirò volentieri.

CAVALIERE:

Voi siete la prima donna di questo mondo, con cui ho avuto la sofferenza
di trattar con piacere.

MIRANDOLINA:

Le dirò, signor Cavaliere: non già ch\'io meriti niente, ma alle volte
si danno questi sangui che s\'incontrano. Questa simpatia, questo genio,
si dà anche fra persone che non si conoscono. Anch\'io provo per lei
quello che non ho sentito per alcun altro.

CAVALIERE:

Ho paura che voi mi vogliate far perdere la mia quiete.

MIRANDOLINA:

Oh via, signor Cavaliere, se è un uomo savio, operi da suo pari. Non dia
nelle debolezze degli altri. In verità, se me n\'accorgo, qui non ci
vengo più. Anch\'io mi sento un non so che di dentro, che non ho più
sentito; ma non voglio impazzire per uomini, e molto meno per uno che ha
in odio le donne; e che forse forse per provarmi, e poi burlarsi di me,
viene ora con un discorso nuovo a tentarmi. Signor Cavaliere, mi
favorisca un altro poco di Borgogna.

CAVALIERE:

Eh! Basta\...

(Versa il vino in un bicchiere.)

MIRANDOLINA:

(Sta lì lì per cadere).

(Da sé.)

CAVALIERE:

Tenete.

(Le dà il bicchiere col vino.)

MIRANDOLINA:

Obbligatissima. Ma ella non beve?

CAVALIERE:

Sì, beverò. (Sarebbe meglio che io mi ubbriacassi. Un diavolo
scaccerebbe l\'altro).

(Da sé, versa il vino nel suo bicchiere.)

MIRANDOLINA:

Signor Cavaliere.

(Con vezzo.)

CAVALIERE:

Che c\'è?

MIRANDOLINA:

Tocchi.

(Gli fa toccare il bicchiere col suo.)

Che vivano i buoni amici.

CAVALIERE:

Che vivano.

(Un poco languente.)

MIRANDOLINA:

Viva\... chi si vuol bene\... senza malizia tocchi!

CAVALIERE:

Evviva\...

SCENA QUINTA {#Section0010.xhtml#sigil_toc_id_66}
------------

Il Marchese e detti.

MARCHESE:

Son qui ancor io. E che viva?

CAVALIERE:

Come, signor Marchese?

(Alterato.)

MARCHESE:

Compatite, amico. Ho chiamato. Non c\'è nessuno.

MIRANDOLINA:

Con sua licenza\...

(Vuol andar via.)

CAVALIERE:

Fermatevi.

(A Mirandolina.)

Io non mi prendo con voi cotanta libertà.

(Al Marchese.)

MARCHESE:

Vi domando scusa. Siamo amici. Credeva che foste solo. Mi rallegro
vedervi accanto alla nostra adorabile padroncina. Ah! Che dite? Non è un
capo d\'opera?

MIRANDOLINA:

Signore, io ero qui per servire il signor Cavaliere. Mi è venuto un poco
di male, ed egli mi ha soccorso con un bicchierin di Borgogna.

MARCHESE:

È Borgogna quello?

(Al Cavaliere.)

CAVALIERE:

Sì, è Borgogna.

MARCHESE:

Ma di quel vero?

CAVALIERE:

Almeno l\'ho pagato per tale.

MARCHESE:

Io me n\'intendo. Lasciate che lo senta, e vi saprò dire se è, o se non
è.

CAVALIERE:

Ehi!

(Chiama.)

SCENA SESTA {#Section0010.xhtml#sigil_toc_id_67}
-----------

Il Servitore colle ova, e detti.

CAVALIERE:

Un bicchierino al Marchese.

(Al Servitore.)

MARCHESE:

Non tanto piccolo il bicchierino. Il Borgogna non è liquore. Per
giudicarne bisogna beverne a sufficienza.

SERVITORE:

Ecco le ova.

(Vuol metterle in tavola.)

CAVALIERE:

Non voglio altro.

MARCHESE:

Che vivanda è quella?

CAVALIERE:

Ova.

MARCHESE:

Non mi piacciono.

(Il Servitore le porta via.)

MIRANDOLINA:

Signor Marchese, con licenza del signor Cavaliere, senta
quell\'intingoletto fatto colle mie mani.

MARCHESE:

Oh sì. Ehi. Una sedia.

(Il Servitore gli reca una sedia e mette il bicchiere sulla sottocoppa.)

Una forchetta.

CAVALIERE:

Via, recagli una posata.

(Il Servitore la va a prendere.)

MIRANDOLINA:

Signor Cavaliere, ora sto meglio. Me n\'anderò.

(S\'alza.)

MARCHESE:

Fatemi il piacere, restate ancora un poco.

MIRANDOLINA:

Ma signore, ho da attendere a\' fatti miei; e poi il signor
Cavaliere\...

MARCHESE:

Vi contentate ch\'ella resti ancora un poco?

(Al Cavaliere.)

CAVALIERE:

Che volete da lei?

MARCHESE:

Voglio farvi sentire un bicchierino di vin di Cipro che, da che siete al
mondo, non avrete sentito il compagno. E ho piacere che Mirandolina lo
senta, e dica il suo parere.

CAVALIERE:

Via, per compiacere il signor Marchese, restate.

(A Mirandolina.)

MIRANDOLINA:

Il signor Marchese mi dispenserà.

MARCHESE:

Non volete sentirlo?

MIRANDOLINA:

Un\'altra volta, Eccellenza.

CAVALIERE:

Via, restate.

MIRANDOLINA:

Me lo comanda?

(Al Cavaliere.)

CAVALIERE:

Vi dico che restiate.

MIRANDOLINA:

Obbedisco.

(Siede.)

CAVALIERE:

(Mi obbliga sempre più).

(Da sé.)

MARCHESE:

Oh che roba! Oh che intingolo! Oh che odore! Oh che sapore!

(Mangiando.)

CAVALIERE:

(Il Marchese avrà gelosia, che siate vicina a me).

(Piano a Mirandolina.)

MIRANDOLINA:

(Non m\'importa di lui né poco, né molto).

(Piano al Cavaliere.)

CAVALIERE:

(Siete anche voi nemica degli uomini?).

(Piano a Mirandolina.)

MIRANDOLINA:

(Come ella lo è delle donne).

(Come sopra.)

CAVALIERE:

(Queste mie nemiche si vanno vendicando di me).

(Come sopra.)

MIRANDOLINA:

(Come, signore?).

(Come sopra.)

CAVALIERE:

(Eh! furba! Voi vedrete benissimo\...).

(Come sopra.)

MARCHESE:

Amico, alla vostra salute.

(Beve il vino di Borgogna.)

CAVALIERE:

Ebbene? Come vi pare?

MARCHESE:

Con vostra buona grazia, non val niente. Sentite il mio vin di Cipro.

CAVALIERE:

Ma dov\'è questo vino di Cipro?

MARCHESE:

L\'ho qui, l\'ho portato con me, voglio che ce lo godiamo: ma! è di
quello. Eccolo.

(Tira fuori una bottiglia assai piccola.)

MIRANDOLINA:

Per quel che vedo, signor Marchese, non vuole che il suo vino ci vada
alla testa.

MARCHESE:

Questo? Si beve a gocce, come lo spirito di melissa. Ehi? Li
bicchierini.

(Apre la bottiglia.)

SERVITORE:

(porta de\' bicchierini da vino di Cipro.)

MARCHESE:

Eh, son troppo grandi. Non ne avete di più piccoli?

(Copre la bottiglia colla mano.)

CAVALIERE:

Porta quei da rosolio.

(Al Servitore.)

MIRANDOLINA:

Io credo che basterebbe odorarlo.

MARCHESE:

Uh caro! Ha un odor che consola.

(Lo annusa.)

SERVITORE:

(porta tre bicchierini sulla sottocoppa.)

MARCHESE:

(versa pian piano, e non empie li bicchierini, poi lo dispensa al
Cavaliere, a Mirandolina, e l\'altro per sé, turando bene la bottiglia)

Che nettare! Che ambrosia! Che manna distillata!

(Bevendo.)

CAVALIERE:

(Che vi pare di questa porcheria?).

(A Mirandolina, piano.)

MIRANDOLINA:

(Lavature di fiaschi).

(Al Cavaliere, piano.)

MARCHESE:

Ah! Che dite?

(Al Cavaliere.)

CAVALIERE:

Buono, prezioso.

MARCHESE:

Ah! Mirandolina, vi piace?

MIRANDOLINA:

Per me, signore, non posso dissimulare; non mi piace, lo trovo cattivo,
e non posso dir che sia buono. Lodo chi sa fingere. Ma chi sa fingere in
una cosa, saprà fingere nell\'altre ancora.

CAVALIERE:

(Costei mi dà un rimprovero; non capisco il perché).

(Da sé.)

MARCHESE:

Mirandolina, voi di questa sorta di vini non ve ne intendete. Vi
compatisco. Veramente il fazzoletto che vi ho donato, l\'avete
conosciuto e vi è piaciuto, ma il vin di Cipro non lo conoscete.

(Finisce di bere.)

MIRANDOLINA:

(Sente come si vanta?).

(Al Cavaliere, piano.)

CAVALIERE:

(Io non farei così).

(A Mirandolina, piano.)

MIRANDOLINA:

(Il di lei vanto sta nel disprezzare le donne).

(Come sopra.)

CAVALIERE:

(E il vostro nel vincere tutti gli uomini).

(Come sopra.)

MIRANDOLINA:

(Tutti no).

(Con vezzo, al Cavaliere, piano.)

CAVALIERE:

(Tutti sì.)

(Con qualche passione, piano a Mirandolina.)

MARCHESE:

Ehi? Tre bicchierini politi.

(Al Servitore, il quale glieli porta sopra una sottocoppa.)

MIRANDOLINA:

Per me non ne voglio più.

MARCHESE:

No, no, non dubitate: non faccio per voi.

(Mette del vino di Cipro nei tre bicchieri.)

Galantuomo, con licenza del vostro padrone, andate dal Conte
d\'Albafiorita, e ditegli per parte mia, forte, che tutti sentano, che
lo prego di assaggiare un poco del mio vino di Cipro.

SERVITORE:

Sarà servito. (Questo non li ubbriaca certo).

(Da sé; parte.)

CAVALIERE:

Marchese, voi siete assai generoso.

MARCHESE:

Io? Domandatelo a Mirandolina.

MIRANDOLINA:

Oh certamente!

MARCHESE:

L\'ha veduto il fazzoletto il Cavaliere?

(A Mirandolina.)

MIRANDOLINA:

Non lo ha ancora veduto.

MARCHESE:

Lo vedrete.

(Al Cavaliere.)

Questo poco di balsamo me lo salvo per questa sera.

(Ripone la bottiglia con un dito di vino avanzato.)

MIRANDOLINA:

Badi che non gli faccia male, signor Marchese.

MARCHESE:

Eh! Sapete che cosa mi fa male?

(A Mirandolina.)

MIRANDOLINA:

Che cosa?

MARCHESE:

I vostri begli occhi.

MIRANDOLINA:

Davvero?

MARCHESE:

Cavaliere mio, io sono innamorato di costei perdutamente.

CAVALIERE:

Me ne dispiace.

MARCHESE:

Voi non avete mai provato amore per le donne. Oh, se lo provaste,
compatireste ancora me.

CAVALIERE:

Sì, vi compatisco.

MARCHESE:

E son geloso come una bestia. La lascio stare vicino a voi, perché so
chi siete; per altro non lo soffrirei per centomila doppie.

CAVALIERE:

(Costui principia a seccarmi).

(Da sé.)

SCENA SETTIMA {#Section0010.xhtml#sigil_toc_id_68}
-------------

Il Servitore con una bottiglia sulla sottocoppa, e detti.

SERVITORE:

Il signor Conte ringrazia V.E., e manda una bottiglia di vino di
Canarie.

(Al Marchese.)

MARCHESE:

Oh, oh, vorrà mettere il suo vin di Canarie col mio vino di Cipro?
Lascia vedere. Povero pazzo! È una porcheria, lo conosco all\'odore.

(S\'alza e tiene la bottiglia in mano.)

CAVALIERE:

Assaggiatelo prima.

(Al Marchese.)

MARCHESE:

Non voglio assaggiar niente. Questa è una impertinenza che mi fa il
Conte, compagna di tante altre. Vuol sempre starmi al di sopra. Vuol
soverchiarmi, vuol provocarmi, per farmi far delle bestialità. Ma giuro
al cielo, ne farò una che varrà per cento. Mirandolina, se non lo
cacciate via, nasceranno delle cose grandi, sì, nasceranno delle cose
grandi. Colui è un temerario. Io son chi sono, e non voglio soffrire
simile affronti.

(Parte, e porta via la bottiglia.)

SCENA OTTAVA {#Section0010.xhtml#sigil_toc_id_69}
------------

Il Cavaliere, Mirandolina ed il Servitore.

CAVALIERE:

Il povero Marchese è pazzo.

MIRANDOLINA:

Se a caso mai la bile gli facesse male, ha portato via la bottiglia per
ristorarsi.

CAVALIERE:

È pazzo, vi dico. E voi lo avete fatto impazzire.

MIRANDOLINA:

Sono di quelle che fanno impazzare gli uomini?

CAVALIERE:

Sì, voi siete\...

(Con affanno.)

MIRANDOLINA:

Signor Cavaliere, con sua licenza.

(S\'alza.)

CAVALIERE:

Fermatevi.

MIRANDOLINA:

Perdoni; io non faccio impazzare nessuno.

(Andando.)

CAVALIERE:

Ascoltatemi.

(S\'alza, ma resta alla tavola.)

MIRANDOLINA:

Scusi.

(Andando.)

CAVALIERE:

Fermatevi, vi dico.

(Con imperio.)

MIRANDOLINA:

Che pretende da me?

(Con alterezza voltandosi.)

CAVALIERE:

Nulla.

(Si confonde.)

Beviamo un altro bicchiere di Borgogna.

MIRANDOLINA:

Via signore, presto, presto, che me ne vada.

CAVALIERE:

Sedete.

MIRANDOLINA:

In piedi, in piedi.

CAVALIERE:

Tenete.

(Con dolcezza le dà il bicchiere.)

MIRANDOLINA:

Faccio un brindisi, e me ne vado subito. Un brindisi che mi ha insegnato
mia nonna.

"Viva Bacco, e viva Amore:\
L\'uno e l\'altro ci consola;\
Uno passa per la gola,\
L\'altro va dagli occhi al cuore.\
Bevo il vin, cogli occhi poi\...\
Faccio quel che fate voi."

(Parte.)

SCENA NONA {#Section0010.xhtml#sigil_toc_id_70}
----------

Il Cavaliere, ed il Servitore.

CAVALIERE:

Bravissima, venite qui: sentite. Ah malandrina! Se nè fuggita. Se n\'è
fuggita, e mi ha lasciato cento diavoli che mi tormentano.

SERVITORE:

Comanda le frutta in tavola?

(Al Cavaliere.)

CAVALIERE:

Va al diavolo ancor tu.

(Il Servitore parte.)

Bevo il vin, cogli occhi poi, faccio quel che fate voi? Che brindisi
misterioso è questo? Ah maladetta, ti conosco. Mi vuoi abbattere, mi
vuoi assassinare. Ma lo fa con tanta grazia! Ma sa così bene
insinuarsi\... Diavolo, diavolo, me la farai tu vedere? No, anderò a
Livorno. Costei non la voglio più rivedere. Che non mi venga più tra i
piedi. Maledettissime donne! Dove vi sono donne, lo giuro non vi anderò
mai più.

(Parte.)

SCENA DECIMA {#Section0010.xhtml#sigil_toc_id_71}
------------

Camera del Conte.\
Il Conte d\'Albafiorita, Ortensia e Dejanira.

CONTE:

Il Marchese di Forlipopoli è un carattere curiosissimo. È nato nobile,
non si può negare; ma fra suo padre e lui hanno dissipato, ed ora non ha
appena da vivere. Tuttavolta gli piace fare il grazioso.

ORTENSIA:

Si vede che vorrebbe essere generoso, ma non ne ha.

DEJANIRA:

Dona quel poco che può, e vuole che tutto il mondo lo sappia.

CONTE:

Questo sarebbe un bel carattere per una delle vostre commedie.

ORTENSIA:

Aspetti che arrivi la compagnia, e che si vada in teatro, e può darsi
che ce lo godiamo.

DEJANIRA:

Abbiamo noi dei personaggi, che per imitare i caratteri sono fatti a
posta.

CONTE:

Ma se volete che ce lo godiamo, bisogna che con lui seguitiate a
fingervi dame.

ORTENSIA:

Io lo farò certo. Ma Dejanira subito dà di bianco.

DEJANIRA:

Mi vien da ridere, quando i gonzi mi credono una signora.

CONTE:

Con me avete fatto bene a scoprirvi. In questa maniera mi date campo di
far qualche cosa in vostro vantaggio.

ORTENSIA:

Il signor Conte sarà il nostro protettore.

DEJANIRA:

Siamo amiche, goderemo unitamente le di lei grazie.

CONTE:

Vi dirò, vi parlerò con sincerità. Vi servirò, dove potrò farlo, ma ho
un certo impegno, che non mi permetterà frequentare la vostra casa.

ORTENSIA:

Ha qualche amoretto, signor Conte?

CONTE:

Sì, ve lo dirò in confidenza. La padrona della locanda.

ORTENSIA:

Capperi! Veramente una gran signora! Mi meraviglio di lei, signor Conte,
che si perda con una locandiera!

DEJANIRA:

Sarebbe minor male, che si compiacesse d\'impiegare le sue finezze per
una comica.

CONTE:

Il far all\'amore con voi altre, per dirvela, mi piace poco. Ora ci
siete, ora non ci siete.

ORTENSIA:

Non è meglio così, signore? In questa maniera non si eternano le
amicizie, e gli uomini non si rovinano.

CONTE:

Ma io, tant\'è, sono impegnato; le voglio bene, e non la vo\'
disgustare.

DEJANIRA:

Ma che cosa ha di buono costei?

CONTE:

Oh! Ha del buono assai.

ORTENSIA:

Ehi, Dejanira. È bella, rossa.

(Fa cenno che si belletta.)

CONTE:

Ha un grande spirito.

DEJANIRA:

Oh, in materia di spirito, la vorreste mettere con noi?

CONTE:

Ora basta. Sia come esser si voglia; Mirandolina mi piace, e se volete
la mia amicizia, avete a dirne bene, altrimenti fate conto di non avermi
mai conosciuto.

ORTENSIA:

Oh signor Conte, per me dico che Mirandolina è una dea Venere.

DEJANIRA:

Sì, sì, vero. Ha dello spirito, parla bene.

CONTE:

Ora mi date gusto.

ORTENSIA:

Quando non vuol altro, sarà servito.

CONTE:

Oh! Avete veduto quello ch\'è passato per sala?

(Osservando dentro la scena.)

ORTENSIA:

L\'ho veduto.

CONTE:

Quello è un altro bel carattere da commedia.

ORTENSIA:

È uno che non può vedere le donne.

DEJANIRA:

Oh che pazzo!

ORTENSIA:

Avrà qualche brutta memoria di qualche donna.

CONTE:

Oibò; non è mai stato innamorato. Non ha mai voluto trattar con donne.
Le sprezza tutte, e basta dire che egli disprezza ancora Mirandolina.

ORTENSIA:

Poverino! Se mi ci mettessi attorno io, scommetto lo farei cambiare
opinione.

DEJANIRA:

Veramente una gran cosa! Questa è un\'impresa che la vorrei pigliare
sopra di me.

CONTE:

Sentite, amiche. Così per puro divertimento. Se vi dà l\'anima
d\'innamorarlo, da cavaliere vi faccio un bel regalo.

ORTENSIA:

Io non intendo essere ricompensata per questo: lo farò per mio spasso.

DEJANIRA:

Se il signor Conte vuol usarci qualche finezza, non l\'ha da fare per
questo. Sinché arrivano i nostri compagni, ci divertiremo un poco.

CONTE:

Dubito che non farete niente.

ORTENSIA:

Signor Conte, ha ben poca stima di noi.

DEJANIRA:

Non siamo vezzose come Mirandolina; ma finalmente sappiamo qualche poco
il viver del mondo.

CONTE:

Volete che lo mandiamo a chiamare?

ORTENSIA:

Faccia come vuole.

CONTE:

Ehi? Chi è di là?

SCENA UNDICESIMA {#Section0010.xhtml#sigil_toc_id_72}
----------------

Il Servitore del Conte, e detti.

CONTE:

Di\' al Cavaliere di Ripafratta, che favorisca venir da me, che mi preme
di parlargli.

(Al Servitore.)

SERVITORE:

Nella sua camera so che non c\'è.

CONTE:

L\'ho veduto andar verso la cucina. Lo troverai.

SERVITORE:

Subito.

(Parte.)

CONTE:

(Che mai è andato a far verso la cucina? Scommetto che è andato a
strapazzare Mirandolina, perché gli ha dato mal da mangiare).

(Da sé.)

ORTENSIA:

Signor Conte, io aveva pregato il signor Marchese che mi mandasse il suo
calzolaro, ma ho paura di non vederlo.

CONTE:

Non pensate altro. Vi servirò io.

DEJANIRA:

A me aveva il signor Marchese promesso un fazzoletto. Ma! ora me lo
porta!

CONTE:

De\' fazzoletti ne troveremo.

DEJANIRA:

Egli è che ne avevo proprio di bisogno.

CONTE:

Se questo vi gradisce, siete padrona. È pulito.

(Le offre il suo di seta.)

DEJANIRA:

Obbligatissima alle sue finezze.

CONTE:

Oh! Ecco il Cavaliere. Sarà meglio che sostenghiate il carattere di
dame, per poterlo meglio obbligare ad ascoltarvi per civiltà. Ritiratevi
un poco indietro; che, se vi vede, fugge.

ORTENSIA:

Come si chiama?

CONTE:

Il Cavaliere di Ripafratta, toscano.

DEJANIRA:

Ha moglie?

CONTE:

Non può vedere le donne.

ORTENSIA:

È ricco?

(Ritirandosi.)

CONTE:

Sì, Molto.

DEJANIRA:

È generoso?

(Ritirandosi.)

CONTE:

Piuttosto.

DEJANIRA:

Venga, venga.

(Si ritira.)

ORTENSIA:

Tempo, e non dubiti.

(Si ritira.)

SCENA DODICESIMA {#Section0010.xhtml#sigil_toc_id_73}
----------------

Il Cavaliere e detti.

CAVALIERE:

Conte, siete voi che mi volete?

CONTE:

Sì; io v\'ho dato il presente incomodo.

CAVALIERE:

Che cosa posso fare per servirvi?

CONTE:

Queste due dame hanno bisogno di voi.

(Gli addita le due donne, le quali subito s\'avanzano.)

CAVALIERE:

Disimpegnatemi. Io non ho tempo di trattenermi.

ORTENSIA:

Signor Cavaliere, non intendo di recargli incomodo.

DEJANIRA:

Una parola in grazia, signor Cavaliere.

CAVALIERE:

Signore mie, vi supplico perdonarmi. Ho un affar di premura.

ORTENSIA:

In due parole vi sbrighiamo.

DEJANIRA:

Due paroline, e non più, signore.

CAVALIERE:

(Maledettissimo Conte!).

(Da sé.)

CONTE:

Caro amico, due dame che pregano, vuole la civiltà che si ascoltino.

CAVALIERE:

Perdonate. In che vi posso servire?

(Alle donne, con serietà.)

ORTENSIA:

Non siete voi toscano, signore?

CAVALIERE:

Sì, signora.

DEJANIRA:

Avrete degli amici in Firenze?

CAVALIERE:

Ho degli amici, e ho de\' parenti.

DEJANIRA:

Sappiate, signore\... Amica, principiate a dir voi.

(Ad Ortensia.)

ORTENSIA:

Dirò, signor Cavaliere\... Sappia che un certo caso\...

CAVALIERE:

Via, signore, vi supplico. Ho un affar di premura.

CONTE:

Orsù, capisco che la mia presenza vi dà soggezione. Confidatevi con
libertà al Cavaliere, ch\'io vi levo l\'incomodo.

(Partendo.)

CAVALIERE:

No, amico, restate\... Sentite.

CONTE:

So il mio dovere. Servo di lor signore.

(Parte.)

SCENA TREDICESIMA {#Section0010.xhtml#sigil_toc_id_74}
-----------------

Ortensia, Dejanira ed il Cavaliere.

ORTENSIA:

Favorisca, sediamo.

CAVALIERE:

Scusi, non ho volontà di sedere.

DEJANIRA:

Così rustico colle donne?

CAVALIERE:

Favoriscano dirmi che cosa vogliono.

ORTENSIA:

Abbiamo bisogno del vostro aiuto, della vostra protezione, della vostra
bontà.

CAVALIERE:

Che cosa vi è accaduto?

DEJANIRA:

I nostri mariti ci hanno abbandonate.

CAVALIERE:

Abbandonate? Come! Due dame abbandonate? Chi sono i vostri mariti?

(Con alterezza.)

DEJANIRA:

Amica, non vado avanti sicuro.

(Ad Ortensia.)

ORTENSIA:

(È tanto indiavolato, che or ora mi confondo ancor io).

(Da sé.)

CAVALIERE:

Signore, vi riverisco.

(In atto di partire.)

ORTENSIA:

Come! Così ci trattate?

DEJANIRA:

Un cavaliere tratta così?

CAVALIERE:

Perdonatemi. Io son uno che ama assai la mia pace. Sento due dame
abbandonate dai loro mariti. Qui ci saranno degl\'impegni non pochi; io
non sono atto a\' maneggi. Vivo a me stesso. Dame riveritissime, da me
non potete sperare né consiglio, né aiuto.

ORTENSIA:

Oh via, dunque; non lo tenghiamo più in soggezione il nostro
amabilissimo Cavaliere.

DEJANIRA:

Sì, parliamogli con sincerità.

CAVALIERE:

Che nuovo linguaggio è questo?

ORTENSIA:

Noi non siamo dame.

CAVALIERE:

No?

DEJANIRA:

Il signor Conte ha voluto farvi uno scherzo.

CAVALIERE:

Lo scherzo è fatto. Vi riverisco.

(Vuol partire.)

ORTENSIA:

Fermatevi un momento.

CAVALIERE:

Che cosa volete?

DEJANIRA:

Degnateci per un momento della vostra amabile conversazione.

CAVALIERE:

Ho che fare. Non posso trattenermi.

ORTENSIA:

Non vi vogliamo già mangiar niente.

DEJANIRA:

Non vi leveremo la vostra reputazione.

ORTENSIA:

Sappiamo che non potete vedere le donne.

CAVALIERE:

Se lo sapete, l\'ho caro. Vi riverisco.

(Vuol partire.)

ORTENSIA:

Ma sentite: noi non siamo donne che possano darvi ombra.

CAVALIERE:

Chi siete?

ORTENSIA:

Diteglielo voi, Dejanira.

DEJANIRA:

Glielo potete dire anche voi.

CAVALIERE:

Via, chi siete?

ORTENSIA:

Siamo due commedianti.

CAVALIERE:

Due commedianti! Parlate, parlate, che non ho più paura di voi. Son ben
prevenuto in favore dell\'arte vostra.

ORTENSIA:

Che vuol dire? Spiegatevi.

CAVALIERE:

So che fingete in iscena e fuor di scena; e con tal prevenzione non ho
paura di voi.

DEJANIRA:

Signore, fuori di scena io non so fingere.

CAVALIERE:

Come si chiama ella? La signora Sincera?

(A Dejanira.)

DEJANIRA:

Io mi chiamo\...

CAVALIERE:

È ella la signora Buonalana?

(Ad Ortensia.)

ORTENSIA:

Caro signor Cavaliere\...

CAVALIERE:

Come si diletta di miccheggiare?

(Ad Ortensia.)

ORTENSIA:

Io non sono\...

CAVALIERE:

I gonzi come li tratta, padrona mia?

(A Dejanira.)

DEJANIRA:

Non son di quelle\...

CAVALIERE:

Anch\'io so parlar in gergo.

ORTENSIA:

Oh che caro signor Cavaliere!

(Vuol prenderlo per un braccio.)

CAVALIERE:

Basse le cere.

(Dandole nelle mani.)

ORTENSIA:

Diamine! Ha più del contrasto, che del Cavaliere.

CAVALIERE:

Contrasto vuol dire contadino. Vi ho capito. E vi dirò che siete due
impertinenti.

DEJANIRA:

A me questo?

ORTENSIA:

A una donna della mia sorte?

CAVALIERE:

Bello quel viso trionfato!

(Ad Ortensia.)

ORTENSIA:

(Asino!).

(Parte.)

CAVALIERE:

Bello quel tuppè finto!

(A Dejanira.)

DEJANIRA:

(Maledetto).

(Parte.)

SCENA QUATTORDICESIMA {#Section0010.xhtml#sigil_toc_id_75}
---------------------

Il Cavaliere, poi il di lui Servitore.

CAVALIERE:

Ho trovata ben io la maniera di farle andare. Che si pensavano? Di
tirarmi nella rete? Povere sciocche! Vadano ora dal Conte e gli narrino
la bella scena. Se erano dame, per rispetto mi conveniva fuggire; ma
quando posso, le donne le strapazzo col maggior piacere del mondo. Non
ho però potuto strapazzar Mirandolina. Ella mi ha vinto con tanta
civiltà, che mi trovo obbligato quasi ad amarla. Ma è donna; non me ne
voglio fidare. Voglio andar via. Domani anderò via. Ma se aspetto a
domani? Se vengo questa sera a dormir a casa, chi mi assicura che
Mirandolina non finisca a rovinarmi?

(Pensa.)

Sì; facciamo una risoluzione da uomo.

SERVITORE:

Signore.

CAVALIERE:

Che cosa vuoi?

SERVITORE:

Il signor Marchese è nella di lei camera che l\'aspetta, perché desidera
di parlargli.

CAVALIERE:

Che vuole codesto pazzo? Denari non me ne cava più di sotto. Che
aspetti, e quando sarà stracco di aspettare, se n\'anderà. Va dal
cameriere della locanda e digli che subito porti il mio conto.

SERVITORE:

Sarà obbedita.

(In atto di partire.)

CAVALIERE:

Senti. Fa che da qui a due ore siano pronti i bauli.

SERVITORE:

Vuol partire forse?

CAVALIERE:

Sì, portami qui la spada ed il cappello, senza che se n\'accorga il
Marchese.

SERVITORE:

Ma se mi vede fare i bauli?

CAVALIERE:

Dica ciò che vuole. M\'hai inteso.

SERVITORE:

(Oh, quanto mi dispiace andar via, per causa di Mirandolina!).

(Da sé, parte.)

CAVALIERE:

Eppure è vero. Io sento nel partir di qui una dispiacenza nuova, che non
ho mai provata. Tanto peggio per me, se vi restassi. Tanto più presto mi
conviene partire. Sì, donne, sempre più dirò male di voi; sì, voi ci
fate del male, ancora quando ci volete fare del bene.

SCENA QUINDICESIMA {#Section0010.xhtml#sigil_toc_id_76}
------------------

Fabrizio e detto.

FABRIZIO:

È vero, signore, che vuole il conto?

CAVALIERE:

Sì, l\'avete portato?

FABRIZIO:

Adesso la padrona lo fa.

CAVALIERE:

Ella fa i conti?

FABRIZIO:

Oh, sempre ella. Anche quando viveva suo padre. Scrive e sa far di conto
meglio di qualche giovane di negozio.

CAVALIERE:

(Che donna singolare è costei!).

(Da sé.)

FABRIZIO:

Ma vuol ella andar via così presto?

CAVALIERE:

Sì, così vogliono i miei affari.

FABRIZIO:

La prego di ricordarsi del cameriere.

CAVALIERE:

Portate il conto, e so quel che devo fare.

FABRIZIO:

Lo vuol qui il conto?

CAVALIERE:

Lo voglio qui; in camera per ora non ci vado.

FABRIZIO:

Fa bene; in camera sua vi è quel seccatore del signor Marchese. Carino!
Fa l\'innamorato della padrona; ma può leccarsi le dita. Mirandolina
deve esser mia moglie.

CAVALIERE:

Il conto.

(Alterato.)

FABRIZIO:

La servo subito.

(Parte.)

SCENA SEDICESIMA {#Section0010.xhtml#sigil_toc_id_77}
----------------

CAVALIERE:

(solo)

Tutti sono invaghiti di Mirandolina. Non è maraviglia, se ancor io
principiava a sentirmi accendere. Ma anderò via; supererò questa
incognita forza\... Che vedo? Mirandolina? Che vuole da me? Ha un foglio
in mano. Mi porterà il conto. Che cosa ho da fare? Convien soffrire
quest\'ultimo assalto. Già da qui a due ore io parto.

SCENA DICIASSETTESIMA {#Section0010.xhtml#sigil_toc_id_78}
---------------------

Mirandolina con un foglio in mano, e detto.

MIRANDOLINA:

Signore.

(Mestamente.)

CAVALIERE:

Che c\'è, Mirandolina?

MIRANDOLINA:

Perdoni.

(Stando indietro.)

CAVALIERE:

Venite avanti.

MIRANDOLINA:

Ha domandato il suo conto; l\'ho servita.

(Mestamente.)

CAVALIERE:

Date qui.

MIRANDOLINA:

Eccolo.

(Si asciuga gli occhi col grembiale, nel dargli il conto.)

CAVALIERE:

Che avete? Piangete?

MIRANDOLINA:

Niente, signore, mi è andato del fumo negli occhi.

CAVALIERE:

Del fumo negli occhi? Eh! basta\... quanto importa il conto?

(legge.)

Venti paoli? In quattro giorni un trattamento si generoso: venti paoli?

MIRANDOLINA:

Quello è il suo conto.

CAVALIERE:

E i due piatti particolari che mi avete dato questa mattina, non ci sono
nel conto?

MIRANDOLINA:

Perdoni. Quel ch\'io dono, non lo metto in conto.

CAVALIERE:

Me li avete voi regalati?

MIRANDOLINA:

Perdoni la libertà. Gradisca per un atto di\...

(Si copre, mostrando di piangere.)

CAVALIERE:

Ma che avete?

MIRANDOLINA:

Non so se sia il fumo, o qualche flussione di occhi.

CAVALIERE:

Non vorrei che aveste patito, cucinando per me quelle due preziose
vivande.

MIRANDOLINA:

Se fosse per questo, lo soffrirei\... volentieri\...

(Mostra trattenersi di piangere.)

CAVALIERE:

(Eh, se non vado via!).

(Da sé.)

Orsù, tenete. Queste sono due doppie. Godetele per amor mio\... e
compatitemi\...

(S\'imbroglia.)

MIRANDOLINA:

(senza parlare, cade come svenuta sopra una sedia.)

CAVALIERE:

Mirandolina. Ahimè! Mirandolina. È svenuta. Che fosse innamorata di me?
Ma così presto? E perché no? Non sono io innamorato di lei? Cara
Mirandolina\... Cara? Io cara ad una donna? Ma se è svenuta per me. Oh,
come tu sei bella! Avessi qualche cosa per farla rinvenire. Io che non
pratico donne, non ho spiriti, non ho ampolle. Chi è di là? Vi è
nessuno? Presto?\... Anderò io. Poverina! Che tu sia benedetta!

(Parte, e poi ritorna.)

MIRANDOLINA:

Ora poi è caduto affatto. Molte sono le nostre armi, colle quali si
vincono gli uomini. Ma quando sono ostinati, il colpo di riserva
sicurissimo è uno svenimento. Torna, torna.

(Si mette come sopra.)

CAVALIERE:

(torna con un vaso d\'acqua.)

Eccomi, eccomi. E non è ancor rinvenuta. Ah, certamente costei mi ama.

(La spruzza, ed ella si va movendo.)

Animo, animo. Son qui cara. Non partirò più per ora.

SCENA DICIOTTESIMA {#Section0010.xhtml#sigil_toc_id_79}
------------------

Il Servitore colla spada e cappello, e detti.

SERVITORE:

Ecco la spada ed il cappello.

(Al Cavaliere.)

CAVALIERE:

Va via.

(Al Servitore, con ira.)

SERVITORE:

I bauli\...

CAVALIERE:

Va via, che tu sia maledetto.

SERVITORE:

Mirandolina\...

CAVALIERE:

Va, che ti spacco la testa.

(Lo minaccia col vaso; il Servitore parte.)

E non rinviene ancora? La fronte le suda. Via, cara Mirandolina, fatevi
coraggio, aprite gli occhi. Parlatemi con libertà.

SCENA DICIANNOVESIMA {#Section0010.xhtml#sigil_toc_id_80}
--------------------

Il Marchese ed il Conte, e detti.

MARCHESE:

Cavaliere?

CONTE:

Amico?

CAVALIERE:

(Oh maldetti!).

(Va smaniando.)

MARCHESE:

Mirandolina.

MIRANDOLINA:

Oimè!

(S\'alza.)

MARCHESE:

Io l\'ho fatta rinvenire.

CONTE:

Mi rallegro, signor Cavaliere.

MARCHESE:

Bravo quel signore, che non può vedere le donne.

CAVALIERE:

Che impertinenza?

CONTE:

Siete caduto?

CAVALIERE:

Andate al diavolo quanti siete.

(Getta il vaso in terra, e lo rompe verso il Conte ed il Marchese, e
parte furiosamente.)

CONTE:

Il Cavaliere è diventato pazzo.

(Parte.)

MARCHESE:

Di questo affronto voglio soddisfazione.

(Parte.)

MIRANDOLINA:

L\'impresa è fatta. Il di lui cuore è in fuoco, in fiamme, in cenere.
Restami solo, per compiere la mia vittoria, che si renda pubblico il mio
trionfo, a scorno degli uomini presuntuosi, e ad onore del nostro sesso.

(Parte.)

[]{#Section0011.xhtml}

ATTO TERZO
==========

SCENA PRIMA {#Section0011.xhtml#sigil_toc_id_81}
-----------

Camera di Mirandolina con tavolino e biancheria da stirare.\
Mirandolina, poi Fabrizio.

MIRANDOLINA:

Orsù, l\'ora del divertimento è passata. Voglio ora badare a\' fatti
miei. Prima che questa biancheria si prosciughi del tutto, voglio
stirarla. Ehi, Fabrizio.

FABRIZIO:

Signora.

MIRANDOLINA:

Fatemi un piacere. Portatemi il ferro caldo.

FABRIZIO:

Signora sì.

(Con serietà, in atto di partire.)

MIRANDOLINA:

Scusate, se do a voi questo disturbo.

FABRIZIO:

Niente, signora. Finché io mangio il vostro pane, sono obbligato a
servirvi.

(Vuol partire.)

MIRANDOLINA:

Fermatevi; sentite: non siete obbligato a servirmi in queste cose; ma so
che per me lo fate volentieri ed io\... basta, non dico altro.

FABRIZIO:

Per me vi porterei l\'acqua colle orecchie. Ma vedo che tutto è gettato
via.

MIRANDOLINA:

Perché gettato via? Sono forse un\'ingrata?

FABRIZIO:

Voi non degnate i poveri uomini. Vi piace troppo la nobiltà.

MIRANDOLINA:

Uh povero pazzo! Se vi potessi dir tutto! Via, via andatemi a pigliar il
ferro.

FABRIZIO:

Ma se ho veduto io con questi miei occhi\...

MIRANDOLINA:

Andiamo, meno ciarle. Portatemi il ferro.

FABRIZIO:

Vado, vado, vi servirò, ma per poco.

(Andando.)

MIRANDOLINA:

Con questi uomini, più che loro si vuol bene, si fa peggio.

(Mostrando parlar da sé, ma per esser sentita.)

FABRIZIO:

Che cosa avete detto?

(Con tenerezza, tornando indietro.)

MIRANDOLINA:

Via, mi portate questo ferro?

FABRIZIO:

Sì, ve lo porto. (Non so niente. Ora la mi tira su, ora la mi butta giù.
Non so niente).

(Da sé, parte.)

SCENA SECONDA {#Section0011.xhtml#sigil_toc_id_82}
-------------

Mirandolina, poi il Servitore del Cavaliere.

MIRANDOLINA:

Povero sciocco! Mi ha da servire a suo marcio dispetto. Mi par di ridere
a far che gli uomini facciano a modo mio. E quel caro signor Cavaliere,
ch\'era tanto nemico delle donne? Ora, se volessi, sarei padrona di
fargli fare qualunque bestialità.

SERVITORE:

Signora Mirandolina.

MIRANDOLINA:

Che c\'è, amico?

SERVITORE:

Il mio padrone la riverisce, e manda a vedere come sta!

MIRANDOLINA:

Ditegli che sto benissimo.

SERVITORE:

Dice così, che beva un poco di questo spirito di melissa, che le farà
assai bene.

(Le dà una boccetta d\'oro.)

MIRANDOLINA:

È d\'oro questa boccetta?

SERVITORE:

Sì signora, d\'oro, lo so di sicuro.

MIRANDOLINA:

Perché non mi ha dato lo spirito di melissa, quando mi è venuto
quell\'orribile svenimento?

SERVITORE:

Allora questa boccetta egli non l\'aveva.

MIRANDOLINA:

Ed ora come l\'ha avuta?

SERVITORE:

Sentite. In confidenza. Mi ha mandato ora a chiamar un orefice, l\'ha
comprata, e l\'ha pagata dodici zecchini; e poi mi ha mandato dallo
speziale e comprar lo spirito.

MIRANDOLINA:

Ah, ah,ah.

(Ride.)

SERVITORE:

Ridete?

MIRANDOLINA:

Rido, perché mi manda il medicamento, dopo che son guarita del male.

SERVITORE:

Sarà buono per un\'altra volta.

MIRANDOLINA:

Via, ne beverò un poco per preservativo.

(Beve.)

Tenete, ringraziatelo.

(Gli vuol dar la boccetta.)

SERVITORE:

Oh! la boccetta è vostra.

MIRANDOLINA:

Come mia?

SERVITORE:

Sì. Il padrone l\'ha comprata a posta.

MIRANDOLINA:

A posta per me?

SERVITORE:

Per voi; ma zitto.

MIRANDOLINA:

Portategli la sua boccetta, e ditegli che lo ringrazio.

SERVITORE:

Eh via.

MIRANDOLINA:

Vi dico che gliela portiate, che non la voglio.

SERVITORE:

Gli volete fare quest\'affronto?

MIRANDOLINA:

Meno ciarle. Fate il vostro dovere. Tenete.

SERVITORE:

Non occorr\'altro. Gliela porterò. (Oh che donna! Ricusa dodici
zecchini! Una simile non l\'ho più ritrovata, e durerò fatica a
trovarla).

(Da sé, parte.)

SCENA TERZA {#Section0011.xhtml#sigil_toc_id_83}
-----------

Mirandolina, poi Fabrizio.

MIRANDOLINA:

Uh, è cotto, stracotto e biscottato! Ma siccome quel che ho fatto con
lui, non l\'ho fatto per interesse, voglio ch\'ei confessi la forza
delle donne, senza poter dire che sono interessate e venali.

FABRIZIO:

Ecco qui il ferro.

(Sostenuto, col ferro da stirare in mano.)

MIRANDOLINA:

È ben caldo?

FABRIZIO:

Signora sì, è caldo; così foss\'io abbruciato.

MIRANDOLINA:

Che cosa vi è di nuovo?

FABRIZIO:

Questo signor Cavaliere manda le ambasciate, manda i regali. Il
Servitore me l\'ha detto.

MIRANDOLINA:

Signor sì, mi ha mandato una boccettina d\'oro, ed io gliel\'ho
rimandata indietro.

FABRIZIO:

Gliel\'avete rimandata indietro?

MIRANDOLINA:

Sì, domandatelo al Servitore medesimo.

FABRIZIO:

Perché gliel\'avete rimandata indietro?

MIRANDOLINA:

Perché\... Fabrizio\... non dica\... Orsù, non parliamo altro.

FABRIZIO:

Cara Mirandolina, compatitemi.

MIRANDOLINA:

Via, andate, lasciatemi stirare.

FABRIZIO:

Io non v\'impedisco di fare\...

MIRANDOLINA:

Andatemi a preparare un altro ferro, e quando è caldo, portatelo.

FABRIZIO:

Sì, vado. Credetemi, che se parlo\...

MIRANDOLINA:

Non dite altro. Mi fate venire la rabbia.

FABRIZIO:

Sto cheto. (Ell\'è una testolina bizzarra, ma le voglio bene).

(Da sé, parte.)

MIRANDOLINA:

Anche questa è buona. Mi faccio merito con Fabrizio d\'aver ricusata la
boccetta d\'oro del Cavaliere. Questo vuol dir saper vivere, saper fare,
saper profittare di tutto, con buona grazia, con pulizia, con un poco di
disinvoltura. In materia d\'accortezza, non voglio che si dica ch\'io
faccia torto al sesso.

(Va stirando.)

SCENA QUARTA {#Section0011.xhtml#sigil_toc_id_84}
------------

Il Cavaliere e detta.

CAVALIERE:

(Eccola. Non ci volevo venire, e il diavolo mi ci ha strascinato!.

(Da sé, indietro.)

MIRANDOLINA:

(Eccolo, eccolo).

(Lo vede colla coda dell\'occhio, e stira.)

CAVALIERE:

Mirandolina?

MIRANDOLINA:

Oh signor Cavaliere! Serva umilissima.

(Stirando.)

CAVALIERE:

Come state?

MIRANDOLINA:

Benissimo, per servirla.

(Stirando senza guardarlo.)

CAVALIERE:

Ho motivo di dolermi di voi.

MIRANDOLINA:

Perché, signore?

(Guardandolo un poco.)

CAVALIERE:

Perché avete ricusato una piccola boccettina, che vi ho mandato.

MIRANDOLINA:

Che voleva ch\'io ne facessi?

(Stirando.)

CAVALIERE:

Servirvene nelle occorrenze.

MIRANDOLINA:

Per grazia del cielo, non sono soggetta agli svenimenti. Mi è accaduto
oggi quello che mi è accaduto mai più.

(Stirando.)

CAVALIERE:

Cara mirandolina\... non vorrei esser io stato cagione di quel funesto
accidente.

MIRANDOLINA:

Eh sì, ho timore che ella appunto ne sia stata la causa.

(Stirando.)

CAVALIERE:

Io? Davvero?

(Con passione.)

MIRANDOLINA:

Mi ha fatto bere quel maledetto vino di Borgogna, e mi ha fatto male.

(Stirando con rabbia.)

CAVALIERE:

Come? Possibile?

(Rimane mortificato.)

MIRANDOLINA:

È così senz\'altro. In camera sua non ci vengo mai più.

(Stirando.)

CAVALIERE:

V\'intendo. In camera mia non ci verrete più? Capisco il mistero. Sì, lo
capisco. Ma veniteci, cara, che vi chiamerete contenta.

(Amoroso.)

MIRANDOLINA:

Questo ferro è poco caldo. Ehi; Fabrizio? se l\'altro ferro è caldo,
portatelo.

(Forte verso la scena.)

CAVALIERE:

Fatemi questa grazia, tenete questa boccetta.

MIRANDOLINA:

In verità, signor Cavaliere, dei regali io non ne prendo.

(Con disprezzo, stirando.)

CAVALIERE:

Li avete pur presi dal Conte d\'Albafiorita.

MIRANDOLINA:

Per forza. Per non disgustarlo.

(Stirando.)

CAVALIERE:

E vorreste fare a me questo torto? e disgustarmi?

MIRANDOLINA:

Che importa a lei, che una donna la disgusti? Già le donne non le può
vedere.

CAVALIERE:

Ah, Mirandolina! ora non posso dire così.

MIRANDOLINA:

Signor Cavaliere, a che ora fa la luna nuova?

CAVALIERE:

Il mio cambiamento non è lunatico. Questo è un prodigio della vostra
bellezza, della vostra grazia.

MIRANDOLINA:

Ah, ah, ah.

(Ride forte, e stira.)

CAVALIERE:

Ridete?

MIRANDOLINA:

Non vuol che rida? Mi burla, e non vuol ch\'io rida?

CAVALIERE:

Eh furbetta! Vi burlo eh? Via, prendete questa boccetta.

MIRANDOLINA:

Grazie, grazie.

(Stirando.)

CAVALIERE:

Prendetela, o mi farete andare in collera.

MIRANDOLINA:

Fabrizio, il ferro.

(Chiamando forte, con caricatura.)

CAVALIERE:

La prendete, o non la prendete?

(Alterato.)

MIRANDOLINA:

Furia, furia.

(Prende la boccetta, e con disprezzo la getta nel paniere della
biancheria.)

CAVALIERE:

La gettate così?

MIRANDOLINA:

Fabrizio!

(Chiama forte, come sopra.)

SCENA QUINTA {#Section0011.xhtml#sigil_toc_id_85}
------------

Fabrizio col ferro, e detti.

FABRIZIO:

Son qua.

(Vedendo il Cavaliere, s\'ingelosisce.)

MIRANDOLINA:

È caldo bene?

(Prende il ferro.)

FABRIZIO:

Signora sì.

(Sostenuto.)

MIRANDOLINA:

Che avete, che mi parete turbato?

(A Fabrizio, con tenerezza.)

FABRIZIO:

Niente, padrona, niente.

MIRANDOLINA:

Avete male?

(Come sopra.)

FABRIZIO:

Datemi l\'altro ferro, se volete che lo metta nel fuoco.

MIRANDOLINA:

In verità, ho paura che abbiate male.

(Come sopra.)

CAVALIERE:

Via, dategli il ferro, e che se ne vada.

MIRANDOLINA:

Gli voglio bene, sa ella? È il mio cameriere fidato.

(Al Cavaliere.)

CAVALIERE:

(Non posso più).

(Da sé, smaniando.)

MIRANDOLINA:

Tenete, caro, scaldatelo.

(Dà il ferro a Fabrizio.)

FABRIZIO:

Signora padrona\...

(Con tenerezza.)

MIRANDOLINA:

Via, via, presto.

(Lo scaccia.)

FABRIZIO:

(Che vivere è questo? Sento che non posso più).

(Da sé, parte.)

SCENA SESTA {#Section0011.xhtml#sigil_toc_id_86}
-----------

Il Cavaliere e Mirandolina.

CAVALIERE:

Gran finezze, signora, al suo cameriere!

MIRANDOLINA:

E per questo, che cosa vorrebbe dire?

CAVALIERE:

Si vede che ne siete invaghita.

MIRANDOLINA:

Io innamorata di un cameriere? Mi fa un bel complimento, signore; non
sono di sì cattivo gusto io. Quando volessi amare, non getterei il mio
tempo sì malamente.

(Stirando.)

CAVALIERE:

Voi meritereste l\'amore di un re.

MIRANDOLINA:

Del re di spade, o del re di coppe?

(Stirando.)

CAVALIERE:

Parliamo sul serio, Mirandolina, e lasciamo gli scherzi.

MIRANDOLINA:

Parli pure, che io l\'ascolto.

(Stirando.)

CAVALIERE:

Non potreste per un poco lasciar di stirare?

MIRANDOLINA:

Oh perdoni! Mi preme allestire questa biancheria per domani.

CAVALIERE:

Vi preme dunque quella biancheria più di me?

MIRANDOLINA:

Sicuro.

(Stirando.)

CAVALIERE:

E ancora lo confermate?

MIRANDOLINA:

Certo. Perché di questa biancheria me ne ho da servire, e di lei non
posso far capitale di niente.

(Stirando.)

CAVALIERE:

Anzi potete dispor di me con autorità.

MIRANDOLINA:

Eh, che ella non può vedere le donne.

CAVALIERE:

Non mi tormentate più. Vi siete vendicata abbastanza. Stimo voi, stimo
le donne che sono della vostra sorte, se pur ve ne sono. Vi stimo, vi
amo, e vi domando pietà.

MIRANDOLINA:

Sì signore, glielo diremo.

(Stirando in fretta, si fa cadere un manicotto.)

CAVALIERE:

(leva di terra il manicotto, e glielo dà)

Credetemi\...

MIRANDOLINA:

Non s\'incomodi.

CAVALIERE:

Voi meritate di esser servita.

MIRANDOLINA:

Ah, ah, ah.

(Ride forte.)

CAVALIERE:

Ridete?

MIRANDOLINA:

Rido, perché mi burla.

CAVALIERE:

Mirandolina, non posso più.

MIRANDOLINA:

Le vien male?

CAVALIERE:

Sì, mi sento mancare.

MIRANDOLINA:

Tenga il suo spirito di melissa.

(Gli getta con disprezzo la boccetta.)

CAVALIERE:

Non mi trattate con tanta asprezza. Credetemi, vi amo, ve lo giuro.

(Vuol prenderle la mano, ed ella col ferro lo scotta.)

Aimè!

MIRANDOLINA:

Perdoni: non l\'ho fatto apposta.

CAVALIERE:

Pazienza! Questo è niente. Mi avete fatto una scottatura più grande.

MIRANDOLINA:

Dove, signore?

CAVALIERE:

Nel cuore.

MIRANDOLINA:

Fabrizio.

(Chiama ridendo.)

CAVALIERE:

Per carità, non chiamate colui.

MIRANDOLINA:

Ma se ho bisogno dell\'altro ferro.

CAVALIERE:

Aspettate\... (ma no\...) chiamerò il mio servitore.

MIRANDOLINA:

Eh! Fabrizio\...

(Vuol chiamare Fabrizio.)

CAVALIERE:

Giuro al cielo, se viene colui, gli spacco la testa.

MIRANDOLINA:

Oh, questa è bella! Non mi potrò servire della mia gente?

CAVALIERE:

Chiamate un altro; colui non lo posso vedere.

MIRANDOLINA:

Mi pare ch\'ella si avanzi un poco troppo, signor Cavaliere.

(Si scosta dal tavolino col ferro in mano.)

CAVALIERE:

Compatitemi\... son fuori di me.

MIRANDOLINA:

Anderò io in cucina, e sarà contento.

CAVALIERE:

No, cara, fermatevi.

MIRANDOLINA:

È una cosa curiosa questa.

(Passeggiando.)

CAVALIERE:

Compatitemi.

(Le va dietro.)

MIRANDOLINA:

Non posso chiamar chi voglio?

(Passeggia.)

CAVALIERE:

Lo confesso. Ho gelosia di colui.

(Le va dietro.)

MIRANDOLINA:

(Mi vien dietro come un cagnolino).

(Da sé, passeggiando.)

CAVALIERE:

Questa è la prima volta ch\'io provo che cosa sia amore.

MIRANDOLINA:

Nessuno mi ha mai comandato.

(Camminando.)

CAVALIERE:

Non intendo di comandarvi: vi prego.

(La segue.)

MIRANDOLINA:

Ma che cosa vuole da me?

(Voltandosi con alterezza.)

CAVALIERE:

Amore, compassione, pietà.

MIRANDOLINA:

Un uomo che stamattina non poteva vedere le donne, oggi chiede amore e
pietà? Non gli abbado, non può essere, non gli credo. (Crepa, schiatta,
impara a disprezzar le donne).

(Da sé, parte.)

SCENA SETTIMA {#Section0011.xhtml#sigil_toc_id_87}
-------------

CAVALIERE:

(solo)

Oh maledetto il punto, in cui ho principiato a mirar costei! Son caduto
nel laccio, e non vi è più rimedio.

SCENA OTTAVA {#Section0011.xhtml#sigil_toc_id_88}
------------

Il Marchese e detto.

MARCHESE:

Cavaliere, voi mi avete insultato.

CAVALIERE:

Compatitemi, fu un accidente.

MARCHESE:

Mi meraviglio di voi.

CAVALIERE:

Finalmente il vaso non vi ha colpito.

MARCHESE:

Una gocciola d\'acqua mi ha macchiato il vestito.

CAVALIERE:

Torno a dir, compatitemi.

MARCHESE:

Questa è una impertinenza.

CAVALIERE:

Non l\'ho fatto apposta. Compatitemi per la terza volta.

MARCHESE:

Voglio soddisfazione.

CAVALIERE:

Se non volete compatirmi, se volete soddisfazione, son qui, non ho
soggezione di voi.

MARCHESE:

Ho paura che questa macchia non voglia andar via; questo è quello che mi
fa andare in collera.

(Cangiandosi.)

CAVALIERE:

Quando un cavalier vi chiede scusa, che pretendete di più?

(Con isdegno.)

MARCHESE:

Se non l\'avete fatto a malizia, lasciamo stare.

CAVALIERE:

Vi dico, che son capace di darvi qualunque soddisfazione.

MARCHESE:

Via, non parliamo altro.

CAVALIERE:

Cavaliere malnato.

MARCHESE:

Oh questa è bella! A me è passata la collera, e voi ve la fate venire.

CAVALIERE:

Ora per l\'appunto mi avete trovato in buona luna.

MARCHESE:

Vi compatisco, so che male avete.

CAVALIERE:

I fatti vostri io non li ricerco.

MARCHESE:

Signor inimico delle donne, ci siete caduto eh?

CAVALIERE:

Io? Come?

MARCHESE:

Sì, siete innamorato\...

CAVALIERE:

Sono il diavolo che vi porti.

MARCHESE:

Che serve nascondersi?\...

CAVALIERE:

Lasciatemi stare, che giuro al cielo ve ne farò pentire.

(Parte.)

SCENA NONA {#Section0011.xhtml#sigil_toc_id_89}
----------

MARCHESE:

(solo)

È innamorato, si vergogna, e non vorrebbe che si sapesse. Ma forse non
vorrà che si sappia, perché ha paura di me; avrà soggezione a
dichiararsi per mio rivale. Mi dispiace assaissimo di questa macchia; se
sapessi come fare a levarla! Queste donne sogliono avere della terra da
levar le macchie.

(Osserva nel tavolino e nel paniere.)

Bella questa boccetta! Che sia d\'oro o di princisbech? Eh, sarà di
princisbech: se fosse d\'oro, non la lascerebbero qui; se vi fosse
dell\'acqua della regina, sarebbe buona per levar questa macchia.

(Apre, odora e gusta.)

È spirito di melissa. Tant\'è tanto sarà buono. Voglio provare.

SCENA DECIMA {#Section0011.xhtml#sigil_toc_id_90}
------------

Dejanira e detto.

DEJANIRA:

Signor Marchese, che fa qui solo? Non favorisce mai?

MARCHESE:

Oh signora Contessa. Veniva or ora per riverirla.

DEJANIRA:

Che cosa stava facendo?

MARCHESE:

Vi dirò. Io sono amantissimo della pulizia. Voleva levare questa piccola
macchia.

DEJANIRA:

Con che, signore?

MARCHESE:

Con questo spirito di melissa.

DEJANIRA:

Oh perdoni, lo spirito di melissa non serve, anzi farebbe venire la
macchia più grande.

MARCHESE:

Dunque, come ho da fare?

DEJANIRA:

Ho io un segreto per cavar le macchie.

MARCHESE:

Mi farete piacere a insegnarmelo.

DEJANIRA:

Volentieri. M\'impegno con uno scudo far andar via quella macchia, che
non si vedrà nemmeno dove sia stata.

MARCHESE:

Vi vuole uno scudo?

DEJANIRA:

Sì, signore, vi pare una grande spesa?

MARCHESE:

È meglio provare lo spirito di Melissa.

DEJANIRA:

Favorisca: è buono quello spirito?

MARCHESE:

Prezioso, sentite.

(Le dà la boccetta.)

DEJANIRA:

Oh, io ne so fare del meglio.

(Assaggiandolo.)

MARCHESE:

Sapete fare degli spiriti?

DEJANIRA:

Sì, signore mi diletto di tutto.

MARCHESE:

Brava, damina, brava. Così mi piace.

DEJANIRA:

Sarà d\'oro questa boccetta?

MARCHESE:

Non volete? È oro sicuro. (Non conosce l\'oro del princisbech).

(Da sé.)

DEJANIRA:

È sua, signor Marchese?

MARCHESE:

È mia, e vostra se comandate.

DEJANIRA:

Obbligatissima alle sue grazie.

(La mette via.)

MARCHESE:

Eh! so che scherzate.

DEJANIRA:

Come? Non me l\'ha esibita?

MARCHESE:

Non è cosa da vostra pari. È una bagattella. Vi servirò di cosa
migliore, se ne avete voglia.

DEJANIRA:

Oh, mi meraviglio. È anche troppo. La ringrazio, signor Marchese.

MARCHESE:

Sentite. In confidenza. Non è oro. È princisbech.

DEJANIRA:

Tanto meglio. La stimo più che se fosse oro. E poi, quel che viene dalle
sue mani, è tutto prezioso.

MARCHESE:

Basta. Non so che dire; servitevi, se vi degnate. (Pazienza! Bisognerà
pagarla a Mirandolina. Che cosa può valere? Un filippo?).

(Da sé.)

DEJANIRA:

Il signor Marchese è un cavalier generoso.

MARCHESE:

Mi vergogno a regalar queste bagattelle. Vorrei che quella boccetta
fosse d\'oro.

DEJANIRA:

In verità, pare propriamente oro.

(La tira fuori, e la osserva.)

Ognuno s\'ingannerebbe.

MARCHESE:

È vero, chi non ha pratica dell\'oro, s\'inganna: ma io lo conosco
subito.

DEJANIRA:

Anche al peso par che sia oro.

MARCHESE:

E pur non è vero.

DEJANIRA:

Voglio farla vedere alla mia compagna.

MARCHESE:

Sentite, signora Contessa, non la fate vedere a Mirandolina. È una
ciarliera. Non so se mi capite.

DEJANIRA:

Intendo benissimo. La fo vedere solamente ad Ortensia.

MARCHESE:

Alla Baronessa?

DEJANIRA:

Sì, sì, alla Baronessa.

(Ridendo parte.)

SCENA UNDICESIMA {#Section0011.xhtml#sigil_toc_id_91}
----------------

Il Marchese, poi il Servitore del Cavaliere.

MARCHESE:

Credo che se ne rida, perché mi ha levato con quel bel garbo la
boccettina. Tant\'era se fosse stata d\'oro. Manco male, che con poco
l\'aggiusterò. Se Mirandolina vorrà la sua boccetta, gliela pagherò,
quando ne avrò.

SERVITORE:

(cerca sul tavolo)

Dove diamine sarà questa boccetta?

MARCHESE:

Che cosa cercate, galantuomo?

SERVITORE:

Cerco una boccetta di spirito di melissa. La signora Mirandolina la
vorrebbe. Dice che l\'ha lasciata qui, ma non la ritrovo.

MARCHESE:

Era una boccettina di princisbech?

SERVITORE:

No signore, era d\'oro.

MARCHESE:

D\'oro?

SERVITORE:

Certo che era d\'oro. L\'ho veduta comprar io per dodici zecchini.

(Cerca.)

MARCHESE:

(Oh povero me!).

(Da sé.)

Ma come lasciar così una boccetta d\'oro?

SERVITORE:

Se l\'è scordata, ma io non la trovo.

MARCHESE:

Mi pare ancora impossibile che fosse d\'oro.

SERVITORE:

Era oro, gli dico. L\'ha forse veduta V.E.?

MARCHESE:

Io?\... Non ho veduto niente.

SERVITORE:

Basta. Le dirò che non la trovo. Suo danno. Doveva mettersela in tasca.

(Parte.)

SCENA DODICESIMA {#Section0011.xhtml#sigil_toc_id_92}
----------------

Il Marchese, poi il Conte.

MARCHESE:

Oh povero Marchese di Forlipopoli! Ho donata una boccetta d\'oro, che
val dodici zecchini, e l\'ho donata per princisbech. Come ho da
regolarmi in un caso di tanta importanza? Se recupero la boccetta dalla
Contessa, mi fo ridicolo presso di lei; se Mirandolina viene a scoprire
ch\'io l\'abbia avuta, è in pericolo il mio decoro. Son cavaliere. Devo
pagarla. Ma non ho danari.

CONTE:

Che dite, signor Marchese, della bellissima novità?

MARCHESE:

Di quale novità?

CONTE:

Il Cavaliere Selvatico, il disprezzator delle donne, è innamorato di
Mirandolina.

MARCHESE:

L\'ho caro. Conosca suo malgrado il merito di questa donna; veda che io
non m\'invaghisco di chi non merita; e peni e crepi per gastigo della
sua impertinenza.

CONTE:

Ma se Mirandolina gli corrisponde?

MARCHESE:

Ciò non può essere. Ella non farà a me questo torto. Sa chi sono. Sa
cosa ho fatto per lei.

CONTE:

Io ho fatto per essa assai più di voi. Ma tutto è gettato. Mirandolina
coltiva il Cavaliere di Ripafratta, ha usato verso di lui quelle
attenzioni che non ha praticato né a voi, né a me; e vedesi che, colle
donne, più che si sa, meno si merita, e che burlandosi esse di che le
adora, corrono dietro a chi le disprezza.

MARCHESE:

Se ciò fosse vero\... ma non può essere.

CONTE:

Perché non può essere?

MARCHESE:

Vorreste mettere il Cavaliere a confronto di me?

CONTE:

Non l\'avete veduta voi stesso sedere alla di lui tavola? Con noi ha
praticato mai un atto di simile confidenza? A lui biancheria distinta.
Servito in tavola prima di tutti. Le pietanze gliele fa ella colle sue
mani. I servidori vedono tutto, e parlano. Fabrizio freme di gelosia. E
poi quello svenimento, vero o finto che fosse, non è segno manifesto
d\'amore?

MARCHESE:

Come! A lui si fanno gl\'intingoli saporiti, e a me carnaccia di bue, e
minestra di riso lungo? Sì, è vero, questo è uno strapazzo al mio grado,
alla mia condizione.

CONTE:

Ed io che ho speso tanto per lei?

MARCHESE:

Ed io che la regalava continuamente? Le ho fino dato da bere di quel
vino di Cipro così prezioso. Il Cavaliere non avrà fatto con costei una
minima parte di quello che abbiamo fatto noi.

CONTE:

Non dubitate, che anch\'egli l\'ha regalata.

MARCHESE:

Sì? Che cosa le ha donato?

CONTE:

Una boccettina d\'oro con dello spirito di melissa.

MARCHESE:

(Oimè!)

(Da sé.)

Come lo avete saputo?

CONTE:

Il di lui servidore l\'ha detto al mio.

MARCHESE:

(Sempre peggio. Entro in un impegno col Cavaliere).

(Da sé.)

CONTE:

Vedo che costei è un\'ingrata; voglio assolutamente lasciarla. Voglio
partire or ora da questa locanda indegna.

MARCHESE:

Sì, fate bene, andate.

CONTE:

E voi che siete un cavaliere di tanta riputazione, dovreste partire con
me.

MARCHESE:

Ma\... dove dovrei andare?

CONTE:

Vi troverò io un alloggio. Lasciate pensare a me.

MARCHESE:

Quest\'alloggio\... sarà per esempio\...

CONTE:

Andremo in casa d\'un mio paesano. Non ispenderemo nulla.

MARCHESE:

Basta, siete tanto mio amico, che non posso dirvi di no.

CONTE:

Andiamo, e vendichiamoci di questa femmina sconoscente.

MARCHESE:

Sì, andiamo. (Ma come sarà poi della boccetta? Son cavaliere, non posso
fare una malazione).

(Da sé.)

CONTE:

Non vi pentite, signor Marchese, andiamo via di qui. Fatemi questo
piacere, e poi comandatemi dove posso, che vi servirò.

MARCHESE:

Vi dirò. In confidenza, ma che nessuno lo sappia. Il mio fattore mi
ritarda qualche volta le mie rimesse\...

CONTE:

Le avete forse da dar qualche cosa?

MARCHESE:

Sì, dodici zecchini.

CONTE:

Dodici zecchini? Bisogna che sia dei mesi, che non pagate.

MARCHESE:

Così è, le devo dodici zecchini. Non posso di qua partire senza pagarla.
Se voi mi faceste il piacere\...

CONTE:

Volentieri. Eccovi dodici zecchini.

(Tira fuori la borsa.)

MARCHESE:

Aspettate. Ora che mi ricordo, sono tredici. (Voglio rendere il suo
zecchino anche al Cavaliere).

(Da sé.)

CONTE:

Dodici o tredici è lo stesso per me. Tenete.

MARCHESE:

Ve li renderò quanto prima.

CONTE:

Servitevi quanto vi piace. Danari a me non ne mancano; e per vendicarmi
di costei, spenderei mille doppie.

MARCHESE:

Sì, veramente è un\'ingrata. Ho speso tanto per lei, e mi tratta così.

CONTE:

Voglio rovinare la sua locanda. Ho fatto andar via anche quelle due
commedianti.

MARCHESE:

Dove sono le commedianti?

CONTE:

Erano qui: Ortensia e Dejanira.

MARCHESE:

Come! Non sono dame?

CONTE:

No. Sono due comiche. Sono arrivati i loro comnpagni, e la favola è
terminata.

MARCHESE:

(La mia boccetta!).

(Da sé.)

Dove sono alloggiate?

CONTE:

In una casa vicino al teatro.

MARCHESE:

(Vado subito a ricuperare la mia boccetta).

(Da se, parte.)

CONTE:

Con costei mi voglio vendicar così. Il Cavaliere poi, che ha saputo
fingere per tradirmi, in altra maniera me ne renderà conto.

(Parte.)

SCENA TREDICESIMA {#Section0011.xhtml#sigil_toc_id_93}
-----------------

Camera con tre porte.

MIRANDOLINA:

(sola)

Oh meschina me! Sono nel brutto impegno! Se il Cavaliere mi arriva, sto
fresca. Si è indiavolato maledettamente. Non vorrei che il diavolo lo
tentasse di venir qui. Voglio chiudere questa porta.

(Serra la porta da dove è venuta.)

Ora principio quasi a pentirmi di quel che ho fatto. È vero che mi sono
assai divertita nel farmi correr dietro a tal segno un superbo, un
disprezzator delle donne; ma ora che il satiro è sulle furie, vedo in
pericolo la mia riputazione e la mia vita medesima. Qui mi convien
risolvere quelche cosa di grande. Son sola, non ho nessuno dal cuore che
mi difenda. Non ci sarebbe altri che quel buon uomo di Fabrizio, che in
tal caso mi potesse giovare. Gli prometterò di sposarlo\... Ma\...
prometti, prometti, si stancherà di credermi\... Sarebbe quasi meglio
ch\'io lo sposassi davvero. Finalmente con un tal matrimonio posso
sperar di mettere al coperto il mio interesse e la mia reputazione,
senza pregiudicare alla mia libertà.

SCENA QUATTORDICESIMA {#Section0011.xhtml#sigil_toc_id_94}
---------------------

Il Cavaliere di dentro, e detta; poi Fabrizio.\
Il Cavaliere batte per di dentro alla porta.

MIRANDOLINA:

Battono a questa porta: chi sarà mai?

(S\'accosta.)

CAVALIERE:

Mirandolina.

(Di dentro.)

MIRANDOLINA:

(L\'amico è qui).

(Da sé.)

CAVALIERE:

Mirandolina, apritemi.

(Come sopra.)

MIRANDOLINA:

(Aprirgli? Non sono sì gonza). Che comanda, signor Cavaliere?

CAVALIERE:

Apritemi.

(Di dentro.)

MIRANDOLINA:

Favorisca andare nella sua camera, e mi aspetti, che or ora son da lei.

CAVALIERE:

Perché non volete aprirmi?

(Come sopra.)

MIRANDOLINA:

Arrivano de\' forestieri. Mi faccia questa grazia, vada, che or ora sono
da lei.

CAVALIERE:

Vado: se non venite, povera voi.

(Parte.)

MIRANDOLINA:

Se non venite, povera voi! Povera me, se vi andassi. La cosa va sempre
peggio. Rimediamoci, se si può. È andato via?

(Guarda al buco della chiave.)

Sì, sì, è andato. Mi aspetta in camera, ma non vi vado. Ehi? Fabrizio.

(Ad un\'altra porta.)

Sarebbe bella che ora Fabrizio si vendicasse di me, e non volesse\...
Oh, non vi è pericolo. Ho io certe manierine, certe smorfiette, che
bisogna che caschino, se fossero di macigno. Fabrizio.

(Chiama ad un\'altra porta.)

FABRIZIO:

Avete chiamato?

MIRANDOLINA:

Venite qui; voglio farvi una confidenza.

FABRIZIO:

Son qui.

MIRANDOLINA:

Sappiate che il Cavaliere di Ripafratta si è scoperto innamorato di me.

FABRIZIO:

Eh, me ne sono accorto.

MIRANDOLINA:

Sì? Ve ne siete accorto? Io in verità non me ne sono mai avveduta.

FABRIZIO:

Povera semplice! Non ve ne siete accorta! Non avete veduto, quando
stiravate col ferro, le smorfie che vi faceva? La gelosia che aveva di
me?

MIRANDOLINA:

Io che opero senza malizia, prendo le cose con indifferenza. Basta; ora
mi ha dette certe parole, che in verità, Fabrizio, mi hanno fatto
arrossire.

FABRIZIO:

Vedete: questo vuol dire perché siete una giovane sola, senza padre,
senza madre, senza nessuno. Se foste maritata, non andrebbe così.

MIRANDOLINA:

Orsù, capisco che dite bene; ho pensato di maritarmi.

FABRIZIO:

Ricordatevi di vostro padre.

MIRANDOLINA:

Sì, me ne ricordo.

SCENA QUINDICESIMA {#Section0011.xhtml#sigil_toc_id_95}
------------------

Il Cavaliere di dentro e detti.\
Il Cavaliere batte alla porta dove era prima.

MIRANDOLINA:

Picchiano.

(A Fabrizio.)

FABRIZIO:

Chi è che picchia?

(Forte verso la porta.)

CAVALIERE:

Apritemi.

(Di dentro.)

MIRANDOLINA:

Il Cavaliere.

(A Fabrizio.)

FABRIZIO:

Che cosa vuole?

(S\'accosta per aprirgli.)

MIRANDOLINA:

Aspettate ch\'io parta.

FABRIZIO:

Di che avete timore?

MIRANDOLINA:

Caro Fabrizio, non so, ho paura della mia onestà.

(Parte.)

FABRIZIO:

Non dubitate, io vi difenderò.

CAVALIERE:

Apritemi, giuro al cielo.

(Di dentro.)

FABRIZIO:

Che comanda, signore? Che strepiti sono questi? In una locanda onorata
non si fa così.

CAVALIERE:

Apri questa porta.

(Si sente che la sforza.)

FABRIZIO:

Cospetto del diavolo! Non vorrei precipitare. Uomini, chi è di là? Non
ci è nessuno?

SCENA SEDICESIMA {#Section0011.xhtml#sigil_toc_id_96}
----------------

Il Marchese ed il Conte dalla porta di mezzo, e detti.

CONTE:

Che c\'è?

(Sulla porta.)

MARCHESE:

Che rumore è questo?

(Sulla porta.)

FABRIZIO:

Signori, li prego: il signor Cavaliere di Ripafratta vuole sforzare
quella porta.

(Piano, che il Cavaliere non senta.)

CAVALIERE:

Aprimi, o la getto abbasso.

(Di dentro.)

MARCHESE:

Che sia diventato pazzo? Andiamo via.

(Al Conte.)

CONTE:

Apritegli.

(A Fabrizio.)

Ho volontà per appunto di parlar con lui.

FABRIZIO:

Aprirò; ma le supplico\...

CONTE:

Non dubitate. Siamo qui noi.

MARCHESE:

(Se vedo niente niente, me la colgo).

(Da sé.)\
(Fabrizio apre, ed entra il Cavaliere.)

CAVALIERE:

Giuro al cielo, dov\'è?

FABRIZIO:

Chi cercate, signore?

CAVALIERE:

Mirandolina dov\'è?

FABRIZIO:

Io non lo so.

MARCHESE:

(L\'ha con Mirandolina. Non è niente).

(Da sé.)

CAVALIERE:

Scellerata, la troverò.

(S\'incammina, e scopre il Conte e il Marchese.)

CONTE:

Con chi l\'avete?

(Al Cavaliere.)

MARCHESE:

Cavaliere, noi siamo amici.

CAVALIERE:

(Oimè! Non vorrei per tutto l\'oro del mondo che nota fosse questa mia
debolezza).

(Da sé.)

FABRIZIO:

Che cosa vuole, signore, dalla padrona?

CAVALIERE:

A te non devo rendere questi conti. Quando comando, voglio esser
servito. Pago i miei denari per questo, e giuro al cielo, ella avrà che
fare con me.

FABRIZIO:

V.S. paga i suoi denari per essere servito nelle cose lecite e oneste:
ma non ha poi da pretendere, la mi perdoni, che una donna onorata\...

CAVALIERE:

Che dici tu? Che sai tu? Tu non entri ne\' fatti miei. So io quel che ho
ordinato a colei.

FABRIZIO:

Le ha ordinato di venire nella sua camera.

CAVALIERE:

Va via, briccone, che ti rompo il cranio.

FABRIZIO:

Mi meraviglio di lei.

MARCHESE:

Zitto.

(A Fabrizio.)

CONTE:

Andate via.

(A Fabrizio.)

CAVALIERE:

Vattene via di qui.

(A Fabrizio.)

FABRIZIO:

Dico, signore\...

(Riscaldandosi.)

MARCHESE:

Via.

CONTE:

Via.

(Lo cacciano via.)

FABRIZIO:

(Corpo di bacco! Ho proprio voglia di precipitare).

(Da sé, parte.)

SCENA DICIASSETTESIMA {#Section0011.xhtml#sigil_toc_id_97}
---------------------

Il Cavaliere, il Marchese ed il Conte.

CAVALIERE:

(Indegna! Farmi aspettar nella camera?).

(Da sé.)

MARCHESE:

(Che diamine ha?).

(Piano al Conte.)

CONTE:

(Non lo vedete? È innamorato di Mirandolina).

CAVALIERE:

(E si trattiene con Fabrizio? E parla seco di matrimonio?).

(Da sé.)

CONTE:

(Ora è il tempo di vendicarmi).

(Da sé.)

Signor Cavaliere, non conviene ridersi delle debolezze altrui, quando si
ha un cuore fragile come il vostro.

CAVALIERE:

Di che intendete voi di parlare?

CONTE:

So da che provengono le vostre smanie.

CAVALIERE:

Intendete voi di che parli?

(Alterato, al Marchese.)

MARCHESE:

Amico, io non so niente.

CONTE:

Parlo di voi, che col pretesto di non poter soffrire le donne, avete
tentato rapirmi il cuore di Mirandolina, ch\'era già mia conquista.

CAVALIERE:

Io?

(Alterato, verso il Marchese.)

MARCHESE:

Io non parlo.

CONTE:

Voltatevi a me, a me rispondete. Vi vergognate forse d\'aver mal
proceduto?

CAVALIERE:

Io mi vergogno d\'ascoltarvi più oltre, senza dirvi che voi mentite.

CONTE:

A me una mentita?

MARCHESE:

(La cosa va peggiorando).

(Da sé.)

CAVALIERE:

Con qual fondamento potete voi dire?\... (Il Conte non sa ciò che si
dica).

(Al Marchese, irato.)

MARCHESE:

Ma io non me ne voglio impiciare.

CONTE:

Voi siete un mentitore.

MARCHESE:

Vado via.

(Vuol partire.)

CAVALIERE:

Fermatevi.

(Lo trattiene per forza.)

CONTE:

E mi renderete conto\...

CAVALIERE:

Sì, vi renderò conto\... Datemi la vostra spada.

(Al Marchese.)

MARCHESE:

Eh via, acquietatevi tutti due. Caro Conte, cosa importa a voi che il
Cavaliere ami Mirandolina?\...

CAVALIERE:

Io l\'amo? Non è vero; mente chi lo dice.

MARCHESE:

Mente? La mentita non viene da me. Non sono io che lo dico.

CAVALIERE:

Chi dunque?

CONTE:

Io lo dico e lo sostengo, e non ho soggezione di voi.

CAVALIERE:

Datemi quella spada.

(Al Marchese.)

MARCHESE:

No, dico.

CAVALIERE:

Siete ancora voi mio nemico?

MARCHESE:

Io sono amico di tutti.

CONTE:

Azioni indegne son queste.

CAVALIERE:

Ah giuro al Cielo!

(Leva la spada al Marchese, la quale esce col fodero.)

MARCHESE:

Non mi perdete il rispetto.

(Al Cavaliere.)

CAVALIERE:

Se vi chiamate offeso, darò soddisfazione anche a voi.

(Al Marchese.)

MARCHESE:

Via; siete troppo caldo. (Mi dispiace\...)

(Da se, rammaricandosi.)

CONTE:

Io voglio soddisfazione.

(Si mette in guardia.)

CAVALIERE:

Ve la darò.

(Vuol levar il fodero, e non può.)

MARCHESE:

Quella spada non vi conosce\...

CAVALIERE:

Oh maledetta!

(Sforza per cavarlo.)

MARCHESE:

Cavaliere, non farete niente\...

CONTE:

Non ho più sofferenza.

CAVALIERE:

Eccola.

(Cava la spada, e vede essere mezza lama.)

Che è questo?

MARCHESE:

Mi avete rotta la spada.

CAVALIERE:

Il resto dov\'è? Nel fodero non v\'è niente.

MARCHESE:

Sì, è vero; l\'ho rotta nell\'ultimo duello; non me ne ricordavo.

CAVALIERE:

Lasciatemi provveder d\'una spada.

(Al Conte.)

CONTE:

Giuro al cielo, non mi fuggirete di mano.

CAVALIERE:

Che fuggire? Ho cuore di farvi fronte anche con questo pezzo di lama.

MARCHESE:

È lama di Spagna, non ha paura.

CONTE:

Non tanta bravura, signor gradasso.

CAVALIERE:

Sì, con questa lama.

(S\'avventa verso il Conte.)

CONTE:

Indietro.

(Si pone in difesa.)

SCENA DICIOTTESIMA {#Section0011.xhtml#sigil_toc_id_98}
------------------

Mirandolina, Fabrizio e detti.

FABRIZIO:

Alto, alto, padroni.

MIRANDOLINA:

Alto, signori miei, alto.

CAVALIERE:

(Ah maledetta!).

(Vedendo Mirandolina.)

MIRANDOLINA:

Povera me! Colle spade?

MARCHESE:

Vedete? Per causa vostra.

MIRANDOLINA:

Come per causa mia?

CONTE:

Eccolo lì il signor Cavaliere. È innamorato di voi.

CAVALIERE:

Io innamorato? Non è vero; mentite.

MIRANDOLINA:

Il signor Cavaliere innamorato di me? Oh no, signor Conte, ella
s\'inganna. Posso assicurarla, che certamente s\'inganna.

CONTE:

Eh, che siete voi pur d\'accordo\...

MIRANDOLINA:

Si, si vede\...

CAVALIERE:

Che si sa? Che si vede?

(Alterato, verso il Marchese.)

MARCHESE:

Dico, che quando è, si sa\... Quando non è, non si vede.

MIRANDOLINA:

Il signor cavaliere innamorato di me? Egli lo nega, e negandolo in
presenza mia, mi mortifica, mi avvilisce, e mi fa conoscere la sua
costanza e la mia debolezza. Confesso il vero, che se riuscito mi fosse
d\'innamorarlo, avrei creduto di fare la maggior prodezza del mondo. Un
uomo che non può vedere le donne, che le disprezza, che le ha in mal
concetto, non si può sperare d\'innamorarlo. Signori miei, io sono una
donna schietta e sincera: quando devo dir, dico, e non posso celare la
verità. Ho tentato d\'innamorare il signor Cavaliere, ma non ho fatto
niente.

(Al Cavaliere.)

CAVALIERE:

(Ah! Non posso parlare).

(Da sé.)

CONTE:

Lo vedete? Si confonde.

(A Mirandolina.)

MARCHESE:

Non ha coraggio di dir di no.

(A Mirandolina.)

CAVALIERE:

Voi non sapete quel che vi dite.

(Al Marchese, irato.)

MARCHESE:

E sempre l\'avete con me.

(Al Cavaliere, dolcemente.)

MIRANDOLINA:

Oh, il signor Cavaliere non s\'innamora. Conosce l\'arte. Sa la furberia
delle donne: alle parole non crede; delle lagrime non si fida. Degli
svenimenti poi se ne ride.

CAVALIERE:

Sono dunque finte le lagrime delle donne, sono mendaci gli svenimenti?

MIRANDOLINA:

Come! Non lo sa, o finge di non saperlo?

CAVALIERE:

Giuro al cielo! Una tal finzione meriterebbe uno stile nel cuore.

MIRANDOLINA:

Signor Cavaliere, non si riscaldi, perché questi signori diranno ch\'è
innamorato davvero.

CONTE:

Sì, lo è, non lo può nascondere.

MARCHESE:

Si vede negli occhi.

CAVALIERE:

No, non lo sono.

(Irato al Marchese.)

MARCHESE:

E sempre con me.

MIRANDOLINA:

No signore, non è innamorato. Lo dico, lo sostengo, e son pronta a
provarlo.

CAVALIERE:

(Non posso più).

(Da sé.)

Conte, ad altro tempo mi troverete provveduto di spada.

(Getta via la mezza spada del Marchese.)

MARCHESE:

Ehi! la guardia costa denari.

(La prende di terra.)

MIRANDOLINA:

Si fermi, signor Cavaliere, qui ci va della sua riputazione. Questi
signori credono ch\'ella sia innamorato; bisogna disingannarli.

CAVALIERE:

Non vi è questo bisogno.

MIRANDOLINA:

Oh sì, signore. Si trattenga un momento.

CAVALIERE:

(Che far intende costei?).

(Da sé.)

MIRANDOLINA:

Signori, il più certo segno d\'amore è quello della gelosia, e chi non
sente la gelosia, certamente non ama. Se il signor Cavaliere mi amasse,
non potrebbe soffrire ch\'io fossi d\'un altro, ma egli lo soffrirà, e
vedranno\...

CAVALIERE:

Di chi volete voi essere?

MIRANDOLINA:

Di quello a cui mi ha destinato mio padre.

FABRIZIO:

Parlate forse di me?

(A Mirandolina.)

MIRANDOLINA:

Sì, caro Fabrizio, a voi in presenza di questi cavalieri vo\' dar la
mano di sposa.

CAVALIERE:

(Oimè! Con colui? non ho cuor di soffrirlo).

(Da sé, smaniando.)

CONTE:

(Se sposa Fabrizio, non ama il Cavaliere).

(Da sé.)

Sì, sposatevi, e vi prometto trecento scudi.

MARCHESE:

Mirandolina, è meglio un uovo oggi, che una gallina domani. Sposatevi
ora, e vi do subito dodici zecchini.

MIRANDOLINA:

Grazie, signori, non ho bisogno di dote. Sono una povera donna senza
grazia, senza brio, incapace d\'innamorar persone di merito. Ma Fabrizio
mi vuol bene, ed io in questo punto alla presenza loro lo sposo\...

CAVALIERE:

Sì, maledetta, sposati a chi tu vuoi. So che tu m\'ingannasti, so che
trionfi dentro di te medesima d\'avermi avvilito, e vedo sin dove vuoi
cimentare la mia tolleranza. Meriteresti che io pagassi gli inganni tuoi
con un pugnale nel seno; meriteresti ch\'io ti strappassi il cuore, e lo
recassi in mostra alle femmine lusinghiere, alle femmine ingannatrici.
Ma ciò sarebbe un doppiamente avvilirmi. Fuggo dagli occhi tuoi:
maledico le tue lusinghe, le tue lagrime, le tue finzioni; tu mi hai
fatto conoscere qual infausto potere abbia sopra di noi il tuo sesso, e
mi hai fatto a costo mio imparare, che per vincerlo non basta, no,
disprezzarlo, ma ci conviene fuggirlo.

(Parte.)

SCENA DICIANNOVESIMA {#Section0011.xhtml#sigil_toc_id_99}
--------------------

Mirandolina, il Conte, il Marchese e Fabrizio.

CONTE:

Dica ora di non essere innamorato.

MARCHESE:

Se mi dà un\'altra mentita, da cavaliere lo sfido.

MIRANDOLINA:

Zitto, signori zitto. È andato via, e se non torna, e se la cosa passa
così, posso dire di essere fortunata. Pur troppo, poverino, mi è
riuscito d\'innamorarlo, e mi son messa ad un brutto rischio. Non ne
vo\' saper altro. Fabrizio, vieni qui, caro, dammi la mano.

FABRIZIO:

La mano? Piano un poco, signora. Vi dilettate d\'innamorar la gente in
questa maniera, e credete ch\'io vi voglia sposare?

MIRANDOLINA:

Eh via, pazzo! È stato uno scherzo, una bizzarria, un puntiglio. Ero
fanciulla, non avevo nessuno che mi comandasse. Quando sarò maritata, so
io quel che farò.

FABRIZIO:

Che cosa farete?

SCENA ULTIMA {#Section0011.xhtml#sigil_toc_id_100}
------------

Il Servitore del Cavaliere e detti.

SERVITORE:

Signora padrona, prima di partire son venuto a riverirvi.

MIRANDOLINA:

Andate via?

SERVITORE:

Sì. Il padrone va alla Posta. Fa attaccare: mi aspetta colla roba, e ce
ne andiamo a Livorno.

MIRANDOLINA:

Compatite, se non vi ho fatto\...

SERVITORE:

Non ho tempo da trattenermi. Vi ringrazio, e vi riverisco.

(Parte.)

MIRANDOLINA:

Grazie al cielo, è partito. Mi resta qualche rimorso; certamente è
partito con poco gusto. Di questi spassi non me ne cavo mai più.

CONTE:

Mirandolina, fanciulla o maritata che siate, sarò lo stesso per voi.

MARCHESE:

Fate pure capitale della mia protezione.

MIRANDOLINA:

Signori miei, ora che mi marito, non voglio protettori, non voglio
spasimanti, non voglio regali. Sinora mi sono divertita, e ho fatto
male, e mi sono arrischiata troppo, e non lo voglio fare mai più. Questi
è mio marito\...

FABRIZIO:

Ma piano, signora\...

MIRANDOLINA:

Che piano! Che cosa c\'è? Che difficoltà ci sono? Andiamo. Datemi quella
mano.

FABRIZIO:

Vorrei che facessimo prima i nostri patti.

MIRANDOLINA:

Che patti? Il patto è questo: o dammi la mano, o vattene al tuo paese.

FABRIZIO:

Vi darò la mano\... ma poi\...

MIRANDOLINA:

Ma poi, sì, caro, sarò tutta tua; non dubitare di me ti amerò sempre,
sarai l\'anima mia.

FABRIZIO:

Tenete, cara, non posso più.

(Le dà la mano.)

MIRANDOLINA:

(Anche questa è fatta).

(Da sé.)

CONTE:

Mirandolina, voi siete una gran donna, voi avete l\'abilità di condur
gli uomini dove volete.

MARCHESE:

Certamente la vostra maniera obbliga infinitamente.

MIRANDOLINA:

Se è vero ch\'io possa sperar grazie da lor signori, una ne chiedo loro
per ultimo.

CONTE:

Dite pure.

MARCHESE:

Parlate.

FABRIZIO:

(Che cosa mai adesso domanderà?).

(Da sé.)

MIRANDOLINA:

Le supplico per atto di grazia, a provvedersi di un\'altra locanda.

FABRIZIO:

(Brava; ora vedo che la mi vuol bene).

(Da sé.)

CONTE:

Sì, vi capisco e vi lodo. Me ne andrò, ma dovunque io sia, assicuratevi
della mia stima.

MARCHESE:

Ditemi: avete voi perduta una boccettina d\'oro?

MIRANDOLINA:

Sì signore.

MARCHESE:

Eccola qui. L\'ho ritrovata, e ve la rendo. Partirò per compiacervi, ma
in ogni luogo fate pur capitale della mia protezione.

MIRANDOLINA:

Queste espressioni mi saran care, nei limiti della convenienza e
dell\'onestà. Cambiando stato, voglio cambiar costume; e lor signori
ancora profittino di quanto hanno veduto, in vantaggio e sicurezza del
loro cuore; e quando mai si trovassero in occasioni di dubitare, di
dover cedere, di dover cadere, pensino alle malizie imparate, e si
ricordino della Locandiera.

Fine della Commedia
