[]{#Section0001.xhtml}

::: {.immagine_copertina}
<svg xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink" height="100%" width="100%" preserveaspectratio="xMidYMid meet" version="1.1" viewbox="0 0 1057 1500">
<desc>Copertina di La Locandiera, di Carlo Goldoni</desc>
<image height="1500" width="1057" href="../Images/copertina.jpg"></image>
</svg>
Copertina {#Section0001.xhtml#testo_copertina}
=========
:::

[]{#Section0002.xhtml}

Informazioni {.sigil_not_in_toc}
============

Questo e-book è stato realizzato anche grazie al sostegno di:

::: {.box_sponsor}
[![E-text](Images/e-text.png)](http://www.e-text.it/)\
**E-text**\
Editoria, Web design, Multimedia

[Pubblica il tuo libro, o crea il tuo sito con
E-text!](http://www.e-text.it/)
:::

QUESTO E-BOOK:

TITOLO: La locandiera\
AUTORE: Goldoni, Carlo\
TRADUTTORE:\
CURATORE: Antonucci, Giovanni\
NOTE: Note critiche a cura di Laura Barberi

CODICE ISBN E-BOOK: 9788828100522

DIRITTI D\'AUTORE: sì, sulle note critiche

LICENZA: questo testo è distribuito con la licenza specificata al
seguente indirizzo Internet:
http://www.liberliber.it/online/opere/libri/licenze/

COPERTINA: \[elaborazione da\] \"Madame de Sorquainville (1749)\" di
Jean-Baptiste Perronneau (1715--1783) - Musée du Louvre, Paris -
https://commons.wikimedia.org/wiki/File:Perronneau\_Madame\_de\_Sorquainville.jpg
- Pubblico Dominio.

TRATTO DA: La locandiera / Carlo Goldoni ; a cura di Giovanni Antonucci.
- Roma : Tascabili Economici Newton, 1993. - 93 p. ; 20 cm. -
(Centopaginemillelire 71).

CODICE ISBN FONTE: 88-7983-064-3

1a EDIZIONE ELETTRONICA DEL: 2 febbraio 1996\
2a EDIZIONE ELETTRONICA DEL: 2 febbraio 2014

INDICE DI AFFIDABILITÀ: 1\
0: affidabilità bassa\
1: affidabilità standard\
2: affidabilità buona\
3: affidabilità ottima

SOGGETTO:\
PER015000 ARTI RAPPRESENTATIVE / Commedia

DIGITALIZZAZIONE:\
Stefano D'Urso

REVISIONE:\
Susanna Corona\
Ugo Santamaria

IMPAGINAZIONE:\
Catia Righi, catia\_righi\@tin.it (ODT)\
Mariano Piscopo (ePub)\
Rosario Di Mauro (revisione ePub)

PUBBLICAZIONE:\
Catia Righi, catia\_righi\@tin.it\
Ugo Santamaria

[]{#Section0003.xhtml}

Liber Liber
===========

[![Fai una
donazione](Images/2_euro.png)](http://www.liberliber.it/online/aiuta/)

Se questo libro ti è piaciuto, aiutaci a realizzarne altri. Fai una
donazione: <http://www.liberliber.it/online/aiuta/>.

Scopri sul sito Internet di Liber Liber ciò che stiamo realizzando:
migliaia di ebook gratuiti in edizione integrale, audiolibri, brani
musicali con licenza libera, video e tanto altro:
<http://www.liberliber.it/>.

[]{#Section0004.xhtml}

Indice generale

-   [Copertina](../Text/Section0001.xhtml#testo_copertina)
-   [Informazioni](../Text/Section0002.xhtml)
-   [Liber Liber](../Text/Section0003.xhtml)
-   [Note critiche](../Text/Section0005.xhtml)
-   [La locandiera](../Text/Section0006.xhtml)
-   [PERSONAGGI](../Text/Section0007.xhtml)
-   [L\'autore a chi legge](../Text/Section0008.xhtml)
-   [ATTO PRIMO](../Text/Section0009.xhtml)
    -   [SCENA PRIMA](../Text/Section0009.xhtml#sigil_toc_id_39)
    -   [SCENA SECONDA](../Text/Section0009.xhtml#sigil_toc_id_40)
    -   [SCENA TERZA](../Text/Section0009.xhtml#sigil_toc_id_41)
    -   [SCENA QUARTA](../Text/Section0009.xhtml#sigil_toc_id_42)
    -   [SCENA QUINTA](../Text/Section0009.xhtml#sigil_toc_id_43)
    -   [SCENA SESTA](../Text/Section0009.xhtml#sigil_toc_id_44)
    -   [SCENA SETTIMA](../Text/Section0009.xhtml#sigil_toc_id_45)
    -   [SCENA OTTAVA](../Text/Section0009.xhtml#sigil_toc_id_46)
    -   [SCENA NONA](../Text/Section0009.xhtml#sigil_toc_id_47)
    -   [SCENA DECIMA](../Text/Section0009.xhtml#sigil_toc_id_48)
    -   [SCENA UNDICESIMA](../Text/Section0009.xhtml#sigil_toc_id_49)
    -   [SCENA DODICESIMA](../Text/Section0009.xhtml#sigil_toc_id_50)
    -   [SCENA TREDICESIMA](../Text/Section0009.xhtml#sigil_toc_id_51)
    -   [SCENA
        QUATTORDICESIMA](../Text/Section0009.xhtml#sigil_toc_id_52)
    -   [SCENA QUINDICESIMA](../Text/Section0009.xhtml#sigil_toc_id_53)
    -   [SCENA SEDICESIMA](../Text/Section0009.xhtml#sigil_toc_id_54)
    -   [SCENA
        DICIASSETTESIMA](../Text/Section0009.xhtml#sigil_toc_id_55)
    -   [SCENA DICIOTTESIMA](../Text/Section0009.xhtml#sigil_toc_id_56)
    -   [SCENA
        DICIANNOVESIMA](../Text/Section0009.xhtml#sigil_toc_id_57)
    -   [SCENA VENTESIMA](../Text/Section0009.xhtml#sigil_toc_id_58)
    -   [SCENA VENTUNESIMA](../Text/Section0009.xhtml#sigil_toc_id_59)
    -   [SCENA VENTIDUESIMA](../Text/Section0009.xhtml#sigil_toc_id_60)
    -   [SCENA VENTITREESIMA](../Text/Section0009.xhtml#sigil_toc_id_61)
-   [ATTO SECONDO](../Text/Section0010.xhtml)
    -   [SCENA PRIMA](../Text/Section0010.xhtml#sigil_toc_id_62)
    -   [SCENA SECONDA](../Text/Section0010.xhtml#sigil_toc_id_63)
    -   [SCENA TERZA](../Text/Section0010.xhtml#sigil_toc_id_64)
    -   [SCENA QUARTA](../Text/Section0010.xhtml#sigil_toc_id_65)
    -   [SCENA QUINTA](../Text/Section0010.xhtml#sigil_toc_id_66)
    -   [SCENA SESTA](../Text/Section0010.xhtml#sigil_toc_id_67)
    -   [SCENA SETTIMA](../Text/Section0010.xhtml#sigil_toc_id_68)
    -   [SCENA OTTAVA](../Text/Section0010.xhtml#sigil_toc_id_69)
    -   [SCENA NONA](../Text/Section0010.xhtml#sigil_toc_id_70)
    -   [SCENA DECIMA](../Text/Section0010.xhtml#sigil_toc_id_71)
    -   [SCENA UNDICESIMA](../Text/Section0010.xhtml#sigil_toc_id_72)
    -   [SCENA DODICESIMA](../Text/Section0010.xhtml#sigil_toc_id_73)
    -   [SCENA TREDICESIMA](../Text/Section0010.xhtml#sigil_toc_id_74)
    -   [SCENA
        QUATTORDICESIMA](../Text/Section0010.xhtml#sigil_toc_id_75)
    -   [SCENA QUINDICESIMA](../Text/Section0010.xhtml#sigil_toc_id_76)
    -   [SCENA SEDICESIMA](../Text/Section0010.xhtml#sigil_toc_id_77)
    -   [SCENA
        DICIASSETTESIMA](../Text/Section0010.xhtml#sigil_toc_id_78)
    -   [SCENA DICIOTTESIMA](../Text/Section0010.xhtml#sigil_toc_id_79)
    -   [SCENA
        DICIANNOVESIMA](../Text/Section0010.xhtml#sigil_toc_id_80)
-   [ATTO TERZO](../Text/Section0011.xhtml)
    -   [SCENA PRIMA](../Text/Section0011.xhtml#sigil_toc_id_81)
    -   [SCENA SECONDA](../Text/Section0011.xhtml#sigil_toc_id_82)
    -   [SCENA TERZA](../Text/Section0011.xhtml#sigil_toc_id_83)
    -   [SCENA QUARTA](../Text/Section0011.xhtml#sigil_toc_id_84)
    -   [SCENA QUINTA](../Text/Section0011.xhtml#sigil_toc_id_85)
    -   [SCENA SESTA](../Text/Section0011.xhtml#sigil_toc_id_86)
    -   [SCENA SETTIMA](../Text/Section0011.xhtml#sigil_toc_id_87)
    -   [SCENA OTTAVA](../Text/Section0011.xhtml#sigil_toc_id_88)
    -   [SCENA NONA](../Text/Section0011.xhtml#sigil_toc_id_89)
    -   [SCENA DECIMA](../Text/Section0011.xhtml#sigil_toc_id_90)
    -   [SCENA UNDICESIMA](../Text/Section0011.xhtml#sigil_toc_id_91)
    -   [SCENA DODICESIMA](../Text/Section0011.xhtml#sigil_toc_id_92)
    -   [SCENA TREDICESIMA](../Text/Section0011.xhtml#sigil_toc_id_93)
    -   [SCENA
        QUATTORDICESIMA](../Text/Section0011.xhtml#sigil_toc_id_94)
    -   [SCENA QUINDICESIMA](../Text/Section0011.xhtml#sigil_toc_id_95)
    -   [SCENA SEDICESIMA](../Text/Section0011.xhtml#sigil_toc_id_96)
    -   [SCENA
        DICIASSETTESIMA](../Text/Section0011.xhtml#sigil_toc_id_97)
    -   [SCENA DICIOTTESIMA](../Text/Section0011.xhtml#sigil_toc_id_98)
    -   [SCENA
        DICIANNOVESIMA](../Text/Section0011.xhtml#sigil_toc_id_99)
    -   [SCENA ULTIMA](../Text/Section0011.xhtml#sigil_toc_id_100)

[]{#Section0005.xhtml}

Note critiche
=============

a cura di Laura Barberi

Commedia in tre atti rappresentata per la prima volta nel 1753, è una
delle più famose e stilisticamente riuscite tra le numerose scritte da
Carlo Goldoni (1707- 1793). In breve la trama: il conte di Albafiorita e
il marchese di Forlipopoli, ospiti nella locanda di Mirandolina a
Firenze, si contendono il suo amore, il primo con doni che può
facilmente permettersi grazie alla sua buona posizione economica, il
secondo, appartenente a quella parte di nobiltà decaduta e ormai senza
mezzi, tenta di conquistarla con promesse di protezione. Nella locanda
viene ospitato anche il cavaliere di Ripafratta, un convinto misogino
che si vanta di essere immune al fascino femminile ed anzi sostiene di
disprezzare l\'intero sesso \"debole\". Mirandolina, risentita del suo
atteggiamento e della sua insensibilità, decide di provarsi nel farlo
innamorare di sé e senza troppe difficoltà ci riesce, causando gelosie e
liti tra i tre pretendenti. Riuscita nell\'intento di far capitolare il
cavaliere, ella però ne rifiuta l\'amore così come prima aveva rifiutato
la corte dei due nobiluomini e concede la propria mano al cameriere
della locanda Fabrizio.

Questa commedia, che non fu mai tra le preferite del Goldoni (a cui
piacevano di più le sue commedie d\'insieme, corali), fotografa però
chiaramente l\'ormai raggiunta maturità artistica del suo autore. Non
solo l\'assoluta padronanza del mezzo scenico: la sapiente
organizzazione delle varie scene, l\'attenta caratterizzazione di tutti
i personaggi, i dialoghi ben calibrati, il chiaro delineamento
dell\'ambiente sociale all\'interno del quale si svolge l\'azione; ma
anche il raggiungimento di una \"forma\" teatrale originale e a lungo
ricercata negli anni, la materializzazione scenica di quella riforma
teatrale goldoniana che è storicamente il principale merito attribuito
dai critici al grande commediografo veneziano.

L\'opera di Carlo Goldoni, infatti, si colloca in un periodo di acceso
dibattito, in Italia ma anche nel resto d\'Europa, sulle modalità e
sugli intenti del teatro: sempre di più prende piede, sulla spinta del
pensiero illuminista e dell\'opera di [Diderot]{lang="fr"} in Francia o
di [Lessing]{lang="de"} in Germania, il cosiddetto dramma borghese, un
tipo di teatro che doveva ispirarsi alla società contemporanea, i cui
intrecci dovevano risultare plausibili e che doveva avere fini educativi
e di denuncia. In Italia, in particolare, Goldoni si trovò a dover fare
i conti con l\'ingombrante eredità della Commedia dell\'Arte: il
successo che continuava ad arridere alle maschere ed ai canovacci,
seppur usurati, di questa tradizione teatrale rendeva più difficile
qualsiasi innovazione per l\'opposizione sia del pubblico, appunto, sia
degli attori poco propensi a lasciare il loro ruolo di istrioni, maghi
dell\'improvvisazione, per piegarsi alla fedeltà ad un testo scritto;
abituati a recitare sempre solo la stessa parte, ad indossare la stessa
maschera - Arlecchino, Pantalone, Colombina - il cui comportamento e
linguaggio si erano \"sclerotizzati\" da tempo, erano riluttanti a
doversi calare ogni volta in un nuovo personaggio a seconda della
commedia.

In questo contesto la \"riforma\" goldoniana dovette procedere
gradualmente, tenendo conto sia dei gusti del pubblico che delle
necessità degli attori e degli impresari, ma passo passo Goldoni riuscì
a modificare le convenzioni drammaturgiche. Furono sostanzialmente due i
punti fermi di questa riforma: innanzitutto l\'introduzione di un testo
scritto fisso, non modificabile dagli attori, in sostituzione dei
canovacci della Commedia dell\'Arte, più che altro delle semplici
situazioni base, dei punti di partenza che lasciavano ampio spazio
all\'improvvisazione degli attori; e poi l\'eliminazione delle maschere,
vale a dire il passaggio dai tipi ai caratteri, a degli individui ben
precisi. Tutto ciò per rendere il suo teatro realistico, raffigurazione
sulla scena di un mondo reale. Così Mirandolina ne La locandiera è un
personaggio ben definito non solo caratterialmente, ma anche
socialmente; non è più la \"servetta\" intrigante della commedia
precedente, il tipo della \"donnina brillante e capricciosa\", ma è una
locandiera con i suoi affari, i suoi interessi. È un personaggio
radicato in una precisa realtà sociale, lontano da generalizzazioni e
stereotipi, il cui comportamento è dettato non da caratteristiche fisse
ed immutabili, ma dalle sue esperienze e dalla sua posizione sociale ed
è significativo che a conclusione della commedia Mirandolina sposi un
borghese come lei, il cameriere Fabrizio.

Una conclusione che conferma come, aldilà del piacere per una storia ben
congeniata, molti siano i temi di quest\'opera. L\'irrompere sul
palcoscenico di valori borghesi, quali l\'operosità, il senso della
misura, l\'attenzione al guadagno, non rappresenta il solo motivo di
interesse, per così dire, sociologico che questa commedia propone: non
va tralasciata infatti la parte di \"eroe positivo\" che la donna
ricopre all\'interno de La locandiera come pure in altre commedie di
Goldoni. La pubblicistica illuministica di quegli anni dibatteva
ampiamente sulla necessità di un nuovo e più importante ruolo per la
donna nella società e l\'autore veneziano ne rende testimonianza fedele
nel suo teatro, aderendo anche in questo caso a quelle spinte
modernizzatrici che stavano scuotendo l\'[ancien régime]{lang="fr"} in
tutta Europa. Tanti elementi diversi che però non fanno che confermare
la validità e la godibilità di questa commedia ancora oggi a più di due
secoli di distanza dalla sua stesura.

[]{#Section0006.xhtml}

La locandiera {.testo_frontespizio_titolo}
=============

di Carlo Goldoni

[]{#Section0007.xhtml}

PERSONAGGI
==========

Il Cavaliere di Ripafratta\
Il Marchese di Forlipopoli\
Il Conte d\'Albafiorita\
Mirandolina, [locandiera]{.testo_corsivo}\
Ortensia [comica]{.testo_corsivo}\
Dejanira [comica]{.testo_corsivo}\
Fabrizio, [cameriere di locanda]{.testo_corsivo}\
Servitore, [del Cavaliere]{.testo_corsivo}\
Servitore, [del Conte]{.testo_corsivo}

La scena si rappresenta in Firenze, nella locanda di Mirandolina.

[]{#Section0008.xhtml}

L\'autore a chi legge
=====================

Fra tutte le Commedie da me sinora composte, starei per dire essere
questa la più morale, la più utile, la più istruttiva. Sembrerà ciò
essere un paradosso a chi soltanto vorrà fermarsi a considerare il
carattere della [Locandiera]{.testo_corsivo}, e dirà anzi non aver io
dipinto altrove una donna più lusinghiera, più pericolosa di questa. Ma
chi rifletterà al carattere e agli avvenimenti del Cavaliere, troverà un
esempio vivissimo della presunzione avvilita, ed una scuola che insegna
a fuggire i pericoli, per non soccombere alle cadute.

Mirandolina fa altrui vedere come s\'innamorano gli uomini. Principia a
entrar in grazia del disprezzator delle donne, secondandolo nel modo suo
di pensare, lodandolo in quelle cose che lo compiacciono, ed eccitandolo
perfino a biasimare le donne istesse. Superata con ciò l\'avversione che
aveva il Cavaliere per essa, principia a usargli delle attenzioni, gli
fa delle finezze studiate, mostrandosi lontana dal volerlo obbligare
alla gratitudine. Lo visita, lo serve in tavola, gli parla con umiltà e
con rispetto, e in lui vedendo scemare la ruvidezza, in lei s\'aumenta
l\'ardire. Dice delle tronche parole, avanza degli sguardi, e senza
ch\'ei se ne avveda, gli dà delle ferite mortali. Il pover\'uomo conosce
il pericolo, e lo vorrebbe fuggire, ma la femmina accorta con due
lagrimette l\'arresta, e con uno svenimento l\'atterra, lo precipita,
l\'avvilisce. Pare impossibile, che in poche ore un uomo possa
innamorarsi a tal segno: un uomo, aggiungasi, disprezzator delle donne,
che mai ha seco loro trattato; ma appunto per questo più facilmente egli
cade, perché sprezzandole senza conoscerle, e non sapendo quali sieno le
arti loro, e dove fondino la speranza de\' loro trionfi, ha creduto che
bastar gli dovesse a difendersi la sua avversione, ed ha offerto il
petto ignudo ai colpi dell\'inimico.

Io medesimo diffidava quasi a principio di vederlo innamorato
ragionevolmente sul fine della Commedia, e pure, condotto dalla natura,
di passo in passo, come nella Commedia si vede, mi è riuscito di darlo
vinto alla fine dell\'Atto secondo.

Io non sapeva quasi cosa mi fare nel terzo, ma venutomi in mente, che
sogliono coteste lusinghiere donne, quando vedono ne\' loro lacci gli
amanti, aspramente trattarli, ho voluto dar un esempio di questa barbara
crudeltà, di questo ingiurioso disprezzo con cui si burlano dei
miserabili che hanno vinti, per mettere in orrore la schiavitù che si
procurano gli sciagurati, e rendere odioso il carattere delle
incantatrici Sirene. La Scena dello [stirare]{.testo_corsivo} allora
quando la Locandiera si burla del Cavaliere che languisce, non muove gli
animi a sdegno contro colei, che dopo averlo innamorato l\'insulta? Oh
bello specchio agli occhi della gioventù! Dio volesse che io medesimo
cotale specchio avessi avuto per tempo, che non avrei veduto ridere del
mio pianto qualche barbara Locandiera. Oh di quante Scene mi hanno
provveduto le mie vicende medesime!\... Ma non è il luogo questo né di
vantarmi delle mie follie, né di pentirmi delle mie debolezze. Bastami
che alcun mi sia grato della lezione che gli offerisco. Le donne che
oneste sono, giubileranno anch\'esse che si smentiscano codeste
simulatrici, che disonorano il loro sesso, ed esse femmine lusinghiere
arrossiranno in guardarmi, e non importa che mi dicano
nell\'incontrarmi: che tu sia maledetto!

Deggio avvisarvi, Lettor carissimo, di una picciola mutazione, che alla
presente Commedia ho fatto. Fabrizio, il cameriere della Locanda,
parlava in veneziano, quando si recitò la prima volta; l\'ho fatto
allora per comodo del personaggio, solito a favellar da Brighella; ove
l\'ho convertito in toscano, sendo disdicevole cosa introdurre senza
necessità in una Commedia un linguaggio straniero. Ciò ho voluto
avvertire, perché non so come la stamperà il Bettinelli; può essere
ch\'ei si serva di questo mio originale, e Dio lo voglia, perché almeno
sarà a dover penneggiato. Ma lo scrupolo ch\'ei si è fatto di stampare
le cose mie come io le ho abbozzate, lo farà trascurare anche questa
comodità.

[]{#Section0009.xhtml}

ATTO PRIMO
==========

SCENA PRIMA {#Section0009.xhtml#sigil_toc_id_39}
-----------

Sala di locanda.\
Il Marchese di Forlipopoli ed il Conte d\'Albafiorita

MARCHESE:

Fra voi e me vi è qualche differenza.

CONTE:

Sulla locanda tanto vale il vostro denaro, quanto vale il mio.

MARCHESE:

Ma se la locandiera usa a me delle distinzioni, mi si convengono più che
a voi.

CONTE:

Per qual ragione?

MARCHESE:

Io sono il Marchese di Forlipopoli.

CONTE:

Ed io sono il Conte d\'Albafiorita.

MARCHESE:

Sì, Conte! Contea comprata.

CONTE:

Io ho comprata la contea, quando voi avete venduto il marchesato.

MARCHESE:

Oh basta: son chi sono, e mi si deve portar rispetto.

CONTE:

Chi ve lo perde il rispetto? Voi siete quello, che con troppa libertà
parlando\...

MARCHESE:

Io sono in questa locanda, perché amo la locandiera. Tutti lo sanno, e
tutti devono rispettare una giovane che piace a me.

CONTE:

Oh, questa è bella! Voi mi vorreste impedire ch\'io amassi Mirandolina?
Perché credete ch\'io sia in Firenze? Perché credete ch\'io sia in
questa locanda?

MARCHESE:

Oh bene. Voi non farete niente.

CONTE:

Io no, e voi sì?

MARCHESE:

Io sì, e voi no. Io son chi sono. Mirandolina ha bisogno della mia
protezione.

CONTE:

Mirandolina ha bisogno di denari, e non di protezione.

MARCHESE:

Denari?\... non ne mancano.

CONTE:

Io spendo uno zecchino il giorno, signor Marchese, e la regalo
continuamente.

MARCHESE:

Ed io quel che fo non lo dico.

CONTE:

Voi non lo dite, ma già si sa.

MARCHESE:

Non si sa tutto.

CONTE:

Sì! caro signor Marchese, si sa. I camerieri lo dicono. Tre paoletti il
giorno.

MARCHESE:

A proposito di camerieri; vi è quel cameriere che ha nome Fabrizio, mi
piace poco. Parmi che la locandiera lo guardi assai di buon occhio.

CONTE:

Può essere che lo voglia sposare. Non sarebbe cosa mal fatta. Sono sei
mesi che è morto il di lei padre. Sola una giovane alla testa di una
locanda si troverà imbrogliata. Per me, se si marita, le ho promesso
trecento scudi.

MARCHESE:

Se si mariterà, io sono il suo protettore, e farò io\... E so io quello
che farò.

CONTE:

Venite qui: facciamola da buoni amici. Diamole trecento scudi per uno.

MARCHESE:

Quel ch\'io faccio, lo faccio segretamente, e non me ne vanto. Son chi
sono. Chi è di là?

(Chiama.)

CONTE:

(Spiantato! Povero e superbo!).

(Da sé.)

SCENA SECONDA {#Section0009.xhtml#sigil_toc_id_40}
-------------

Fabrizio e detti.

FABRIZIO:

Mi comandi, signore.

(Al Marchese.)

MARCHESE:

Signore? Chi ti ha insegnato la creanza?

FABRIZIO:

La perdoni.

CONTE:

Ditemi: come sta la padroncina?

(A Fabrizio.)

FABRIZIO:

Sta bene, illustrissimo.

MARCHESE:

È alzata dal letto?

FABRIZIO:

Illustrissimo sì.

MARCHESE:

Asino.

FABRIZIO:

Perché, illustrissimo signore?

MARCHESE:

Che cos\'è questo illustrissimo?

FABRIZIO:

È il titolo che ho dato anche a quell\'altro Cavaliere.

MARCHESE:

Tra lui e me vi è qualche differenza.

CONTE:

Sentite?

(A Fabrizio.)

FABRIZIO:

(Dice la verità. Ci è differenza: me ne accorgo nei conti).

(Piano al Conte.)

MARCHESE:

Di\' alla padrona che venga da me, che le ho da parlare.

FABRIZIO:

Eccellenza sì. Ho fallato questa volta?

MARCHESE:

Va bene. Sono tre mesi che lo sai; ma sei un impertinente.

FABRIZIO:

Come comanda, Eccellenza.

CONTE:

Vuoi vedere la differenza che passa fra il Marchese e me?

MARCHESE:

Che vorreste dire?

CONTE:

Tieni. Ti dono uno zecchino. Fa che anch\'egli te ne doni un altro.

FABRIZIO:

Grazie, illustrissimo.

(Al Conte.)

Eccellenza\...

(Al Marchese.)

MARCHESE:

Non getto il mio, come i pazzi. Vattene.

FABRIZIO:

Illustrissimo signore, il cielo la benedica.

(Al Conte.)

Eccellenza. (Rifinito. Fuor del suo paese non vogliono esser titoli per
farsi stimare, vogliono esser quattrini).

(Da sé, parte.)

SCENA TERZA {#Section0009.xhtml#sigil_toc_id_41}
-----------

Il Marchese ed il Conte.

MARCHESE:

Voi credete di soverchiarmi con i regali, ma non farete niente. Il mio
grado val più di tutte le vostre monete.

CONTE:

Io non apprezzo quel che vale, ma quello che si può spendere.

MARCHESE:

Spendete pure a rotta di collo. Mirandolina non fa stima di voi.

CONTE:

Con tutta la vostra gran nobiltà, credete voi di essere da lei stimato?
Vogliono esser denari.

MARCHESE:

Che denari? Vuol esser protezione. Esser buono in un incontro di far un
piacere.

CONTE:

Sì, esser buono in un incontro di prestar cento doppie.

MARCHESE:

Farsi portar rispetto bisogna.

CONTE:

Quando non mancano denari, tutti rispettano.

MARCHESE:

Voi non sapete quel che vi dite.

CONTE:

L\'intendo meglio di voi.

SCENA QUARTA {#Section0009.xhtml#sigil_toc_id_42}
------------

Il Cavaliere di Ripafratta dalla sua camera, e detti.

CAVALIERE:

Amici, che cos\'è questo romore? Vi è qualche dissensione fra di voi
altri?

CONTE:

Si disputava sopra un bellissimo punto.

MARCHESE:

II Conte disputa meco sul merito della nobiltà.

(Ironico.)

CONTE:

Io non levo il merito alla nobiltà: ma sostengo, che per cavarsi dei
capricci, vogliono esser denari.

CAVALIERE:

Veramente, Marchese mio\...

MARCHESE:

Orsù, parliamo d\'altro.

CAVALIERE:

Perché siete venuti a simil contesa?

CONTE:

Per un motivo il più ridicolo della terra.

MARCHESE:

Sì, bravo! il Conte mette tutto in ridicolo.

CONTE:

Il signor Marchese ama la nostra locandiera. Io l\'amo ancor più di lui.
Egli pretende corrispondenza, come un tributo alla sua nobiltà. Io la
spero, come una ricompensa alle mie attenzioni. Pare a voi che la
questione non sia ridicola?

MARCHESE:

Bisogna sapere con quanto impegno io la proteggo.

CONTE:

Egli la protegge, ed io spendo.

(Al Cavaliere.)

CAVALIERE:

In verità non si può contendere per ragione alcuna che io meriti meno.
Una donna vi altera? vi scompone? Una donna? che cosa mai mi convien
sentire? Una donna? Io certamente non vi è pericolo che per le donne
abbia che dir con nessuno. Non le ho mai amate, non le ho mai stimate, e
ho sempre creduto che sia la donna per l\'uomo una infermità
insopportabile.

MARCHESE:

In quanto a questo poi, Mirandolina ha un merito estraordinario.

CONTE:

Sin qua il signor Marchese ha ragione. La nostra padroncina della
locanda è veramente amabile.

MARCHESE:

Quando l\'amo io, potete credere che in lei vi sia qualche cosa di
grande.

CAVALIERE:

In verità mi fate ridere. Che mai può avere di stravagante costei, che
non sia comune all\'altre donne?

MARCHESE:

Ha un tratto nobile, che incatena.

CONTE:

È bella, parla bene, veste con pulizia, è di un ottimo gusto.

CAVALIERE:

Tutte cose che non vagliono un fico. Sono tre giorni ch\'io sono in
questa locanda, e non mi ha fatto specie veruna.

CONTE:

Guardatela, e forse ci troverete del buono.

CAVALIERE:

Eh, pazzia! L\'ho veduta benissimo. È una donna come l\'altre.

MARCHESE:

Non è come l\'altre, ha qualche cosa di più. Io che ho praticate le
prime dame, non ho trovato una donna che sappia unire, come questa, la
gentilezza e il decoro.

CONTE:

Cospetto di bacco! Io son sempre stato solito trattar donne: ne conosco
li difetti ed il loro debole. Pure con costei, non ostante il mio lungo
corteggio e le tante spese per essa fatte, non ho potuto toccarle un
dito.

CAVALIERE:

Arte, arte sopraffina. Poveri gonzi! Le credete, eh? A me non la
farebbe. Donne? Alla larga tutte quante elle sono.

CONTE:

Non siete mai stato innamorato?

CAVALIERE:

Mai, né mai lo sarò. Hanno fatto il diavolo per darmi moglie, né mai
l\'ho voluta.

MARCHESE:

Ma siete unico della vostra casa: non volete pensare alla successione?

CAVALIERE:

Ci ho pensato più volte ma quando considero che per aver figliuoli mi
converrebbe soffrire una donna, mi passa subito la volontà.

CONTE:

Che volete voi fare delle vostre ricchezze?

CAVALIERE:

Godermi quel poco che ho con i miei amici.

MARCHESE:

Bravo, Cavaliere, bravo; ci goderemo.

CONTE:

E alle donne non volete dar nulla?

CAVALIERE:

Niente affatto. A me non ne mangiano sicuramente.

CONTE:

Ecco la nostra padrona. Guardatela, se non è adorabile.

CAVALIERE:

Oh la bella cosa! Per me stimo più di lei quattro volte un bravo cane da
caccia.

MARCHESE:

Se non la stimate voi, la stimo io.

CAVALIERE:

Ve la lascio, se fosse più bella di Venere.

SCENA QUINTA {#Section0009.xhtml#sigil_toc_id_43}
------------

Mirandolina e detti.

MIRANDOLINA:

M\'inchino a questi cavalieri. Chi mi domanda di lor signori?

MARCHESE:

Io vi domando, ma non qui.

MIRANDOLINA:

Dove mi vuole, Eccellenza?

MARCHESE:

Nella mia camera.

MIRANDOLINA:

Nella sua camera? Se ha bisogno di qualche cosa verra il cameriere a
servirla.

MARCHESE:

(Che dite di quel contegno?).

(Al Cavaliere.)

CAVALIERE:

(Quello che voi chiamate contegno, io lo chiamerei temerità,
impertinenza).

(Al Marchese.)

CONTE:

Cara Mirandolina, io vi parlerò in pubblico, non vi darò l\'incomodo di
venire nella mia camera. Osservate questi orecchini. Vi piacciono?

MIRANDOLINA:

Belli.

CONTE:

Sono diamanti, sapete?

MIRANDOLINA:

Oh, gli conosco. Me ne intendo anch\'io dei diamanti.

CONTE:

E sono al vostro comando.

CAVALIERE:

(Caro amico, voi li buttate via).

(Piano al Conte.)

MIRANDOLINA:

Perché mi vuol ella donare quegli orecchini?

MARCHESE:

Veramente sarebbe un gran regalo! Ella ne ha de\' più belli al doppio.

CONTE:

Questi sono legati alla moda. Vi prego riceverli per amor mio.

CAVALIERE:

(Oh che pazzo!).

(Da sé.)

MIRANDOLINA:

No, davvero, signore\...

CONTE:

Se non li prendete, mi disgustate.

MIRANDOLINA:

Non so che dire\... mi preme tenermi amici gli avventori della mia
locanda. Per non disgustare il signor Conte, li prenderò.

CAVALIERE:

(Oh che forca!).

(Da sé.)

CONTE:

(Che dite di quella prontezza di spirito?).

(Al Cavaliere.)

CAVALIERE:

(Bella prontezza! Ve li mangia, e non vi ringrazia nemmeno).

(Al Conte.)

MARCHESE:

Veramente, signor Conte, vi siete acquistato gran merito. Regalare una
donna in pubblico, per vanità! Mirandolina, vi ho da parlare a
quattr\'occhi, fra voi e me: son Cavaliere.

MIRANDOLINA:

(Che arsura! Non gliene cascano).

(Da sé.)

Se altro non mi comandano, io me n\'anderò.

CAVALIERE:

Ehi! padrona. La biancheria che mi avete dato, non mi gusta. Se non ne
avete di meglio, mi provvederò.

(Con disprezzo.)

MIRANDOLINA:

Signore, ve ne sarà di meglio. Sarà servita, ma mi pare che la potrebbe
chiedere con un poco di gentilezza.

CAVALIERE:

Dove spendo il mio denaro, non ho bisogno di far complimenti.

CONTE:

Compatitelo. Egli è nemico capitale delle donne.

(A Mirandolina.)

CAVALIERE:

Eh, che non ho bisogno d\'essere da lei compatito.

MIRANDOLINA:

Povere donne! che cosa le hanno fatto? Perché così crudele con noi,
signor Cavaliere?

CAVALIERE:

Basta così. Con me non vi prendete maggior confidenza. Cambiatemi la
biancheria. La manderò a prender pel servitore. Amici, vi sono schiavo.

(Parte.)

SCENA SESTA {#Section0009.xhtml#sigil_toc_id_44}
-----------

Il Marchese, il Conte e Mirandolina.

MIRANDOLINA:

Che uomo salvatico! Non ho veduto il compagno.

CONTE:

Cara Mirandolina, tutti non conoscono il vostro merito.

MIRANDOLINA:

In verità, son così stomacata del suo mal procedere, che or ora lo
licenzio a dirittura.

MARCHESE:

Sì; e se non vuol andarsene, ditelo a me, che lo farò partire
immediatamente. Fate pur uso della mia protezione.

CONTE:

E per il denaro che aveste a perdere, io supplirò e pagherò tutto.
(Sentite, mandate via anche il Marchese, che pagherò io).

(Piano a Mirandolina.)

MIRANDOLINA:

Grazie, signori miei, grazie. Ho tanto spirito che basta, per dire ad un
forestiere ch\'io non lo voglio, e circa all\'utile, la mia locanda non
ha mai camere in ozio.

SCENA SETTIMA {#Section0009.xhtml#sigil_toc_id_45}
-------------

Fabrizio e detti.

FABRIZIO:

Illustrissimo, c\'è uno che la domanda.

(Al Conte.)

CONTE:

Sai chi sia?

FABRIZIO:

Credo ch\'egli sia un legatore di gioje. (Mirandolina, giudizio; qui non
istate bene).

(Piano a Mirandolina, e parte.)

CONTE:

Oh sì, mi ha da mostrare un gioiello. Mirandolina, quegli orecchini,
voglio che li accompagniamo.

MIRANDOLINA:

Eh no, signor Conte\...

CONTE:

Voi meritate molto, ed io i denari non li stimo niente. Vado a vedere
questo gioiello. Addio, Mirandolina; signor Marchese, la riverisco!

(Parte.)

SCENA OTTAVA {#Section0009.xhtml#sigil_toc_id_46}
------------

Il Marchese e Mirandolina.

MARCHESE:

(Maledetto Conte! Con questi suoi denari mi ammazza).

(Da sé.)

MIRANDOLINA:

In verità il signor Conte s\'incomoda troppo.

MARCHESE:

Costoro hanno quattro soldi, e li spendono per vanità, per albagia. Io
li conosco, so il viver del mondo.

MIRANDOLINA:

Eh, il viver del mondo lo so ancor io.

MARCHESE:

Pensano che le donne della vostra sorta si vincano con i regali.

MIRANDOLINA:

I regali non fanno male allo stomaco.

MARCHESE:

Io crederei di farvi un\'ingiuria, cercando di obbligarvi con i
donativi.

MIRANDOLINA:

Oh, certamente il signor Marchese non mi ha ingiuriato mai.

MARCHESE:

E tali ingiurie non ve le farò.

MIRANDOLINA:

Lo credo sicurissimamente.

MARCHESE:

Ma dove posso, comandatemi.

MIRANDOLINA:

Bisognerebbe ch\'io sapessi, in che cosa può Vostra Eccellenza.

MARCHESE:

In tutto. Provatemi.

MIRANDOLINA:

Ma verbigrazia, in che?

MARCHESE:

Per bacco! Avete un merito che sorprende.

MIRANDOLINA:

Troppe grazie, Eccellenza.

MARCHESE:

Ah! direi quasi uno sproposito. Maledirei quasi la mia Eccellenza.

MIRANDOLINA:

Perché, signore?

MARCHESE:

Qualche volta mi auguro di essere nello stato del Conte.

MIRANDOLINA:

Per ragione forse de\' suoi denari?

MARCHESE:

Eh! Che denari! Non li stimo un fico. Se fossi un Conte ridicolo come
lui\...

MIRANDOLINA:

Che cosa farebbe?

MARCHESE:

Cospetto del diavolo\... vi sposerei.

(Parte.)

SCENA NONA {#Section0009.xhtml#sigil_toc_id_47}
----------

MIRANDOLINA:

(sola)

Uh, che mai ha detto! L\'eccellentissimo signor Marchese Arsura mi
sposerebbe? Eppure, se mi volesse sposare, vi sarebbe una piccola
difficoltà. Io non lo vorrei. Mi piace l\'arrosto, e del fumo non so che
farne. Se avessi sposati tutti quelli che hanno detto volermi, oh, avrei
pure tanti mariti! Quanti arrivano a questa locanda, tutti di me
s\'innamorano, tutti mi fanno i cascamorti; e tanti e tanti mi
esibiscono di sposarmi a dirittura. E questo signor Cavaliere, rustico
come un orso, mi tratta sì bruscamente? Questi è il primo forestiere
capitato alla mia locanda, il quale non abbia avuto piacere di trattare
con me. Non dico che tutti in un salto s\'abbiano a innamorare: ma
disprezzarmi così? è una cosa che mi muove la bile terribilmente. È
nemico delle donne? Non le può vedere? Povero pazzo! Non avrà ancora
trovato quella che sappia fare. Ma la troverà. La troverà. E chi sa che
non l\'abbia trovata? Con questi per l\'appunto mi ci metto di picca.
Quei che mi corrono dietro, presto presto mi annoiano. La nobiltà non fa
per me. La ricchezza la stimo e non la stimo. Tutto il mio piacere
consiste in vedermi servita, vagheggiata, adorata. Questa è la mia
debolezza, e questa è la debolezza di quasi tutte le donne. A maritarmi
non ci penso nemmeno; non ho bisogno di nessuno; vivo onestamente, e
godo la mia libertà. Tratto con tutti, ma non m\'innamoro mai di
nessuno. Voglio burlarmi di tante caricature di amanti spasimati; e
voglio usar tutta l\'arte per vincere, abbattere e conquassare quei
cuori barbari e duri che son nemici di noi, che siamo la miglior cosa
che abbia prodotto al mondo la bella madre natura.

SCENA DECIMA {#Section0009.xhtml#sigil_toc_id_48}
------------

Fabrizio e detta.

FABRIZIO:

Ehi, padrona.

MIRANDOLINA:

Che cosa c\'è?

FABRIZIO:

Quel forestiere che è alloggiato nella camera di mezzo, grida della
biancheria; dice che è ordinaria, e che non la vuole.

MIRANDOLINA:

Lo so, lo so. Lo ha detto anche a me, e lo voglio servire.

FABRIZIO:

Benissimo. Venitemi dunque a metter fuori la roba, che gliela possa
portare.

MIRANDOLINA:

Andate, andate, gliela porterò io.

FABRIZIO:

Voi gliela volete portare?

MIRANDOLINA:

Sì, io.

FABRIZIO:

Bisogna che vi prema molto questo forestiere.

MIRANDOLINA:

Tutti mi premono. Badate a voi.

FABRIZIO:

(Già me n\'avvedo. Non faremo niente. Ella mi lusinga; ma non faremo
niente).

(Da sé.)

MIRANDOLINA:

(Povero sciocco! Ha delle pretensioni. Voglio tenerlo in isperanza,
perché mi serva con fedeltà).

(Da sé.)

FABRIZIO:

Si è sempre costumato, che i forestieri li serva io.

MIRANDOLINA:

Voi con i forestieri siete un poco troppo ruvido.

FABRIZIO:

E voi siete un poco troppo gentile.

MIRANDOLINA:

So quel quel che fo, non ho bisogno di correttori.

FABRIZIO:

Bene, bene. Provvedetevi di cameriere.

MIRANDOLINA:

Perché, signor Fabrizio? è disgustato di me?

FABRIZIO:

Vi ricordate voi che cosa ha detto a noi due vostro padre, prima
ch\'egli morisse?

MIRANDOLINA:

Sì; quando mi vorrò maritare, mi ricorderò di quel che ha detto mio
padre.

FABRIZIO:

Ma io son delicato di pelle, certe cose non le posso soffrire.

MIRANDOLINA:

Ma che credi tu ch\'io mi sia? Una frasca? Una civetta? Una pazza? Mi
maraviglio di te. Che voglio fare io dei forestieri che vanno e vengono?
Se il tratto bene, lo fo per mio interesse, per tener in credito la mia
locanda. De\' regali non ne ho bisogno. Per far all\'amore? Uno mi
basta: e questo non mi manca; e so chi merita, e so quello che mi
conviene. E quando vorrò maritarmi\... mi ricorderò di mio padre. E chi
mi averà servito bene, non potrà lagnarsi di me. Son grata. Conosco il
merito\... Ma io non son conosciuta. Basta, Fabrizio, intendetemi, se
potete.

(Parte.)

FABRIZIO:

Chi può intenderla, è bravo davvero. Ora pare che la mi voglia, ora che
la non mi voglia. Dice che non è una frasca, ma vuol far a suo modo. Non
so che dire. Staremo a vedere. Ella mi piace, le voglio bene,
accomoderei con essa i miei interessi per tutto il tempo di vita mia.
Ah! bisognerà chiuder un occhio, e lasciar correre qualche cosa.
Finalmente i forestieri vanno e vengono. Io resto sempre. Il meglio sarà
sempre per me.

(Parte.)

SCENA UNDICESIMA {#Section0009.xhtml#sigil_toc_id_49}
----------------

Camera del Cavaliere.\
Il Cavaliere ed un Servitore.

SERVITORE:

Illustrissimo, hanno portato questa lettera.

CAVALIERE:

Portami la cioccolata.

(Il Servitore parte.)\
(Il Cavaliere apre la lettera.)

Siena, primo Gennaio 1753.

(Chi scrive?)

Orazio Taccagni. Amico carissimo. La tenera amicizia che a voi mi lega,
mi rende sollecito ad avvisarvi essere necessario il vostro ritorno in
patria. È morto il Conte Manna\...

(Povero Cavaliere! Me ne dispiace).

Ha lasciato la sua unica figlia nubile erede di centocinquanta mila
scudi. Tutti gli amici vostri vorrebbero che toccasse a voi una tal
fortuna, e vanno maneggiando\... Non s\'affatichino per me, che non
voglio saper nulla. Lo sanno pure ch\'io non voglio donne per i piedi. E
questo mio caro amico, che lo sa più d\'ogni altro, mi secca peggio di
tutti.

(Straccia la lettera.)

Che importa a me di centocinquanta mila scudi? Finché son solo, mi basta
meno. Se fossi accompagnato, non mi basterebbe assai più. Moglie a me!
Piuttosto una febbre quartana.

SCENA DODICESIMA {#Section0009.xhtml#sigil_toc_id_50}
----------------

Il Marchese e detto.

MARCHESE:

Amico, vi contentate ch\'io venga a stare un poco con voi?

CAVALIERE:

Mi fate onore.

MARCHESE:

Almeno fra me e voi possiamo trattarci con confidenza; ma quel somaro
del Conte non è degno di stare in conversazione con noi.

CAVALIERE:

Caro Marchese, compatitemi; rispettate gli altri, se volete essere
rispettato voi pure.

MARCHESE:

Sapete il mio naturale. Io fo le cortesie a tutti, ma colui non lo posso
soffrire.

CAVALIERE:

Non lo potete soffrire, perché vi è rivale in amore! Vergogna! Un
cavaliere della vostra sorta innamorarsi d\'una locandiera! Un uomo
savio, come siete voi, correr dietro a una donna!

MARCHESE:

Cavaliere mio, costei mi ha stregato.

CAVALIERE:

Oh! pazzie! debolezze! Che stregamenti! Che vuol dire che le donne non
mi stregheranno? Le loro fattucchierie consistono nei loro vezzi, nelle
loro lusinghe, e chi ne sta lontano, come fo io, non ci è pericolo che
si lasci ammaliare.

MARCHESE:

Basta! ci penso e non ci penso: quel che mi dà fastidio e che
m\'inquieta, è il mio fattor di campagna.

CAVALIERE:

Vi ha fatto qualche porcheria?

MARCHESE:

Mi ha mancato di parola.

SCENA TREDICESIMA {#Section0009.xhtml#sigil_toc_id_51}
-----------------

Il Servitore con una cioccolata e detti.

CAVALIERE:

Oh mi dispiace\... Fanne subito un\'altra.

(Al Servitore.)

SERVITORE:

In casa per oggi non ce n\'è altra, illustrissimo.

CAVALIERE:

Bisogna che ne provveda. Se vi degnate di questa\...

(Al Marchese.)

MARCHESE:

(prende la cioccolata, e si mette a berla senza complimenti, seguitando
poi a discorrere e bere, come segue)

Questo mio fattore, come io vi diceva\...

(Beve.)

CAVALIERE:

(Ed io resterò senza).

(Da sé.)

MARCHESE:

Mi aveva promesso mandarmi con l\'ordinario\...

(Beve.)

venti zecchini\...

(Beve.)

CAVALIERE:

(Ora viene con una seconda stoccata).

(Da sé.)

MARCHESE:

E non me li ha mandati\...

(Beve.)

CAVALIERE:

Li manderà un\'altra volta.

MARCHESE:

Il punto sta\... il punto sta\...

(Finisce di bere.)

Tenete.

(Dà la chicchera al Servitore.)

Il punto sta che sono in un grande impegno, e non so come fare.

CAVALIERE:

Otto giorni più, otto giorni meno\...

MARCHESE:

Ma voi che siete Cavaliere, sapete quel che vuol dire il mantener la
parola. Sono in impegno; e\... corpo di bacco! Darei della pugna in
cielo.

CAVALIERE:

Mi dispiace di vedervi scontento. (Se sapessi come uscirne con
riputazione!)

(Da sé.)

MARCHESE:

Voi avreste difficoltà per otto giorni di farmi il piacere?

CAVALIERE:

Caro Marchese, se potessi, vi servirei di cuore; se ne avessi, ve li
avrei esibiti a dirittura. Ne aspetto, e non ne ho.

MARCHESE:

Non mi darete ad intendere d\'esser senza denari.

CAVALIERE:

Osservate. Ecco tutta la mia ricchezza. Non arrivano a due zecchini.

(Mostra uno zecchino e varie monete.)

MARCHESE:

Quello è uno zecchino d\'oro.

CAVALIERE:

Sì; l\'ultimo, non ne ho più.

MARCHESE:

Prestatemi quello, che vedrò intanto\...

CAVALIERE:

Ma io poi\...

MARCHESE:

Di che avete paura? Ve lo renderò.

CAVALIERE:

Non so che dire; servitevi.

(Gli dà lo zecchino.)

MARCHESE:

Ho un affare di premura\... amico: obbligato per ora: ci rivedremo a
pranzo.

(Prende lo zecchino, e parte.)

SCENA QUATTORDICESIMA {#Section0009.xhtml#sigil_toc_id_52}
---------------------

CAVALIERE:

(solo)

Bravo! Il signor Marchese mi voleva frecciare venti zecchini, e poi si è
contentato di uno. Finalmente uno zecchino non mi preme di perderlo, e
se non me lo rende, non mi verrà più a seccare. Mi dispiace più, che mi
ha bevuto la mia cioccolata. Che indiscretezza! E poi: son chi sono. Son
Cavaliere. Oh garbatissimo Cavaliere!

SCENA QUINDICESIMA {#Section0009.xhtml#sigil_toc_id_53}
------------------

Mirandolina colla biancheria, e detto.

MIRANDOLINA:

Permette, illustrissimo?

(Entrando con qualche soggezione.)

CAVALIERE:

Che cosa volete?

(Con asprezza.)

MIRANDOLINA:

Ecco qui della biancheria migliore.

(S\'avanza un poco.)

CAVALIERE:

Bene. Mettetela lì.

(Accenna il tavolino.)

MIRANDOLINA:

La supplico almeno degnarsi vedere se è di suo genio.

CAVALIERE:

Che roba è?

MIRANDOLINA:

Le lenzuola son di rensa.

(S\'avanza ancor più.)

CAVALIERE:

Rensa?

MIRANDOLINA:

Sì signore, di dieci paoli al braccio. Osservi.

CAVALIERE:

Non pretendevo tanto. Bastavami qualche cosa meglio di quel che mi avete
dato.

MIRANDOLINA:

Questa biancheria l\'ho fatta per personaggi di merito: per quelli che
la sanno conoscere; e in verità, illustrissimo, la do per esser lei, ad
un altro non la darei.

CAVALIERE:

Per esser lei! Solito complimento.

MIRANDOLINA:

Osservi il servizio di tavola.

CAVALIERE:

Oh! Queste tele di Fiandra, quando si lavano, perdono assai. Non vi è
bisogno che le insudiciate per me.

MIRANDOLINA:

Per un Cavaliere della sua qualità, non guardo a queste piccole cose. Di
queste salviette ne ho parecchie, e le serberò per V.S. illustrissima.

CAVALIERE:

(Non si può però negare, che costei non sia una donna obbligante).

(Da sé.)

MIRANDOLINA:

(Veramente ha una faccia burbera da non piacergli le donne).

(Da sé.)

CAVALIERE:

Date la mia biancheria al mio cameriere, o ponetela lì, in qualche
luogo. Non vi è bisogno che v\'incomodiate per questo.

MIRANDOLINA:

Oh, io non m\'incomodo mai, quando servo Cavaliere di sì alto merito.

CAVALIERE:

Bene, bene, non occorr\'altro. (Costei vorrebbe adularmi. Donne! Tutte
così).

(Da sé.)

MIRANDOLINA:

La metterò nell\'arcova.

CAVALIERE:

Sì, dove volete.

(Con serietà.)

MIRANDOLINA:

(Oh! vi è del duro. Ho paura di non far niente).

(Da sé, va a riporre la biancheria.)

CAVALIERE:

(I gonzi sentono queste belle parole, credono a chi le dice, e cascano).

(Da sè.)

MIRANDOLINA:

A pranzo, che cosa comanda?

(Ritornando senza la biancheria.)

CAVALIERE:

Mangerò quello che vi sarà.

MIRANDOLINA:

Vorrei pur sapere il suo genio. Se le piace una cosa più dell\'altra, lo
dica con libertà.

CAVALIERE:

Se vorrò qualche cosa, lo dirò al cameriere.

MIRANDOLINA:

Ma in queste cose gli uomini non hanno l\'attenzione e la pazienza che
abbiamo noi donne. Se le piacesse qualche intingoletto, qualche
salsetta, favorisca di dirlo a me.

CAVALIERE:

Vi ringrazio: ma né anche per questo verso vi riuscirà di far con me
quello che avete fatto col Conte e col Marchese.

MIRANDOLINA:

Che dice della debolezza di quei due cavalieri? Vengono alla locanda per
alloggiare, e pretendono poi di voler fare all\'amore colla locandiera.
Abbiamo altro in testa noi, che dar retta alle loro ciarle. Cerchiamo di
fare il nostro interesse; se diamo loro delle buone parole, lo facciamo
per tenerli a bottega; e poi, io principalmente, quando vedo che si
lusingano, rido come una pazza.

CAVALIERE:

Brava! Mi piace la vostra sincerità.

MIRANDOLINA:

Oh! non ho altro di buono, che la sincerità.

CAVALIERE:

Ma però, con chi vi fa la corte, sapete fingere.

MIRANDOLINA:

Io fingere? Guardimi il cielo. Domandi un poco a quei due signori che
fanno gli spasimati per me, se ho mai dato loro un segno d\'affetto. Se
ho mai scherzato con loro in maniera che si potessero lusingare con
fondamento. Non li strapazzo, perché il mio interesse non lo vuole, ma
poco meno. Questi uomini effeminati non li posso vedere. Sì come
abborrisco anche le donne che corrono dietro agli uomini. Vede? Io non
sono una ragazza. Ho qualche annetto; non sono bella, ma ho avute delle
buone occasioni; eppure non ho mai voluto maritarmi, perché stimo
infinitamente la mia libertà.

CAVALIERE:

Oh sì, la libertà è un gran tesoro.

MIRANDOLINA:

E tanti la perdono scioccamente.

CAVALIERE:

So io ben quel che faccio. Alla larga.

MIRANDOLINA:

Ha moglie V.S. illustrissima?

CAVALIERE:

Il cielo me ne liberi. Non voglio donne.

MIRANDOLINA:

Bravissimo. Si conservi sempre così. Le donne, signore\... Basta, a me
non tocca a dirne male.

CAVALIERE:

Voi siete per altro la prima donna, ch\'io senta parlar così.

MIRANDOLINA:

Le dirò: noi altre locandiere vediamo e sentiamo delle cose assai; e in
verità compatisco quegli uomini, che hanno paura del nostro sesso.

CAVALIERE:

(È curiosa costei).

(Da sé.)

MIRANDOLINA:

Con permissione di V.S. illustrissima.

(Finge voler partire.)

CAVALIERE:

Avete premura di partire?

MIRANDOLINA:

Non vorrei esserle importuna.

CAVALIERE:

No, mi fate piacere; mi divertite.

MIRANDOLINA:

Vede, signore? Così fo con gli altri. Mi trattengo qualche momento; sono
piuttosto allegra, dico delle barzellette per divertirli, ed essi subito
credono\... Se la m\'intende, e mi fanno i cascamorti.

CAVALIERE:

Questo accade, perché avete buona maniera.

MIRANDOLINA:

Troppa bontà, illustrissimo.

(Con una riverenza.)

CAVALIERE:

Ed essi s\'innamorano.

MIRANDOLINA:

Guardi che debolezza! Innamorarsi subito di una donna!

CAVALIERE:

Questa io non l\'ho mai potuta capire.

MIRANDOLINA:

Bella fortezza! Bella virilità!

CAVALIERE:

Debolezze! Miserie umane!

MIRANDOLINA:

Questo è il vero pensare degli uomini. Signor Cavaliere, mi porga la
mano.

CAVALIERE:

Perché volete ch\'io vi porga la mano?

MIRANDOLINA:

Favorisca; si degni; osservi, sono pulita.

CAVALIERE:

Ecco la mano.

MIRANDOLINA:

Questa è la prima volta, che ho l\'onore d\'aver per la mano un uomo,
che pensa veramente da uomo.

CAVALIERE:

Via, basta così.

(Ritira la mano.)

MIRANDOLINA:

Ecco. Se io avessi preso per la mano uno di que\' due signori sguaiati,
avrebbe tosto creduto ch\'io spasimassi per lui. Sarebbe andato in
deliquio. Non darei loro una semplice libertà, per tutto l\'oro del
mondo. Non sanno vivere. Oh benedetto in conversare alla libera! senza
attacchi, senza malizia, senza tante ridicole scioccherie.
Illustrissimo, perdoni la mia impertinenza. Dove posso servirla, mi
comandi con autorità, e avrò per lei quell\'attenzione, che non ho mai
avuto per alcuna persona di questo mondo.

CAVALIERE:

Per quale motivo avete tanta parzialità per me?

MIRANDOLINA:

Perché, oltre il suo merito, oltre la sua condizione, sono almeno sicura
che con lei posso trattare con libertà, senza sospetto che voglia fare
cattivo uso delle mie attenzioni, e che mi tenga in qualità di serva,
senza tormentarmi con pretensioni ridicole, con caricature affettate.

CAVALIERE:

(Che diavolo ha costei di stravagante, ch\'io non capisco!).

(Da sé.)

MIRANDOLINA:

(Il satiro si anderà a poco a poco addomesticando).

(Da sé.)

CAVALIERE:

Orsù, se avete da badare alle cose vostre, non restate per me.

MIRANDOLINA:

Sì signore, vado ad attendere alle faccende di casa. Queste sono i miei
amori, i miei passatempi. Se comanderà qualche cosa, manderò il
cameriere.

CAVALIERE:

Bene\... Se qualche volta verrete anche voi, vi vedrò volentieri.

MIRANDOLINA:

Io veramente non vado mai nelle camere dei forestieri, ma da lei ci
verrò qualche volta.

CAVALIERE:

Da me\... Perché?

MIRANDOLINA:

Perché, illustrissimo signore, ella mi piace assaissimo.

CAVALIERE:

Vi piaccio io?

MIRANDOLINA:

Mi piace, perché non è effeminato, perché non è di quelli che
s\'innamorano. (Mi caschi il naso, se avanti domani non l\'innamoro).

(Da sé.)

SCENA SEDICESIMA {#Section0009.xhtml#sigil_toc_id_54}
----------------

CAVALIERE:

(solo)

Eh! So io quel che fo. Colle donne? Alla larga. Costei sarebbe una di
quelle che potrebbero farmi cascare più delle altre. Quella verità,
quella scioltezza di dire, è cosa poco comune. Ha un non so che di
estraordinario; ma non per questo mi lascerei innamorare. Per un poco di
divertimento, mi fermerei più tosto con questa che con un\'altra. Ma per
fare all\'amore? Per perdere la libertà? Non vi è pericolo. Pazzi, pazzi
quelli che s\'innamorano delle donne.

(Parte.)

SCENA DICIASSETTESIMA {#Section0009.xhtml#sigil_toc_id_55}
---------------------

Altra camera di locanda.\
Ortensia, Dejanira, Fabrizio.

FABRIZIO:

Che restino servite qui, illustrissime. Osservino quest\'altra camera.
Quella per dormire, e questa per mangiare, per ricevere, per servirsene
come comandano.

ORTENSIA:

Va bene, va bene. Siete voi padrone, o cameriere?

FABRIZIO:

Cameriere, ai comandi di V.S. illustrissima.

DEJANIRA:

(Ci dà delle illustrissime).

(Piano a Ortensia, ridendo.)

ORTENSIA:

(Bisogna secondare il lazzo).

Cameriere?

FABRIZIO:

Illustrissima.

ORTENSIA:

Dite al padrone che venga qui, voglio parlar con lui per il trattamento.

FABRIZIO:

Verrà la padrona; la servo subito. (Chi diamine saranno queste due
signore così sole? All\'aria, all\'abito, paiono dame).

(Da sé, parte.)

SCENA DICIOTTESIMA {#Section0009.xhtml#sigil_toc_id_56}
------------------

Dejanira e Ortensia.

DEJANIRA:

Ci dà dell\'illustrissime. Ci ha creduto due dame.

ORTENSIA:

Bene. Così ci tratterà meglio.

DEJANIRA:

Ma ci farà pagare di più.

ORTENSIA:

Eh, circa i conti, avrà da fare con me. Sono degli anni assai, che
cammino il mondo.

DEJANIRA:

Non vorrei che con questi titoli entrassimo in qualche impegno.

ORTENSIA:

Cara amica, siete di poco spirito. Due commedianti avvezze a far sulla
scena da contesse, da marchese e da principesse, avranno difficoltà a
sostenere un carattere sopra di una locanda?

DEJANIRA:

Verranno i nostri compagni, e subito ci sbianchiranno.

ORTENSIA:

Per oggi non possono arrivare a Firenze. Da Pisa a qui in navicello vi
vogliono almeno tre giorni.

DEJANIRA:

Guardate che bestialità! Venire in navicello!

ORTENSIA:

Per mancanza di lugagni. È assai che siamo venute noi in calesse.

DEJANIRA:

È stata buona quella recita di più che abbiamo fatto.

ORTENSIA:

Sì, ma se non istavo io alla porta, non si faceva niente.

SCENA DICIANNOVESIMA {#Section0009.xhtml#sigil_toc_id_57}
--------------------

Fabrizio e dette.

FABRIZIO:

La padrona or ora sarà a servirle.

ORTENSIA:

Bene.

FABRIZIO:

Ed io le supplico a comandarmi. Ho servito altre dame: mi darò l\'onor
di servir con tutta l\'attenzione anche le signorie loro illustrissime.

ORTENSIA:

Occorrendo, mi varrò di voi.

DEJANIRA:

(Ortensia queste parti le fa benissimo).

(Da sé.)

FABRIZIO:

Intanto le supplico, illustrissime signore, favorirmi il loro riverito
nome per la consegna.

(Tira fuori un calamaio ed un libriccino.)

DEJANIRA:

(Ora viene il buono).

ORTENSIA:

Perché ho da dar il mio nome?

FABRIZIO:

Noialtri locandieri siamo obbligati a dar il nome, il casato, la patria
e la condizione di tutti i passeggeri che alloggiano alla nostra
locanda. E se non lo facessimo, meschini noi.

DEJANIRA:

(Amica, i titoli sono finiti).

(Piano ad Ortensia.)

ORTENSIA:

Molti daranno anche il nome finto.

FABRIZIO:

In quanto a questo poi, noialtri scriviamo il nome che ci dettano, e non
cerchiamo di più.

ORTENSIA:

Scrivete. La Baronessa Ortensia del Poggio, palermitana.

FABRIZIO:

(Siciliana? Sangue caldo).

(Scrivendo.)

Ella, illustrissima?

(A Dejanira.)

DEJANIRA:

Ed io\... (Non so che mi dire).

ORTENSIA:

Via, Contessa Dejanira, dategli il vostro nome.

FABRIZIO:

Vi supplico.

(A Dejanira.)

DEJANIRA:

Non l\'avete sentito?

(A Fabrizio.)

FABRIZIO:

L\'illustrissima signora Contessa Dejanira\...

(Scrivendo.)

Il cognome?

DEJANIRA:

Anche il cognome?

(A Fabrizio.)

ORTENSIA:

Sì, dal Sole, romana.

(A Fabrizio.)

FABRIZIO:

Non occorr\'altro. Perdonino l\'incomodo. Ora verrà la padrona. (L\'ho
io detto, che erano due dame? Spero che farò de\' buoni negozi. Mancie
non ne mancheranno).

(Parte.)

DEJANIRA:

Serva umilissima della signora Baronessa.

ORTENSIA:

Contessa, a voi m\'inchino.

(Si burlano vicendevolmente.)

DEJANIRA:

Qual fortuna mi offre la felicissima congiuntura di rassegnarvi il mio
profondo rispetto?

ORTENSIA:

Dalla fontana del vostro cuore scaturir non possono che torrenti di
grazie.

SCENA VENTESIMA {#Section0009.xhtml#sigil_toc_id_58}
---------------

Mirandolina e dette.

DEJANIRA:

Madama, voi mi adulate.

(Ad Ortensia, con caricatura.)

ORTENSIA:

Contessa, al vostro merito ci converrebbe assai più.

(Fa lo stesso.)

MIRANDOLINA:

(Oh che dame cerimoniose).

(Da sé, in disparte.)

DEJANIRA:

(Oh quanto mi vien da ridere!).

(Da sé.)

ORTENSIA:

Zitto: è qui la padrona.

(Piano a Dejanira.)

MIRANDOLINA:

M\'inchino a queste dame.

ORTENSIA:

Buon giorno, quella giovane.

DEJANIRA:

Signora padrona, vi riverisco.

(A Mirandolina.)

ORTENSIA:

Ehi!

(Fa cenno a Dejanira, che si sostenga.)

MIRANDOLINA:

Permetta ch\'io le baci la mano.

(Ad Ortensia.)

ORTENSIA:

Siete obbligante.

(Le dà la mano.)

DEJANIRA:

(ride da sé.)

MIRANDOLINA:

Anche ella, illustrissima.

(Chiede la mano a Dejanira.)

DEJANIRA:

Eh, non importa\...

ORTENSIA:

Via, gradite le finezze di questa giovane. Datele la mano.

MIRANDOLINA:

La supplico.

DEJANIRA:

Tenete.

(Le dà la mano, si volta, e ride.)

MIRANDOLINA:

Ride, illustrissima? Di che?

ORTENSIA:

Che cara Contessa! Ride ancora di me. Ho detto uno sproposito, che l\'ha
fatta ridere.

MIRANDOLINA:

(Io giuocherei che non sono dame. Se fossero dame, non sarebbero sole).

(Da sé.)

ORTENSIA:

Circa il trattamento, converrà poi discorrere.

(A Mirandolina.)

MIRANDOLINA:

Ma! Sono sole? Non hanno cavalieri, non hanno servitori, non hanno
nessuno?

ORTENSIA:

Il Barone mio marito\...

DEJANIRA:

(ride forte).

MIRANDOLINA:

Perché ride, signora?

(A Dejanira.)

ORTENSIA:

Via, perché ridete?

DEJANIRA:

Rido del Barone di vostro marito.

ORTENSIA:

Sì, è un Cavaliere giocoso: dice sempre delle barzellette; verrà quanto
prima col Conte Orazio, marito della Contessina.

DEJANIRA

(fa forza per trattenersi dal ridere).

MIRANDOLINA:

La fa ridere anche il signor Conte?

(A Dejanira.)

ORTENSIA:

Ma via, Contessina, tenetevi un poco nel vostro decoro.

MIRANDOLINA:

Signore mie, favoriscano in grazia. Siamo sole, nessuno ci sente. Questa
contea, questa baronia, sarebbe mai\...

ORTENSIA:

Che cosa vorreste voi dire? Mettereste in dubbio la nostra nobiltà?

MIRANDOLINA:

Perdoni, illustrissima, non si riscaldi, perché farà ridere la signora
Contessa.

DEJANIRA:

Eh via, che serve?

ORTENSIA:

Contessa, Contessa!

(Minacciandola.)

MIRANDOLINA:

Io so che cosa voleva dire, illustrissima.

(A Dejanira.)

DEJANIRA:

Se l\'indovinate, vi stimo assai.

MIRANDOLINA:

Volevate dire: Che serve che fingiamo d\'esser due dame, se siamo due
pedine? Ah! non è vero?

DEJANIRA:

E che sì che ci conoscete?

(A Mirandolina.)

ORTENSIA:

Che brava commediante! Non è buona da sostenere un carattere.

DEJANIRA:

Fuori di scena io non so fingere.

MIRANDOLINA:

Brava, signora Baronessa; mi piace il di lei spirito. Lodo la sua
franchezza.

ORTENSIA:

Qualche volta mi prendo un poco di spasso.

MIRANDOLINA:

Ed io amo infinitamente le persone di spirito. Servitevi pure nella mia
locanda, che siete padrone; ma vi prego bene, se mi capitassero persone
di rango, cedermi quest\'appartamento, ch\'io vi darò dei camerini assai
comodi.

DEJANIRA:

Sì, volentieri.

ORTENSIA:

Ma io, quando spendo il mio denaro, intendo volere esser servita come
una dama, e in questo appartamento ci sono, e non me ne anderò.

MIRANDOLINA:

Via, signora Baronessa, sia buona\... Oh! Ecco un cavaliere che è
alloggiato in questa locanda. Quando vede donne, sempre si caccia
avanti.

ORTENSIA:

È ricco?

MIRANDOLINA:

Io non so i fatti suoi.

SCENA VENTUNESIMA {#Section0009.xhtml#sigil_toc_id_59}
-----------------

Il Marchese e dette.

MARCHESE:

È permesso? Si può entrare?

ORTENSIA:

Per me è padrone.

MARCHESE:

Servo di lor signore.

DEJANIRA:

Serva umilissima.

ORTENSIA:

La riverisco divotamente.

MARCHESE:

Sono forestiere?

(A Mirandolina.)

MIRANDOLINA:

Eccellenza sì. Sono venute ad onorare la mia locanda.

ORTENSIA:

(È un\'Eccellenza! Capperi!).

(Da sé.)

DEJANIRA:

(Già Ortensia lo vorrà per sé).

(Da sé.)

MARCHESE:

E chi sono queste signore?

(A Mirandolina.)

MIRANDOLINA:

Questa è la Baronessa Ortensia del Poggio, e questa la Contessa Dejanira
dal Sole.

MARCHESE:

Oh compitissime dame!

ORTENSIA:

E ella chi è, signore?

MARCHESE:

Io sono il Marchese di Forlipopoli.

DEJANIRA:

(La locandiera vuol seguitare a far la commedia).

(Da sé.)

ORTENSIA:

Godo aver l\'onore di conoscere un cavaliere così compito.

MARCHESE:

Se vi potessi servire, comandatemi. Ho piacere che siate venute ad
alloggiare in questa locanda. Troverete una padrona di garbo.

MIRANDOLINA:

Questo cavaliere è pieno di bontà. Mi onora della sua protezione.

MARCHESE:

Sì, certamente. Io la proteggo, e proteggo tutti quelli che vengono
nella sua locanda; e se vi occorre nulla, comandate.

ORTENSIA:

Occorrendo, mi prevarrò delle sue finezze.

MARCHESE:

Anche voi, signora Contessa, fate capitale di me.

DEJANIRA:

Potrò ben chiamarmi felice, se avrò l\'alto onore di essere annoverata
nel ruolo delle sue umilissime serve.

MIRANDOLINA:

(Ha detto un concetto da commedia).

(Ad Ortensia.)

ORTENSIA:

(Il titolo di Contessa l\'ha posta in soggezione).

(A Mirandolina.)\
(Il Marchese tira fuori di tasca un bel fazzoletto di seta, lo spiega, e
finge volersi asciugar la fronte.)

MIRANDOLINA:

Un gran fazzoletto, signor Marchese!

MARCHESE:

Ah! Che ne dite? È bello? Sono di buon gusto io?

(A Mirandolina.)

MIRANDOLINA:

Certamente è di ottimo gusto.

MARCHESE:

Ne avete più veduti di così belli?

(Ad Ortensia.)

ORTENSIA:

È superbo. Non ho veduto il compagno. (Se me lo donasse, lo prenderei).

(Da sé.)

MARCHESE:

Questo viene da Londra.

(A Dejanira.)

DEJANIRA:

È bello, mi piace assai.

MARCHESE:

Son di buon gusto io?

DEJANIRA:

(E non dice a\' vostri comandi).

(Da sé.)

MARCHESE:

M\'impegno che il Conte non sa spendere. Getta via il denaro, e non
compra mai una galanteria di buon gusto.

MIRANDOLINA:

Il signor Marchese conosce, distingue, sa, vede, intende.

MARCHESE:

(piega il fazzoletto con attenzione)

Bisogna piegarlo bene, acciò non si guasti. Questa sorta di roba bisogna
custodirla con attenzione. Tenete.

(Lo presenta a Mirandolina.)

MIRANDOLINA:

Vuole ch\'io lo faccia mettere nella sua camera?

MARCHESE:

No. Mettetelo nella vostra.

MIRANDOLINA:

Perché\... nella mia?

MARCHESE:

Perché\... ve lo dono.

MIRANDOLINA:

Oh, Eccellenza, perdoni\...

MARCHESE:

Tant\'è. Ve lo dono.

MIRANDOLINA:

Ma io non voglio.

MARCHESE:

Non mi fate andar in collera.

MIRANDOLINA:

Oh, in quanto a questo poi, il signor Marchese lo sa, io non voglio
disgustar nessuno. Acciò non vada in collera, lo prenderò.

DEJANIRA:

(Oh che bel lazzo!).

(Ad Ortensia.)

ORTENSIA:

(E poi dicono delle commedianti).

(A Dejanira.)

MARCHESE:

Ah! Che dite? Un fazzoletto di quella sorta, l\'ho donato alla mia
padrona di casa.

(Ad Ortensia.)

ORTENSIA:

È un cavaliere generoso.

MARCHESE:

Sempre così.

MIRANDOLINA:

(Questo è il primo regalo che mi ha fatto, e non so come abbia avuto
quel fazzoletto).

(Da sé.)

DEJANIRA:

Signor Marchese, se ne trovano di quei fazzoletti in Firenze? Avrei
volontà d\'averne uno compagno.

MARCHESE:

Compagno di questo sarà difficile; ma vedremo.

MIRANDOLINA:

(Brava la signora Contessina).

(Da sé.)

ORTENSIA:

Signor Marchese, voi che siete pratico della città, fatemi il piacere di
mandarmi un bravo calzolaro, perché ho bisogno di scarpe.

MARCHESE:

Sì, vi manderò il mio.

MIRANDOLINA:

(Tutte alla vita; ma non ce n\'è uno per la rabbia).

(Da sé.)

ORTENSIA:

Caro signor Marchese, favorirà tenerci un poco di compagnia.

DEJANIRA:

Favorirà a pranzo con noi.

MARCHESE:

Sì, volentieri. (Ehi Mirandolina, non abbiate gelosia, son vostro, già
lo sapete).

MIRANDOLINA:

(S\'accomodi pure: ho piacere che si diverta).

(Al Marchese.)

ORTENSIA:

Voi sarete la nostra conversazione.

DEJANIRA:

Non conosciamo nessuno. Non abbiamo altri che voi.

MARCHESE:

Oh care le mie damine! Vi servirò di cuore.

SCENA VENTIDUESIMA {#Section0009.xhtml#sigil_toc_id_60}
------------------

Il Conte e detti.

CONTE:

Mirandolina, io cercava voi.

MIRANDOLINA:

Son qui con queste dame.

CONTE:

Dame? M\'inchino umilmente.

ORTENSIA:

Serva divota. (Questo è un guasco più badia! di quell\'altro).

(Piano a Dejanira.)

DEJANIRA:

(Ma io non sono buona per miccheggiare).

(Piano ad Ortensia.)

MARCHESE:

(Ehi! Mostrate al Conte il fazzoletto).

(Piano a Mirandolina.)

MIRANDOLINA:

Osservi signor Conte, il bel regalo che mi ha fatto il signor Marchese.

(Mostra il fazzoletto al Conte.)

CONTE:

Oh, me ne rallegro! Bravo, signor Marchese.

MARCHESE:

Eh niente, niente. Bagattelle. Riponetelo via; non voglio che lo
diciate. Quel che fo, non s\'ha da sapere.

MIRANDOLINA:

(Non s\'ha da sapere, e me lo fa mostrare. La superbia contrasta con la
povertà).

(Da sé.)

CONTE:

Con licenza di queste dame, vorrei dirvi una parola.

(A Mirandolina.)

ORTENSIA:

S\'accomodi con libertà.

MARCHESE:

Quel fazzoletto in tasca lo manderete a male.

(A Mirandolina.)

MIRANDOLINA:

Eh, lo riporrò nella bambagia, perché non si ammacchi!

CONTE:

Osservate questo piccolo gioiello di diamanti.

(A Mirandolina.)

MIRANDOLINA:

Bello assai.

CONTE:

È compagno degli orecchini che vi ho donato.

(Ortensia e Dejanira osservano, e parlano piano fra loro.)

MIRANDOLINA:

Certo è compagno, ma è ancora più bello.

MARCHESE:

(Sia maledetto il Conte, i suoi diamanti, i suoi denari, e il suo
diavolo che se lo porti).

(Da sé.)

CONTE:

Ora, perché abbiate il fornimento compagno, ecco ch\'io vi dono il
gioiello.

(A Mirandolina.)

MIRANDOLINA:

Non lo prendo assolutamente.

CONTE:

Non mi farete questa male creanza.

MIRANDOLINA:

Oh! delle male creanze non ne faccio mai. Per non disgustarla, lo
prenderò.

(Ortensia e Dejanira parlano come sopra, osservando la generosità del
Conte.)

MIRANDOLINA:

Ah! Che ne dice, signor Marchese? Questo gioiello non è galante?

MARCHESE:

Nel suo genere il fazzoletto è più di buon gusto.

CONTE:

Sì, ma da genere a genere vi è una bella distanza.

MARCHESE:

Bella cosa! Vantarsi in pubblico di una grande spesa.

CONTE:

Sì, sì, voi fate i vostri regali in segreto.

MIRANDOLINA:

(Posso ben dire con verità questa volta, che fra due litiganti il terzo
gode).

(Da sé.)

MARCHESE:

E così, damine mie, sarò a pranzo con voi.

ORTENSIA:

Quest\'altro signore chi è?

(Al Conte.)

CONTE:

Sono il Conte d\'Albafiorita, per obbedirvi.

DEJANIRA:

Capperi! È una famiglia illustre, io la conosco.

(Anch\'ella s\'accosta al Conte.)

CONTE:

Sono a\' vostri comandi.

(A Dejanira.)

ORTENSIA:

È qui alloggiato?

(Al Conte.)

CONTE:

Sì, signora.

DEJANIRA:

Si trattiene molto?

(Al Conte.)

CONTE:

Credo di sì.

MARCHESE:

Signore mie, sarete stanche di stare in piedi, volete ch\'io vi serva
nella vostra camera?

ORTENSIA:

Obbligatissima.

(Con disprezzo.)

Di che paese è, signor Conte?

CONTE:

Napolitano.

ORTENSIA:

Oh! Siamo mezzi patrioti. Io sono palermitana.

DEJANIRA:

Io son romana; ma sono stata a Napoli, e appunto per un mio interesse
desiderava parlare con un cavaliere napolitano.

CONTE:

Vi servirò, signore. Siete sole? Non avete uomini?

MARCHESE:

Ci sono io, signore: e non hanno bisogno di voi.

ORTENSIA:

Siamo sole, signor Conte. Poi vi diremo il perché.

CONTE:

Mirandolina.

MIRANDOLINA:

Signore.

CONTE:

Fate preparare nella mia camera per tre. Vi degnerete di favorirmi?

(Ad Ortensia e Dejanira.)

ORTENSIA:

Riceveremo le vostre finezze.

MARCHESE:

Ma io sono stato invitato da queste dame.

CONTE:

Esse sono padrone di servirsi come comandano, ma alla mia piccola tavola
in più di tre non ci si sta.

MARCHESE:

Vorrei veder anche questa\...

ORTENSIA:

Andiamo, andiamo, signor Conte. Il signor Marchese ci favorirà un\'altra
volta.

(Parte.)

DEJANIRA:

Signor Marchese, se trova il fazzoletto, mi raccomando.

(Parte.)

MARCHESE:

Conte, Conte, voi me la pagherete.

CONTE:

Di che vi lagnate?

MARCHESE:

Son chi sono, e non si tratta così. Basta\... Colei vorrebbe un
fazzoletto? Un fazzoletto di quella sorta? Non l\'avrà. Mirandolina,
tenetelo caro. Fazzoletti di quella sorta non se ne trovano. Dei
diamanti se ne trovano, ma dei fazzoletti di quella sorta non se ne
trovano.

(Parte.)

MIRANDOLINA:

(Oh che bel pazzo!).

(Da sé.)

CONTE:

Cara Mirandolina, avrete voi dispiacere ch\'io serva queste due dame?

MIRANDOLINA:

Niente affatto, signore.

CONTE:

Lo faccio per voi. Lo faccio per accrescer utile ed avventori alla
vostra locanda; per altro io son vostro, è vostro il mio cuore, e vostre
son le mie ricchezze, delle quali disponetene liberamente, che io vi
faccio padrona.

(Parte.)

SCENA VENTITREESIMA {#Section0009.xhtml#sigil_toc_id_61}
-------------------

MIRANDOLINA:

(sola)

Con tutte le sue ricchezze, con tutti li suoi regali, non arriverà mai
ad innamorarmi; e molto meno lo farà il Marchese colla sua ridicola
protezione. Se dovessi attaccarmi ad uno di questi due, certamente lo
farei con quello che spende più. Ma non mi preme né dell\'uno, né
dell\'altro. Sono in impegno d\'innamorar il Cavaliere di Ripafratta, e
non darei un tal piacere per un gioiello il doppio più grande di questo.
Mi proverò; non so se avrò l\'abilità che hanno quelle due brave
comiche, ma mi proverò. Il Conte ed il Marchese, frattanto che con
quelle si vanno trattenendo, mi lasceranno in pace; e potrò a mio
bell\'agio trattar col Cavaliere. Possibile ch\'ei non ceda? Chi è
quello che possa resistere ad una donna, quando le dà tempo di poter far
uso dell\'arte sua? Chi fugge non può temer d\'esser vinto, ma chi si
ferma, chi ascolta, e se ne compiace, deve o presto o tardi a suo
dispetto cadere.

(Parte.)

[]{#Section0010.xhtml}

ATTO SECONDO
============

SCENA PRIMA {#Section0010.xhtml#sigil_toc_id_62}
-----------

Camera del Cavaliere, con tavola apparecchiata per il pranzo e sedie.\
Il Cavaliere ed il suo Servitore, poi Fabrizio.\
Il Cavaliere passeggia con un libro.\
Fabrizio mette la zuppa in tavola.

FABRIZIO:

Dite al vostro padrone, se vuol restare servito, che la zuppa è in
tavola.

(Al Servitore.)

SERVITORE:

Glielo potete dire anche voi.

(A Fabrizio.)

FABRIZIO:

È tanto stravagante, che non gli parlo niente volentieri.

SERVITORE:

Eppure non è cattivo. Non può veder le donne, per altro cogli uomini è
dolcissimo.

FABRIZIO:

(Non può veder le donne? Povero sciocco! Non conosce il buono).

(Da sé, parte.)

SERVITORE:

Illustrissimo, se comoda, è in tavola.

(Il Cavaliere mette giù il libro, e va a sedere a tavola.)

CAVALIERE:

Questa mattina parmi che si pranzi prima del solito.

(Al Servitore, mangiando.)\
(Il Servitore dietro la sedia del Cavaliere, col tondo sotto il
braccio.)

SERVITORE:

Questa camera è stata servita prima di tutte. Il signor Conte
d\'Albafiorita strepitava che voleva essere servito il primo, ma la
padrona ha voluto che si desse in tavola prima a V.S. illustrissima.

CAVALIERE:

Sono obbligato a costei per l\'attenzione che mi dimostra.

SERVITORE:

È una assai compita donna, illustrissimo. In tanto mondo che ho veduto,
non ho trovato una locandiera più garbata di questa.

CAVALIERE:

Ti piace, eh?

(Voltandosi un poco indietro.)

SERVITORE:

Se non fosse per far torto al mio padrone, vorrei venire a stare con
Mirandolina per cameriere.

CAVALIERE:

Povero sciocco! Che cosa vorresti ch\'ella facesse di te?

(Gli dà il tondo, ed egli lo muta.)

SERVITORE:

Una donna di questa sorta, la vorrei servir come un cagnolino.

(Va per un piatto.)

CAVALIERE:

Per bacco! Costei incanta tutti. Sarebbe da ridere che incantasse anche
me. Orsù, domani me ne vado a Livorno. S\'ingegni per oggi, se può, ma
si assicuri che non sono sì debole. Avanti ch\'io superi l\'avversion
per le donne, ci vuol altro.

SCENA SECONDA {#Section0010.xhtml#sigil_toc_id_63}
-------------

Il Servitore col lesso ed un altro piatto, e detto.

SERVITORE:

Ha detto la padrona, che se non le piacesse il pollastro, le manderà un
piccione.

CAVALIERE:

Mi piace tutto. E questo che cos\'è?

SERVITORE:

Disse la padrona, ch\'io le sappia dire se a V.S. illustrissima piace
questa salsa, che l\'ha fatta ella colle sue mani.

CAVALIERE:

Costei mi obbliga sempre più.

(L\'assaggia.)

È preziosa. Dille che mi piace, che la ringrazio.

SERVITORE:

Glielo dirò, illustrissimo.

CAVALIERE:

Vaglielo a dir subito.

SERVITORE:

Subito. (Oh che prodigio! Manda un complimento a una donna!).

(Da sé, parte.)

CAVALIERE:

È una salsa squisita. Non ho sentita la meglio.

(Va mangiando.)

Certamente, se Mirandolina farà così, avrà sempre de\' forestieri. Buona
tavola, buona biancheria. E poi non si può negare che non sia gentile;
ma quel che più stimo in lei, è la sincerità. Oh, quella sincerità è
pure la bella cosa! Perché non posso io vedere le donne? Perché sono
finte, bugiarde, lusinghiere. Ma quella bella sincerità\...

SCENA TERZA {#Section0010.xhtml#sigil_toc_id_64}
-----------

Il servitore e detto.

SERVITORE:

Ringrazia V.S. illustrissima della bontà che ha d\'aggradire le sue
debolezze.

CAVALIERE:

Bravo, signor cerimoniere, bravo.

SERVITORE:

Ora sta facendo colle sue mani un altro piatto; non so dire che cosa
sia.

CAVALIERE:

Sta facendo?

SERVITORE:

Sì signore.

CAVALIERE:

Dammi da bere.

SERVITORE:

La servo.

(Va a prendere da bere.)

CAVALIERE:

Orsù, con costei bisognerà corrispondere con generosità. È troppo
compita; bisogna pagare il doppio. Trattarla bene, ma andar via presto.

(Il Servitore gli presenta da bere.)

CAVALIERE:

Il Conte è andato a pranzo?

(Beve.)

SERVITORE:

Illustrissimo sì, in questo momento. Oggi fa trattamento. Ha due dame a
tavola con lui.

CAVALIERE:

Due dame? Chi sono?

SERVITORE:

Sono arrivate a questa locanda poche ore sono. Non so chi sieno.

CAVALIERE:

Le conosceva il Conte?

SERVITORE:

Credo di no; ma appena le ha vedute, le ha invitate a pranzo seco.

CAVALIERE:

Che debolezza! Appena vede due donne, subito si attacca. Ed esse
accettano. E sa il cielo chi sono; ma sieno quali esser vogliono, sono
donne, e tanto basta. Il Conte si rovinerà certamente. Dimmi: il
Marchese è a tavola?

SERVITORE:

È uscito di casa, e non si è ancora veduto.

CAVALIERE:

In tavola.

(Fa mutare il tondo.)

SERVITORE:

La servo.

CAVALIERE:

A tavola con due dame! Oh che bella compagnia! Colle loro smorfie mi
farebbero passar l\'appetito.

SCENA QUARTA {#Section0010.xhtml#sigil_toc_id_65}
------------

Mirandolina con un tondo in mano, ed il Servitore, e detto.

MIRANDOLINA:

È permesso?

CAVALIERE:

Chi è di là?

SERVITORE:

Comandi.

CAVALIERE:

Leva là quel tondo di mano.

MIRANDOLINA:

Perdoni. Lasci ch\'io abbia l\'onore di metterlo in tavola colle mie
mani. (Mette in tavola la vivanda.)

CAVALIERE:

Questo non è offizio vostro.

MIRANDOLINA:

Oh signore, chi son io? Una qualche signora? Sono una serva di chi
favorisce venire alla mia locanda.

CAVALIERE:

(Che umiltà!).

(Da sé.)

MIRANDOLINA:

In verità, non avrei difficoltà di servire in tavola tutti, ma non lo
faccio per certi riguardi: non so s\'ella mi capisca. Da lei vengo senza
scrupoli, con franchezza.

CAVALIERE:

Vi ringrazio. Che vivanda è questa?

MIRANDOLINA:

Egli è un intingoletto fatto colle mie mani.

CAVALIERE:

Sarà buono. Quando lo avete fatto voi, sarà buono.

MIRANDOLINA:

Oh! troppa bontà, signore. Io non so far niente di bene; ma bramerei
saper fare, per dar nel genio ad un Cavalier sì compìto.

CAVALIERE:

(Domani a Livorno).

(Da sé.)

Se avete che fare, non istate a disagio per me.

MIRANDOLINA:

Niente, signore: la casa è ben provveduta di cuochi e servitori. Avrei
piacere di sentire, se quel piatto le dà nel genio.

CAVALIERE:

Volentieri, subito.

(Lo assaggia.)

Buono, prezioso. Oh che sapore! Non conosco che cosa sia.

MIRANDOLINA:

Eh, io, signore, ho de\' secreti particolari. Queste mani sanno far
delle belle cose!

CAVALIERE:

Dammi da bere.

(Al Servitore, con qualche passione.)

MIRANDOLINA:

Dietro questo piatto, signore, bisogna beverlo buono.

CAVALIERE:

Dammi del vino di Borgogna.

(Al Servitore.)

MIRANDOLINA:

Bravissimo. Il vino di Borgogna è prezioso. Secondo me, per pasteggiare
è il miglior vino che si possa bere.

(Il Servitore presenta la bottiglia in tavola, con un bicchiere.)

CAVALIERE:

Voi siete di buon gusto in tutto.

MIRANDOLINA:

In verità, che poche volte m\'inganno.

CAVALIERE:

Eppure questa volta voi v\'ingannate.

MIRANDOLINA:

In che, signore?

CAVALIERE:

In credere ch\'io meriti d\'essere da voi distinto.

MIRANDOLINA:

Eh, signor Cavaliere\...

(Sospirando.)

CAVALIERE:

Che cosa c\'è? Che cosa sono questi sospiri?

(Alterato.)

MIRANDOLINA:

Le dirò: delle attenzioni ne uso a tutti, e mi rattristo quando penso
che non vi sono che ingrati.

CAVALIERE:

Io non vi sarò ingrato.

(Con placidezza.)

MIRANDOLINA:

Con lei non pretendo di acquistar merito, facendo unicamente il mio
dovere.

CAVALIERE:

No, no, conosco benissimo\... Non sono cotanto rozzo quanto voi mi
credete. Di me non avrete a dolervi.

(Versa il vino nel bicchiere.)

MIRANDOLINA:

Ma\... signore\... io non l\'intendo.

CAVALIERE:

Alla vostra salute.

(Beve.)

MIRANDOLINA:

Obbligatissima; mi onora troppo.

CAVALIERE:

Questo vino è prezioso.

MIRANDOLINA:

Il Borgogna è la mia passione.

CAVALIERE:

Se volete, siete padrona.

(Le offerisce il vino.)

MIRANDOLINA:

Oh! Grazie, signore.

CAVALIERE:

Avete pranzato?

MIRANDOLINA:

Illustrissimo sì.

CAVALIERE:

Ne volete un bicchierino?

MIRANDOLINA:

Io non merito queste grazie.

CAVALIERE:

Davvero, ve lo do volentieri.

MIRANDOLINA:

Non so che dire. Riceverò le sue finezze.

CAVALIERE:

Porta un bicchiere.

(Al Servitore.)

MIRANDOLINA:

No, no, se mi permette: prenderò questo.

(Prende il bicchiere del Cavaliere.)

CAVALIERE:

Oibò. Me ne sono servito io.

MIRANDOLINA:

Beverò le sue bellezze.

(Ridendo.)\
(Il Servitore mette l\'altro bicchiere nella sottocoppa.)

CAVALIERE:

Eh galeotta!

(Versa il vino.)

MIRANDOLINA:

Ma è qualche tempo che ho mangiato: ho timore che mi faccia male.

CAVALIERE:

Non vi è pericolo.

MIRANDOLINA:

Se mi favorisse un bocconcino di pane\...

CAVALIERE:

Volentieri. Tenete.

(Le dà un pezzo di pane.)\
(Mirandolina col bicchiere in una mano, e nell\'altra il pane, mostra di
stare a disagio, e non saper come fare la zuppa.)

CAVALIERE:

Voi state in disagio. Volete sedere?

MIRANDOLINA:

Oh! Non son degna di tanto, signore.

CAVALIERE:

Via, via, siamo soli. Portale una sedia.

(Al Servitore.)

SERVITORE:

(Il mio padrone vuol morire: non ha mai fatto altrettanto.)

(Da sé; va a prendere la sedia.)

MIRANDOLINA:

Se lo sapessero il signor Conte ed il signor Marchese, povera me!

CAVALIERE:

Perché?

MIRANDOLINA:

Cento volte mi hanno voluto obbligare a bere qualche cosa, o a mangiare,
e non ho mai voluto farlo.

CAVALIERE:

Via, accomodatevi.

MIRANDOLINA:

Per obbedirla.

(Siede, e fa la zuppa nel vino.)

CAVALIERE:

Senti.

(Al Servitore, piano.)

(Non lo dire a nessuno, che la padrona sia stata a sedere alla mia
tavola).

SERVITORE:

(Non dubiti).

(Piano.)

(Questa novità mi sorprende).

(Da sé.)

MIRANDOLINA:

Alla salute di tutto quello che dà piacere al signor Cavaliere.

CAVALIERE:

Vi ringrazio, padroncina garbata.

MIRANDOLINA:

Di questo brindisi alle donne non ne tocca.

CAVALIERE:

No? Perché?

MIRANDOLINA:

Perché so che le donne non le può vedere.

CAVALIERE:

È vero, non le ho mai potute vedere.

MIRANDOLINA:

Si conservi sempre così.

CAVALIERE:

Non vorrei\...

(Si guarda dal Servitore.)

MIRANDOLINA:

Che cosa, signore?

CAVALIERE:

Sentite.

(Le parla nell\'orecchio.)

(Non vorrei che voi mi faceste mutar natura).

MIRANDOLINA:

Io, signore? Come?

CAVALIERE:

Va via.

(Al Servitore.)

SERVITORE:

Comanda in tavola?

CAVALIERE:

Fammi cucinare due uova, e quando son cotte, portale.

SERVITORE:

Come le comanda le uova?

CAVALIERE:

Come vuoi, spicciati.

SERVITORE:

Ho inteso. (Il padrone si va riscaldando).

(Da sé, parte.)

CAVALIERE:

Mirandolina, voi siete una garbata giovine.

MIRANDOLINA:

Oh signore, mi burla.

CAVALIERE:

Sentite. Voglio dirvi una cosa vera, verissima, che ritornerà in vostra
gloria.

MIRANDOLINA:

La sentirò volentieri.

CAVALIERE:

Voi siete la prima donna di questo mondo, con cui ho avuto la sofferenza
di trattar con piacere.

MIRANDOLINA:

Le dirò, signor Cavaliere: non già ch\'io meriti niente, ma alle volte
si danno questi sangui che s\'incontrano. Questa simpatia, questo genio,
si dà anche fra persone che non si conoscono. Anch\'io provo per lei
quello che non ho sentito per alcun altro.

CAVALIERE:

Ho paura che voi mi vogliate far perdere la mia quiete.

MIRANDOLINA:

Oh via, signor Cavaliere, se è un uomo savio, operi da suo pari. Non dia
nelle debolezze degli altri. In verità, se me n\'accorgo, qui non ci
vengo più. Anch\'io mi sento un non so che di dentro, che non ho più
sentito; ma non voglio impazzire per uomini, e molto meno per uno che ha
in odio le donne; e che forse forse per provarmi, e poi burlarsi di me,
viene ora con un discorso nuovo a tentarmi. Signor Cavaliere, mi
favorisca un altro poco di Borgogna.

CAVALIERE:

Eh! Basta\...

(Versa il vino in un bicchiere.)

MIRANDOLINA:

(Sta lì lì per cadere).

(Da sé.)

CAVALIERE:

Tenete.

(Le dà il bicchiere col vino.)

MIRANDOLINA:

Obbligatissima. Ma ella non beve?

CAVALIERE:

Sì, beverò. (Sarebbe meglio che io mi ubbriacassi. Un diavolo
scaccerebbe l\'altro).

(Da sé, versa il vino nel suo bicchiere.)

MIRANDOLINA:

Signor Cavaliere.

(Con vezzo.)

CAVALIERE:

Che c\'è?

MIRANDOLINA:

Tocchi.

(Gli fa toccare il bicchiere col suo.)

Che vivano i buoni amici.

CAVALIERE:

Che vivano.

(Un poco languente.)

MIRANDOLINA:

Viva\... chi si vuol bene\... senza malizia tocchi!

CAVALIERE:

Evviva\...

SCENA QUINTA {#Section0010.xhtml#sigil_toc_id_66}
------------

Il Marchese e detti.

MARCHESE:

Son qui ancor io. E che viva?

CAVALIERE:

Come, signor Marchese?

(Alterato.)

MARCHESE:

Compatite, amico. Ho chiamato. Non c\'è nessuno.

MIRANDOLINA:

Con sua licenza\...

(Vuol andar via.)

CAVALIERE:

Fermatevi.

(A Mirandolina.)

Io non mi prendo con voi cotanta libertà.

(Al Marchese.)

MARCHESE:

Vi domando scusa. Siamo amici. Credeva che foste solo. Mi rallegro
vedervi accanto alla nostra adorabile padroncina. Ah! Che dite? Non è un
capo d\'opera?

MIRANDOLINA:

Signore, io ero qui per servire il signor Cavaliere. Mi è venuto un poco
di male, ed egli mi ha soccorso con un bicchierin di Borgogna.

MARCHESE:

È Borgogna quello?

(Al Cavaliere.)

CAVALIERE:

Sì, è Borgogna.

MARCHESE:

Ma di quel vero?

CAVALIERE:

Almeno l\'ho pagato per tale.

MARCHESE:

Io me n\'intendo. Lasciate che lo senta, e vi saprò dire se è, o se non
è.

CAVALIERE:

Ehi!

(Chiama.)

SCENA SESTA {#Section0010.xhtml#sigil_toc_id_67}
-----------

Il Servitore colle ova, e detti.

CAVALIERE:

Un bicchierino al Marchese.

(Al Servitore.)

MARCHESE:

Non tanto piccolo il bicchierino. Il Borgogna non è liquore. Per
giudicarne bisogna beverne a sufficienza.

SERVITORE:

Ecco le ova.

(Vuol metterle in tavola.)

CAVALIERE:

Non voglio altro.

MARCHESE:

Che vivanda è quella?

CAVALIERE:

Ova.

MARCHESE:

Non mi piacciono.

(Il Servitore le porta via.)

MIRANDOLINA:

Signor Marchese, con licenza del signor Cavaliere, senta
quell\'intingoletto fatto colle mie mani.

MARCHESE:

Oh sì. Ehi. Una sedia.

(Il Servitore gli reca una sedia e mette il bicchiere sulla sottocoppa.)

Una forchetta.

CAVALIERE:

Via, recagli una posata.

(Il Servitore la va a prendere.)

MIRANDOLINA:

Signor Cavaliere, ora sto meglio. Me n\'anderò.

(S\'alza.)

MARCHESE:

Fatemi il piacere, restate ancora un poco.

MIRANDOLINA:

Ma signore, ho da attendere a\' fatti miei; e poi il signor
Cavaliere\...

MARCHESE:

Vi contentate ch\'ella resti ancora un poco?

(Al Cavaliere.)

CAVALIERE:

Che volete da lei?

MARCHESE:

Voglio farvi sentire un bicchierino di vin di Cipro che, da che siete al
mondo, non avrete sentito il compagno. E ho piacere che Mirandolina lo
senta, e dica il suo parere.

CAVALIERE:

Via, per compiacere il signor Marchese, restate.

(A Mirandolina.)

MIRANDOLINA:

Il signor Marchese mi dispenserà.

MARCHESE:

Non volete sentirlo?

MIRANDOLINA:

Un\'altra volta, Eccellenza.

CAVALIERE:

Via, restate.

MIRANDOLINA:

Me lo comanda?

(Al Cavaliere.)

CAVALIERE:

Vi dico che restiate.

MIRANDOLINA:

Obbedisco.

(Siede.)

CAVALIERE:

(Mi obbliga sempre più).

(Da sé.)

MARCHESE:

Oh che roba! Oh che intingolo! Oh che odore! Oh che sapore!

(Mangiando.)

CAVALIERE:

(Il Marchese avrà gelosia, che siate vicina a me).

(Piano a Mirandolina.)

MIRANDOLINA:

(Non m\'importa di lui né poco, né molto).

(Piano al Cavaliere.)

CAVALIERE:

(Siete anche voi nemica degli uomini?).

(Piano a Mirandolina.)

MIRANDOLINA:

(Come ella lo è delle donne).

(Come sopra.)

CAVALIERE:

(Queste mie nemiche si vanno vendicando di me).

(Come sopra.)

MIRANDOLINA:

(Come, signore?).

(Come sopra.)

CAVALIERE:

(Eh! furba! Voi vedrete benissimo\...).

(Come sopra.)

MARCHESE:

Amico, alla vostra salute.

(Beve il vino di Borgogna.)

CAVALIERE:

Ebbene? Come vi pare?

MARCHESE:

Con vostra buona grazia, non val niente. Sentite il mio vin di Cipro.

CAVALIERE:

Ma dov\'è questo vino di Cipro?

MARCHESE:

L\'ho qui, l\'ho portato con me, voglio che ce lo godiamo: ma! è di
quello. Eccolo.

(Tira fuori una bottiglia assai piccola.)

MIRANDOLINA:

Per quel che vedo, signor Marchese, non vuole che il suo vino ci vada
alla testa.

MARCHESE:

Questo? Si beve a gocce, come lo spirito di melissa. Ehi? Li
bicchierini.

(Apre la bottiglia.)

SERVITORE:

(porta de\' bicchierini da vino di Cipro.)

MARCHESE:

Eh, son troppo grandi. Non ne avete di più piccoli?

(Copre la bottiglia colla mano.)

CAVALIERE:

Porta quei da rosolio.

(Al Servitore.)

MIRANDOLINA:

Io credo che basterebbe odorarlo.

MARCHESE:

Uh caro! Ha un odor che consola.

(Lo annusa.)

SERVITORE:

(porta tre bicchierini sulla sottocoppa.)

MARCHESE:

(versa pian piano, e non empie li bicchierini, poi lo dispensa al
Cavaliere, a Mirandolina, e l\'altro per sé, turando bene la bottiglia)

Che nettare! Che ambrosia! Che manna distillata!

(Bevendo.)

CAVALIERE:

(Che vi pare di questa porcheria?).

(A Mirandolina, piano.)

MIRANDOLINA:

(Lavature di fiaschi).

(Al Cavaliere, piano.)

MARCHESE:

Ah! Che dite?

(Al Cavaliere.)

CAVALIERE:

Buono, prezioso.

MARCHESE:

Ah! Mirandolina, vi piace?

MIRANDOLINA:

Per me, signore, non posso dissimulare; non mi piace, lo trovo cattivo,
e non posso dir che sia buono. Lodo chi sa fingere. Ma chi sa fingere in
una cosa, saprà fingere nell\'altre ancora.

CAVALIERE:

(Costei mi dà un rimprovero; non capisco il perché).

(Da sé.)

MARCHESE:

Mirandolina, voi di questa sorta di vini non ve ne intendete. Vi
compatisco. Veramente il fazzoletto che vi ho donato, l\'avete
conosciuto e vi è piaciuto, ma il vin di Cipro non lo conoscete.

(Finisce di bere.)

MIRANDOLINA:

(Sente come si vanta?).

(Al Cavaliere, piano.)

CAVALIERE:

(Io non farei così).

(A Mirandolina, piano.)

MIRANDOLINA:

(Il di lei vanto sta nel disprezzare le donne).

(Come sopra.)

CAVALIERE:

(E il vostro nel vincere tutti gli uomini).

(Come sopra.)

MIRANDOLINA:

(Tutti no).

(Con vezzo, al Cavaliere, piano.)

CAVALIERE:

(Tutti sì.)

(Con qualche passione, piano a Mirandolina.)

MARCHESE:

Ehi? Tre bicchierini politi.

(Al Servitore, il quale glieli porta sopra una sottocoppa.)

MIRANDOLINA:

Per me non ne voglio più.

MARCHESE:

No, no, non dubitate: non faccio per voi.

(Mette del vino di Cipro nei tre bicchieri.)

Galantuomo, con licenza del vostro padrone, andate dal Conte
d\'Albafiorita, e ditegli per parte mia, forte, che tutti sentano, che
lo prego di assaggiare un poco del mio vino di Cipro.

SERVITORE:

Sarà servito. (Questo non li ubbriaca certo).

(Da sé; parte.)

CAVALIERE:

Marchese, voi siete assai generoso.

MARCHESE:

Io? Domandatelo a Mirandolina.

MIRANDOLINA:

Oh certamente!

MARCHESE:

L\'ha veduto il fazzoletto il Cavaliere?

(A Mirandolina.)

MIRANDOLINA:

Non lo ha ancora veduto.

MARCHESE:

Lo vedrete.

(Al Cavaliere.)

Questo poco di balsamo me lo salvo per questa sera.

(Ripone la bottiglia con un dito di vino avanzato.)

MIRANDOLINA:

Badi che non gli faccia male, signor Marchese.

MARCHESE:

Eh! Sapete che cosa mi fa male?

(A Mirandolina.)

MIRANDOLINA:

Che cosa?

MARCHESE:

I vostri begli occhi.

MIRANDOLINA:

Davvero?

MARCHESE:

Cavaliere mio, io sono innamorato di costei perdutamente.

CAVALIERE:

Me ne dispiace.

MARCHESE:

Voi non avete mai provato amore per le donne. Oh, se lo provaste,
compatireste ancora me.

CAVALIERE:

Sì, vi compatisco.

MARCHESE:

E son geloso come una bestia. La lascio stare vicino a voi, perché so
chi siete; per altro non lo soffrirei per centomila doppie.

CAVALIERE:

(Costui principia a seccarmi).

(Da sé.)

SCENA SETTIMA {#Section0010.xhtml#sigil_toc_id_68}
-------------

Il Servitore con una bottiglia sulla sottocoppa, e detti.

SERVITORE:

Il signor Conte ringrazia V.E., e manda una bottiglia di vino di
Canarie.

(Al Marchese.)

MARCHESE:

Oh, oh, vorrà mettere il suo vin di Canarie col mio vino di Cipro?
Lascia vedere. Povero pazzo! È una porcheria, lo conosco all\'odore.

(S\'alza e tiene la bottiglia in mano.)

CAVALIERE:

Assaggiatelo prima.

(Al Marchese.)

MARCHESE:

Non voglio assaggiar niente. Questa è una impertinenza che mi fa il
Conte, compagna di tante altre. Vuol sempre starmi al di sopra. Vuol
soverchiarmi, vuol provocarmi, per farmi far delle bestialità. Ma giuro
al cielo, ne farò una che varrà per cento. Mirandolina, se non lo
cacciate via, nasceranno delle cose grandi, sì, nasceranno delle cose
grandi. Colui è un temerario. Io son chi sono, e non voglio soffrire
simile affronti.

(Parte, e porta via la bottiglia.)

SCENA OTTAVA {#Section0010.xhtml#sigil_toc_id_69}
------------

Il Cavaliere, Mirandolina ed il Servitore.

CAVALIERE:

Il povero Marchese è pazzo.

MIRANDOLINA:

Se a caso mai la bile gli facesse male, ha portato via la bottiglia per
ristorarsi.

CAVALIERE:

È pazzo, vi dico. E voi lo avete fatto impazzire.

MIRANDOLINA:

Sono di quelle che fanno impazzare gli uomini?

CAVALIERE:

Sì, voi siete\...

(Con affanno.)

MIRANDOLINA:

Signor Cavaliere, con sua licenza.

(S\'alza.)

CAVALIERE:

Fermatevi.

MIRANDOLINA:

Perdoni; io non faccio impazzare nessuno.

(Andando.)

CAVALIERE:

Ascoltatemi.

(S\'alza, ma resta alla tavola.)

MIRANDOLINA:

Scusi.

(Andando.)

CAVALIERE:

Fermatevi, vi dico.

(Con imperio.)

MIRANDOLINA:

Che pretende da me?

(Con alterezza voltandosi.)

CAVALIERE:

Nulla.

(Si confonde.)

Beviamo un altro bicchiere di Borgogna.

MIRANDOLINA:

Via signore, presto, presto, che me ne vada.

CAVALIERE:

Sedete.

MIRANDOLINA:

In piedi, in piedi.

CAVALIERE:

Tenete.

(Con dolcezza le dà il bicchiere.)

MIRANDOLINA:

Faccio un brindisi, e me ne vado subito. Un brindisi che mi ha insegnato
mia nonna.

"Viva Bacco, e viva Amore:\
L\'uno e l\'altro ci consola;\
Uno passa per la gola,\
L\'altro va dagli occhi al cuore.\
Bevo il vin, cogli occhi poi\...\
Faccio quel che fate voi."

(Parte.)

SCENA NONA {#Section0010.xhtml#sigil_toc_id_70}
----------

Il Cavaliere, ed il Servitore.

CAVALIERE:

Bravissima, venite qui: sentite. Ah malandrina! Se nè fuggita. Se n\'è
fuggita, e mi ha lasciato cento diavoli che mi tormentano.

SERVITORE:

Comanda le frutta in tavola?

(Al Cavaliere.)

CAVALIERE:

Va al diavolo ancor tu.

(Il Servitore parte.)

Bevo il vin, cogli occhi poi, faccio quel che fate voi? Che brindisi
misterioso è questo? Ah maladetta, ti conosco. Mi vuoi abbattere, mi
vuoi assassinare. Ma lo fa con tanta grazia! Ma sa così bene
insinuarsi\... Diavolo, diavolo, me la farai tu vedere? No, anderò a
Livorno. Costei non la voglio più rivedere. Che non mi venga più tra i
piedi. Maledettissime donne! Dove vi sono donne, lo giuro non vi anderò
mai più.

(Parte.)

SCENA DECIMA {#Section0010.xhtml#sigil_toc_id_71}
------------

Camera del Conte.\
Il Conte d\'Albafiorita, Ortensia e Dejanira.

CONTE:

Il Marchese di Forlipopoli è un carattere curiosissimo. È nato nobile,
non si può negare; ma fra suo padre e lui hanno dissipato, ed ora non ha
appena da vivere. Tuttavolta gli piace fare il grazioso.

ORTENSIA:

Si vede che vorrebbe essere generoso, ma non ne ha.

DEJANIRA:

Dona quel poco che può, e vuole che tutto il mondo lo sappia.

CONTE:

Questo sarebbe un bel carattere per una delle vostre commedie.

ORTENSIA:

Aspetti che arrivi la compagnia, e che si vada in teatro, e può darsi
che ce lo godiamo.

DEJANIRA:

Abbiamo noi dei personaggi, che per imitare i caratteri sono fatti a
posta.

CONTE:

Ma se volete che ce lo godiamo, bisogna che con lui seguitiate a
fingervi dame.

ORTENSIA:

Io lo farò certo. Ma Dejanira subito dà di bianco.

DEJANIRA:

Mi vien da ridere, quando i gonzi mi credono una signora.

CONTE:

Con me avete fatto bene a scoprirvi. In questa maniera mi date campo di
far qualche cosa in vostro vantaggio.

ORTENSIA:

Il signor Conte sarà il nostro protettore.

DEJANIRA:

Siamo amiche, goderemo unitamente le di lei grazie.

CONTE:

Vi dirò, vi parlerò con sincerità. Vi servirò, dove potrò farlo, ma ho
un certo impegno, che non mi permetterà frequentare la vostra casa.

ORTENSIA:

Ha qualche amoretto, signor Conte?

CONTE:

Sì, ve lo dirò in confidenza. La padrona della locanda.

ORTENSIA:

Capperi! Veramente una gran signora! Mi meraviglio di lei, signor Conte,
che si perda con una locandiera!

DEJANIRA:

Sarebbe minor male, che si compiacesse d\'impiegare le sue finezze per
una comica.

CONTE:

Il far all\'amore con voi altre, per dirvela, mi piace poco. Ora ci
siete, ora non ci siete.

ORTENSIA:

Non è meglio così, signore? In questa maniera non si eternano le
amicizie, e gli uomini non si rovinano.

CONTE:

Ma io, tant\'è, sono impegnato; le voglio bene, e non la vo\'
disgustare.

DEJANIRA:

Ma che cosa ha di buono costei?

CONTE:

Oh! Ha del buono assai.

ORTENSIA:

Ehi, Dejanira. È bella, rossa.

(Fa cenno che si belletta.)

CONTE:

Ha un grande spirito.

DEJANIRA:

Oh, in materia di spirito, la vorreste mettere con noi?

CONTE:

Ora basta. Sia come esser si voglia; Mirandolina mi piace, e se volete
la mia amicizia, avete a dirne bene, altrimenti fate conto di non avermi
mai conosciuto.

ORTENSIA:

Oh signor Conte, per me dico che Mirandolina è una dea Venere.

DEJANIRA:

Sì, sì, vero. Ha dello spirito, parla bene.

CONTE:

Ora mi date gusto.

ORTENSIA:

Quando non vuol altro, sarà servito.

CONTE:

Oh! Avete veduto quello ch\'è passato per sala?

(Osservando dentro la scena.)

ORTENSIA:

L\'ho veduto.

CONTE:

Quello è un altro bel carattere da commedia.

ORTENSIA:

È uno che non può vedere le donne.

DEJANIRA:

Oh che pazzo!

ORTENSIA:

Avrà qualche brutta memoria di qualche donna.

CONTE:

Oibò; non è mai stato innamorato. Non ha mai voluto trattar con donne.
Le sprezza tutte, e basta dire che egli disprezza ancora Mirandolina.

ORTENSIA:

Poverino! Se mi ci mettessi attorno io, scommetto lo farei cambiare
opinione.

DEJANIRA:

Veramente una gran cosa! Questa è un\'impresa che la vorrei pigliare
sopra di me.

CONTE:

Sentite, amiche. Così per puro divertimento. Se vi dà l\'anima
d\'innamorarlo, da cavaliere vi faccio un bel regalo.

ORTENSIA:

Io non intendo essere ricompensata per questo: lo farò per mio spasso.

DEJANIRA:

Se il signor Conte vuol usarci qualche finezza, non l\'ha da fare per
questo. Sinché arrivano i nostri compagni, ci divertiremo un poco.

CONTE:

Dubito che non farete niente.

ORTENSIA:

Signor Conte, ha ben poca stima di noi.

DEJANIRA:

Non siamo vezzose come Mirandolina; ma finalmente sappiamo qualche poco
il viver del mondo.

CONTE:

Volete che lo mandiamo a chiamare?

ORTENSIA:

Faccia come vuole.

CONTE:

Ehi? Chi è di là?

SCENA UNDICESIMA {#Section0010.xhtml#sigil_toc_id_72}
----------------

Il Servitore del Conte, e detti.

CONTE:

Di\' al Cavaliere di Ripafratta, che favorisca venir da me, che mi preme
di parlargli.

(Al Servitore.)

SERVITORE:

Nella sua camera so che non c\'è.

CONTE:

L\'ho veduto andar verso la cucina. Lo troverai.

SERVITORE:

Subito.

(Parte.)

CONTE:

(Che mai è andato a far verso la cucina? Scommetto che è andato a
strapazzare Mirandolina, perché gli ha dato mal da mangiare).

(Da sé.)

ORTENSIA:

Signor Conte, io aveva pregato il signor Marchese che mi mandasse il suo
calzolaro, ma ho paura di non vederlo.

CONTE:

Non pensate altro. Vi servirò io.

DEJANIRA:

A me aveva il signor Marchese promesso un fazzoletto. Ma! ora me lo
porta!

CONTE:

De\' fazzoletti ne troveremo.

DEJANIRA:

Egli è che ne avevo proprio di bisogno.

CONTE:

Se questo vi gradisce, siete padrona. È pulito.

(Le offre il suo di seta.)

DEJANIRA:

Obbligatissima alle sue finezze.

CONTE:

Oh! Ecco il Cavaliere. Sarà meglio che sostenghiate il carattere di
dame, per poterlo meglio obbligare ad ascoltarvi per civiltà. Ritiratevi
un poco indietro; che, se vi vede, fugge.

ORTENSIA:

Come si chiama?

CONTE:

Il Cavaliere di Ripafratta, toscano.

DEJANIRA:

Ha moglie?

CONTE:

Non può vedere le donne.

ORTENSIA:

È ricco?

(Ritirandosi.)

CONTE:

Sì, Molto.

DEJANIRA:

È generoso?

(Ritirandosi.)

CONTE:

Piuttosto.

DEJANIRA:

Venga, venga.

(Si ritira.)

ORTENSIA:

Tempo, e non dubiti.

(Si ritira.)

SCENA DODICESIMA {#Section0010.xhtml#sigil_toc_id_73}
----------------

Il Cavaliere e detti.

CAVALIERE:

Conte, siete voi che mi volete?

CONTE:

Sì; io v\'ho dato il presente incomodo.

CAVALIERE:

Che cosa posso fare per servirvi?

CONTE:

Queste due dame hanno bisogno di voi.

(Gli addita le due donne, le quali subito s\'avanzano.)

CAVALIERE:

Disimpegnatemi. Io non ho tempo di trattenermi.

ORTENSIA:

Signor Cavaliere, non intendo di recargli incomodo.

DEJANIRA:

Una parola in grazia, signor Cavaliere.

CAVALIERE:

Signore mie, vi supplico perdonarmi. Ho un affar di premura.

ORTENSIA:

In due parole vi sbrighiamo.

DEJANIRA:

Due paroline, e non più, signore.

CAVALIERE:

(Maledettissimo Conte!).

(Da sé.)

CONTE:

Caro amico, due dame che pregano, vuole la civiltà che si ascoltino.

CAVALIERE:

Perdonate. In che vi posso servire?

(Alle donne, con serietà.)

ORTENSIA:

Non siete voi toscano, signore?

CAVALIERE:

Sì, signora.

DEJANIRA:

Avrete degli amici in Firenze?

CAVALIERE:

Ho degli amici, e ho de\' parenti.

DEJANIRA:

Sappiate, signore\... Amica, principiate a dir voi.

(Ad Ortensia.)

ORTENSIA:

Dirò, signor Cavaliere\... Sappia che un certo caso\...

CAVALIERE:

Via, signore, vi supplico. Ho un affar di premura.

CONTE:

Orsù, capisco che la mia presenza vi dà soggezione. Confidatevi con
libertà al Cavaliere, ch\'io vi levo l\'incomodo.

(Partendo.)

CAVALIERE:

No, amico, restate\... Sentite.

CONTE:

So il mio dovere. Servo di lor signore.

(Parte.)

SCENA TREDICESIMA {#Section0010.xhtml#sigil_toc_id_74}
-----------------

Ortensia, Dejanira ed il Cavaliere.

ORTENSIA:

Favorisca, sediamo.

CAVALIERE:

Scusi, non ho volontà di sedere.

DEJANIRA:

Così rustico colle donne?

CAVALIERE:

Favoriscano dirmi che cosa vogliono.

ORTENSIA:

Abbiamo bisogno del vostro aiuto, della vostra protezione, della vostra
bontà.

CAVALIERE:

Che cosa vi è accaduto?

DEJANIRA:

I nostri mariti ci hanno abbandonate.

CAVALIERE:

Abbandonate? Come! Due dame abbandonate? Chi sono i vostri mariti?

(Con alterezza.)

DEJANIRA:

Amica, non vado avanti sicuro.

(Ad Ortensia.)

ORTENSIA:

(È tanto indiavolato, che or ora mi confondo ancor io).

(Da sé.)

CAVALIERE:

Signore, vi riverisco.

(In atto di partire.)

ORTENSIA:

Come! Così ci trattate?

DEJANIRA:

Un cavaliere tratta così?

CAVALIERE:

Perdonatemi. Io son uno che ama assai la mia pace. Sento due dame
abbandonate dai loro mariti. Qui ci saranno degl\'impegni non pochi; io
non sono atto a\' maneggi. Vivo a me stesso. Dame riveritissime, da me
non potete sperare né consiglio, né aiuto.

ORTENSIA:

Oh via, dunque; non lo tenghiamo più in soggezione il nostro
amabilissimo Cavaliere.

DEJANIRA:

Sì, parliamogli con sincerità.

CAVALIERE:

Che nuovo linguaggio è questo?

ORTENSIA:

Noi non siamo dame.

CAVALIERE:

No?

DEJANIRA:

Il signor Conte ha voluto farvi uno scherzo.

CAVALIERE:

Lo scherzo è fatto. Vi riverisco.

(Vuol partire.)

ORTENSIA:

Fermatevi un momento.

CAVALIERE:

Che cosa volete?

DEJANIRA:

Degnateci per un momento della vostra amabile conversazione.

CAVALIERE:

Ho che fare. Non posso trattenermi.

ORTENSIA:

Non vi vogliamo già mangiar niente.

DEJANIRA:

Non vi leveremo la vostra reputazione.

ORTENSIA:

Sappiamo che non potete vedere le donne.

CAVALIERE:

Se lo sapete, l\'ho caro. Vi riverisco.

(Vuol partire.)

ORTENSIA:

Ma sentite: noi non siamo donne che possano darvi ombra.

CAVALIERE:

Chi siete?

ORTENSIA:

Diteglielo voi, Dejanira.

DEJANIRA:

Glielo potete dire anche voi.

CAVALIERE:

Via, chi siete?

ORTENSIA:

Siamo due commedianti.

CAVALIERE:

Due commedianti! Parlate, parlate, che non ho più paura di voi. Son ben
prevenuto in favore dell\'arte vostra.

ORTENSIA:

Che vuol dire? Spiegatevi.

CAVALIERE:

So che fingete in iscena e fuor di scena; e con tal prevenzione non ho
paura di voi.

DEJANIRA:

Signore, fuori di scena io non so fingere.

CAVALIERE:

Come si chiama ella? La signora Sincera?

(A Dejanira.)

DEJANIRA:

Io mi chiamo\...

CAVALIERE:

È ella la signora Buonalana?

(Ad Ortensia.)

ORTENSIA:

Caro signor Cavaliere\...

CAVALIERE:

Come si diletta di miccheggiare?

(Ad Ortensia.)

ORTENSIA:

Io non sono\...

CAVALIERE:

I gonzi come li tratta, padrona mia?

(A Dejanira.)

DEJANIRA:

Non son di quelle\...

CAVALIERE:

Anch\'io so parlar in gergo.

ORTENSIA:

Oh che caro signor Cavaliere!

(Vuol prenderlo per un braccio.)

CAVALIERE:

Basse le cere.

(Dandole nelle mani.)

ORTENSIA:

Diamine! Ha più del contrasto, che del Cavaliere.

CAVALIERE:

Contrasto vuol dire contadino. Vi ho capito. E vi dirò che siete due
impertinenti.

DEJANIRA:

A me questo?

ORTENSIA:

A una donna della mia sorte?

CAVALIERE:

Bello quel viso trionfato!

(Ad Ortensia.)

ORTENSIA:

(Asino!).

(Parte.)

CAVALIERE:

Bello quel tuppè finto!

(A Dejanira.)

DEJANIRA:

(Maledetto).

(Parte.)

SCENA QUATTORDICESIMA {#Section0010.xhtml#sigil_toc_id_75}
---------------------

Il Cavaliere, poi il di lui Servitore.

CAVALIERE:

Ho trovata ben io la maniera di farle andare. Che si pensavano? Di
tirarmi nella rete? Povere sciocche! Vadano ora dal Conte e gli narrino
la bella scena. Se erano dame, per rispetto mi conveniva fuggire; ma
quando posso, le donne le strapazzo col maggior piacere del mondo. Non
ho però potuto strapazzar Mirandolina. Ella mi ha vinto con tanta
civiltà, che mi trovo obbligato quasi ad amarla. Ma è donna; non me ne
voglio fidare. Voglio andar via. Domani anderò via. Ma se aspetto a
domani? Se vengo questa sera a dormir a casa, chi mi assicura che
Mirandolina non finisca a rovinarmi?

(Pensa.)

Sì; facciamo una risoluzione da uomo.

SERVITORE:

Signore.

CAVALIERE:

Che cosa vuoi?

SERVITORE:

Il signor Marchese è nella di lei camera che l\'aspetta, perché desidera
di parlargli.

CAVALIERE:

Che vuole codesto pazzo? Denari non me ne cava più di sotto. Che
aspetti, e quando sarà stracco di aspettare, se n\'anderà. Va dal
cameriere della locanda e digli che subito porti il mio conto.

SERVITORE:

Sarà obbedita.

(In atto di partire.)

CAVALIERE:

Senti. Fa che da qui a due ore siano pronti i bauli.

SERVITORE:

Vuol partire forse?

CAVALIERE:

Sì, portami qui la spada ed il cappello, senza che se n\'accorga il
Marchese.

SERVITORE:

Ma se mi vede fare i bauli?

CAVALIERE:

Dica ciò che vuole. M\'hai inteso.

SERVITORE:

(Oh, quanto mi dispiace andar via, per causa di Mirandolina!).

(Da sé, parte.)

CAVALIERE:

Eppure è vero. Io sento nel partir di qui una dispiacenza nuova, che non
ho mai provata. Tanto peggio per me, se vi restassi. Tanto più presto mi
conviene partire. Sì, donne, sempre più dirò male di voi; sì, voi ci
fate del male, ancora quando ci volete fare del bene.

SCENA QUINDICESIMA {#Section0010.xhtml#sigil_toc_id_76}
------------------

Fabrizio e detto.

FABRIZIO:

È vero, signore, che vuole il conto?

CAVALIERE:

Sì, l\'avete portato?

FABRIZIO:

Adesso la padrona lo fa.

CAVALIERE:

Ella fa i conti?

FABRIZIO:

Oh, sempre ella. Anche quando viveva suo padre. Scrive e sa far di conto
meglio di qualche giovane di negozio.

CAVALIERE:

(Che donna singolare è costei!).

(Da sé.)

FABRIZIO:

Ma vuol ella andar via così presto?

CAVALIERE:

Sì, così vogliono i miei affari.

FABRIZIO:

La prego di ricordarsi del cameriere.

CAVALIERE:

Portate il conto, e so quel che devo fare.

FABRIZIO:

Lo vuol qui il conto?

CAVALIERE:

Lo voglio qui; in camera per ora non ci vado.

FABRIZIO:

Fa bene; in camera sua vi è quel seccatore del signor Marchese. Carino!
Fa l\'innamorato della padrona; ma può leccarsi le dita. Mirandolina
deve esser mia moglie.

CAVALIERE:

Il conto.

(Alterato.)

FABRIZIO:

La servo subito.

(Parte.)

SCENA SEDICESIMA {#Section0010.xhtml#sigil_toc_id_77}
----------------

CAVALIERE:

(solo)

Tutti sono invaghiti di Mirandolina. Non è maraviglia, se ancor io
principiava a sentirmi accendere. Ma anderò via; supererò questa
incognita forza\... Che vedo? Mirandolina? Che vuole da me? Ha un foglio
in mano. Mi porterà il conto. Che cosa ho da fare? Convien soffrire
quest\'ultimo assalto. Già da qui a due ore io parto.

SCENA DICIASSETTESIMA {#Section0010.xhtml#sigil_toc_id_78}
---------------------

Mirandolina con un foglio in mano, e detto.

MIRANDOLINA:

Signore.

(Mestamente.)

CAVALIERE:

Che c\'è, Mirandolina?

MIRANDOLINA:

Perdoni.

(Stando indietro.)

CAVALIERE:

Venite avanti.

MIRANDOLINA:

Ha domandato il suo conto; l\'ho servita.

(Mestamente.)

CAVALIERE:

Date qui.

MIRANDOLINA:

Eccolo.

(Si asciuga gli occhi col grembiale, nel dargli il conto.)

CAVALIERE:

Che avete? Piangete?

MIRANDOLINA:

Niente, signore, mi è andato del fumo negli occhi.

CAVALIERE:

Del fumo negli occhi? Eh! basta\... quanto importa il conto?

(legge.)

Venti paoli? In quattro giorni un trattamento si generoso: venti paoli?

MIRANDOLINA:

Quello è il suo conto.

CAVALIERE:

E i due piatti particolari che mi avete dato questa mattina, non ci sono
nel conto?

MIRANDOLINA:

Perdoni. Quel ch\'io dono, non lo metto in conto.

CAVALIERE:

Me li avete voi regalati?

MIRANDOLINA:

Perdoni la libertà. Gradisca per un atto di\...

(Si copre, mostrando di piangere.)

CAVALIERE:

Ma che avete?

MIRANDOLINA:

Non so se sia il fumo, o qualche flussione di occhi.

CAVALIERE:

Non vorrei che aveste patito, cucinando per me quelle due preziose
vivande.

MIRANDOLINA:

Se fosse per questo, lo soffrirei\... volentieri\...

(Mostra trattenersi di piangere.)

CAVALIERE:

(Eh, se non vado via!).

(Da sé.)

Orsù, tenete. Queste sono due doppie. Godetele per amor mio\... e
compatitemi\...

(S\'imbroglia.)

MIRANDOLINA:

(senza parlare, cade come svenuta sopra una sedia.)

CAVALIERE:

Mirandolina. Ahimè! Mirandolina. È svenuta. Che fosse innamorata di me?
Ma così presto? E perché no? Non sono io innamorato di lei? Cara
Mirandolina\... Cara? Io cara ad una donna? Ma se è svenuta per me. Oh,
come tu sei bella! Avessi qualche cosa per farla rinvenire. Io che non
pratico donne, non ho spiriti, non ho ampolle. Chi è di là? Vi è
nessuno? Presto?\... Anderò io. Poverina! Che tu sia benedetta!

(Parte, e poi ritorna.)

MIRANDOLINA:

Ora poi è caduto affatto. Molte sono le nostre armi, colle quali si
vincono gli uomini. Ma quando sono ostinati, il colpo di riserva
sicurissimo è uno svenimento. Torna, torna.

(Si mette come sopra.)

CAVALIERE:

(torna con un vaso d\'acqua.)

Eccomi, eccomi. E non è ancor rinvenuta. Ah, certamente costei mi ama.

(La spruzza, ed ella si va movendo.)

Animo, animo. Son qui cara. Non partirò più per ora.

SCENA DICIOTTESIMA {#Section0010.xhtml#sigil_toc_id_79}
------------------

Il Servitore colla spada e cappello, e detti.

SERVITORE:

Ecco la spada ed il cappello.

(Al Cavaliere.)

CAVALIERE:

Va via.

(Al Servitore, con ira.)

SERVITORE:

I bauli\...

CAVALIERE:

Va via, che tu sia maledetto.

SERVITORE:

Mirandolina\...

CAVALIERE:

Va, che ti spacco la testa.

(Lo minaccia col vaso; il Servitore parte.)

E non rinviene ancora? La fronte le suda. Via, cara Mirandolina, fatevi
coraggio, aprite gli occhi. Parlatemi con libertà.

SCENA DICIANNOVESIMA {#Section0010.xhtml#sigil_toc_id_80}
--------------------

Il Marchese ed il Conte, e detti.

MARCHESE:

Cavaliere?

CONTE:

Amico?

CAVALIERE:

(Oh maldetti!).

(Va smaniando.)

MARCHESE:

Mirandolina.

MIRANDOLINA:

Oimè!

(S\'alza.)

MARCHESE:

Io l\'ho fatta rinvenire.

CONTE:

Mi rallegro, signor Cavaliere.

MARCHESE:

Bravo quel signore, che non può vedere le donne.

CAVALIERE:

Che impertinenza?

CONTE:

Siete caduto?

CAVALIERE:

Andate al diavolo quanti siete.

(Getta il vaso in terra, e lo rompe verso il Conte ed il Marchese, e
parte furiosamente.)

CONTE:

Il Cavaliere è diventato pazzo.

(Parte.)

MARCHESE:

Di questo affronto voglio soddisfazione.

(Parte.)

MIRANDOLINA:

L\'impresa è fatta. Il di lui cuore è in fuoco, in fiamme, in cenere.
Restami solo, per compiere la mia vittoria, che si renda pubblico il mio
trionfo, a scorno degli uomini presuntuosi, e ad onore del nostro sesso.

(Parte.)

[]{#Section0011.xhtml}

ATTO TERZO
==========

SCENA PRIMA {#Section0011.xhtml#sigil_toc_id_81}
-----------

Camera di Mirandolina con tavolino e biancheria da stirare.\
Mirandolina, poi Fabrizio.

MIRANDOLINA:

Orsù, l\'ora del divertimento è passata. Voglio ora badare a\' fatti
miei. Prima che questa biancheria si prosciughi del tutto, voglio
stirarla. Ehi, Fabrizio.

FABRIZIO:

Signora.

MIRANDOLINA:

Fatemi un piacere. Portatemi il ferro caldo.

FABRIZIO:

Signora sì.

(Con serietà, in atto di partire.)

MIRANDOLINA:

Scusate, se do a voi questo disturbo.

FABRIZIO:

Niente, signora. Finché io mangio il vostro pane, sono obbligato a
servirvi.

(Vuol partire.)

MIRANDOLINA:

Fermatevi; sentite: non siete obbligato a servirmi in queste cose; ma so
che per me lo fate volentieri ed io\... basta, non dico altro.

FABRIZIO:

Per me vi porterei l\'acqua colle orecchie. Ma vedo che tutto è gettato
via.

MIRANDOLINA:

Perché gettato via? Sono forse un\'ingrata?

FABRIZIO:

Voi non degnate i poveri uomini. Vi piace troppo la nobiltà.

MIRANDOLINA:

Uh povero pazzo! Se vi potessi dir tutto! Via, via andatemi a pigliar il
ferro.

FABRIZIO:

Ma se ho veduto io con questi miei occhi\...

MIRANDOLINA:

Andiamo, meno ciarle. Portatemi il ferro.

FABRIZIO:

Vado, vado, vi servirò, ma per poco.

(Andando.)

MIRANDOLINA:

Con questi uomini, più che loro si vuol bene, si fa peggio.

(Mostrando parlar da sé, ma per esser sentita.)

FABRIZIO:

Che cosa avete detto?

(Con tenerezza, tornando indietro.)

MIRANDOLINA:

Via, mi portate questo ferro?

FABRIZIO:

Sì, ve lo porto. (Non so niente. Ora la mi tira su, ora la mi butta giù.
Non so niente).

(Da sé, parte.)

SCENA SECONDA {#Section0011.xhtml#sigil_toc_id_82}
-------------

Mirandolina, poi il Servitore del Cavaliere.

MIRANDOLINA:

Povero sciocco! Mi ha da servire a suo marcio dispetto. Mi par di ridere
a far che gli uomini facciano a modo mio. E quel caro signor Cavaliere,
ch\'era tanto nemico delle donne? Ora, se volessi, sarei padrona di
fargli fare qualunque bestialità.

SERVITORE:

Signora Mirandolina.

MIRANDOLINA:

Che c\'è, amico?

SERVITORE:

Il mio padrone la riverisce, e manda a vedere come sta!

MIRANDOLINA:

Ditegli che sto benissimo.

SERVITORE:

Dice così, che beva un poco di questo spirito di melissa, che le farà
assai bene.

(Le dà una boccetta d\'oro.)

MIRANDOLINA:

È d\'oro questa boccetta?

SERVITORE:

Sì signora, d\'oro, lo so di sicuro.

MIRANDOLINA:

Perché non mi ha dato lo spirito di melissa, quando mi è venuto
quell\'orribile svenimento?

SERVITORE:

Allora questa boccetta egli non l\'aveva.

MIRANDOLINA:

Ed ora come l\'ha avuta?

SERVITORE:

Sentite. In confidenza. Mi ha mandato ora a chiamar un orefice, l\'ha
comprata, e l\'ha pagata dodici zecchini; e poi mi ha mandato dallo
speziale e comprar lo spirito.

MIRANDOLINA:

Ah, ah,ah.

(Ride.)

SERVITORE:

Ridete?

MIRANDOLINA:

Rido, perché mi manda il medicamento, dopo che son guarita del male.

SERVITORE:

Sarà buono per un\'altra volta.

MIRANDOLINA:

Via, ne beverò un poco per preservativo.

(Beve.)

Tenete, ringraziatelo.

(Gli vuol dar la boccetta.)

SERVITORE:

Oh! la boccetta è vostra.

MIRANDOLINA:

Come mia?

SERVITORE:

Sì. Il padrone l\'ha comprata a posta.

MIRANDOLINA:

A posta per me?

SERVITORE:

Per voi; ma zitto.

MIRANDOLINA:

Portategli la sua boccetta, e ditegli che lo ringrazio.

SERVITORE:

Eh via.

MIRANDOLINA:

Vi dico che gliela portiate, che non la voglio.

SERVITORE:

Gli volete fare quest\'affronto?

MIRANDOLINA:

Meno ciarle. Fate il vostro dovere. Tenete.

SERVITORE:

Non occorr\'altro. Gliela porterò. (Oh che donna! Ricusa dodici
zecchini! Una simile non l\'ho più ritrovata, e durerò fatica a
trovarla).

(Da sé, parte.)

SCENA TERZA {#Section0011.xhtml#sigil_toc_id_83}
-----------

Mirandolina, poi Fabrizio.

MIRANDOLINA:

Uh, è cotto, stracotto e biscottato! Ma siccome quel che ho fatto con
lui, non l\'ho fatto per interesse, voglio ch\'ei confessi la forza
delle donne, senza poter dire che sono interessate e venali.

FABRIZIO:

Ecco qui il ferro.

(Sostenuto, col ferro da stirare in mano.)

MIRANDOLINA:

È ben caldo?

FABRIZIO:

Signora sì, è caldo; così foss\'io abbruciato.

MIRANDOLINA:

Che cosa vi è di nuovo?

FABRIZIO:

Questo signor Cavaliere manda le ambasciate, manda i regali. Il
Servitore me l\'ha detto.

MIRANDOLINA:

Signor sì, mi ha mandato una boccettina d\'oro, ed io gliel\'ho
rimandata indietro.

FABRIZIO:

Gliel\'avete rimandata indietro?

MIRANDOLINA:

Sì, domandatelo al Servitore medesimo.

FABRIZIO:

Perché gliel\'avete rimandata indietro?

MIRANDOLINA:

Perché\... Fabrizio\... non dica\... Orsù, non parliamo altro.

FABRIZIO:

Cara Mirandolina, compatitemi.

MIRANDOLINA:

Via, andate, lasciatemi stirare.

FABRIZIO:

Io non v\'impedisco di fare\...

MIRANDOLINA:

Andatemi a preparare un altro ferro, e quando è caldo, portatelo.

FABRIZIO:

Sì, vado. Credetemi, che se parlo\...

MIRANDOLINA:

Non dite altro. Mi fate venire la rabbia.

FABRIZIO:

Sto cheto. (Ell\'è una testolina bizzarra, ma le voglio bene).

(Da sé, parte.)

MIRANDOLINA:

Anche questa è buona. Mi faccio merito con Fabrizio d\'aver ricusata la
boccetta d\'oro del Cavaliere. Questo vuol dir saper vivere, saper fare,
saper profittare di tutto, con buona grazia, con pulizia, con un poco di
disinvoltura. In materia d\'accortezza, non voglio che si dica ch\'io
faccia torto al sesso.

(Va stirando.)

SCENA QUARTA {#Section0011.xhtml#sigil_toc_id_84}
------------

Il Cavaliere e detta.

CAVALIERE:

(Eccola. Non ci volevo venire, e il diavolo mi ci ha strascinato!.

(Da sé, indietro.)

MIRANDOLINA:

(Eccolo, eccolo).

(Lo vede colla coda dell\'occhio, e stira.)

CAVALIERE:

Mirandolina?

MIRANDOLINA:

Oh signor Cavaliere! Serva umilissima.

(Stirando.)

CAVALIERE:

Come state?

MIRANDOLINA:

Benissimo, per servirla.

(Stirando senza guardarlo.)

CAVALIERE:

Ho motivo di dolermi di voi.

MIRANDOLINA:

Perché, signore?

(Guardandolo un poco.)

CAVALIERE:

Perché avete ricusato una piccola boccettina, che vi ho mandato.

MIRANDOLINA:

Che voleva ch\'io ne facessi?

(Stirando.)

CAVALIERE:

Servirvene nelle occorrenze.

MIRANDOLINA:

Per grazia del cielo, non sono soggetta agli svenimenti. Mi è accaduto
oggi quello che mi è accaduto mai più.

(Stirando.)

CAVALIERE:

Cara mirandolina\... non vorrei esser io stato cagione di quel funesto
accidente.

MIRANDOLINA:

Eh sì, ho timore che ella appunto ne sia stata la causa.

(Stirando.)

CAVALIERE:

Io? Davvero?

(Con passione.)

MIRANDOLINA:

Mi ha fatto bere quel maledetto vino di Borgogna, e mi ha fatto male.

(Stirando con rabbia.)

CAVALIERE:

Come? Possibile?

(Rimane mortificato.)

MIRANDOLINA:

È così senz\'altro. In camera sua non ci vengo mai più.

(Stirando.)

CAVALIERE:

V\'intendo. In camera mia non ci verrete più? Capisco il mistero. Sì, lo
capisco. Ma veniteci, cara, che vi chiamerete contenta.

(Amoroso.)

MIRANDOLINA:

Questo ferro è poco caldo. Ehi; Fabrizio? se l\'altro ferro è caldo,
portatelo.

(Forte verso la scena.)

CAVALIERE:

Fatemi questa grazia, tenete questa boccetta.

MIRANDOLINA:

In verità, signor Cavaliere, dei regali io non ne prendo.

(Con disprezzo, stirando.)

CAVALIERE:

Li avete pur presi dal Conte d\'Albafiorita.

MIRANDOLINA:

Per forza. Per non disgustarlo.

(Stirando.)

CAVALIERE:

E vorreste fare a me questo torto? e disgustarmi?

MIRANDOLINA:

Che importa a lei, che una donna la disgusti? Già le donne non le può
vedere.

CAVALIERE:

Ah, Mirandolina! ora non posso dire così.

MIRANDOLINA:

Signor Cavaliere, a che ora fa la luna nuova?

CAVALIERE:

Il mio cambiamento non è lunatico. Questo è un prodigio della vostra
bellezza, della vostra grazia.

MIRANDOLINA:

Ah, ah, ah.

(Ride forte, e stira.)

CAVALIERE:

Ridete?

MIRANDOLINA:

Non vuol che rida? Mi burla, e non vuol ch\'io rida?

CAVALIERE:

Eh furbetta! Vi burlo eh? Via, prendete questa boccetta.

MIRANDOLINA:

Grazie, grazie.

(Stirando.)

CAVALIERE:

Prendetela, o mi farete andare in collera.

MIRANDOLINA:

Fabrizio, il ferro.

(Chiamando forte, con caricatura.)

CAVALIERE:

La prendete, o non la prendete?

(Alterato.)

MIRANDOLINA:

Furia, furia.

(Prende la boccetta, e con disprezzo la getta nel paniere della
biancheria.)

CAVALIERE:

La gettate così?

MIRANDOLINA:

Fabrizio!

(Chiama forte, come sopra.)

SCENA QUINTA {#Section0011.xhtml#sigil_toc_id_85}
------------

Fabrizio col ferro, e detti.

FABRIZIO:

Son qua.

(Vedendo il Cavaliere, s\'ingelosisce.)

MIRANDOLINA:

È caldo bene?

(Prende il ferro.)

FABRIZIO:

Signora sì.

(Sostenuto.)

MIRANDOLINA:

Che avete, che mi parete turbato?

(A Fabrizio, con tenerezza.)

FABRIZIO:

Niente, padrona, niente.

MIRANDOLINA:

Avete male?

(Come sopra.)

FABRIZIO:

Datemi l\'altro ferro, se volete che lo metta nel fuoco.

MIRANDOLINA:

In verità, ho paura che abbiate male.

(Come sopra.)

CAVALIERE:

Via, dategli il ferro, e che se ne vada.

MIRANDOLINA:

Gli voglio bene, sa ella? È il mio cameriere fidato.

(Al Cavaliere.)

CAVALIERE:

(Non posso più).

(Da sé, smaniando.)

MIRANDOLINA:

Tenete, caro, scaldatelo.

(Dà il ferro a Fabrizio.)

FABRIZIO:

Signora padrona\...

(Con tenerezza.)

MIRANDOLINA:

Via, via, presto.

(Lo scaccia.)

FABRIZIO:

(Che vivere è questo? Sento che non posso più).

(Da sé, parte.)

SCENA SESTA {#Section0011.xhtml#sigil_toc_id_86}
-----------

Il Cavaliere e Mirandolina.

CAVALIERE:

Gran finezze, signora, al suo cameriere!

MIRANDOLINA:

E per questo, che cosa vorrebbe dire?

CAVALIERE:

Si vede che ne siete invaghita.

MIRANDOLINA:

Io innamorata di un cameriere? Mi fa un bel complimento, signore; non
sono di sì cattivo gusto io. Quando volessi amare, non getterei il mio
tempo sì malamente.

(Stirando.)

CAVALIERE:

Voi meritereste l\'amore di un re.

MIRANDOLINA:

Del re di spade, o del re di coppe?

(Stirando.)

CAVALIERE:

Parliamo sul serio, Mirandolina, e lasciamo gli scherzi.

MIRANDOLINA:

Parli pure, che io l\'ascolto.

(Stirando.)

CAVALIERE:

Non potreste per un poco lasciar di stirare?

MIRANDOLINA:

Oh perdoni! Mi preme allestire questa biancheria per domani.

CAVALIERE:

Vi preme dunque quella biancheria più di me?

MIRANDOLINA:

Sicuro.

(Stirando.)

CAVALIERE:

E ancora lo confermate?

MIRANDOLINA:

Certo. Perché di questa biancheria me ne ho da servire, e di lei non
posso far capitale di niente.

(Stirando.)

CAVALIERE:

Anzi potete dispor di me con autorità.

MIRANDOLINA:

Eh, che ella non può vedere le donne.

CAVALIERE:

Non mi tormentate più. Vi siete vendicata abbastanza. Stimo voi, stimo
le donne che sono della vostra sorte, se pur ve ne sono. Vi stimo, vi
amo, e vi domando pietà.

MIRANDOLINA:

Sì signore, glielo diremo.

(Stirando in fretta, si fa cadere un manicotto.)

CAVALIERE:

(leva di terra il manicotto, e glielo dà)

Credetemi\...

MIRANDOLINA:

Non s\'incomodi.

CAVALIERE:

Voi meritate di esser servita.

MIRANDOLINA:

Ah, ah, ah.

(Ride forte.)

CAVALIERE:

Ridete?

MIRANDOLINA:

Rido, perché mi burla.

CAVALIERE:

Mirandolina, non posso più.

MIRANDOLINA:

Le vien male?

CAVALIERE:

Sì, mi sento mancare.

MIRANDOLINA:

Tenga il suo spirito di melissa.

(Gli getta con disprezzo la boccetta.)

CAVALIERE:

Non mi trattate con tanta asprezza. Credetemi, vi amo, ve lo giuro.

(Vuol prenderle la mano, ed ella col ferro lo scotta.)

Aimè!

MIRANDOLINA:

Perdoni: non l\'ho fatto apposta.

CAVALIERE:

Pazienza! Questo è niente. Mi avete fatto una scottatura più grande.

MIRANDOLINA:

Dove, signore?

CAVALIERE:

Nel cuore.

MIRANDOLINA:

Fabrizio.

(Chiama ridendo.)

CAVALIERE:

Per carità, non chiamate colui.

MIRANDOLINA:

Ma se ho bisogno dell\'altro ferro.

CAVALIERE:

Aspettate\... (ma no\...) chiamerò il mio servitore.

MIRANDOLINA:

Eh! Fabrizio\...

(Vuol chiamare Fabrizio.)

CAVALIERE:

Giuro al cielo, se viene colui, gli spacco la testa.

MIRANDOLINA:

Oh, questa è bella! Non mi potrò servire della mia gente?

CAVALIERE:

Chiamate un altro; colui non lo posso vedere.

MIRANDOLINA:

Mi pare ch\'ella si avanzi un poco troppo, signor Cavaliere.

(Si scosta dal tavolino col ferro in mano.)

CAVALIERE:

Compatitemi\... son fuori di me.

MIRANDOLINA:

Anderò io in cucina, e sarà contento.

CAVALIERE:

No, cara, fermatevi.

MIRANDOLINA:

È una cosa curiosa questa.

(Passeggiando.)

CAVALIERE:

Compatitemi.

(Le va dietro.)

MIRANDOLINA:

Non posso chiamar chi voglio?

(Passeggia.)

CAVALIERE:

Lo confesso. Ho gelosia di colui.

(Le va dietro.)

MIRANDOLINA:

(Mi vien dietro come un cagnolino).

(Da sé, passeggiando.)

CAVALIERE:

Questa è la prima volta ch\'io provo che cosa sia amore.

MIRANDOLINA:

Nessuno mi ha mai comandato.

(Camminando.)

CAVALIERE:

Non intendo di comandarvi: vi prego.

(La segue.)

MIRANDOLINA:

Ma che cosa vuole da me?

(Voltandosi con alterezza.)

CAVALIERE:

Amore, compassione, pietà.

MIRANDOLINA:

Un uomo che stamattina non poteva vedere le donne, oggi chiede amore e
pietà? Non gli abbado, non può essere, non gli credo. (Crepa, schiatta,
impara a disprezzar le donne).

(Da sé, parte.)

SCENA SETTIMA {#Section0011.xhtml#sigil_toc_id_87}
-------------

CAVALIERE:

(solo)

Oh maledetto il punto, in cui ho principiato a mirar costei! Son caduto
nel laccio, e non vi è più rimedio.

SCENA OTTAVA {#Section0011.xhtml#sigil_toc_id_88}
------------

Il Marchese e detto.

MARCHESE:

Cavaliere, voi mi avete insultato.

CAVALIERE:

Compatitemi, fu un accidente.

MARCHESE:

Mi meraviglio di voi.

CAVALIERE:

Finalmente il vaso non vi ha colpito.

MARCHESE:

Una gocciola d\'acqua mi ha macchiato il vestito.

CAVALIERE:

Torno a dir, compatitemi.

MARCHESE:

Questa è una impertinenza.

CAVALIERE:

Non l\'ho fatto apposta. Compatitemi per la terza volta.

MARCHESE:

Voglio soddisfazione.

CAVALIERE:

Se non volete compatirmi, se volete soddisfazione, son qui, non ho
soggezione di voi.

MARCHESE:

Ho paura che questa macchia non voglia andar via; questo è quello che mi
fa andare in collera.

(Cangiandosi.)

CAVALIERE:

Quando un cavalier vi chiede scusa, che pretendete di più?

(Con isdegno.)

MARCHESE:

Se non l\'avete fatto a malizia, lasciamo stare.

CAVALIERE:

Vi dico, che son capace di darvi qualunque soddisfazione.

MARCHESE:

Via, non parliamo altro.

CAVALIERE:

Cavaliere malnato.

MARCHESE:

Oh questa è bella! A me è passata la collera, e voi ve la fate venire.

CAVALIERE:

Ora per l\'appunto mi avete trovato in buona luna.

MARCHESE:

Vi compatisco, so che male avete.

CAVALIERE:

I fatti vostri io non li ricerco.

MARCHESE:

Signor inimico delle donne, ci siete caduto eh?

CAVALIERE:

Io? Come?

MARCHESE:

Sì, siete innamorato\...

CAVALIERE:

Sono il diavolo che vi porti.

MARCHESE:

Che serve nascondersi?\...

CAVALIERE:

Lasciatemi stare, che giuro al cielo ve ne farò pentire.

(Parte.)

SCENA NONA {#Section0011.xhtml#sigil_toc_id_89}
----------

MARCHESE:

(solo)

È innamorato, si vergogna, e non vorrebbe che si sapesse. Ma forse non
vorrà che si sappia, perché ha paura di me; avrà soggezione a
dichiararsi per mio rivale. Mi dispiace assaissimo di questa macchia; se
sapessi come fare a levarla! Queste donne sogliono avere della terra da
levar le macchie.

(Osserva nel tavolino e nel paniere.)

Bella questa boccetta! Che sia d\'oro o di princisbech? Eh, sarà di
princisbech: se fosse d\'oro, non la lascerebbero qui; se vi fosse
dell\'acqua della regina, sarebbe buona per levar questa macchia.

(Apre, odora e gusta.)

È spirito di melissa. Tant\'è tanto sarà buono. Voglio provare.

SCENA DECIMA {#Section0011.xhtml#sigil_toc_id_90}
------------

Dejanira e detto.

DEJANIRA:

Signor Marchese, che fa qui solo? Non favorisce mai?

MARCHESE:

Oh signora Contessa. Veniva or ora per riverirla.

DEJANIRA:

Che cosa stava facendo?

MARCHESE:

Vi dirò. Io sono amantissimo della pulizia. Voleva levare questa piccola
macchia.

DEJANIRA:

Con che, signore?

MARCHESE:

Con questo spirito di melissa.

DEJANIRA:

Oh perdoni, lo spirito di melissa non serve, anzi farebbe venire la
macchia più grande.

MARCHESE:

Dunque, come ho da fare?

DEJANIRA:

Ho io un segreto per cavar le macchie.

MARCHESE:

Mi farete piacere a insegnarmelo.

DEJANIRA:

Volentieri. M\'impegno con uno scudo far andar via quella macchia, che
non si vedrà nemmeno dove sia stata.

MARCHESE:

Vi vuole uno scudo?

DEJANIRA:

Sì, signore, vi pare una grande spesa?

MARCHESE:

È meglio provare lo spirito di Melissa.

DEJANIRA:

Favorisca: è buono quello spirito?

MARCHESE:

Prezioso, sentite.

(Le dà la boccetta.)

DEJANIRA:

Oh, io ne so fare del meglio.

(Assaggiandolo.)

MARCHESE:

Sapete fare degli spiriti?

DEJANIRA:

Sì, signore mi diletto di tutto.

MARCHESE:

Brava, damina, brava. Così mi piace.

DEJANIRA:

Sarà d\'oro questa boccetta?

MARCHESE:

Non volete? È oro sicuro. (Non conosce l\'oro del princisbech).

(Da sé.)

DEJANIRA:

È sua, signor Marchese?

MARCHESE:

È mia, e vostra se comandate.

DEJANIRA:

Obbligatissima alle sue grazie.

(La mette via.)

MARCHESE:

Eh! so che scherzate.

DEJANIRA:

Come? Non me l\'ha esibita?

MARCHESE:

Non è cosa da vostra pari. È una bagattella. Vi servirò di cosa
migliore, se ne avete voglia.

DEJANIRA:

Oh, mi meraviglio. È anche troppo. La ringrazio, signor Marchese.

MARCHESE:

Sentite. In confidenza. Non è oro. È princisbech.

DEJANIRA:

Tanto meglio. La stimo più che se fosse oro. E poi, quel che viene dalle
sue mani, è tutto prezioso.

MARCHESE:

Basta. Non so che dire; servitevi, se vi degnate. (Pazienza! Bisognerà
pagarla a Mirandolina. Che cosa può valere? Un filippo?).

(Da sé.)

DEJANIRA:

Il signor Marchese è un cavalier generoso.

MARCHESE:

Mi vergogno a regalar queste bagattelle. Vorrei che quella boccetta
fosse d\'oro.

DEJANIRA:

In verità, pare propriamente oro.

(La tira fuori, e la osserva.)

Ognuno s\'ingannerebbe.

MARCHESE:

È vero, chi non ha pratica dell\'oro, s\'inganna: ma io lo conosco
subito.

DEJANIRA:

Anche al peso par che sia oro.

MARCHESE:

E pur non è vero.

DEJANIRA:

Voglio farla vedere alla mia compagna.

MARCHESE:

Sentite, signora Contessa, non la fate vedere a Mirandolina. È una
ciarliera. Non so se mi capite.

DEJANIRA:

Intendo benissimo. La fo vedere solamente ad Ortensia.

MARCHESE:

Alla Baronessa?

DEJANIRA:

Sì, sì, alla Baronessa.

(Ridendo parte.)

SCENA UNDICESIMA {#Section0011.xhtml#sigil_toc_id_91}
----------------

Il Marchese, poi il Servitore del Cavaliere.

MARCHESE:

Credo che se ne rida, perché mi ha levato con quel bel garbo la
boccettina. Tant\'era se fosse stata d\'oro. Manco male, che con poco
l\'aggiusterò. Se Mirandolina vorrà la sua boccetta, gliela pagherò,
quando ne avrò.

SERVITORE:

(cerca sul tavolo)

Dove diamine sarà questa boccetta?

MARCHESE:

Che cosa cercate, galantuomo?

SERVITORE:

Cerco una boccetta di spirito di melissa. La signora Mirandolina la
vorrebbe. Dice che l\'ha lasciata qui, ma non la ritrovo.

MARCHESE:

Era una boccettina di princisbech?

SERVITORE:

No signore, era d\'oro.

MARCHESE:

D\'oro?

SERVITORE:

Certo che era d\'oro. L\'ho veduta comprar io per dodici zecchini.

(Cerca.)

MARCHESE:

(Oh povero me!).

(Da sé.)

Ma come lasciar così una boccetta d\'oro?

SERVITORE:

Se l\'è scordata, ma io non la trovo.

MARCHESE:

Mi pare ancora impossibile che fosse d\'oro.

SERVITORE:

Era oro, gli dico. L\'ha forse veduta V.E.?

MARCHESE:

Io?\... Non ho veduto niente.

SERVITORE:

Basta. Le dirò che non la trovo. Suo danno. Doveva mettersela in tasca.

(Parte.)

SCENA DODICESIMA {#Section0011.xhtml#sigil_toc_id_92}
----------------

Il Marchese, poi il Conte.

MARCHESE:

Oh povero Marchese di Forlipopoli! Ho donata una boccetta d\'oro, che
val dodici zecchini, e l\'ho donata per princisbech. Come ho da
regolarmi in un caso di tanta importanza? Se recupero la boccetta dalla
Contessa, mi fo ridicolo presso di lei; se Mirandolina viene a scoprire
ch\'io l\'abbia avuta, è in pericolo il mio decoro. Son cavaliere. Devo
pagarla. Ma non ho danari.

CONTE:

Che dite, signor Marchese, della bellissima novità?

MARCHESE:

Di quale novità?

CONTE:

Il Cavaliere Selvatico, il disprezzator delle donne, è innamorato di
Mirandolina.

MARCHESE:

L\'ho caro. Conosca suo malgrado il merito di questa donna; veda che io
non m\'invaghisco di chi non merita; e peni e crepi per gastigo della
sua impertinenza.

CONTE:

Ma se Mirandolina gli corrisponde?

MARCHESE:

Ciò non può essere. Ella non farà a me questo torto. Sa chi sono. Sa
cosa ho fatto per lei.

CONTE:

Io ho fatto per essa assai più di voi. Ma tutto è gettato. Mirandolina
coltiva il Cavaliere di Ripafratta, ha usato verso di lui quelle
attenzioni che non ha praticato né a voi, né a me; e vedesi che, colle
donne, più che si sa, meno si merita, e che burlandosi esse di che le
adora, corrono dietro a chi le disprezza.

MARCHESE:

Se ciò fosse vero\... ma non può essere.

CONTE:

Perché non può essere?

MARCHESE:

Vorreste mettere il Cavaliere a confronto di me?

CONTE:

Non l\'avete veduta voi stesso sedere alla di lui tavola? Con noi ha
praticato mai un atto di simile confidenza? A lui biancheria distinta.
Servito in tavola prima di tutti. Le pietanze gliele fa ella colle sue
mani. I servidori vedono tutto, e parlano. Fabrizio freme di gelosia. E
poi quello svenimento, vero o finto che fosse, non è segno manifesto
d\'amore?

MARCHESE:

Come! A lui si fanno gl\'intingoli saporiti, e a me carnaccia di bue, e
minestra di riso lungo? Sì, è vero, questo è uno strapazzo al mio grado,
alla mia condizione.

CONTE:

Ed io che ho speso tanto per lei?

MARCHESE:

Ed io che la regalava continuamente? Le ho fino dato da bere di quel
vino di Cipro così prezioso. Il Cavaliere non avrà fatto con costei una
minima parte di quello che abbiamo fatto noi.

CONTE:

Non dubitate, che anch\'egli l\'ha regalata.

MARCHESE:

Sì? Che cosa le ha donato?

CONTE:

Una boccettina d\'oro con dello spirito di melissa.

MARCHESE:

(Oimè!)

(Da sé.)

Come lo avete saputo?

CONTE:

Il di lui servidore l\'ha detto al mio.

MARCHESE:

(Sempre peggio. Entro in un impegno col Cavaliere).

(Da sé.)

CONTE:

Vedo che costei è un\'ingrata; voglio assolutamente lasciarla. Voglio
partire or ora da questa locanda indegna.

MARCHESE:

Sì, fate bene, andate.

CONTE:

E voi che siete un cavaliere di tanta riputazione, dovreste partire con
me.

MARCHESE:

Ma\... dove dovrei andare?

CONTE:

Vi troverò io un alloggio. Lasciate pensare a me.

MARCHESE:

Quest\'alloggio\... sarà per esempio\...

CONTE:

Andremo in casa d\'un mio paesano. Non ispenderemo nulla.

MARCHESE:

Basta, siete tanto mio amico, che non posso dirvi di no.

CONTE:

Andiamo, e vendichiamoci di questa femmina sconoscente.

MARCHESE:

Sì, andiamo. (Ma come sarà poi della boccetta? Son cavaliere, non posso
fare una malazione).

(Da sé.)

CONTE:

Non vi pentite, signor Marchese, andiamo via di qui. Fatemi questo
piacere, e poi comandatemi dove posso, che vi servirò.

MARCHESE:

Vi dirò. In confidenza, ma che nessuno lo sappia. Il mio fattore mi
ritarda qualche volta le mie rimesse\...

CONTE:

Le avete forse da dar qualche cosa?

MARCHESE:

Sì, dodici zecchini.

CONTE:

Dodici zecchini? Bisogna che sia dei mesi, che non pagate.

MARCHESE:

Così è, le devo dodici zecchini. Non posso di qua partire senza pagarla.
Se voi mi faceste il piacere\...

CONTE:

Volentieri. Eccovi dodici zecchini.

(Tira fuori la borsa.)

MARCHESE:

Aspettate. Ora che mi ricordo, sono tredici. (Voglio rendere il suo
zecchino anche al Cavaliere).

(Da sé.)

CONTE:

Dodici o tredici è lo stesso per me. Tenete.

MARCHESE:

Ve li renderò quanto prima.

CONTE:

Servitevi quanto vi piace. Danari a me non ne mancano; e per vendicarmi
di costei, spenderei mille doppie.

MARCHESE:

Sì, veramente è un\'ingrata. Ho speso tanto per lei, e mi tratta così.

CONTE:

Voglio rovinare la sua locanda. Ho fatto andar via anche quelle due
commedianti.

MARCHESE:

Dove sono le commedianti?

CONTE:

Erano qui: Ortensia e Dejanira.

MARCHESE:

Come! Non sono dame?

CONTE:

No. Sono due comiche. Sono arrivati i loro comnpagni, e la favola è
terminata.

MARCHESE:

(La mia boccetta!).

(Da sé.)

Dove sono alloggiate?

CONTE:

In una casa vicino al teatro.

MARCHESE:

(Vado subito a ricuperare la mia boccetta).

(Da se, parte.)

CONTE:

Con costei mi voglio vendicar così. Il Cavaliere poi, che ha saputo
fingere per tradirmi, in altra maniera me ne renderà conto.

(Parte.)

SCENA TREDICESIMA {#Section0011.xhtml#sigil_toc_id_93}
-----------------

Camera con tre porte.

MIRANDOLINA:

(sola)

Oh meschina me! Sono nel brutto impegno! Se il Cavaliere mi arriva, sto
fresca. Si è indiavolato maledettamente. Non vorrei che il diavolo lo
tentasse di venir qui. Voglio chiudere questa porta.

(Serra la porta da dove è venuta.)

Ora principio quasi a pentirmi di quel che ho fatto. È vero che mi sono
assai divertita nel farmi correr dietro a tal segno un superbo, un
disprezzator delle donne; ma ora che il satiro è sulle furie, vedo in
pericolo la mia riputazione e la mia vita medesima. Qui mi convien
risolvere quelche cosa di grande. Son sola, non ho nessuno dal cuore che
mi difenda. Non ci sarebbe altri che quel buon uomo di Fabrizio, che in
tal caso mi potesse giovare. Gli prometterò di sposarlo\... Ma\...
prometti, prometti, si stancherà di credermi\... Sarebbe quasi meglio
ch\'io lo sposassi davvero. Finalmente con un tal matrimonio posso
sperar di mettere al coperto il mio interesse e la mia reputazione,
senza pregiudicare alla mia libertà.

SCENA QUATTORDICESIMA {#Section0011.xhtml#sigil_toc_id_94}
---------------------

Il Cavaliere di dentro, e detta; poi Fabrizio.\
Il Cavaliere batte per di dentro alla porta.

MIRANDOLINA:

Battono a questa porta: chi sarà mai?

(S\'accosta.)

CAVALIERE:

Mirandolina.

(Di dentro.)

MIRANDOLINA:

(L\'amico è qui).

(Da sé.)

CAVALIERE:

Mirandolina, apritemi.

(Come sopra.)

MIRANDOLINA:

(Aprirgli? Non sono sì gonza). Che comanda, signor Cavaliere?

CAVALIERE:

Apritemi.

(Di dentro.)

MIRANDOLINA:

Favorisca andare nella sua camera, e mi aspetti, che or ora son da lei.

CAVALIERE:

Perché non volete aprirmi?

(Come sopra.)

MIRANDOLINA:

Arrivano de\' forestieri. Mi faccia questa grazia, vada, che or ora sono
da lei.

CAVALIERE:

Vado: se non venite, povera voi.

(Parte.)

MIRANDOLINA:

Se non venite, povera voi! Povera me, se vi andassi. La cosa va sempre
peggio. Rimediamoci, se si può. È andato via?

(Guarda al buco della chiave.)

Sì, sì, è andato. Mi aspetta in camera, ma non vi vado. Ehi? Fabrizio.

(Ad un\'altra porta.)

Sarebbe bella che ora Fabrizio si vendicasse di me, e non volesse\...
Oh, non vi è pericolo. Ho io certe manierine, certe smorfiette, che
bisogna che caschino, se fossero di macigno. Fabrizio.

(Chiama ad un\'altra porta.)

FABRIZIO:

Avete chiamato?

MIRANDOLINA:

Venite qui; voglio farvi una confidenza.

FABRIZIO:

Son qui.

MIRANDOLINA:

Sappiate che il Cavaliere di Ripafratta si è scoperto innamorato di me.

FABRIZIO:

Eh, me ne sono accorto.

MIRANDOLINA:

Sì? Ve ne siete accorto? Io in verità non me ne sono mai avveduta.

FABRIZIO:

Povera semplice! Non ve ne siete accorta! Non avete veduto, quando
stiravate col ferro, le smorfie che vi faceva? La gelosia che aveva di
me?

MIRANDOLINA:

Io che opero senza malizia, prendo le cose con indifferenza. Basta; ora
mi ha dette certe parole, che in verità, Fabrizio, mi hanno fatto
arrossire.

FABRIZIO:

Vedete: questo vuol dire perché siete una giovane sola, senza padre,
senza madre, senza nessuno. Se foste maritata, non andrebbe così.

MIRANDOLINA:

Orsù, capisco che dite bene; ho pensato di maritarmi.

FABRIZIO:

Ricordatevi di vostro padre.

MIRANDOLINA:

Sì, me ne ricordo.

SCENA QUINDICESIMA {#Section0011.xhtml#sigil_toc_id_95}
------------------

Il Cavaliere di dentro e detti.\
Il Cavaliere batte alla porta dove era prima.

MIRANDOLINA:

Picchiano.

(A Fabrizio.)

FABRIZIO:

Chi è che picchia?

(Forte verso la porta.)

CAVALIERE:

Apritemi.

(Di dentro.)

MIRANDOLINA:

Il Cavaliere.

(A Fabrizio.)

FABRIZIO:

Che cosa vuole?

(S\'accosta per aprirgli.)

MIRANDOLINA:

Aspettate ch\'io parta.

FABRIZIO:

Di che avete timore?

MIRANDOLINA:

Caro Fabrizio, non so, ho paura della mia onestà.

(Parte.)

FABRIZIO:

Non dubitate, io vi difenderò.

CAVALIERE:

Apritemi, giuro al cielo.

(Di dentro.)

FABRIZIO:

Che comanda, signore? Che strepiti sono questi? In una locanda onorata
non si fa così.

CAVALIERE:

Apri questa porta.

(Si sente che la sforza.)

FABRIZIO:

Cospetto del diavolo! Non vorrei precipitare. Uomini, chi è di là? Non
ci è nessuno?

SCENA SEDICESIMA {#Section0011.xhtml#sigil_toc_id_96}
----------------

Il Marchese ed il Conte dalla porta di mezzo, e detti.

CONTE:

Che c\'è?

(Sulla porta.)

MARCHESE:

Che rumore è questo?

(Sulla porta.)

FABRIZIO:

Signori, li prego: il signor Cavaliere di Ripafratta vuole sforzare
quella porta.

(Piano, che il Cavaliere non senta.)

CAVALIERE:

Aprimi, o la getto abbasso.

(Di dentro.)

MARCHESE:

Che sia diventato pazzo? Andiamo via.

(Al Conte.)

CONTE:

Apritegli.

(A Fabrizio.)

Ho volontà per appunto di parlar con lui.

FABRIZIO:

Aprirò; ma le supplico\...

CONTE:

Non dubitate. Siamo qui noi.

MARCHESE:

(Se vedo niente niente, me la colgo).

(Da sé.)\
(Fabrizio apre, ed entra il Cavaliere.)

CAVALIERE:

Giuro al cielo, dov\'è?

FABRIZIO:

Chi cercate, signore?

CAVALIERE:

Mirandolina dov\'è?

FABRIZIO:

Io non lo so.

MARCHESE:

(L\'ha con Mirandolina. Non è niente).

(Da sé.)

CAVALIERE:

Scellerata, la troverò.

(S\'incammina, e scopre il Conte e il Marchese.)

CONTE:

Con chi l\'avete?

(Al Cavaliere.)

MARCHESE:

Cavaliere, noi siamo amici.

CAVALIERE:

(Oimè! Non vorrei per tutto l\'oro del mondo che nota fosse questa mia
debolezza).

(Da sé.)

FABRIZIO:

Che cosa vuole, signore, dalla padrona?

CAVALIERE:

A te non devo rendere questi conti. Quando comando, voglio esser
servito. Pago i miei denari per questo, e giuro al cielo, ella avrà che
fare con me.

FABRIZIO:

V.S. paga i suoi denari per essere servito nelle cose lecite e oneste:
ma non ha poi da pretendere, la mi perdoni, che una donna onorata\...

CAVALIERE:

Che dici tu? Che sai tu? Tu non entri ne\' fatti miei. So io quel che ho
ordinato a colei.

FABRIZIO:

Le ha ordinato di venire nella sua camera.

CAVALIERE:

Va via, briccone, che ti rompo il cranio.

FABRIZIO:

Mi meraviglio di lei.

MARCHESE:

Zitto.

(A Fabrizio.)

CONTE:

Andate via.

(A Fabrizio.)

CAVALIERE:

Vattene via di qui.

(A Fabrizio.)

FABRIZIO:

Dico, signore\...

(Riscaldandosi.)

MARCHESE:

Via.

CONTE:

Via.

(Lo cacciano via.)

FABRIZIO:

(Corpo di bacco! Ho proprio voglia di precipitare).

(Da sé, parte.)

SCENA DICIASSETTESIMA {#Section0011.xhtml#sigil_toc_id_97}
---------------------

Il Cavaliere, il Marchese ed il Conte.

CAVALIERE:

(Indegna! Farmi aspettar nella camera?).

(Da sé.)

MARCHESE:

(Che diamine ha?).

(Piano al Conte.)

CONTE:

(Non lo vedete? È innamorato di Mirandolina).

CAVALIERE:

(E si trattiene con Fabrizio? E parla seco di matrimonio?).

(Da sé.)

CONTE:

(Ora è il tempo di vendicarmi).

(Da sé.)

Signor Cavaliere, non conviene ridersi delle debolezze altrui, quando si
ha un cuore fragile come il vostro.

CAVALIERE:

Di che intendete voi di parlare?

CONTE:

So da che provengono le vostre smanie.

CAVALIERE:

Intendete voi di che parli?

(Alterato, al Marchese.)

MARCHESE:

Amico, io non so niente.

CONTE:

Parlo di voi, che col pretesto di non poter soffrire le donne, avete
tentato rapirmi il cuore di Mirandolina, ch\'era già mia conquista.

CAVALIERE:

Io?

(Alterato, verso il Marchese.)

MARCHESE:

Io non parlo.

CONTE:

Voltatevi a me, a me rispondete. Vi vergognate forse d\'aver mal
proceduto?

CAVALIERE:

Io mi vergogno d\'ascoltarvi più oltre, senza dirvi che voi mentite.

CONTE:

A me una mentita?

MARCHESE:

(La cosa va peggiorando).

(Da sé.)

CAVALIERE:

Con qual fondamento potete voi dire?\... (Il Conte non sa ciò che si
dica).

(Al Marchese, irato.)

MARCHESE:

Ma io non me ne voglio impiciare.

CONTE:

Voi siete un mentitore.

MARCHESE:

Vado via.

(Vuol partire.)

CAVALIERE:

Fermatevi.

(Lo trattiene per forza.)

CONTE:

E mi renderete conto\...

CAVALIERE:

Sì, vi renderò conto\... Datemi la vostra spada.

(Al Marchese.)

MARCHESE:

Eh via, acquietatevi tutti due. Caro Conte, cosa importa a voi che il
Cavaliere ami Mirandolina?\...

CAVALIERE:

Io l\'amo? Non è vero; mente chi lo dice.

MARCHESE:

Mente? La mentita non viene da me. Non sono io che lo dico.

CAVALIERE:

Chi dunque?

CONTE:

Io lo dico e lo sostengo, e non ho soggezione di voi.

CAVALIERE:

Datemi quella spada.

(Al Marchese.)

MARCHESE:

No, dico.

CAVALIERE:

Siete ancora voi mio nemico?

MARCHESE:

Io sono amico di tutti.

CONTE:

Azioni indegne son queste.

CAVALIERE:

Ah giuro al Cielo!

(Leva la spada al Marchese, la quale esce col fodero.)

MARCHESE:

Non mi perdete il rispetto.

(Al Cavaliere.)

CAVALIERE:

Se vi chiamate offeso, darò soddisfazione anche a voi.

(Al Marchese.)

MARCHESE:

Via; siete troppo caldo. (Mi dispiace\...)

(Da se, rammaricandosi.)

CONTE:

Io voglio soddisfazione.

(Si mette in guardia.)

CAVALIERE:

Ve la darò.

(Vuol levar il fodero, e non può.)

MARCHESE:

Quella spada non vi conosce\...

CAVALIERE:

Oh maledetta!

(Sforza per cavarlo.)

MARCHESE:

Cavaliere, non farete niente\...

CONTE:

Non ho più sofferenza.

CAVALIERE:

Eccola.

(Cava la spada, e vede essere mezza lama.)

Che è questo?

MARCHESE:

Mi avete rotta la spada.

CAVALIERE:

Il resto dov\'è? Nel fodero non v\'è niente.

MARCHESE:

Sì, è vero; l\'ho rotta nell\'ultimo duello; non me ne ricordavo.

CAVALIERE:

Lasciatemi provveder d\'una spada.

(Al Conte.)

CONTE:

Giuro al cielo, non mi fuggirete di mano.

CAVALIERE:

Che fuggire? Ho cuore di farvi fronte anche con questo pezzo di lama.

MARCHESE:

È lama di Spagna, non ha paura.

CONTE:

Non tanta bravura, signor gradasso.

CAVALIERE:

Sì, con questa lama.

(S\'avventa verso il Conte.)

CONTE:

Indietro.

(Si pone in difesa.)

SCENA DICIOTTESIMA {#Section0011.xhtml#sigil_toc_id_98}
------------------

Mirandolina, Fabrizio e detti.

FABRIZIO:

Alto, alto, padroni.

MIRANDOLINA:

Alto, signori miei, alto.

CAVALIERE:

(Ah maledetta!).

(Vedendo Mirandolina.)

MIRANDOLINA:

Povera me! Colle spade?

MARCHESE:

Vedete? Per causa vostra.

MIRANDOLINA:

Come per causa mia?

CONTE:

Eccolo lì il signor Cavaliere. È innamorato di voi.

CAVALIERE:

Io innamorato? Non è vero; mentite.

MIRANDOLINA:

Il signor Cavaliere innamorato di me? Oh no, signor Conte, ella
s\'inganna. Posso assicurarla, che certamente s\'inganna.

CONTE:

Eh, che siete voi pur d\'accordo\...

MIRANDOLINA:

Si, si vede\...

CAVALIERE:

Che si sa? Che si vede?

(Alterato, verso il Marchese.)

MARCHESE:

Dico, che quando è, si sa\... Quando non è, non si vede.

MIRANDOLINA:

Il signor cavaliere innamorato di me? Egli lo nega, e negandolo in
presenza mia, mi mortifica, mi avvilisce, e mi fa conoscere la sua
costanza e la mia debolezza. Confesso il vero, che se riuscito mi fosse
d\'innamorarlo, avrei creduto di fare la maggior prodezza del mondo. Un
uomo che non può vedere le donne, che le disprezza, che le ha in mal
concetto, non si può sperare d\'innamorarlo. Signori miei, io sono una
donna schietta e sincera: quando devo dir, dico, e non posso celare la
verità. Ho tentato d\'innamorare il signor Cavaliere, ma non ho fatto
niente.

(Al Cavaliere.)

CAVALIERE:

(Ah! Non posso parlare).

(Da sé.)

CONTE:

Lo vedete? Si confonde.

(A Mirandolina.)

MARCHESE:

Non ha coraggio di dir di no.

(A Mirandolina.)

CAVALIERE:

Voi non sapete quel che vi dite.

(Al Marchese, irato.)

MARCHESE:

E sempre l\'avete con me.

(Al Cavaliere, dolcemente.)

MIRANDOLINA:

Oh, il signor Cavaliere non s\'innamora. Conosce l\'arte. Sa la furberia
delle donne: alle parole non crede; delle lagrime non si fida. Degli
svenimenti poi se ne ride.

CAVALIERE:

Sono dunque finte le lagrime delle donne, sono mendaci gli svenimenti?

MIRANDOLINA:

Come! Non lo sa, o finge di non saperlo?

CAVALIERE:

Giuro al cielo! Una tal finzione meriterebbe uno stile nel cuore.

MIRANDOLINA:

Signor Cavaliere, non si riscaldi, perché questi signori diranno ch\'è
innamorato davvero.

CONTE:

Sì, lo è, non lo può nascondere.

MARCHESE:

Si vede negli occhi.

CAVALIERE:

No, non lo sono.

(Irato al Marchese.)

MARCHESE:

E sempre con me.

MIRANDOLINA:

No signore, non è innamorato. Lo dico, lo sostengo, e son pronta a
provarlo.

CAVALIERE:

(Non posso più).

(Da sé.)

Conte, ad altro tempo mi troverete provveduto di spada.

(Getta via la mezza spada del Marchese.)

MARCHESE:

Ehi! la guardia costa denari.

(La prende di terra.)

MIRANDOLINA:

Si fermi, signor Cavaliere, qui ci va della sua riputazione. Questi
signori credono ch\'ella sia innamorato; bisogna disingannarli.

CAVALIERE:

Non vi è questo bisogno.

MIRANDOLINA:

Oh sì, signore. Si trattenga un momento.

CAVALIERE:

(Che far intende costei?).

(Da sé.)

MIRANDOLINA:

Signori, il più certo segno d\'amore è quello della gelosia, e chi non
sente la gelosia, certamente non ama. Se il signor Cavaliere mi amasse,
non potrebbe soffrire ch\'io fossi d\'un altro, ma egli lo soffrirà, e
vedranno\...

CAVALIERE:

Di chi volete voi essere?

MIRANDOLINA:

Di quello a cui mi ha destinato mio padre.

FABRIZIO:

Parlate forse di me?

(A Mirandolina.)

MIRANDOLINA:

Sì, caro Fabrizio, a voi in presenza di questi cavalieri vo\' dar la
mano di sposa.

CAVALIERE:

(Oimè! Con colui? non ho cuor di soffrirlo).

(Da sé, smaniando.)

CONTE:

(Se sposa Fabrizio, non ama il Cavaliere).

(Da sé.)

Sì, sposatevi, e vi prometto trecento scudi.

MARCHESE:

Mirandolina, è meglio un uovo oggi, che una gallina domani. Sposatevi
ora, e vi do subito dodici zecchini.

MIRANDOLINA:

Grazie, signori, non ho bisogno di dote. Sono una povera donna senza
grazia, senza brio, incapace d\'innamorar persone di merito. Ma Fabrizio
mi vuol bene, ed io in questo punto alla presenza loro lo sposo\...

CAVALIERE:

Sì, maledetta, sposati a chi tu vuoi. So che tu m\'ingannasti, so che
trionfi dentro di te medesima d\'avermi avvilito, e vedo sin dove vuoi
cimentare la mia tolleranza. Meriteresti che io pagassi gli inganni tuoi
con un pugnale nel seno; meriteresti ch\'io ti strappassi il cuore, e lo
recassi in mostra alle femmine lusinghiere, alle femmine ingannatrici.
Ma ciò sarebbe un doppiamente avvilirmi. Fuggo dagli occhi tuoi:
maledico le tue lusinghe, le tue lagrime, le tue finzioni; tu mi hai
fatto conoscere qual infausto potere abbia sopra di noi il tuo sesso, e
mi hai fatto a costo mio imparare, che per vincerlo non basta, no,
disprezzarlo, ma ci conviene fuggirlo.

(Parte.)

SCENA DICIANNOVESIMA {#Section0011.xhtml#sigil_toc_id_99}
--------------------

Mirandolina, il Conte, il Marchese e Fabrizio.

CONTE:

Dica ora di non essere innamorato.

MARCHESE:

Se mi dà un\'altra mentita, da cavaliere lo sfido.

MIRANDOLINA:

Zitto, signori zitto. È andato via, e se non torna, e se la cosa passa
così, posso dire di essere fortunata. Pur troppo, poverino, mi è
riuscito d\'innamorarlo, e mi son messa ad un brutto rischio. Non ne
vo\' saper altro. Fabrizio, vieni qui, caro, dammi la mano.

FABRIZIO:

La mano? Piano un poco, signora. Vi dilettate d\'innamorar la gente in
questa maniera, e credete ch\'io vi voglia sposare?

MIRANDOLINA:

Eh via, pazzo! È stato uno scherzo, una bizzarria, un puntiglio. Ero
fanciulla, non avevo nessuno che mi comandasse. Quando sarò maritata, so
io quel che farò.

FABRIZIO:

Che cosa farete?

SCENA ULTIMA {#Section0011.xhtml#sigil_toc_id_100}
------------

Il Servitore del Cavaliere e detti.

SERVITORE:

Signora padrona, prima di partire son venuto a riverirvi.

MIRANDOLINA:

Andate via?

SERVITORE:

Sì. Il padrone va alla Posta. Fa attaccare: mi aspetta colla roba, e ce
ne andiamo a Livorno.

MIRANDOLINA:

Compatite, se non vi ho fatto\...

SERVITORE:

Non ho tempo da trattenermi. Vi ringrazio, e vi riverisco.

(Parte.)

MIRANDOLINA:

Grazie al cielo, è partito. Mi resta qualche rimorso; certamente è
partito con poco gusto. Di questi spassi non me ne cavo mai più.

CONTE:

Mirandolina, fanciulla o maritata che siate, sarò lo stesso per voi.

MARCHESE:

Fate pure capitale della mia protezione.

MIRANDOLINA:

Signori miei, ora che mi marito, non voglio protettori, non voglio
spasimanti, non voglio regali. Sinora mi sono divertita, e ho fatto
male, e mi sono arrischiata troppo, e non lo voglio fare mai più. Questi
è mio marito\...

FABRIZIO:

Ma piano, signora\...

MIRANDOLINA:

Che piano! Che cosa c\'è? Che difficoltà ci sono? Andiamo. Datemi quella
mano.

FABRIZIO:

Vorrei che facessimo prima i nostri patti.

MIRANDOLINA:

Che patti? Il patto è questo: o dammi la mano, o vattene al tuo paese.

FABRIZIO:

Vi darò la mano\... ma poi\...

MIRANDOLINA:

Ma poi, sì, caro, sarò tutta tua; non dubitare di me ti amerò sempre,
sarai l\'anima mia.

FABRIZIO:

Tenete, cara, non posso più.

(Le dà la mano.)

MIRANDOLINA:

(Anche questa è fatta).

(Da sé.)

CONTE:

Mirandolina, voi siete una gran donna, voi avete l\'abilità di condur
gli uomini dove volete.

MARCHESE:

Certamente la vostra maniera obbliga infinitamente.

MIRANDOLINA:

Se è vero ch\'io possa sperar grazie da lor signori, una ne chiedo loro
per ultimo.

CONTE:

Dite pure.

MARCHESE:

Parlate.

FABRIZIO:

(Che cosa mai adesso domanderà?).

(Da sé.)

MIRANDOLINA:

Le supplico per atto di grazia, a provvedersi di un\'altra locanda.

FABRIZIO:

(Brava; ora vedo che la mi vuol bene).

(Da sé.)

CONTE:

Sì, vi capisco e vi lodo. Me ne andrò, ma dovunque io sia, assicuratevi
della mia stima.

MARCHESE:

Ditemi: avete voi perduta una boccettina d\'oro?

MIRANDOLINA:

Sì signore.

MARCHESE:

Eccola qui. L\'ho ritrovata, e ve la rendo. Partirò per compiacervi, ma
in ogni luogo fate pur capitale della mia protezione.

MIRANDOLINA:

Queste espressioni mi saran care, nei limiti della convenienza e
dell\'onestà. Cambiando stato, voglio cambiar costume; e lor signori
ancora profittino di quanto hanno veduto, in vantaggio e sicurezza del
loro cuore; e quando mai si trovassero in occasioni di dubitare, di
dover cedere, di dover cadere, pensino alle malizie imparate, e si
ricordino della Locandiera.

Fine della Commedia
[]{#Section0001.xhtml}

[![Copertina di Il servitore di due padroni di Carlo Goldoni](Images/copertina.jpg)](../Text/Section0002.xhtml) {.testo_copertina title="Copertina"}
===============================================================================================================

[]{#Section0002.xhtml}

Informazioni
============

Questo e-book è stato realizzato anche grazie al sostegno di:

::: {.box_sponsor}
[![E-text](Images/e-text.png)](http://www.e-text.it/)\
**E-text**\
Editoria, Web design, Multimedia

[Pubblica il tuo libro, o crea il tuo sito con
E-text!](http://www.e-text.it/)
:::

QUESTO E-BOOK:

TITOLO: Il servitore di due padroni\
AUTORE: Goldoni, Carlo\
TRADUTTORE:\
CURATORE: Folena, Gianfranco\
NOTE:

CODICE ISBN E-BOOK: 9788828100188

DIRITTI D\'AUTORE: no

LICENZA: questo testo è distribuito con la licenza specificata al
seguente indirizzo Internet:
<http://www.liberliber.it/online/opere/libri/licenze/>.

COPERTINA: \[elaborazione da\] \"Harlequin and a Lady\" di Konstantin
Somov (1869--1939). - Russian museum in Saint Petersburg.\
https://commons.wikimedia.org/wiki/File:Konstantin\_Somov\_Harlequin\_and\_a\_Lady\_1921.jpg.
- Pubblico Dominio.

TRATTO DA: Il servitore di due padroni / Carlo Goldoni. - Opere di Carlo
Goldoni / a cura di Gianfranco Folena. - Milano : U. Mursia, 1969. -
XXIX, 1618 ; 20 cm. (I classici italiani ; 7)

CODICE ISBN FONTE: n.d.

1a EDIZIONE ELETTRONICA DEL: 1 novembre 1996\
2a EDIZIONE ELETTRONICA DEL: 15 novembre 2016

INDICE DI AFFIDABILITÀ: 1\
0: affidabilità bassa\
1: affidabilità standard\
2: affidabilità buona\
3: affidabilità ottima

SOGGETTO:\
PER015000 ARTI RAPPRESENTATIVE / Commedia

DIGITALIZZAZIONE:\
Mirko Spadaro, mirko\_spadaro\@rcm.inet.it\
Claudio Paganelli, paganelli\@mclink.it

REVISIONE:\
Claudio Paganelli, paganelli\@mclink.it\
Ugo Santamaria

IMPAGINAZIONE:\
Rosario Di Mauro (ODT)\
Mariano Piscopo (ePub)\
Rosario Di Mauro (revisione ePub)

PUBBLICAZIONE:\
Claudio Paganelli, paganelli\@mclink.it\
Ugo Santamaria

Informazioni sul \"progetto Manuzio\"

Il \"progetto Manuzio\" è una iniziativa dell\'associazione culturale
Liber Liber. Aperto a chiunque voglia collaborare, si pone come scopo la
pubblicazione e la diffusione gratuita di opere letterarie in formato
elettronico. Ulteriori informazioni sono disponibili sul sito Internet:
<http://www.liberliber.it/>

Aiuta anche tu il \"progetto Manuzio\"

Se questo \"libro elettronico\" è stato di tuo gradimento, o se
condividi le finalità del \"progetto Manuzio\", invia una donazione a
Liber Liber. Il tuo sostegno ci aiuterà a far crescere ulteriormente la
nostra biblioteca. Qui le istruzioni:
<http://www.liberliber.it/online/aiuta/>

[]{#Section0003.xhtml}

Indice generale

-   [Copertina](../Text/Section0001.xhtml)
-   [Informazioni](../Text/Section0002.xhtml)
-   [L\'autore a chi legge](../Text/Section0005.xhtml)
-   [PERSONAGGI](../Text/Section0006.xhtml)
-   [ATTO PRIMO](../Text/Section0007.xhtml)
    -   [SCENA PRIMA](../Text/Section0007.xhtml#sigil_toc_id_1)
    -   [SCENA SECONDA](../Text/Section0007.xhtml#sigil_toc_id_2)
    -   [SCENA TERZA](../Text/Section0007.xhtml#sigil_toc_id_3)
    -   [SCENA QUARTA](../Text/Section0007.xhtml#sigil_toc_id_4)
    -   [SCENA QUINTA](../Text/Section0007.xhtml#sigil_toc_id_5)
    -   [SCENA SESTA](../Text/Section0007.xhtml#sigil_toc_id_6)
    -   [SCENA SETTIMA](../Text/Section0007.xhtml#sigil_toc_id_7)
    -   [SCENA OTTAVA](../Text/Section0007.xhtml#sigil_toc_id_8)
    -   [SCENA NONA](../Text/Section0007.xhtml#sigil_toc_id_9)
    -   [SCENA DECIMA](../Text/Section0007.xhtml#sigil_toc_id_10)
    -   [SCENA UNDICESIMA](../Text/Section0007.xhtml#sigil_toc_id_11)
    -   [SCENA DODICESIMA](../Text/Section0007.xhtml#sigil_toc_id_12)
    -   [SCENA TREDICESIMA](../Text/Section0007.xhtml#sigil_toc_id_13)
    -   [SCENA
        QUATTORDICESIMA](../Text/Section0007.xhtml#sigil_toc_id_14)
    -   [SCENA QUINDICESIMA](../Text/Section0007.xhtml#sigil_toc_id_15)
    -   [SCENA SEDICESIMA](../Text/Section0007.xhtml#sigil_toc_id_16)
    -   [SCENA
        DICIASSETTESIMA](../Text/Section0007.xhtml#sigil_toc_id_17)
    -   [SCENA DICIOTTESIMA](../Text/Section0007.xhtml#sigil_toc_id_18)
    -   [SCENA
        DICIANNOVESIMA](../Text/Section0007.xhtml#sigil_toc_id_19)
    -   [SCENA VENTESIMA](../Text/Section0007.xhtml#sigil_toc_id_20)
    -   [SCENA VENTUNESIMA](../Text/Section0007.xhtml#sigil_toc_id_21)
    -   [SCENA VENTIDUESIMA](../Text/Section0007.xhtml#sigil_toc_id_22)
-   [ATTO SECONDO](../Text/Section0008.xhtml)
    -   [SCENA PRIMA](../Text/Section0008.xhtml#sigil_toc_id_23)
    -   [SCENA SECONDA](../Text/Section0008.xhtml#sigil_toc_id_24)
    -   [SCENA TERZA](../Text/Section0008.xhtml#sigil_toc_id_25)
    -   [SCENA QUARTA](../Text/Section0008.xhtml#sigil_toc_id_26)
    -   [SCENA QUINTA](../Text/Section0008.xhtml#sigil_toc_id_27)
    -   [SCENA SESTA](../Text/Section0008.xhtml#sigil_toc_id_28)
    -   [SCENA SETTIMA](../Text/Section0008.xhtml#sigil_toc_id_29)
    -   [SCENA OTTAVA](../Text/Section0008.xhtml#sigil_toc_id_30)
    -   [SCENA NONA](../Text/Section0008.xhtml#sigil_toc_id_31)
    -   [SCENA DECIMA](../Text/Section0008.xhtml#sigil_toc_id_32)
    -   [SCENA UNDICESIMA](../Text/Section0008.xhtml#sigil_toc_id_33)
    -   [SCENA DODICESIMA](../Text/Section0008.xhtml#sigil_toc_id_34)
    -   [SCENA TREDICESIMA](../Text/Section0008.xhtml#sigil_toc_id_35)
    -   [SCENA
        QUATTORDICESIMA](../Text/Section0008.xhtml#sigil_toc_id_36)
    -   [SCENA QUINDICESIMA](../Text/Section0008.xhtml#sigil_toc_id_37)
    -   [SCENA SEDICESIMA](../Text/Section0008.xhtml#sigil_toc_id_38)
    -   [SCENA
        DICIASSETTESIMA](../Text/Section0008.xhtml#sigil_toc_id_39)
    -   [SCENA DICIOTTESIMA](../Text/Section0008.xhtml#sigil_toc_id_40)
    -   [SCENA
        DICIANNOVESIMA](../Text/Section0008.xhtml#sigil_toc_id_41)
    -   [SCENA VENTESIMA](../Text/Section0008.xhtml#sigil_toc_id_42)
-   [ATTO TERZO](../Text/Section0009.xhtml)
    -   [SCENA PRIMA](../Text/Section0009.xhtml#sigil_toc_id_43)
    -   [SCENA SECONDA](../Text/Section0009.xhtml#sigil_toc_id_44)
    -   [SCENA TERZA](../Text/Section0009.xhtml#sigil_toc_id_45)
    -   [SCENA QUARTA](../Text/Section0009.xhtml#sigil_toc_id_46)
    -   [SCENA QUINTA](../Text/Section0009.xhtml#sigil_toc_id_47)
    -   [SCENA SESTA](../Text/Section0009.xhtml#sigil_toc_id_48)
    -   [SCENA SETTIMA](../Text/Section0009.xhtml#sigil_toc_id_49)
    -   [SCENA OTTAVA](../Text/Section0009.xhtml#sigil_toc_id_50)
    -   [SCENA NONA](../Text/Section0009.xhtml#sigil_toc_id_51)
    -   [SCENA DECIMA](../Text/Section0009.xhtml#sigil_toc_id_52)
    -   [SCENA UNDICESIMA](../Text/Section0009.xhtml#sigil_toc_id_53)
    -   [SCENA DODICESIMA](../Text/Section0009.xhtml#sigil_toc_id_54)
    -   [SCENA TREDICESIMA](../Text/Section0009.xhtml#sigil_toc_id_55)
    -   [SCENA
        QUATTORDICESIMA](../Text/Section0009.xhtml#sigil_toc_id_56)
    -   [SCENA QUINDICESIMA](../Text/Section0009.xhtml#sigil_toc_id_57)
    -   [SCENA SEDICESIMA](../Text/Section0009.xhtml#sigil_toc_id_58)
    -   [SCENA ULTIMA](../Text/Section0009.xhtml#sigil_toc_id_59)

[]{#Section0004.xhtml}

Il servitore di due padroni

di Carlo Goldoni

[]{#Section0005.xhtml}

L\'autore a chi legge
=====================

Troverai, Lettor carissimo, la presente Commedia diversa moltissimo
dall\'altre mie, che lette avrai finora. Ella non è di carattere, se non
se carattere considerare si voglia quello del
[Truffaldino]{.testo_corsivo}, che un servitore sciocco ed astuto nel
medesimo tempo ci rappresenta: sciocco cioè in quelle cose le quali
impensatamente e senza studio egli opera, ma accortissimo allora quando
l\'interesse e la malizia l\'addestrano, che è il vero carattere del
villano.\
Ella può chiamarsi piuttosto Commedia giocosa, perché di essa il gioco
di [Truffaldino]{.testo_corsivo} forma la maggior parte. Rassomiglia
moltissimo alle commedie usuali degl\'Istrioni, se non che scevra mi
pare di tutte quelle improprietà grossolane, che nel mio Teatro Comico
ho condannate, e che dal Mondo sono oramai generalmente aborrite.\
Improprietà potrebbe parere agli scrupolosi, che
[Truffaldino]{.testo_corsivo} mantenga l\'equivoco della sua doppia
servitù, anche in faccia dei due padroni medesimi soltanto per questo,
perché niuno di essi lo chiama mai col suo nome; che se una volta sola,
o [Florindo]{.testo_corsivo}, o [Beatrice]{.testo_corsivo}, nell\'Atto
terzo, dicessero [Truffaldino]{.testo_corsivo}, in luogo di dir sempre
[il mio Servitore]{.testo_corsivo}, l\'equivoco sarebbe sciolto e la
commedia sarebbe allora terminata. Ma di questi equivoci, sostenuti
dall\'arte dell\'Inventore, ne sono piene le Commedie non solo, ma le
Tragedie ancora; e quantunque io m\'ingegni d\'essere osservante del
verisimile in una Commedia giocosa, credo che qualche cosa, che non sia
impossibile, si possa facilitare.\
Sembrerà a taluno ancora, che troppa distanza siavi dalla sciocchezza
l\'astuzia di [Truffaldino]{.testo_corsivo}; per esempio: lacerare una
cambiale per disegnare la scalcherìa di una tavola, pare l\'eccesso
della goffaggine. Servire a due padroni, in due camere, nello stesso
tempo, con tanta prontezza e celerità, pare l\'eccesso della furberia.
Ma appunto quel ch\'io dissi a principio del carattere di
[Truffaldino]{.testo_corsivo}: sciocco allor che opera senza pensamento,
come quando lacera la cambiale; astutissimo quando opera con malizia,
come nel servire a due tavole comparisce.\
Se poi considerar vogliamo la catastrofe della Commedia, la peripezia,
l\'intreccio, [Truffaldino]{.testo_corsivo} non fa figura da
protagonista, anzi, se escludere vogliamo la supposta vicendevole morte
de\' due amanti, creduta per opera di questo servo, la Commedia si
potrebbe fare senza di lui; ma anche di ciò abbiamo infiniti esempi,
quali io non adduco per non empire soverchiamente i fogli; e perché non
mi credo in debito di provare ciò che mi lusingo non potermi essere
contraddetto; per altro il celebre [Molière]{lang="fr"} istesso mi
servirebbe di scorta a giustificarmi.\
Quando io composi la presente Commedia, che fu nell\'anno 1745, in Pisa,
fra le cure legali, per trattenimento e per genio, non la scrissi io
già, come al presente si vede. A riserva di tre o quattro scene per
atto, le più interessanti per le parti serie, tutto il resto della
Commedia era accennato soltanto, in quella maniera che i commedianti
sogliono denominare \"a soggetto\"; cioè uno scenario disteso, in cui
accennando il proposito, le tracce, e la condotta e il fine de\'
ragionamenti, che dagli Attori dovevano farsi, era poi in libertà de\'
medesimi supplire all\'improvviso, con adattate parole e acconci lazzi,
spiritosi concetti. In fatti fu questa mia Commedia all\'improvviso così
bene eseguita da\' primi Attori che la rappresentarono, che io me ne
compiacqui moltissimo, e non ho dubbio a credere che meglio essi non
l'abbiano all'improvviso adornata, di quello possa aver io fatto
scrivendola. I sali del [Truffaldino]{.testo_corsivo}, le facezie, le
vivezze sono cose che riescono più saporite, quando prodotte sono sul
fatto dalla prontezza di spirito, dall'occasione, dal brio. Quel celebre
eccellente comico, noto all'Italia tutta pel nome appunto di
[Truffaldino]{.testo_corsivo}, ha una prontezza tale di spirito, una
tale abbondanza di sali e naturalezza di termini, che sorprende: e
volendo io provvedermi per le parti di lui. Questa Commedia l'ha
disegnata espressamente per lui, anzi mi ha egli medesimo l'argomento
proposto, argomento un po\' difficile in vero, che ha posto in cimento
tutto il genio mio per la Comica artificiosa, e tutto il talento suo per
l'esecuzione.\
L\'ho poi veduta in altre parti da altri comici rappresentare, e per
mancanza forse non di merito, ma di quelle notizie che dallo scenario
soltanto aver non poteano, parmi ch'ella decadesse moltissimo dal primo
aspetto. Mi sono per questa ragione indotto a scriverla tutta, non già
per obbligare quelli che sosterranno il carattere del
[Truffaldino]{.testo_corsivo} a dir per l'appunto le parole mie, quando
di meglio ne sappian dire, ma per dichiarare la mia intenzione, e per
una strada assai dritta condurli al fine.\
Affaticato mi sono a distendere tutti i lazzi più necessari, tutte le
più minute osservazioni, per renderla facile quanto mai ho potuto, e se
non ha essa il merito della Critica, della Morale, della istruzione,
abbia almeno quello di una ragionevole condotta e di un discreto
ragionevole gioco.\
Prego però que\' tali, che la parte del [Truffaldino]{.testo_corsivo}
rappresenteranno, qualunque volta aggiungere del suo vi volessero,
astenersi dalle parole sconce, da\' lazzi sporchi; sicuri che di tali
cose ridono soltanto quelli della vil plebe, e se ne offendono le
gentili persone.

[]{#Section0006.xhtml}

PERSONAGGI
==========

Pantalone de\' Bisognosi\
Clarice, [sua figliuola]{.testo_corsivo}\
Il Dottore Lombardi Silvio, [di lui figliuolo]{.testo_corsivo}\
Beatrice, [torinese, in abito da uomo sotto nome di Federigo
Rasponi]{.testo_corsivo}\
Florindo Aretusi, [torinese di lei amante]{.testo_corsivo}\
Brighella, [locandiere]{.testo_corsivo}\
Smeraldina, [cameriera di Clarice]{.testo_corsivo}\
Truffaldino, [servitore di Beatrice, poi di Florindo]{.testo_corsivo}\
Un cameriere della locanda, [che parla]{.testo_corsivo}\
Un servitore di Pantalone, [che parla]{.testo_corsivo}\
Due facchini, [che parlano]{.testo_corsivo}\
Camerieri d\'osteria, [che non parlano]{.testo_corsivo}

La scena si rappresenta in Venezia

[]{#Section0007.xhtml}

ATTO PRIMO
==========

SCENA PRIMA {#Section0007.xhtml#sigil_toc_id_1}
-----------

Camera in casa di Pantalone Pantalone, il Dottore, Clarice, Silvio,
Brighella, Smeraldina, un altro Servitore di Pantalone.

SILVIO

Eccovi la mia destra, e con questa vi dono tutto il mio cuore

(a Clarice, porgendole la mano).

PANTALONE

Via, no ve vergognè; dèghe la man anca vu. Cusì sarè promessi, e presto
presto sarè maridai

(a Clarice).

CLARICE

Sì caro Silvio, eccovi la mia destra. Prometto di essere vostra sposa.

SILVIO

Ed io prometto esser vostro.

(Si danno la mano.)

DOTTORE

Bravissimi, anche questa è fatta. Ora non si torna più indietro.

SMERALDINA

(Oh bella cosa! Propriamente anch\'io me ne struggo di voglia).

PANTALONE

Vualtri sarè testimoni de sta promission, seguida tra Clarice mia fia e
el sior Silvio, fio degnissimo del nostro sior dottor Lombardi

(a Brighella ed al Servitore).

BRIGHELLA

Sior sì, sior compare, e la ringrazio de sto onor che la se degna de
farme

(a Pantalone).

PANTALONE

Vedeu? Mi son stà compare alle vostre nozze, e vu se testimonio alle
nozze de mia fia. Non ho volesto chiamar compari, invidar parenti,
perchè anca sior Dottor el xè del mio temperamento; ne piase far le
cosse senza strepito, senza grandezze. Magneremo insieme, se goderemo
tra de nu, e nissun ne disturberà. Cossa diseu, putti, faremio pulito?

(a Clarice e Silvio).

SILVIO

Io non desidero altro che essere vicino alla mia cara sposa.

SMERALDINA

(Certo che questa è la migliore vivanda).

DOTTORE

Mio figlio non è amante della vanità. Egli è un giovane di buon cuore.
Ama la vostra figliuola, e non pensa ad altro.

PANTALONE

Bisogna dir veramente che sto matrimonio el sia stà destinà dal cielo,
perché se a Turin no moriva sior Federigo Rasponi, mio corrispondente,
savè che mia fia ghe l\'aveva promessa a elo, e no la podeva toccar al
mio caro sior zenero

(verso Silvio).

SILVIO

Certamente io posso dire di essere fortunato. Non so se dirà così la
signora Clarice.

CLARICE

Caro Silvio, mi fate torto. Sapete pur se vi amo; per obbedire il signor
padre avrei sposato quel torinese, ma il mio cuore è sempre stato per
voi.

DOTTORE

Eppur è vero; il cielo, quando ha decretato una cosa, la fa nascere per
vie non prevedute. Come è succeduta la morte di Federigo Rasponi?

(a Pantalone).

PANTALONE

Poverazzo! L\'è stà mazzà de notte per causa de una sorella\... No so
gnente. I gh\'ha dà una ferìa e el xè restà sulla botta.

BRIGHELLA

Elo successo a Turin sto fatto?

(a Pantalone).

PANTALONE

A Turin.

BRIGHELLA

Oh, povero signor! Me despiase infinitamente.

PANTALONE

Lo conossevi sior Federigo Rasponi?

(a Brighella).

BRIGHELLA

Siguro che lo conosseva. So stà a Turin tre anni e ho conossudo anca so
sorella. Una zovene de spirito, de corazo; la se vestiva da omo,
l\'andava a cavallo, e lu el giera innamorà de sta so sorella. Oh! chi
l\'avesse mai dito!

PANTALONE

Ma! Le disgrazie le xè sempre pronte. Orsù, no parlemo de malinconie.
Saveu cossa che v\'ho da dir, missier Brighella caro? So che ve diletè
de laorar ben in cusina. Vorave che ne fessi un per de piatti a vostro
gusto.

BRIGHELLA

La servirò volentiera. No fazzo per dir, ma alla mia locanda tutti se
contenta. I dis cusì che in nissun logo i magna, come che se magna da
mi. La sentirà qualcossa de gusto.

PANTALONE

Bravo. Roba brodosa, vedè, che se possa bagnarghe drento delle molene de
pan.

(Si sente picchiare).

Oh! i batte. Varda chi è, Smeraldina.

SMERALDINA

Subito

(parte, e poi ritorna).

CLARICE

Signor padre, con vostra buona licenza.

PANTALONE

Aspettè; vegnimo tutti. Sentimo chi xè.

SMERALDINA

(torna)

Signore, è un servitore di un forestiere che vorrebbe farvi
un\'imbasciata. A me non ha voluto dir nulla. Dice che vuol parlar col
padrone.

PANTALONE

Diseghe che el vegna avanti. Sentiremo cossa che el vol.

SMERALDINA

Lo farò venire

(parte).

CLARICE

Ma io me ne anderei, signor padre.

PANTALONE

Dove?

CLARICE

Che so io? Nella mia camera.

PANTALONE

Siora no, siora no; stè qua. (Sti novizzi non vòi gnancora che i lassemo
soli)

(piano al Dottore).

DOTTORE

(Saviamente, con prudenza)

(piano a Pantalone).

SCENA SECONDA {#Section0007.xhtml#sigil_toc_id_2}
-------------

Truffaldino, Smeraldina e detti.

TRUFFALDINO

Fazz umilissima reverenza a tutti lor siori. Oh, che bella compagnia!
Oh, che bella conversazion!

PANTALONE

Chi seu, amigo? Cossa comandeu?

(a Truffaldino).

TRUFFALDINO

Chi èla sta garbata signora?

(a Pantalone, accennando Clarice).

PANTALONE

La xè mia fia.

TRUFFALDINO

Me ne ralegher.

SMERALDINA

E di più è sposa

(a Truffaldino).

TRUFFALDINO

Me ne consolo. E ella chi èla?

(a Smeraldina).

SMERALDINA

Sono la sua cameriera, signore.

TRUFFALDINO

Me ne congratulo.

PANTALONE

Oh via, sior, a monte le cerimonie. Cossa voleu da mi? Chi seu? Chi ve
manda?

TRUFFALDINO

Adasio, adasio, colle bone. Tre interrogazion in t\'una volta l\'è
troppo per un poveromo.

PANTALONE

(Mi credo che el sia un sempio costù)

(piano al Dottore).

DOTTORE

(Mi par piuttosto un uomo burlevole)

(piano a Pantalone).

TRUFFALDINO

V. S. è la sposa?

(a Smeraldina).

SMERALDINA

Oh!

(sospirando)

Signor no.

PANTALONE

Voleu dir chi sè, o voleu andar a far i fatti vostri?

TRUFFALDINO

Co no la vol altro che saver chi son, in do parole me sbrigo. Son
servitor del me padron (a Pantalone). E cusì, tornando al nostro
proposito\...

(voltandosi a Smeraldina).

PANTALONE

Mo chi xèlo el vostro padron?

TRUFFALDINO

L\'è un forestier che vorave vegnir a farghe una visita

(a Pantalone).

Sul proposito dei sposi, discorreremo

(a Smeraldina, come sopra).

PANTALONE

Sto forestier chi xèlo? Come se chiamelo?

TRUFFALDINO

Oh, l\'è longa. L\'è el sior Federigo Rasponi torinese, el me padron,
che la reverisse, che l\'è vegnù a posta, che l\'è da basso, che el
manda l\'ambassada, che el vorria passar, che el me aspetta colla
risposta. Èla contenta? Vorla saver altro?

(a Pantalone. Tutti fanno degli atti di ammirazione).

Tornemo a nu\...

(a Smeraldina, come sopra).

PANTALONE

Mo vegni qua, parlè co mi. Cossa diavolo diseu?

TRUFFALDINO

E se la vol saver chi son mi, mi son Truffaldin Batocchio, dalle vallade
de Bergamo.

PANTALONE

No m\'importa de saver chi siè vu. Voria che me tornessi a dir chi xè
sto vostro padron. Ho paura de aver strainteso.

TRUFFALDINO

Povero vecchio! El sarà duro de recchie. El me padron l\'è el sior
Federigo Rasponi da Turin.

PANTALONE

Andè via, che sè un pezzo de matto. Sior Federigo Rasponi da Turin el xè
morto.

TRUFFALDINO

L\'è morto?

PANTALONE

L\'è morto seguro. Pur troppo per elo.

TRUFFALDINO

(Diavol! Che el me padron sia morto? L\'ho pur lassà vivo da basso!).
Disì da bon, che l\'è morto?

PANTALONE

Ve digo assolutamente che el xè morto.

DOTTORE

Sì, è la verità; è morto; non occorre metterlo in dubbio.

TRUFFALDINO

(Oh, povero el me padron! Ghe sarà vegnù un accidente). Con so bona
grazia

(si licenzia).

PANTALONE

No volè altro da mi?

TRUFFALDINO

Co l\'è morto, no m\'occorre altro. (Voi ben andar a veder, se l\'è la
verità)

(da sé, parte e poi ritorna).

PANTALONE

Cossa credemio che el sia costù? Un furbo, o un matto?

DOTTORE

Non saprei. Pare che abbia un poco dell\'uno e un poco dell\'altro.

BRIGHELLA

A mi el me par piuttosto un semplizotto. L\'è bergamasco, no crederia
che el fuss un baron

SMERALDINA

Anche l\'idea l\'ha buona. (Non mi dispiace quel morettino).

PANTALONE

Ma cossa se insonielo de sior Federigo?

CLARICE

Se fosse vero ch\'ei fosse qui, sarebbe per me una nuova troppo cattiva.

PANTALONE

Che spropositi! No aveu visto anca vu le lettere?

(a Clarice).

SILVIO

Se anche fosse egli vivo e fosse qui, sarebbe venuto tardi.

TRUFFALDINO

(ritorna)

Me maraveio de lor siori. No se tratta cusì colla povera zente. No se
inganna cusì i forestieri. No le son azion da galantomeni. E me ne farò
render conto.

PANTALONE

(Vardemose, che el xè matto). Coss\'è stà? Cossa v\'ali fatto?

TRUFFALDINO

Andarme a dir che sior Federigh Rasponi l\'è morto?

PANTALONE

E cusì?

TRUFFALDINO

E cusì l\'è qua, vivo, san, spiritoso e brillante, che el vol reverirla,
se la se contenta.

PANTALONE

Sior Federigo?

TRUFFALDINO

Sior Federigo.

PANTALONE

Rasponi?

TRUFFALDINO

Rasponi.

PANTALONE

Da Turin?

TRUFFALDINO

Da Turin.

PANTALONE

Fio mio, andè all\'ospeal, che sè matto.

TRUFFALDINO

Corpo del diavolo! Me farissi bestemiar come un zogador. Mo se l\'è qua,
in casa, in sala, che ve vegna el malanno.

PANTALONE

Adessadesso ghe rompo el muso.

DOTTORE

No, signor Pantalone, fate una cosa; ditegli che faccia venire innanzi
questo tale, ch\'egli crede essere Federigo Rasponi.

PANTALONE

Via, felo vegnir avanti sto morto ressuscità.

TRUFFALDINO

Che el sia stà morto e che el sia resuscità pol esser, mi no gh\'ho
niente in contrario. Ma adesso l\'è vivo, e el vederì coi vostri occhi.
Vagh a dirghe che el vegna. E da qua avanti imparè a trattar coi
forestieri, coi omeni della me sorte, coi bergamaschi onorati

(a Pantalone, con collera).

Quella giovine, a so tempo se parleremo

(a Smeraldina, e parte).

CLARICE

(Silvio mio, tremo tutta)

(piano a Silvio).

SILVIO

(Non dubitate; in qualunque evento sarete mia)

(piano a Clarice).

DOTTORE

Ora ci chiariremo della verità.

PANTALONE

Pol vegnir qualche baronato a darme da intender delle fandonie.

BRIGHELLA

Mi, come ghe diseva, sior compare, l\'ho conossudo el sior Federigo; se
el sarà lu, vederemo.

SMERALDINA

(Eppure quel morettino non ha una fisonomia da bugiardo. Voglio veder se
mi riesce\...). Con buona grazia di lor signori

(parte).

SCENA TERZA {#Section0007.xhtml#sigil_toc_id_3}
-----------

Beatrice in abito da uomo, sotto nome di Federigo, e detti.

BEATRICE

Signor Pantalone, la gentilezza che io ho ammirato nelle vostre lettere,
non corrisponde al trattamento che voi mi fate in persona. Vi mando il
servo, vi fo passar l\'ambasciata, e voi mi fate stare all\'aria aperta,
senza degnarvi di farmi entrare che dopo una mezz\'ora?

PANTALONE

La compatissa\... Ma chi xèla ella, patron?

BEATRICE

Federigo Rasponi di Torino, per obbedirvi.

(Tutti fanno atti d\'ammirazione).

BRIGHELLA

(Cossa vedio? Coss\'è sto negozio? Questo no l\'è Federigo, l\'è la
siora Beatrice so sorella. Voi osservar dove tende sto inganno).

PANTALONE

Mi resto attonito\... Me consolo de vederla san e vivo, quando avevimo
avudo delle cattive nove. (Ma gnancora no ghe credo, savè)

(piano al Dottore).

BEATRICE

Lo so: fu detto che in una rissa rimasi estinto. Grazie al cielo, fui
solamente ferito; e appena risanato, intrapresi il viaggio di Venezia,
già da gran tempo con voi concertato.

PANTALONE

No so cossa dir. La so ciera xè da galantomo: ma mi gh\'ho riscontri
certi e seguri, che sior Federigo sia morto; onde la vede ben\... se no
la me dà qualche prova in contrario\...

BEATRICE

È giustissimo il vostro dubbio; conosco la necessità di giustificarmi.
Eccovi quattro lettere dei vostri amici corrispondenti, una delle quali
è del ministro della nostra banca. Riconoscerete le firme, e vi
accerterete dell\'esser mio

(dà quattro lettere a Pantalone, il quale le legge da sé).

CLARICE

(Ah Silvio, siamo perduti!)

(piano a Silvio).

SILVIO

(La vita perderò, ma non voi!)

(piano a Clarice).

BEATRICE

(Oimè! Qui Brighella? Come diamine qui si ritrova costui? Egli mi
conoscerà certamente; non vorrei che mi discoprisse)

(da sé, avvedendosi di Brighella).

Amico, mi par di conoscervi

(forte a Brighella).

BRIGHELLA

Sì signor, no la s\'arrecorda a Turin Brighella Cavicchio?

BEATRICE

Ah sì, ora vi riconosco

(si va accostando a Brighella)

Bravo galantuomo, che fate in Venezia? (Per amor del cielo, non mi
scoprite)

(piano a Brighella).

BRIGHELLA

(Non gh\'è dubbio)

(piano a Beatrice).

Fazzo el locandier, per servirla

(forte alla medesima).

BEATRICE

Oh, per l\'appunto; giacché ho il piacer di conoscervi, verro ad
alloggiare alla vostra locanda.

BRIGHELLA

La me farà grazia. (Qualche contrabando, siguro).

PANTALONE

Ho sentio tutto. Certo che ste lettere le me accompagna el sior Federigo
Rasponi, e se ella me le presenta, bisognerave creder che la fosse\...
come che dise ste lettere.

BEATRICE

Se qualche dubbio ancor vi restasse, ecco qui messer Brighella; egli mi
conosce, egli può assicurarvi dell\'esser mio.

BRIGHELLA

Senz\'altro, sior compare, lo assicuro mi.

PANTALONE

Co la xè cusì, co me l\'attesta, oltre le lettere, anca mio compare
Brighella, caro sior Federigo, me ne consolo con ella, e ghe domando
scusa se ho dubita.

CLARICE

Signor padre, quegli è dunque il signor Federigo Rasponi?

PANTALONE

Mo el xè elo lu.

CLARICE

(Me infelice, che sarà di noi?)

(piano a Silvio).

SILVIO

(Non dubitate, vi dico; siete mia e vi difenderò)

(piano a Clarice).

PANTALONE

(Cossa diseu, dottor, xèlo vegnù a tempo?)

(piano al Dottore).

DOTTORE

*Accidit in puncto, quod non contingit in anno.*

BEATRICE

Signor Pantalone, chi è quella signora

(accennando Clarice).

PANTALONE

La xè Clarice mia fia.

BEATRICE

Quella a me destinata in isposa?

PANTALONE

Sior sì, giusto quella.

(Adesso son in t\'un bell\'intrigo).

BEATRICE

Signora, permettetemi ch\'io abbia l\'onore di riverirvi

(a Clarice).

CLARICE

Serva divota

(sostenuta).

BEATRICE

Molto freddamente m\'accoglie

(a Pantalone).

PANTALONE

Cossa vorla far? La xè timida de natura.

BEATRICE

E quel signore è qualche vostro parente?

(a Pantalone, accennando Silvio).

PANTALONE

Sior sì; el xè un mio nevodo.

SILVIO

No signore, non sono suo nipote altrimenti, sono lo sposo della signora
Clarice

(a Beatrice).

DOTTORE

(Bravo! Non ti perdere. Di\'la tua ragione, ma senza precipitare)

(piano a Silvio).

BEATRICE

Come! Voi sposo della signora Clarice? Non è ella a me destinata?

PANTALONE

Via, via. Mi scoverzirò tutto. Caro sior Federigo, se credeva che fosse
vera la vostra disgrazia che fussi morto, e cussi aveva dà mia fia a
sior Silvio; qua no ghe xè un mal al mondo. Finalmente sè arriva in
tempo. Clarice xè vostra, se la volè, e mi son qua a mantegnirve la mia
parola. Sior Silvio, no so cossa dir; vedè coi vostri occhi la verità.
Savè cossa che v\'ho dito, e de mi no ve podè lamentar.

SILVIO

Ma il signor Federigo non si contenterà di prendere una sposa, che porse
ad altri la mano.

BEATRICE

Io poi non sono si delicato. La prenderò non ostante.

(Voglio anche prendermi un poco di divertimento).

DOTTORE

(Che buon marito alla moda! Non mi dispiace).

BEATRICE

Spero che la signora Clarice non ricuserà la mia mano.

SILVIO

Orsù, signore, tardi siete arrivato. La signora Clarice deve esser mia,
né sperate che io ve la ceda. Se il signor Pantalone mi farà torto,
saprò vendicarmene; e chi vorrà Clarice, dovrà contenderla con questa
spada

(parte).

DOTTORE

(Bravo, corpo di Bacco!).

BEATRICE

(No, no, per questa via non voglio morire).

DOTTORE

Padrone mio, V. S. è arrivato un po\' tardi. La signora Clarice l\'ha da
sposare mio figlio. La legge parla chiaro. *Prior in tempore, potior in
iure*

(parte).

BEATRICE

Ma voi, signora sposa, non dite nulla?

(a Clarice).

CLARICE

Dico che siete venuto per tormentarmi

(parte).

SCENA QUARTA {#Section0007.xhtml#sigil_toc_id_4}
------------

Pantalone, Beatrice e Brighella, poi il Servitore di Pantalone.

PANTALONE

Come, pettegola? Cossa distu?

(le vuol correr dietro).

BEATRICE

Fermatevi, signor Pantalone; la compatisco. Non conviene prenderla con
asprezza. Col tempo spero di potermi meritare la di lei grazia. Intanto
andremo esaminando i nostri conti, che è uno dei due motivi per cui,
come vi è noto, mi son portato a Venezia.

PANTALONE

Tutto xè all\'ordine per el nostro conteggio. Ghe farò veder el conto
corrente; i so bezzi xè parechiai, e faremo el saldo co la vorrà.

BEATRICE

Verrò con più comodo a riverirvi; per ora, se mi permettete, andrò con
Brighella a spedire alcuni piccioli affari che mi sono stati
raccomandati. Egli è pratico della città, potrà giovarmi nelle mie
premure.

PANTALONE

La se serva come che la vol; e se la gh\'ha bisogno de gnente, la
comanda.

BEATRICE

Se mi darete un poco di denaro, mi farete piacere; non ho voluto
prenderne meco per non discapitare nelle monete.

PANTALONE

Volentiera, la servirò. Adesso no gh\'è el cassier. Subito che el vien,
ghe manderò i bezzi fina a casa. No vala a star da mio compare
Brighella?

BEATRICE

Certamente, vado da lui; e poi manderò il mio servitore; egli è
fidatissimo, gli si può fidar ogni cosa.

PANTALONE

Benissimo; la servirò come la comanda, e se la vol restar da mi a far
penitenza, la xè parona.

BEATRICE

Per oggi vi ringrazio. Un\'altra volta sarò a incomodarvi.

PANTALONE

Donca starò attendendola.

SERVITORE

Signore, è domandato

(a Pantalone).

PANTALONE

Da chi?

SERVITORE

Di là\... non saprei\... (Vi sono degl\'imbrogli)

(piano a Pantalone, e parte).

PANTALONE

Vegno subito. Con so bona grazia. La scusa, se no la compagno.
Brighella, vu sè de casa; servilo vu sior Federigo.

BEATRICE

Non vi prendete pena per me.

PANTALONE

Bisogna che vaga. A bon reverirla. (Non voria che nascesse qualche
diavolezzo)

(parte).

SCENA QUINTA {#Section0007.xhtml#sigil_toc_id_5}
------------

Beatrice e Brighella.

BRIGHELLA

Se pol saver, siora Beatrice?\...

BEATRICE

Chetatevi, per amor del cielo, non mi scoprite. II povero mio fratello è
morto, ed è rimasto ucciso o dalle mani di Florindo Aretusi, o da alcun
altro per di lui cagione. Vi sovverrete che Florindo mi amava, e mio
fratello non voleva che io gli corrispondessi. Si attaccarono non so
come: Federigo morì, e Florindo, per timore della giustizia, se n\'è
fuggito senza potermi dare un addio. Sa il cielo se mi dispiace la morte
del povero mio fratello, e quanto ho pianto per sua cagione; ma oramai
non vi è più rimedio, e mi duole la perdita di Florindo. So che a
Venezia erasi egli addrizzato, ed io ho fatto la risoluzione di
seguitarlo. Cogli abiti e colle lettere credenziali di mio fratello,
eccomi qui arrivata colla speranza di ritrovarvi l\'amante. Il signor
Pantalone, in grazia di quelle lettere, e in grazia molto più della
vostra asserzione, mi crede già Federigo. Faremo il saldo dei nostri
conti, riscuoterò del denaro, e potrò soccorrere anche Florindo, se ne
avrà di bisogno. Guardate dove conduce amore! Secondatemi, caro
Brighella, aiutatemi; sarete largamente ricompensato.

BRIGHELLA

Tutto va bene, ma no vorave esser causa mi che sior Pantalon, sotto bona
fede, ghe pagasse el contante e che po el restasse burlà.

BEATRICE

Come burlato? Morto mio fratello, non sono io l\'erede?

BRIGHELLA

L\'è la verità. Ma perché no scovrirse?

BEATRICE

Se mi scopro, non faccio nulla. Pantalone principierà a volermi far da
tutore, e tutti mi seccheranno, che non istà bene, che non conviene, e
che so io? Voglio la mia libertà. Durerà poco, ma pazienza. Frattanto
qualche cosa sarà.

BRIGHELLA

Veramente, signora, l\'è sempre stada un spiritin bizzarro. La lassa far
a mi, la staga su la mia fede. La se lassa servir.

BEATRICE

Andiamo alla vostra locanda.

BRIGHELLA

El so servitor dov\'elo?

BEATRICE

Ha detto che mi aspetterà sulla strada.

BRIGHELLA

Dove l\'ala tolto quel martuffo? Nol sa gnanca parlar.

BEATRICE

L\'ho preso per viaggio. Pare sciocco qualche volta, ma non lo è; e
circa la fedeltà non me ne posso dolere.

BRIGHELLA

Ah, la fedeltà l\'è una bella cossa. Andemo, la resta servida, vardè
amor cossa che el fa far.

BEATRICE

Questo non è niente. Amor ne fa far di peggio

(parte).

BRIGHELLA

Eh, avemo principià ben. Andando in là, no se sa cossa possa succeder

(parte).

SCENA SESTA {#Section0007.xhtml#sigil_toc_id_6}
-----------

Strada colla locanda di Brighella Truffaldino solo.

TRUFFALDINO

Son stuffo d\'aspettar, che no posso più. Co sto me patron se magna
poco, e quel poco el me lo fa suspirar. Mezzozorno della città l\'è sonà
che è mezz\'ora, e el mezzozorno delle mie budelle l\'è sonà che sarà do
ore. Almanco savesse dove s\'ha da andar a alozar. I alter subit che i
arriva in qualche città, la prima cossa i va all\'osteria. Lu, sior no,
el lassa i bauli in barca del corrier. el va a far visite, e nol se
recorda del povero servitor. Quand ch\'i dis, bisogna servir i padroni
con amor! Bisogna dir ai padroni, ch\'i abbia un poco de carità per la
servitù. Qua gh\'è una locanda; quasi quasi anderia a veder se ghe fuss
da devertir el dente; ma se el padron me cerca? So danno, che l\'abbia
un poco de discrezion. Voi andar; ma adess che ghe penso, gh\'è
un\'altra piccola difficoltà, che no me l\'arrecordava; non ho gnanca un
quattrin. Oh povero Truffaldin! Più tost che far el servitor, corpo del
diavol, me voi metter a far\... cossa mo? Per grazia del Cielo, mi no so
far gnente

SCENA SETTIMA {#Section0007.xhtml#sigil_toc_id_7}
-------------

Florindo da viaggio con un Facchino col baule in spalla, e detto.

FACCHINO

Ghe digo che no posso più; el pesa che el mazza.

FLORINDO

Ecco qui un\'insegna d\'osteria o di locanda. Non puoi far questi
quattro passi?

FACCHINO

Aiuto; el baul va in terra.

FLORINDO

L\'ho detto che tu non saresti stato al caso: sei troppo debole: non hai
forza

(regge il baule sulle spalle del Facchino).

TRUFFALDINO

(Se podess vadagnar diese soldi)

(osservando il Facchino).

Signor, comandela niente da mi? La possio servir?

(a Florindo).

FLORINDO

Caro galantuomo, aiutate a portare questo baule in quell\'albergo.

TRUFFALDINO

Subito, la lassa far a mi. La varda come se fa. Passa via

(va colla spalla sotto il baule, lo prende tutto sopra di sé, e caccia
in terra il Facchino con una spinta).

FLORINDO

Bravissimo.

TRUFFALDINO

Se nol pesa gnente!

(entra nella locanda col baule).

FLORINDO

Vedete come si fa?

(al Facchino).

FACCHINO

Mi no so far de più. Fazzo el facchin per desgrazia; ma son fiol de una
persona civil.

FLORINDO

Che cosa faceva vostro padre?

FACCHINO

Mio padre? El scortegava i agnelli per la città.

FLORINDO

(Costui è un pazzo; non occorr\'altro)

(vuol andare nella locanda).

FACCHINO

Lustrissimo, la favorissa.

FLORINDO

Che cosa?

FACCHINO

I bezzi della portadura.

FLORINDO

Quanto ti ho da dare per dieci passi? Ecco lì la corriera

(accenna dentro alla scena).

FACCHINO

Mi no conto i passi; la me paga

(stende la mano).

FLORINDO

Eccoti cinque soldi

(gli mette una moneta in mano).

FACCHINO

La me paga

(tiene la mano stesa).

FLORINDO

O che pazienza! Eccotene altri cinque

(fa come sopra).

FACCHINO

La me paga

(come sopra).

FLORINDO

(gli dà un calcio)

Sono annoiato.

FACCHINO

Adesso son pagà

(parte).

SCENA OTTAVA {#Section0007.xhtml#sigil_toc_id_8}
------------

Florindo, poi Truffaldino.

FLORINDO

Che razza di umori si danno! Aspettava proprio che io lo maltrattassi.
Oh, andiamo un po\' a vedere che albergo è questo\...

TRUFFALDINO

Signor, l\'è restada servida.

FLORINDO

Che alloggio è codesto?

TRUFFALDINO

L\'è una bona locanda, signor. Boni letti, bei specchi, una cusina
bellissima, con un odor che consola. Ho parlà col camerier. La sarà
servida da re.

FLORINDO

Voi che mestiere fate?

TRUFFALDINO

El servitor.

FLORINDO

Siete veneziano?

TRUFFALDINO

No son venezian, ma son qua del Stato. Son bergamasco, per servirla.

FLORINDO

Adesso avete padrone?

TRUFFALDINO

Adesso\... veramente non l\'ho.

FLORINDO

Siete senza padrone?

TRUFFALDINO

Eccome qua; la vede, son senza padron. (Qua nol gh\'è el me padron, mi
no digo busie).

FLORINDO

Verreste voi a servirmi?

TRUFFALDINO

A servirla? Perché no? (Se i patti fusse meggio, me cambieria de
camisa).

FLORINDO

Almeno per il tempo ch\'io sto in Venezia.

TRUFFALDINO

Benissimo. Quanto me vorla dar?

FLORINDO

Quanto pretendete?

TRUFFALDINO

Ghe dirò: un altro padron che aveva, e che adesso qua nol gh\'ho più, el
me dava un felippo al mese e le spese.

FLORINDO

Bene, e tanto vi darò io.

TRUFFALDINO

Bisognerave che la me dasse qualcossetta de più.

FLORINDO

Che cosa pretendereste di più?

TRUFFALDINO

Un soldetto al zorno per el tabacco.

FLORINDO

Sì, volentieri; ve lo darò.

TRUFFALDINO

Co l\'è cusì, stago con lu.

FLORINDO

Ma vi vorrebbe un poco d\'informazione dei fatti vostri.

TRUFFALDINO

Co no la vol altro che informazion dei fatti mii, la vada a Bergamo, che
tutti ghe dirà chi son.

FLORINDO

Non avete nessuno in Venezia che vi conosca?

TRUFFALDINO

Son arrivà stamattina, signor.

FLORINDO

Orsù; mi parete un uomo da bene. Vi proverò.

TRUFFALDINO

La me prova, e la vederà.

FLORINDO

Prima d\'ogni altra cosa, mi preme vedere se alla Posta vi siano lettere
per me. Eccovi mezzo scudo; andate alla Posta di Torino, domandate se vi
sono lettere di Florindo Aretusi; se ve ne sono, prendetele e portatele
subito, che vi aspetto.

TRUFFALDINO

Intanto la fazza parecchiar da disnar.

FLORINDO

Sì, bravo, farò preparare. (È faceto: non mi dispiace. A poco alla volta
ne farò la prova)

(entra nella locanda).

SCENA NONA {#Section0007.xhtml#sigil_toc_id_9}
----------

Truffaldino, poi Beatrice da uomo e Brighella.

TRUFFALDINO

Un soldo al zorno de più, i è trenta soldi al mese; no l\'è gnanca vero
che quell\'alter me daga un felippo; el me dà diese pauli. Pol esser che
diese pauli i fazza un felippo, ma mi nol so de seguro. E po quel sior
turinese nol vedo più. L\'è un matto. L\'è un zovenotto che no gh\'ha
barba e no gh\'ha giudizio. Lassemolo andar; andemo alla Posta per sto
sior\...

(vuol partire ed incontra Beatrice).

BEATRICE

Bravissimo. Così mi aspetti?

TRUFFALDINO

Son qua, signor. V\'aspetto ancora.

BEATRICE

E perchè vieni a aspettarmi qui, e non nella strada dove ti ho detto? È
un accidente che ti abbia ritrovato.

TRUFFALDINO

Ho spasseggià un pochetto, perché me passasse la fame.

BEATRICE

Orsù, va in questo momento alla barca del corriere. Fatti consegnare il
mio baule e portalo alla locanda di messer Brighella\...

BRIGHELLA

Eccola l\'à la mia locanda; nol pol fallar.

BEATRICE

Bene dunque, sbrigati, che ti aspetto.

TRUFFALDINO

(Diavolo! In quella locanda!).

BEATRICE

Tieni, nello stesso tempo anderai alla Posta di Torino e domanderai se
vi sono mie lettere. Anzi domanda se vi sono lettere di Federigo Rasponi
e di Beatrice Rasponi. Aveva da venir meco anche mia sorella, e per un
incomodo è restata in villa, qualche amica le potrebbe scrivere; guarda
se ci sono lettere o per lei, o per me.

TRUFFALDINO

(Mi no so quala far. Son l\'omo più imbroià de sto mondo).

BRIGHELLA

(Come aspettela lettere al so nome vero e al so nome finto, se l\'è
partida segretamente?)

(piano a Beatrice).

BEATRICE

(Ho lasciato ordine che mi si scriva ad un servitor mio fedele che
amministra le cose della mia casa; non so con qual nome egli mi possa
scrivere. Ma andiamo, che con comodo vi narrerò ogni cosa)

(piano a Brighella).

Spicciati, va alla Posta e va alla corriera. Prendi le lettere, fa
portar il baule nella locanda, ti aspetto

(entra nella locanda).

TRUFFALDINO

Sì vu el padron della locanda?

(a Brighella).

BRIGHELLA

Si ben, son mi. Porteve ben, e no ve dubitè, che ve farò magnar ben

(entra nella locanda).

SCENA DECIMA {#Section0007.xhtml#sigil_toc_id_10}
------------

Truffaldino, poi Silvio.

TRUFFALDINO

Oh bella! Ghe n\'è tanti che cerca un padron, e mi ghe n\'ho trovà do.
Come diavol oia da far? Tutti do no li posso servir. No? E perché no? No
la saria una bella cossa servirli tutti do, e guadagnar do salari, e
magnar el doppio? La saria bella, se no i se ne accorzesse. E se i se ne
accorze, cossa pèrdio? Gnente. Se uno me manda via, resto con
quell\'altro. Da galantomo, che me vai provar. Se la durasse anca un dì
solo, me vòi provar. Alla fin averò sempre fatto una bella cossa. Animo;
andemo alla Posta per tutti do

(incamminandosi).

SILVIO

(Questi è il servo di Federigo Rasponi). Galantuomo

(a Truffaldino).

TRUFFALDINO

Signor.

SILVIO

Dov\'è il nostro padrone?

TRUFFALDINO

El me padron? L\'è là in quella locanda.

SILVIO

Andate subito dal vostro padrone, ditegli ch\'io gli voglio parlare;
s\'è uomo d\'onore, venga giù, ch\'io l\'attendo.

TRUFFALDINO

Ma caro signor\...

SILVIO

Andate subito

(con voce alta).

TRUFFALDINO

Ma la sappia che el me padron\...

SILVIO

Meno repliche, giuro al cielo.

TRUFFALDINO

Ma qualo ha da vegnir?\...

SILVIO

Subito, o ti bastono.

TRUFFALDINO

(No so gnente, manderò el primo che troverò)

(entra nella locanda).

SCENA UNDICESIMA {#Section0007.xhtml#sigil_toc_id_11}
----------------

Silvio, poi Florindo e Truffaldino.

SILVIO

No, non sarà mai vero ch\'io soffra vedermi innanzi agli occhi un
rivale. Se Federigo scampò la vita una volta, non gli succederà sempre
la stessa sorte. O ha da rinunziare ogni pretensione sopra Clarice, o
l\'avrà da far meco\... Esce altra gente dalla locanda. Non vorrei
essere disturbato

(si ritira dalla parte opposta).

TRUFFALDINO

Ecco là quel sior che butta fogo da tutte le bande

(accenna Silvio a Florindo).

FLORINDO

Io non lo conosco. Che cosa vuole da me?

(a Truffaldino).

TRUFFALDINO

Mi no so gnente. Vado a tor le lettere; con so bona grazia. (No voggio
impegni)

(da sé, e parte).

SILVIO

(E Federigo non viene).

FLORINDO

(Voglio chiarirmi della verità). Signore, siete voi che mi avete
domandato?

(a Silvio)

SILVIO

Io? Non ho nemmeno l\'onor di conoscervi.

FLORINDO

Eppure quel servitore, che ora di qui è partito, mi ha detto che con
voce imperiosa e con minaccie avete preteso di provocarmi.

SILVIO

Colui m\'intese male; dissi che parlar volevo al di lui padrone.

FLORINDO

Bene, io sono il di lui padrone.

SILVIO

Voi, il suo padrone?

FLORINDO

Senz\'altro. Egli sta al mio servizio.

SILVIO

Perdonate dunque, o il vostro servitore è simile ad un altro che ho
veduto stamane, o egli serve qualche altra persona.

FLORINDO

Egli serve me, non ci pensate.

SILVIO

Quand\'è così, torno a chiedervi scusa.

FLORINDO

Non vi è male. Degli equivoci ne nascon sempre.

SILVIO

Siete voi forestiere, signore?

FLORINDO

Turinese, a\'vostri comandi.

SILVIO

Turinese appunto era quello con cui desiderava sfogarmi.

FLORINDO

Se è mio paesano, può essere ch\'io lo conosca, e s\'egli vi ha
disgustato, m\'impiegherò volentieri per le vostre giuste soddisfazioni.

SILVIO

Conoscete voi un certo Federigo Rasponi?

FLORINDO

Ah! l\'ho conosciuto pur troppo.

SILVIO

Pretende egli per una parola avuta dal padre togliere a me una sposa,
che questa mane mi ha giurato la fede.

FLORINDO

Non dubitate, amico, Federigo Rasponi non può involarvi la sposa. Egli è
morto.

SILVIO

Si, tutti credevano ch\'ei fosse morto, ma stamane giunse vivo e sano in
Venezia, per mio malanno, per mia disperazione.

FLORINDO

Signore, voi mi fate rimaner di sasso.

SILVIO

Ma! ci sono rimasto anch\'io.

FLORINDO

Federigo Rasponi vi assicuro che è morto.

SILVIO

Federigo Rasponi vi assicuro che è vivo.

FLORINDO

Badate bene che v\'ingannerete.

SILVIO

Il signor Pantalone de\' Bisognosi, padre della ragazza, ha fatto tutte
le possibili diligenze per assicurarsene, ed ha certissime prove che sia
egli proprio in persona.

FLORINDO

(Dunque non restò ucciso, come tutti credettero, nella rissa!).

SILVIO

O egli, o io, abbiamo da rinunziare agli amori di Clarice, o alla vita.

FLORINDO

(Qui Federigo? Fuggo dalla giustizia, e mi trovo a fronte il nemico!).

SILVIO

È molto che voi non lo abbiate veduto. Doveva alloggiare in codesta
locanda.

FLORINDO

Non l\'ho veduto; qui m\'hanno detto che non vi era forestiere nessuno.

SILVIO

Avrà cambiato pensiere. Signore, scusate se vi ho importunato Se lo
vedete, ditegli che per suo meglio abbandoni l\'idea di cotali nozze.
Silvio Lombardi è il mio nome; avrò l\'onore di riverirvi.

FLORINDO

Gradirò sommamente la vostra amicizia. (Resto pieno di confusione).

SILVIO

Il vostro nome, in grazia, poss\'io saperlo?

FLORINDO

(Non vo\' scoprirmi). Orazio Ardenti per obbedirvi.

SILVIO

Signor Orazio, sono a\' vostri comandi

(parte).

SCENA DODICESIMA {#Section0007.xhtml#sigil_toc_id_12}
----------------

Florindo solo.

FLORINDO

Come può darsi che una stoccata, che lo passò dal fianco alle reni, non
l\'abbia ucciso? Lo vidi pure io stesso disteso al suolo, involto nel
proprio sangue. Intesi dire che spirato egli era sul colpo. Pure
potrebbe darsi che morto non fosse. Il ferro toccato non lo avrà nelle
parti vitali. La confusione fa travedere. L\'esser io fuggito da Torino
subito dopo il fatto, che a me per la inimicizia nostra venne imputato,
non mi ha lasciato luogo a rilevare la verità. Dunque, giacché non è
morto, sarà meglio ch\'io ritorni a Torino, ch\'io vada a consolare la
mia diletta Beatrice, che vive forse penando, e piange per la mia
lontananza.

SCENA TREDICESIMA {#Section0007.xhtml#sigil_toc_id_13}
-----------------

Truffaldino con un altro Facchino che porta il baule di Beatrice, e
detto. Truffaldino s\'avanza alcuni passi col Facchino, poi accorgendosi
di Florindo e dubitando esser veduto, fa ritirare il Facchino.

TRUFFALDINO

Andemo con mi\... Oh diavol! L è qua quest\'alter padron. Retirete,
camerada, e aspetteme su quel canton

(il Facchino si ritira).

FLORINDO

(Sì, senz\'altro. Ritornerò a Torino).

TRUFFALDINO

Son qua, signor\...

FLORINDO

Truffaldino, vuoi venir a Torino con me?

TRUFFALDINO

Quando?

FLORINDO

Ora, subito.

TRUFFALDINO

Senza disnar?

FLORINDO

No; si pranzerà, e poi ce n\'andremo.

TRUFFALDINO

Benissimo; disnando ghe penserò.

FLORINDO

Sei stato alla Posta?

TRUFFALDINO

Signor sì.

FLORINDO

Hai trovato mie lettere?

TRUFFALDINO

Ghe n\'ho trovà.

FLORINDO

Dove sono?

TRUFFALDINO

Adesso le troverò

(tira fuori di tasca tre lettere).

(Oh diavolo! Ho confuso quelle de un padron con quelle dell\'altro. Come
faroio a trovar fora le soe? Mi no so lezer).

FLORINDO

Animo, dà qui le mie lettere.

TRUFFALDINO

Adesso, signor. (Son imbroiado). Ghe dirò, signor. Ste tre lettere no le
vien tutte a V. S. Ho trovà un servitor che me cognosse, che semo stadi
a servir a Bergamo insieme; gh\'ho dit che andava alla Posta, e el m\'ha
pregà che veda se gh\'era niente per el so padron. Me par che ghe ne
fusse una, ma no la conosso più, no so quala che la sia.

FLORINDO

Lascia vedere a me; prenderò le mie, e l\'altra te la renderò.

TRUFFALDINO

Tolì pur. Me preme de servir l\'amigo.

FLORINDO

(Che vedo? Una lettera diretta a Beatrice Rasponi? A Beatrice Rasponi in
Venezia!).

TRUFFALDINO

L\'avì trovada quella del me camerada?

FLORINDO

Chi è questo tuo camerata, che ti ha dato una tale incombenza?

TRUFFALDINO

L\'è un servitor\... che gh\'ha nome Pasqual.

FLORINDO

Chi serve costui?

TRUFFALDINO

Mi no lo so, signor.

FLORINDO

Ma se ti ha detto di cercar le lettere del suo padrone, ti avrà dato il
nome.

TRUFFALDINO

Naturalmente. (L\'imbroio cresce).

FLORINDO

Ebbene, che nome ti ha dato?

TRUFFALDINO

No me l\'arrecordo.

FLORINDO

Come!\...

TRUFFALDINO

El me l\'ha scritto su un pezzo de carta.

FLORINDO

E dov\'è la carta?

TRUFFALDINO

L\'ho lassada alla Posta.

FLORINDO

(Io sono in un mare di confusioni).

TRUFFALDINO

(Me vado inzegnando alla meio).

FLORINDO

Dove sta di casa questo Pasquale?

TRUFFALDINO

Non lo so in verità.

FLORINDO

Come potrai ricapitargli la lettera?

TRUFFALDINO

El m\'ha dito che se vederemo in piazza.

FLORINDO

(Io non so che pensare).

TRUFFALDINO

(Se la porto fora netta, l\'è un miracolo). La me favorissa quella
lettera, che vederò de trovarlo.

FLORINDO

No, questa lettera voglio aprirla.

TRUFFALDINO

Ohibò; no la fazza sta cossa. La sa pur, che pena gh\'è a avrir le
lettere.

FLORINDO

Tant\'è, questa lettera m\'interessa troppo. È diretta a persona, che mi
appartiene per qualche titolo. Senza scrupolo la posso aprire

(l\'apre).

TRUFFALDINO

(Schiavo siori. El l\'ha fatta).

FLORINDO

(legge)

Illustrissima signora padrona.\
La di lei partenza da questa città ha dato motivo di discorrere a tutto
il paese; e tutti capiscono ch\'ella abbia fatto tale risoluzione per
seguitare il signor Florindo. Lo Corte ha penetrato ch\'ella sia fuggita
in abito da uomo, e non lascia di far diligenze per rintracciarla e
farla arrestare. Io non ho spedito la presente da questa Posta di Torino
per Venezia a dirittura, per non iscoprire il paese dov\'ella mi ha
confidato che pensava portarsi; ma l\'ho inviata ad un amico di Genova,
perché poi di la la trasmettesse a Venezia. Se avrò novità di rimarco,
non lascerò di comunicargliele collo stesso metodo, e umilmente mi
rassegno.\
Umilissimo e fedelissimo servitore\
Tognin della Doira.

TRUFFALDINO

(Che bell\'azion! Lezer i fatti d\'i altri).

FLORINDO

(Che intesi mai? Che lessi? Beatrice partita di casa sua? in abito
d\'uomo? per venire in traccia di me? Ella mi ama davvero. Volesse il
cielo che io la ritrovassi in Venezia!). Va, caro Truffaldino, usa ogni
diligenza per ritrovare Pasquale; procura di ricavare da lui chi sia il
suo padrone, se uomo, se donna. Rileva dove sia alloggiato, e se puoi,
conducilo qui da me, che a te e a lui darò una mancia assai generosa.

TRUFFALDINO

Deme la lettera; procurerò de trovarlo.

FLORINDO

Eccola, mi raccomando a te. Questa cosa mi preme infinitamente.

TRUFFALDINO

Ma ghe l\'ho da dar cusì averta?

FLORINDO

Digli che è stato un equivoco, un accidente. Non mi trovare difficoltà.

TRUFFALDINO

E a Turin se va più per adesso?

FLORINDO

No, non si va più per ora. Non perder tempo. Procura di ritrovar
Pasquale. (Beatrice in Venezia, Federigo in Venezia. Se la trova il
fratello, misera lei; farò io tutte le diligenze possibili per
rinvenirla)

(parte).

SCENA QUATTORDICESIMA {#Section0007.xhtml#sigil_toc_id_14}
---------------------

Truffaldino solo, poi il Facchino col baule.

TRUFFALDINO

Ho gusto da galantomo, che no se vada via. Ho volontà de veder come me
riesce sti do servizi. Vòi provar la me abilità. Sta lettera, che va a
st\'alter me padron, me despias de averghela da portar averta.
M\'inzegnerò de piegarla

(fa varie piegature cattive).

Adess mo bisogneria bollarla. Se savess come far! Ho vist la me siora
nonna, che delle volte la bollava le lettere col pan mastegà. Voio
provar

(tira fuori di tasca un pezzetto di pane).

Me despiase consumar sto tantin de pan; ma ghe vol pazenzia

(mastica un po\'di pane per sigillare la lettera, ma non volendo
l\'inghiotte).

Oh diavolo! L\'è andà zo. Bisogna mastegarghene un altro boccon

(fa lo stesso e l\'inghiotte).

No gh\'è remedio, la natura repugna. Me proverò un\'altra volta

(mastica, come sopra. Vorrebbe inghiottir il pane, ma si trattiene, e
con gran fatica se lo leva di bocca).

Oh, l\'è vegnù. Bollerò la lettera

(la sigilla col pane).

Me par che la staga ben. Gran mi per far le cosse pulito! Oh, no
m\'arrecordava più del facchin. Camerada, vegnì avanti, tolì su el baul

(verso la scena).

FACCHINO

(col baule in spalla)

Son qua, dove l\'avemio da portar?

TRUFFALDINO

Portel in quella locanda, che adess vegno anca mi.

FACCHINO

E chi pagherà?

SCENA QUINDICESIMA {#Section0007.xhtml#sigil_toc_id_15}
------------------

Beatrice, che esce dalla locanda, e detti.

BEATRICE

È questo il mio baule?

(a Truffaldino).

TRUFFALDINO

Signor sì.

BEATRICE

Portatelo nella mia camera

(al Facchino).

FACCHINO

Qual èla la so camera?

BEATRICE

Domandatelo al cameriere.

FACCHINO

Semo d\'accordo trenta soldi.

BEATRICE

Andate, che vi pagherò.

FACCHINO

Che la fazza presto.

BEATRICE

Non mi seccate.

FACCHINO

Adessadesso ghe butto el baul in mezzo alla strada

(entra nella locanda).

TRUFFALDINO

Gran persone gentili che son sti facchini!

BEATRICE

Sei stato alla Posta?

TRUFFALDINO

Signor si.

BEATRICE

Lettere mie ve ne sono?

TRUFFALDINO

Ghe n\'era una de vostra sorella.

BEATRICE

Bene, dov\'è?

TRUFFALDINO

Eccola qua

(le dà la lettera).

BEATRICE

Questa lettera è stata aperta.

TRUFFALDINO

Averta? Oh! no pol esser.

BEATRICE

Aperta e sigillata ora col pane.

TRUFFALDINO

Mi no saveria mai come che la fusse.

BEATRICE

Non lo sapresti, eh? Briccone, indegno; chi ha aperto questa lettera?
Voglio saperlo.

TRUFFALDINO

Ghe dirò, signor, ghe confesserò la verità. Semo tutti capaci de fallar.
Alla Posta gh\'era una lettera mia; so poco lezer; e in fallo, in vece
de averzer la mia, ho averto la soa. Ghe domando perdon.

BEATRICE

Se la cosa fosse così, non vi sarebbe male.

TRUFFALDINO

L\'è così da povero fiol.

BEATRICE

L\'hai letta questa lettera? Sai che cosa contiene?

TRUFFALDINO

Niente affatto. L\'è un carattere che no capisso.

BEATRICE

L\'ha veduta nessuno?

TRUFFALDINO

Oh!

(maravigliandosi).

BEATRICE

Bada bene, veh!

TRUFFALDINO

Uh!

(come sopra).

BEATRICE

(Non vorrei che costui m\'ingannasse)

(legge piano).

TRUFFALDINO

(Anca questa l\'è tacconada).

BEATRICE

(Tognino è un servitore fedele. Gli ho dell\'obbligazione). Orsù, io
vado per un interesse poco lontano Tu va nella locanda, apri il baule,
eccoti le chiavi e da\'un poco d\'aria ai miei vestiti. Quando torno, si
pranzerà (Il signor Pantalone non si vede, ed a me premono queste
monete)

(parte).

SCENA SEDICESIMA {#Section0007.xhtml#sigil_toc_id_16}
----------------

Truffaldino, poi Pantalone.

TRUFFALDINO

Mo l\'è andada ben, che no la podeva andar meio. Son un omo de garbo; me
stimo cento scudi de più de quel che no me stimava.

PANTALONE

Disè, amigo, el vostro padron xèlo in casa?

TRUFFALDINO

Sior no, nol ghe xè.

PANTALONE

Saveu dove che el sia?

TRUFFALDINO

Gnanca.

PANTALONE

Vienlo a casa a disnar?

TRUFFALDINO

Mi crederave de sì.

PANTALONE

Tolè, col vien a casa, deghe sta borsa co sti cento ducati. No posso
trattegnirme, perché gl\'ho da far. Ve reverisso

(parte).

SCENA DICIASSETTESIMA {#Section0007.xhtml#sigil_toc_id_17}
---------------------

Truffaldino, poi Florindo.

TRUFFALDINO

La diga, la senta. Bon viazo. Non m\'ha gnanca dito a qual dei mi
padroni ghe l\'ho da dar.

FLORINDO

E bene, hai tu ritrovato Pasquale?

TRUFFALDINO

Sior no, no l\'ho trovà Pasqual, ma ho trovà uno, che m\'ha dà una borsa
con cento ducati.

FLORINDO

Cento ducati? Per farne che?

TRUFFALDINO

Disim la verità, sior padron, aspetteu denari da nissuna banda?

FLORINDO

Sì ho presentata una lettera ad un mercante.

TRUFFALDINO

Donca sti quattrini i sarà vostri.

FLORINDO

Che cosa ha detto chi te li ha dati?

TRUFFALDINO

El m\'ha dit, che li daga al me padron.

FLORINDO

Dunque sono miei senz\'altro. Non sono io il tuo padrone? Che dubbio
c\'è?

TRUFFALDINO

(Nol sa gnente de quell\'alter padron).

FLORINDO

E non sai chi te li abbia dati?

TRUFFALDINO

Mi no so; me par quel viso averlo visto un\'altra volta, ma no me
recordo.

FLORINDO

Sarà un mercante, a cui sono raccomandato.

TRUFFALDINO

El sarà lu senz\'altro.

FLORINDO

Ricordati di Pasquale.

TRUFFALDINO

Dopo disnar lo troverò.

FLORINDO

Andiamo dunque a sollecitare il pranzo

(entra nella locanda).

TRUFFALDINO

Andemo pur. Manco mal che sta volta non ho fallà. La borsa l\'ho dada a
chi l\'aveva d\'aver

(entra nella locanda).

SCENA DICIOTTESIMA {#Section0007.xhtml#sigil_toc_id_18}
------------------

Camera in casa di Pantalone Pantalone e Clarice, poi Smeraldina.

PANTALONE

Tant\'è; sior Federigo ha da esser vostro mario. Ho dà parola, e no son
un bambozzo.

CLARICE

Siete padrone di me, signor padre; ma questa, compatitemi, è una
tirannia.

PANTALONE

Quando sior Federigo v\'ha fatto domandar, ve l\'ho dito; vu non m\'avè
resposo de no volerlo. Allora dovevi parlar; adesso no sè più a tempo.

CLARICE

La soggezione, il rispetto, mi fecero ammutolire.

PANTALONE

Fè che el respetto e la suggizion fazza l\'istesso anca adesso.

CLARICE

Non posso, signor padre.

PANTALONE

No? per cossa?

CLARICE

Federigo non lo sposerò certamente.

PANTALONE

Ve despiaselo tanto?

CLARICE

È odioso agli occhi miei.

PANTALONE

Anca sì che mi ve insegno el modo de far che el ve piasa?

CLARICE

Come mai, signore?

PANTALONE

Desmenteghève sior Silvio, e vederè che el ve piaserà.

CLARICE

Silvio è troppo fortemente impresso nell\'anima mia; e voi
coll\'approvazione vostra lo avete ancora più radicato.

PANTALONE

(Da una banda la compatisso). Bisogna far de necessità vertù.

CLARICE

Il mio cuore non è capace di uno sforzo sì grande.

PANTALONE

Feve animo, bisogna farlo\...

SMERALDINA

Signor padrone, è qui il signor Federigo, che vuol riverirla.

PANTALONE

Ch\'el vegna, che el xè patron.

CLARICE

Oimè! Che tormento!

(piange).

SMERALDINA

Che avete, signora padrona? Piangete? In verità avete torto. Non avete
veduto com\'è bellino il signor Federigo? Se toccasse a me una tal
fortuna, non vorrei piangere, no; vorrei ridere con tanto di bocca

(parte).

PANTALONE

Via, fia mia, no te far veder a pianzer.

CLARICE

Ma se mi sento scoppiar il cuore.

SCENA DICIANNOVESIMA {#Section0007.xhtml#sigil_toc_id_19}
--------------------

Beatrice da uomo, e detti.

BEATRICE

Riverisco il signor Pantalone.

PANTALONE

Padron reverito. Àla recevesto una borsa con cento ducati?

BEATRICE

Io no.

PANTALONE

Ghe l\'ho dada za un poco al so servitor. La m\'ha dito che el xè un omo
fidà.

BEATRICE

Sì, non vi è pericolo. Non l\'ho veduto: me li darà, quando torno a
casa. (Che ha la signora Clarice che piange?)

(piano a Pantalone).

PANTALONE

(Caro sior Federigo, bisogna compatirla. La nova della so morte xè stada
causa de sto mal. Col tempo spero che la se scambierà)

(piano a Beatrice).

BEATRICE

(Fate una cosa, signor Pantalone, lasciatemi un momento in libertà con
lei, per vedere se mi riuscisse d\'aver una buona parola)

(come sopra).

PANTALONE

Sior Sì; vago e vegno. (Voggio provarle tutte). Fia mia, aspetteme, che
adesso torno. Tien un poco de compagnia al to novizzo. (Via, abbi
giudizio)

(piano a Clarice, e parte).

SCENA VENTESIMA {#Section0007.xhtml#sigil_toc_id_20}
---------------

Beatrice e Clarice.

BEATRICE

Deh, signora Clarice\...

CLARICE

Scostatevi, e non ardite d\'importunarmi.

BEATRICE

Così severa con chi vi è destinato in consorte?

CLARICE

Se sarò strascinata per forza alle vostre nozze, avrete da me la mano,
ma non il cuore.

BEATRICE

Voi siete sdegnata meco, eppure io spero placarvi.

CLARICE

V\'aborrirò in eterno.

BEATRICE

Se mi conosceste, voi non direste così.

CLARICE

Vi conosco abbastanza per lo sturbatore della mia pace.

BEATRICE

Ma io ho il modo di consolarvi.

CLARICE

V\'ingannate; altri che Silvio consolare non mi potrebbe.

BEATRICE

Certo che non posso darvi quella consolazione, che dar vi potrebbe il
vostro Silvio, ma posso contribuire alla vostra felicità.

CLARICE

Mi par assai, signore, che parlandovi io in una maniera la più aspra del
mondo, vogliate ancor tormentarmi.

BEATRICE

(Questa povera giovane mi fa pietà; non ho cuore di vederla penare).

CLARICE

(La passione mi fa diventare ardita, temeraria, incivile).

BEATRICE

Signora Clarice, vi ho da confidare un segreto.

CLARICE

Non vi prometto la segretezza. Tralasciate di confidarmelo.

BEATRICE

La vostra austerità mi toglie il modo di potervi render felice.

CLARICE

Voi non mi potete rendere che sventurata.

BEATRICE

V\'ingannate; e per convincervi vi parlerò schiettamente. Se voi non
volete me, io non saprei che fare di voi. Se avete ad altri impegnata la
destra, anch\'io con altri ho impegnato il cuore.

CLARICE

Ora cominciate a piacermi.

BEATRICE

Non vel dissi che aveva io il modo di consolarvi?

CLARICE

Ah, temo che mi deludiate.

BEATRICE

No, signora, non fingo. Parlovi col cuore sulle labbra; e se mi
promettete quella segretezza che mi negaste poc\'anzi, vi confiderò un
arcano, che metterà in sicuro la vostra pace.

CLARICE

Giuro di osservare il più rigoroso silenzio.

BEATRICE

Io non sono Federigo Rasponi, ma Beatrice di lui sorella.

CLARICE

Oh! che mi dite mai! Voi donna?

BEATRICE

Sì, tale io sono. Pensate, se aspiravo di cuore alle vostre nozze.

CLARICE

E di vostro fratello che nuova ci date?

BEATRICE

Egli morì pur troppo d\'un colpo di spada. Fu creduto autore della di
lui morte un amante mio, di cui sotto di queste spoglie mi porto in
traccia. Pregovi per tutte le sacre leggi d\'amicizia e d\'amore di non
tradirmi. So che incauta sono io stata confidandovi un tale arcano, ma
l\'ho fatto per più motivi; primieramente, perché mi doleva vedervi
afflitta; in secondo luogo, perché mi pare conoscere in voi che siate
una ragazza da potersi compromettere di segretezza; per ultimo, perché
il vostro Silvio mi ha minacciato e non vorrei che, sollecitato da voi,
mi ponesse in qualche cimento.

CLARICE

A Silvio mi permettete voi ch\'io lo dica?

BEATRICE

No, anzi ve lo proibisco assolutamente.

CLARICE

Bene, non parlerò.

BEATRICE

Badate che mi fido di voi.

CLARICE

Ve lo giuro di nuovo, non parlerò.

BEATRICE

Ora non mi guarderete più di mal occhio.

CLARICE

Anzi vi sarò amica; e, se posso giovarvi, disponete di me.

BEATRICE

Anch\'io vi giuro eterna la mia amicizia. Datemi la vostra mano.

CLARICE

Eh, non vorrei\...

BEATRICE

Avete paura ch\'io non sia donna? Vi darò evidenti prove della verità.

CLARICE

Credetemi, ancora mi pare un sogno.

BEATRICE

Infatti la cosa non è ordinaria.

CLARICE

È stravagantissima.

BEATRICE

Orsù, io me ne voglio andare. Tocchiamoci la mano in segno di buona
amicizia e di fedeltà.

CLARICE

Ecco la mano; non ho nessun dubbio che m\'inganniate.

SCENA VENTUNESIMA {#Section0007.xhtml#sigil_toc_id_21}
-----------------

Pantalone e dette.

PANTALONE

Bravi! Me ne rallegro infinitamente. (Fia mia, ti t\'ha giustà molto
presto)

(a Clarice).

BEATRICE

Non vel dissi, signor Pantalone, ch\'io l\'avrei placata?

PANTALONE

Bravo! Avè fatto più vu in quattro minuti, che no averave fatto mi in
quattr\'anni.

CLARICE

(Ora sono in un laberinto maggiore).

PANTALONE

Donca stabiliremo presto sto matrimonio

(a Clarice).

CLARICE

Non abbiate tanta fretta, signore.

PANTALONE

Come! Se se tocca le manine in scondon, e non ho d\'aver pressa? No, no,
no voggio che me succeda desgrazie. Doman se farà tutto.

BEATRICE

Sarà necessario, signor Pantalone, che prima accomodiamo le nostre
partite, che vediamo il nostro conteggio.

PANTALONE

Faremo tutto. Queste le xè cosse che le se fa in do ore. Doman daremo
l\'anello.

CLARICE

Deh, signor padre\...

PANTALONE

Siora fia, vago in sto ponto a dir le parole a sior Silvio.

CLARICE

Non lo irritate, per amor del cielo.

PANTALONE

Coss\'è? Ghe ne vustu do?

CLARICE

Non dico questo. Ma\...

PANTALONE

Ma e mo, la xè finia. Schiavo, siori

(vuol partire).

BEATRICE

Udite\...

(a Pantalone).

PANTALONE

Sè mario e muggier

(partendo).

CLARICE

Piuttosto\...

(a Pantalone).

PANTALONE

Stassera la descorreremo

(parte).

SCENA VENTIDUESIMA {#Section0007.xhtml#sigil_toc_id_22}
------------------

Beatrice e Clarice.

CLARICE

Ah, signora Beatrice, esco da un affanno per entrare in un altro.

BEATRICE

Abbiate pazienza. Tutto può succedere, fuor ch\'io vi sposi.

CLARICE

E se Silvio mi crede infedele?

BEATRICE

Durerà per poco l\'inganno.

CLARICE

Se gli potessi svelare la verità\...

BEATRICE

Io non vi disimpegno dal giuramento.

CLARICE

Che devo fare dunque?

BEATRICE

Soffrire un poco.

CLARICE

Dubito che sia troppo penosa una tal sofferenza.

BEATRICE

Non dubitate, che dopo i timori, dopo gli affanni, riescono più graditi
gli amorosi contenti

(parte).

CLARICE

Non posso lusingarmi di provar i contenti, finchè mi vedo circondata da
pene. Ah, pur troppo egli è vero: in questa vita per lo più o si pena, o
si spera, e poche volte si gode

(parte).

[]{#Section0008.xhtml}

ATTO SECONDO
============

SCENA PRIMA {#Section0008.xhtml#sigil_toc_id_23}
-----------

Cortile in casa di Pantalone\
Silvio e il Dottore.

SILVIO

Signor padre, vi prego lasciarmi stare.

DOTTORE

Fermati; rispondimi un poco.

SILVIO

Sono fuori di me.

DOTTORE

Per qual motivo sei tu venuto nel cortile del signor Pantalone?

SILVIO

Perché voglio, o che egli mi mantenga quella parola che mi ha dato, o
che mi renda conto del gravissimo affronto.

DOTTORE

Ma questa è una cosa che non conviene farla nella propria casa di
Pantalone. Tu sei un pazzo a lasciarti trasportar dalla collera.

SILVIO

Chi tratta male con noi, non merita alcun rispetto.

DOTTORE

È vero, ma non per questo si ha da precipitare. Lascia fare a me, Silvio
mio, lascia un po\' ch\'io gli parli; può essere ch\'io lo illumini e
gli faccia conoscere il suo dovere. Ritirati in qualche loco, e
aspettami; esci di questo cortile, non facciamo scene. Aspetterò io il
signor Pantalone.

SILVIO

Ma io, signor padre\...

DOTTORE

Ma io, signor figliuolo, voglio poi esser obbedito.

SILVIO

Sì, v\'obbedirò. Me n\'anderò. Parlategli. Vi aspetto dallo speziale. Ma
se il signor Pantalone persiste, avrà che fare con me

(parte).

SCENA SECONDA {#Section0008.xhtml#sigil_toc_id_24}
-------------

Il Dottore, poi Pantalone.

DOTTORE

Povero figliuolo, lo compatisco. Non doveva mai il signor Pantalone
lusingarlo a tal segno, prima di essere certo della morte del torinese.
Vorrei pure vederlo quieto, e non vorrei che la collera me lo facesse
precipitare.

PANTALONE

(Cossa fa el Dottor in casa mia?).

DOTTORE

Oh, signor Pantalone, vi riverisco.

PANTALONE

Schiavo, sior Dottor. Giusto adesso vegniva a cercar de vu e de vostro
fio.

DOTTORE

Sì? Bravo, m\'immagino che dovevate venir in traccia di noi, per
assicurarci che la signora Clarice sarà moglie di Silvio.

PANTALONE

Anzi vegniva per dirve\...

(mostrando difficoltà di parlare).

DOTTORE

No, non c\'è bisogno di altre giustificazioni. Compatisco il caso in cui
vi siete trovato. Tutto vi si passa in grazia della buona amicizia.

PANTALONE

Seguro, che considerando la promessa fatta a sior Federigo\...

(titubando, come sopra).

DOTTORE

E colto all\'improvviso da lui, non avete avuto tempo a riflettere; e
non avete pensato all\'affronto che si faceva alla nostra casa.

PANTALONE

No se pol dir affronto, quando con un altro contratto\...

DOTTORE

So che cosa volete dire. Pareva a prima vista che la promessa col
turinese fosse indissolubile, perché stipulata per via di contratto. Ma
quello era un contratto seguito fra voi e lui; e il nostro è confermato
dalla fanciulla.

PANTALONE

Xè vero; ma\...

DOTTORE

E sapete bene che in materia di matrimoni: *Consensus et non concubitus
facit virum*.

PANTALONE

Mi no so de latin; ma ve digo\...

DOTTORE

E le ragazze non bisogna sacrificarle.

PANTALONE

Aveu altro da dir?

DOTTORE

Per me ho detto.

PANTALONE

Aveu fenio?

DOTTORE

Ho finito.

PANTALONE

Possio parlar?

DOTTORE

Parlate.

PANTALONE

Sior dottor caro, con tutta la vostra dottrina\...

DOTTORE

Circa alla dote ci aggiusteremo. Poco più, poco meno, non guarderò.

PANTALONE

Semo da capo. Voleu lassarme parlar?

DOTTORE

Parlate.

PANTALONE

Ve digo che la vostra dottrina xè bella e bona; ma in sto caso no la
conclude.

DOTTORE

E voi comporterete che segua un tal matrimonio?

PANTALONE

Per mi giera impegnà, che no me podeva cavar. Mia fia xè contenta; che
difficoltà possio aver? Vegniva a posta a cercar de vu o de sior Silvio,
per dirve sta cossa. La me despiase assae, ma non ghe vedo remedio.

DOTTORE

Non mi maraviglio della vostra figliuola; mi maraviglio di voi, che
trattiate si malamente con me. Se non eravate sicuro della morte del
signor Federigo, non avevate a impegnarvi col mio figliuolo; e se con
lui vi siete impegnato, avete a mantener la parola a costo di tutto. La
nuova della morte di Federigo giustificava bastantemente, anche presso
di lui, la vostra nuova risoluzione, né poteva egli rimproverarvi, né
aveva luogo a pretendere veruna soddisfazione. Gli sponsali contratti
questa mattina fra la signora Clarice ed il mio figliuolo *coram
testibus* non potevano essere sciolti da una semplice parola data da voi
ad un altro. Mi darebbe l\'animo colle ragioni di mio figliuolo render
nullo ogni nuovo contratto, e obbligar vostra figlia a prenderlo per
marito; ma mi vergognerei d\'avere in casa mia una nuora di così poca
riputazione, una figlia di un uomo senza parola, come voi siete. Signor
Pantalone, ricordatevi che l\'avete fatta a me, che l\'avete fatta alla
casa Lombardi verrà il tempo che forse me la dovrete pagare: sì, verrà
il tempo: [omnia tempus habent]{.testo_corsivo}.

(parte).

SCENA TERZA {#Section0008.xhtml#sigil_toc_id_25}
-----------

Pantalone, poi Silvio.

PANTALONE

Andè, che ve mando. No me n\'importa un figo, e no gh\'ho paura de vu.
Stimo più la casa Rasponi de cento case Lombardi. Un fio unico e ricco
de sta qualità se stenta a trovarlo. L\'ha da esser cussì.

SILVIO

(Ha bel dire mio padre. Chi si può tenere, si tenga).

PANTALONE

(Adesso, alla segonda de cambio)

(vedendo Silvio).

SILVIO

Schiavo suo, signore

(bruscamente).

PANTALONE

Patron reverito. (La ghe fuma).

SILVIO

Ho inteso da mio padre un certo non so che; crediamo poi che sia la
verità?

PANTALONE

Co ghe l\'ha dito so sior padre, sarà vero.

SILVIO

Sono dunque stabiliti gli sponsali della signora Clarice col signor
Federigo?

PANTALONE

Sior sì, stabiliti e conclusi.

SILVIO

Mi maraviglio che me lo diciate con tanta temerità. Uomo senza parola,
senza riputazione.

PANTALONE

Come parlela, padron? Co un omo vecchio della mia sorte la tratta cussì?

SILVIO

Non so chi mi tenga, che non vi passi da parte a parte.

PANTALONE

No son miga una rana, padron. In casa mia se vien a far ste bulae?

SILVIO

Venite fuori di questa casa.

PANTALONE

Me maraveggio de ella, sior.

SILVIO

Fuori, se siete un uomo d\'onore.

PANTALONE

Ai omeni della mia sorte se ghe porta respetto.

SILVIO

Siete un vile, un codardo, un plebeo.

PANTALONE

Sè un tocco de temerario.

SILVIO

Eh, giuro al Cielo\...

(mette mano alla spada).

PANTALONE

Agiuto

(mette mano al pistolese)

SCENA QUARTA {#Section0008.xhtml#sigil_toc_id_26}
------------

Beatrice colla spada alla mano, e detti.

BEATRICE

Eccomi; sono io in vostra difesa

(a Pantalone, e rivolta la spada contro Silvio).

PANTALONE

Sior zenero, me raccomando

(a Beatrice).

SILVIO

Con te per l\'appunto desideravo di battermi

(a Beatrice).

BEATRICE

(Son nell\'impegno).

SILVIO

Rivolgi a me quella spada

(a Beatrice).

PANTALONE

Ah, sior zenero\...

(timoroso).

BEATRICE

Non è la prima volta che io mi sia cimentato. Son qui, non ho timore di
voi

(presenta la spada a Silvio).

PANTALONE

Aiuto. No gh\'è nissun?

(Parte correndo verso la strada). Beatrice e Silvio si battono. Silvio
cade e lascia la spada in terra, e Beatrice gli presenta la punta al
petto.

SCENA QUINTA {#Section0008.xhtml#sigil_toc_id_27}
------------

Clarice e detti.

CLARICE

Oimè! Fermate

(a Beatrice).

BEATRICE

Bella Clarice, in grazia vostra dono a Silvio la vita; e voi, in
ricompensa della mia pietà, ricordatevi del giuramento

(parte).

SCENA SESTA {#Section0008.xhtml#sigil_toc_id_28}
-----------

Silvio e Clarice.

CLARICE

Siete salvo o mio caro?

SILVIO

Ah, perfida ingannatrice! Caro a Silvio? Caro ad un amante schernito, ad
uno sposo tradito?

CLARICE

No, Silvio, non merito i vostri rimproveri. V\'amo, v\'adoro, vi son
fedele.

SILVIO

Ah menzognera! Mi sei fedele, eh? Fedeltà chiami prometter fede ad un
altro amante?

CLARICE

Ciò non feci, ne farò mai. Morirò, prima d\'abbandonarvi.

SILVIO

Sento che vi ha impegnato con un giuramento.

CLARICE

Il giuramento non mi obbliga ad isposarlo.

SILVIO

Che cosa dunque giuraste?

CLARICE

Caro Silvio, compatitemi, non posso dirlo.

SILVIO

Per qual ragione?

CLARICE

Perché giurai di tacere.

SILVIO

Segno dunque che siete colpevole.

CLARICE

No, sono innocente.

SILVIO

Gl\'innocenti non tacciono.

CLARICE

Eppure questa volta rea mi farei parlando.

SILVIO

Questo silenzio a chi l\'avete giurato?

CLARICE

A Federigo.

SILVIO

E con tanto zelo l\'osserverete?

CLARICE

L\'osserverò per non divenire spergiura.

SILVIO

E dite di non amarlo? Semplice chi vi crede. Non vi credo io già,
barbara, ingannatrice! Toglietevi dagli occhi miei.

CLARICE

Se non vi amassi, non sarei corsa qui a precipizio per difendere la
vostra vita.

SILVIO

Odio anche la vita, se ho da riconoscerla da un\'ingrata.

CLARICE

Vi amo con tutto il cuore.

SILVIO

Vi aborrisco con tutta l\'anima.

CLARICE

Morirò, se non vi placate.

SILVIO

Vedrei il vostro sangue più volentieri della infedeltà vostra.

CLARICE

Saprò soddisfarvi

(toglie la spada di terra).

SILVIO

Sì, quella spada potrebbe vendicare i miei torti.

CLARICE

Così barbaro colla vostra Clarice?

SILVIO

Voi mi avete insegnata la crudeltà.

CLARICE

Dunque bramate la morte mia?

SILVIO

Io non so dire che cosa brami.

CLARICE

Vi saprò compiacere

(volta la punta al proprio seno).

SCENA SETTIMA {#Section0008.xhtml#sigil_toc_id_29}
-------------

Smeraldina e detti.

SMERALDINA

Fermatevi; che diamine fate?

(leva la spada a Clarice).

E voi, cane rinnegato, l\'avreste lasciata morire?

(a Silvio).

Che cuore avete di tigre, di leone, di diavolo? Guardate lì il bel
suggettino, per cui le donne s\'abbiano a sbudellare! Oh siete pur
buona, signora padrona. Non vi vuole più forse? Chi non vi vuol, non vi
merita. Vada all\'inferno questo sicario, e voi venite meco, che degli
uomini non ne mancano; m\'impegno avanti sera trovarvene una dozzina

(getta la spada in terra, e Silvio la prende).

CLARICE

(piangendo)

Ingrato! Possibile che la mia morte non vi costasse un sospiro? Sì, mi
ucciderà il dolore; morirò, sarete contento. Però vi sarà nota un giorno
la mia innocenza, e tardi allora, pentito di non avermi creduto,
piangerete la mia sventura e la vostra barbara crudeltà

(parte).

SCENA OTTAVA {#Section0008.xhtml#sigil_toc_id_30}
------------

Silvio e Smeraldina.

SMERALDINA

Questa è una cosa che non so capire. Veder una ragazza che si vuol
ammazzare, e star lì a guardarla, come se vedeste rappresentare una
scena di commedia.

SILVIO

Pazza che sei! Credi tu ch\'ella si volesse uccider davvero?

SMERALDINA

Non so altro io so che, se non arrivavo a tempo, la poverina sarebbe
ita.

SILVIO

Vi voleva ancor tanto prima che la spada giungesse al petto.

SMERALDINA

Sentite che bugiardo! Se stava lì lì per entrare.

SILVIO

Tutte finzioni di voi altre donne.

SMERALDINA

Sì, se fossimo come voi. Dirò, come dice il proverbio: noi abbiamo le
voci, e voi altri avete le noci. Le donne hanno la fama di essere
infedeli, e gli uomini commettono le infedeltà a più non posso. Delle
donne si parla, e degli uomini non si dice nulla. Noi siamo criticate, e
a voi altri si passa tutto. Sapete perché? Perché le leggi le hanno
fatte gli uomini; che se le avessero fatte le donne, si sentirebbe tutto
il contrario. S\'io comandassi, vorrei che tutti gli uomini infedeli
portassero un ramo d\'albero in mano, e so che tutte le città
diventerebbero boschi

(parte).

SCENA NONA {#Section0008.xhtml#sigil_toc_id_31}
----------

Silvio solo.

SILVIO

Sì, che Clarice è infedele, e col pretesto di un giuramento affetta di
voler celare la verità. Ella è una perfida, e l\'atto di volersi ferire
fu un\'invenzione per ingannarmi, per muovermi a compassione di lei. Ma
se il destino mi fece cadere a fronte del mio rivale, non lascierò mai
il pensiero di vendicarmi. Morirà quell\'indegno, e Clarice ingrata
vedrà nel di lui sangue il frutto de\'suoi amori

(parte)

SCENA DECIMA {#Section0008.xhtml#sigil_toc_id_32}
------------

Sala della locanda con due porte in prospetto e due laterali\
Truffaldino, poi Florindo.

TRUFFALDINO

Mo gran desgrazia che l\'è la mia! De do padroni nissun è vegnudo ancora
a disnar. L\'è do ore che è sonà mezzozorno, e nissun se vede. I vegnirà
po tutti do in una volta, e mi sarò imbroiado; tutti do no li poderò
servir, e se scovrirà la fazenda. Zitto, zitto, che ghe n\'è qua un.
Manco mal.

FLORINDO

Ebbene, hai ritrovato codesto Pasquale?

TRUFFALDINO

No avemio dito, signor, che el cercherò dopo che averemo disnà?

FLORINDO

Io sono impaziente.

TRUFFALDINO

El doveva vegnir a disnar un poco più presto.

FLORINDO

(Non vi è modo ch\'io possa assicurarmi se qui si trovi Beatrice).

TRUFFALDINO

El me dis, andemo a ordinar el pranzo, e po el va fora de casa. La roba
sarà andada de mal.

FLORINDO

Per ora non ho volontà di mangiare. (Vo\' tornare alla Posta. Ci voglio
andare da me; qualche cosa forse rileverò).

TRUFFALDINO

La sappia, signor, che in sto paese bisogna magnar, e chi no magna,
s\'ammala.

FLORINDO

Devo uscire per un affar di premura. Se torno a pranzo, bene; quando no,
mangerò questa sera. Tu, se vuoi, fatti dar da mangiare.

TRUFFALDINO

Oh, non occorr\'altro. Co l\'è cusì, che el se comoda, che l\'è padron.

FLORINDO

Questi danari mi pesano; tieni, mettili nel mio baule. Eccoti la chiave

(dà a Truffaldino la borsa dei cento ducati e la chiave).

TRUFFALDINO

La servo, e ghe porto la chiave.

FLORINDO

No, no, me la darai. Non mi vo\'trattenere. Se non torno a pranzo, vieni
alla piazza; attenderò con impazienza che tu abbia ritrovato Pasquale

(parte).

SCENA UNDICESIMA {#Section0008.xhtml#sigil_toc_id_33}
----------------

Truffaldino, poi Beatrice con un foglio in mano.

TRUFFALDINO

Manco mal che l\'ha dito che me fazza dar da magnar; cusì andaremo
d\'accordo. Se nol vol magnar lu, che el lassa star. La mia complession
no l\'è fatta per dezunar. Voi metter via sta borsa, e po subito\...

BEATRICE

Ehi, Truffaldino!

TRUFFALDINO

(Oh diavolo!).

BEATRICE

Il signor Pantalone de\' Bisognosi ti ha dato una borsa con cento
ducati?

TRUFFALDINO

Sior sì, el me l\'ha dada.

BEATRICE

E perché dunque non me la dai?

TRUFFALDINO

Mo vienla a vussioria?

BEATRICE

Se viene a me? Che cosa ti ha detto, quando ti ha dato la borsa?

TRUFFALDINO

El m\'ha dit che la daga al me padron.

BEATRICE

Bene, il tuo padrone chi è?

TRUFFALDINO

Vussioria.

BEATRICE

E perché domandi dunque, se la borsa è mia?

TRUFFALDINO

Donca la sarà soa.

BEATRICE

Dov\'è la borsa?

TRUFFALDINO

Eccola qua

(gli dà la borsa).

BEATRICE

Sono giusti?

TRUFFALDINO

Mi no li ho toccadi, signor.

BEATRICE

(Li conterò poi).

TRUFFALDINO

(Aveva fallà mi colla borsa; ma ho rimedià. Cossa dirà quell\'altro? Se
no i giera soi, nol dirà niente).

BEATRICE

Vi è il padrone della locanda?

TRUFFALDINO

El gh\'è è, signor si.

BEATRICE

Digli che avrò un amico a pranzo con me, che presto presto procuri di
accrescer la tavola più che può.

TRUFFALDINO

Come vorla restar servida? Quanti piatti comandela?

BEATRICE

Il signor Pantalone de\' Bisognosi non è uomo di gran soggezione. Digli
che faccia cinque o sei piatti; qualche cosa di buono.

TRUFFALDINO

Se remettela in mi?

BEATRICE

Sì, ordina tu, fatti onore. Vado a prender l\'amico, che è qui poco
lontano; e quando torno, fa che sia preparato

(in atto di partire).

TRUFFALDINO

La vederà, come la sarà servida.

BEATRICE

Tieni questo foglio, mettilo nel baule. Bada bene veh, che è una lettera
di cambio di quattromila scudi.

TRUFFALDINO

No la se dubita, la metterò via subito.

BEATRICE

Fa\' che sia tutto pronto. (Povero signor Pantalone, ha avuto la gran
paura. Ha bisogno di essere divertito)

(parte).

SCENA DODICESIMA {#Section0008.xhtml#sigil_toc_id_34}
----------------

Truffaldino, poi Brighella.

TRUFFALDINO

Qua bisogna veder de farse onor. La prima volta che sto me padron me
ordina un disnar, voi farghe veder se son de bon gusto. Metterò via sta
carta, e po\... La metterò via dopo, no vòi perder tempo. Oe de là;
gh\'è nissun? Chiameme missier Brighella, diseghe che ghe vòi parlar

(verso la scena).

No consiste tanto un bel disnar in te le pietanze, ma in tel bon ordine;
val più una bella disposizion, che no val una montagna de piatti.

BRIGHELLA

Cossa gh\'è, sior Truffaldin? Cossa comandeu da mi?

TRUFFALDINO

El me padron el gh\'ha un amigo a disnar con lu; el vol che radoppiè la
tavola, ma presto, subito. Aveu el bisogno in cusina?

BRIGHELLA

Da mi gh\'è sempre de tutto. In mezz\'ora posso metter all\'ordine
qualsesia disnar.

TRUFFALDINO

Ben donca. Disìme cossa che ghe darè.

BRIGHELLA

Per do persone, faremo do portade de quattro piatti l\'una; anderà ben?

TRUFFALDINO

(L\'ha dito cinque o sie piatti; sie o otto, no gh\'è mal). Anderà ben.
Cossa ghe sarà in sti piatti?

BRIGHELLA

Nella prima portada ghe daremo la zuppa, la frittura, el lesso e un
fracandò.

TRUFFALDINO

Tre piatti li cognosso; el quarto no so cossa che el sia.

BRIGHELLA

Un piatto alla franzese, un intingolo, una bona vivanda.

TRUFFALDINO

Benissimo, la prima portada va ben; alla segonda.

BRIGHELLA

La segonda ghe daremo l\'arrosto, l\'insalata, un pezzo de carne
pastizzada e un bodin.

TRUFFALDINO

Anca qua gh\'è un piatto che no cognosso; coss\'è sto budellin?

BRIGHELLA

Ho dito un bodin, un piatto all\'inglese, una cossa bona.

TRUFFALDINO

Ben, son contento; ma come disponeremio le vivande in tavola?

BRIGHELLA

L\'è una cossa facile. El camerier farà lu.

TRUFFALDINO

No, amigo, me preme la scalcaria; tutto consiste in saver metter in tola
ben.

BRIGHELLA

Se metterà, per esempio, qua la soppa, qua el fritto, qua l\'alesso e
qua el fracandò

(accenna una qualche distribuzione).

TRUFFALDINO

No, no me piase; e in mezzo no ghe mettè gnente?

BRIGHELLA

Bisognerave che fessimo cinque piatti.

TRUFFALDINO

Ben, far cinque piatti.

BRIGHELLA

In mezzo ghe metteremo una salsa per el lesso.

TRUFFALDINO

No, no savè gnente, caro amigo; la salsa no va ben in mezzo; in mezzo
ghe va la minestra.

BRIGHELLA

E da una banda metteremo el lesso, e da st\'altra la salsa\...

TRUFFALDINO

Oibò, no faremo gnente. Voi altri locandieri savì cusinar, ma no savi
metter in tola. Ve insegnerò mi. Fè conto che questa sia la tavola

(s\'inginocchia con un ginocchio, e accenna il pavimento).

Osservè come se distribuisse sti cinque piatti; per esempio: qua in
mezzo la minestra

(straccia un pezzo della lettera di cambio, e figura di mettere per
esempio un piatto nel mezzo).

Qua da sta parte el lesso

(fa lo stesso, stracciando un altro pezzo di lettera, e mettendo il
pezzo da un canto).

Da st\'altra parte el fritto

(fa lo stesso con un altro pezzo di lettera, ponendolo all\'incontro
dell\'altro).

Qua la salsa, e qua el piatto che no cognosso

(con altri due pezzi della lettera compisce la figura di cinque piatti).

Cossa ve par? Cusì anderala ben?

(a Brighella).

BRIGHELLA

Va ben; ma la salsa l\'è troppo lontana dal lesso.

TRUFFALDINO

Adesso vederemo come se pol far a tirarla più da visin.

SCENA TREDICESIMA {#Section0008.xhtml#sigil_toc_id_35}
-----------------

Beatrice, Pantalone e detti.

BEATRICE

Che cosa fai ginocchioni?

(a Truffaldino).

TRUFFALDINO

Stava qua disegnando la scalcaria

(s\'alza).

BEATRICE

Che foglio è quello?

TRUFFALDINO

(Oh diavolo! la lettera che el m\'ha da!).

BEATRICE

Quella è la mia cambiale.

TRUFFALDINO

La compatissa. La torneremo a unir\...

BEATRICE

Briccone! Così tieni conto delle cose mie? Di cose di tanta importanza?
Tu meriteresti che io ti bastonassi. Che dite, signor Pantalone? Si può
vedere una sciocchezza maggior di questa?

PANTALONE

In verità che la xè da rider. Sarave mal se no ghe fusse caso de
remediarghe; ma co mi ghe ne fazzo un\'altra, la xè giustada.

BEATRICE

Tant\'era se la cambiale veniva di lontan paese. Ignorantaccio!

TRUFFALDINO

Tutto el mal l\'è vegnù, perché Brighella no sa metter i piatti in tola.

BRIGHELLA

El trova difficoltà in tutto.

TRUFFALDINO

Mi son un omo che sa\...

BEATRICE

Va via di qua

(a Truffaldino).

TRUFFALDINO

Val più el bon ordine\...

BEATRICE

Va via, ti dico.

TRUFFALDINO

In materia de scalcheria no ghe la cedo al primo marescalco del mondo

(parte).

BRIGHELLA

No lo capisso quell\'omo: qualche volta l\'è furbo, e qualche volta l\'è
alocco.

BEATRICE

Lo fa lo sciocco, il briccone. Ebbene, ci darete voi da pranzo?

(a Brighella).

BRIGHELLA

Se la vol cinque piatti per portada, ghe vol un poco de tempo.

PANTALONE

Coss\'è ste portade? Coss è sti cinque piatti? Alla bona, alla bona.
Quattro risi, un per de piatti, e schiavo. Mi no son omo da suggizion.

BEATRICE

Sentite? Regolatevi voi

(a Brighella).

BRIGHELLA

Benissimo; ma averia gusto, se qualcossa ghe piasesse, che la me lo
disesse.

PANTALONE

Se ghe fusse delle polpette per mi, che stago mal de denti, le magneria
volentiera.

BEATRICE

Sentite? Delle polpette

(a Brighella).

BRIGHELLA

La sarà servida. La se comoda in quella camera, che adessadesso ghe
mando in tola.

BEATRICE

Dite a Truffaldino che venga a servire.

BRIGHELLA

Ghe lo dirò, signor

(parte).

SCENA QUATTORDICESIMA {#Section0008.xhtml#sigil_toc_id_36}
---------------------

Beatrice, Pantalone, poi Camerieri, poi Truffaldino.

BEATRICE

Il signor Pantalone si contenterà di quel poco che daranno.

PANTALONE

Me maraveggio, cara ella, xè anca troppo l\'incomodo che la se tol; quel
che averave da far mi con elo, el fa elo con mi; ma la vede ben, gh\'ho
quella putta in casa; fin che no xè fatto tutto, no xè lecito che la
staga insieme. Ho accettà le so grazie per devertirme un pochetto; tremo
ancora dalla paura. Se no gieri vu, fio mio, quel cagadonao me sbasiva.

BEATRICE

Ho piacere d\'esser arrivato in tempo.

(I Camerieri portano nella camera indicata da Brighella tutto
l\'occorrente per preparare la tavola, con bicchieri, vino, pane ecc.)

PANTALONE

In sta locanda i xè molto lesti.

BEATRICE

Brighella è un uomo di garbo. In Torino serviva un gran cavaliere, e
porta ancora la sua livrea.

PANTALONE

Ghe xè anca una certa locanda sora Canal Grando, in fazza alle Fabbriche
de Rialto, dove che se magna molto ben; son stà diverse volte con certi
galantomeni, de quei della bona stampa, e son stà cusì ben, che co me
l\'arrecordo, ancora me consolo. Tra le altre cosse me recordo d\'un
certo vin de Borgogna che el dava el becco alle stelle.

BEATRICE

Non vi è maggior piacere al mondo, oltre quello di essere in buona
compagnia.

PANTALONE

Oh se la savesse che compagnia che xè quella! Se la savesse che cuori
tanto fatti! Che sincerità! Che schiettezza! Che belle conversazion, che
s\'ha fatto anca alla Zuecca! Siei benedetti. Sette o otto galantomeni,
che no ghe xè i so compagni a sto mondo.

(I Camerieri escono dalla stanza e tornano verso la cucina.)

BEATRICE

Avete dunque goduto molto con questi?

PANTALONE

L\'è che spero de goder ancora.

TRUFFALDINO

(col piatto in mano della minestra o della zuppa)

La resta servida in camera, che porto in tola

(a Beatrice).

BEATRICE

Va innanzi tu; metti giù la zuppa.

TRUFFALDINO

Eh, la resti servida

(fa le cerimonie).

PANTALONE

El xè curioso sto so servitor. Andemo

(entra in camera).

BEATRICE

Io vorrei meno spirito, e più attenzione

(a Truffaldino, ed entra).

TRUFFALDINO

Guardè che bei trattamenti! Un piatto alla volta! I spende i so
quattrini, e no i gh\'ha niente de bon gusto. Chi sa gnanca se sta
minestra la sarà bona da niente; voi sentir

(assaggia la minestra, prendendone con un cucchiaio che ha in tasca).

Mi gh\'ho sempre le mie arme in scarsella. Eh! no gh\'è mal; la poderave
esser pezo

(entra in camera).

SCENA QUINDICESIMA {#Section0008.xhtml#sigil_toc_id_37}
------------------

Un Cameriere con un piatto, poi Truffaldino, poi Florindo, poi Beatrice
ed altri Camerieri.

CAMERIERE

Quanto sta costui a venir a prender le vivande?

TRUFFALDINO

(dalla camera)

Son qua, camerada; cossa me deu?

CAMERIERE

Ecco il bollito. Vado a prender un altro piatto

(parte).

TRUFFALDINO

Che el sia castrà, o che el sia vedèllo? El me par castrà. Sentimolo un
pochetin

(ne assaggia un poco).

No l\'è né castrà, né vedèllo: l\'è pegora bella e bona

(s\'incammina verso la camera di Beatrice).

FLORINDO

Dove si va?

(l\'incontra).

TRUFFALDINO

(Oh poveretto mi!).

FLORINDO

Dove vai con quel piatto?

TRUFFALDINO

Metteva in tavola, signor.

FLORINDO

A chi?

TRUFFALDINO

A vussioria.

FLORINDO

Perché metti in tavola prima ch\'io venga a casa?

TRUFFALDINO

V\'ho visto a vegnir dalla finestra. (Bisogna trovarla).

FLORINDO

E dal bollito principi a metter in tavola, e non dalla zuppa?

TRUFFALDINO

Ghe dirò, signor, a Venezia la zuppa la se magna in ultima.

FLORINDO

Io costumo diversamente. Voglio la zuppa. Riporta in cucina quel piatto.

TRUFFALDINO

Signor sì la sarà servida.

FLORINDO

E spicciati, che voglio poi riposare.

TRUFFALDINO

Subito

(mostra di ritornare in cucina).

FLORINDO

(Beatrice non la ritroverò mai?)

(entra nell\'altra camera in prospetto).

Truffaldino, entrato Florindo in camera, corre col piatto e lo porta a
Beatrice.

CAMERIERE

(torna con una vivanda)

E sempre bisogna aspettarlo. Truffaldino

(chiama).

TRUFFALDINO

(esce di camera di Beatrice)

Son qua. Presto, andè a parecchiar in quell\'altra camera, che l\'è
arrivado quell\'altro forestier, e portè la minestra subito.

CAMERIERE

Subito

(parte).

TRUFFALDINO

Sta piatanza coss\'èla mo? Bisogna che el sia el fracastor (assaggia).
Bona, bona, da galantomo

(la porta in camera di Beatrice. Camerieri passano e portano
l\'occorrente per preparare la tavola in camera di Florindo).

Bravi. Pulito. I è lesti come gatti

(verso i Camerieri).

Oh se me riuscisse de servir a tavola do padroni; mo la saria la gran
bella cossa.

(Camerieri escono dalla camera di Florindo e vanno verso la cucina).

Presto, fioi, la menestra.

CAMERIERE

Pensate alla vostra tavola, e noi penseremo a questa

(parte).

TRUFFALDINO

Voria pensar a tutte do, se podesse. (Cameriere torna colla minestra per
Florindo). Dè qua a mi, che ghe la porterò mi; andè a parecchiar la roba
per quell\'altra camera.

(Leva la minestra di mano al Cameriere e la porta in camera di
Florindo).

CAMERIERE

Ê curioso costui. Vuol servire di qua e di la. Io lascio fare: già la
mia mancia bisognerà che me la diano. Truffaldino esce di camera di
Florindo.

BEATRICE

Truffaldino

(dalla camera lo chiama).

CAMERIERE

Eh! servite il vostro padrone

(a Truffaldino).

TRUFFALDINO

Son qua

(entra in camera di Beatrice; i Camerieri portano il bollito per
Florindo).

CAMERIERE

Date qui

(lo prende).

Camerieri partono. Truffaldino esce di camera di Beatrice con i tondi
sporchi.

FLORINDO

Truffaldino

(dalla camera lo chiama forte).

TRUFFALDINO

De qua

(vuol prendere il piatto del bollito dal Cameriere).

CAMERIERE

Questo lo porto io.

TRUFFALDINO

No sentì che el me chiama mi?

(gli leva il bollito di mano e lo porta a Florindo).

CAMERIERE

È bellissima. Vuol far tutto.

(I Camerieri portano un piatto di polpette, lo danno al Cameriere e
partono).

CAMERIERE

Lo porterei io in camera, ma non voglio aver che dire con costui.

(Truffaldino esce di camera di Florindo con i tondi sporchi).

Tenete, signor faccendiere; portate queste polpette al vostro padrone.

TRUFFALDINO

Polpette?

(prendendo il piatto in mano).

CAMERIERE

Sì, le polpette ch\'egli ha ordinato

(parte).

TRUFFALDINO

Oh bella! A chi le òi da portar? Chi diavol de sti padroni le averà
ordinade? Se ghel vago a domandar in cusina, no voria metterli in
malizia; se fallo e che no le porta a chi le ha ordenade, quell\'altro
le domanderà e se scoverzirà l\'imbroio. Farò cussi\... Eh, gran mi!
Farò cusì; le spartirò in do tondi, le porterò metà per un, e cusì chi
le averà ordinade, le vederà

(prende un altro tondo di quelli che sono in sala, e divide le polpette
per metà).

Quattro e quattro. Ma ghe n\'è una de più. A chi ghe l\'òia da dar? No
voi che nissun se n\'abbia per mal; me la magnerò mi

(mangia la polpetta).

Adesso va ben. Portemo le polpette a questo (mette in terra l\'altro
tondo, e ne porta uno da Beatrice).

CAMERIERE

(con un bodino all\'inglese)

Truffaldino

(chiama)

TRUFFALDINO

Son qua

(esce dalla camera di Beatrice).

CAMERIERE

Portate questo bodino\...

TRUFFALDINO

Aspettè che vegno

(prende l\'altro tondino di polpette, e lo porta a Florindo).

CAMERIERE

Sbagliate; le polpette vanno di la.

TRUFFALDINO

Sior si, lo so, le ho portade de là; e el me padron manda ste quattro a
regalar a sto forestier

(entra).

CAMERIERE

Si conoscono dunque, sono amici. Potevano desinar insieme.

TRUFFALDINO

(torna in camera di Florindo)

E cusì, coss\'elo sto negozio?

(al Cameriere).

CAMERIERE

Questo è un bodino all\'inglese.

TRUFFALDINO

A chi valo?

CAMERIERE

Al vostro padrone

(parte).

TRUFFALDINO

Che diavolo è sto bodin? L\'odor l\'è prezioso, el par polenta. Oh, se
el fuss polenta, la saria pur una bona cossa! Voi sentir

(tira fuori di tasca una forchetta).

No l\'è polenta, ma el ghe someia

(mangia).

L\'è meio della polenta

(mangia).

BEATRICE

Truffaldino

(dalla camera lo chiama).

TRUFFALDINO

Vegno

(risponde colla bocca piena).

FLORINDO

Truffaldino

(lo chiama dalla sua camera).

TRUFFALDINO

Son qua

(risponde colla bocca piena, come sopra).

Oh che roba preziosa! Un altro bocconcin, e vegno

(segue a mangiare).

BEATRICE

(esce dalla sua camera e vede Truffaldino che mangia; gli dà un calcio e
gli dice)

Vieni a servire

(torna nella sua camera).

Truffaldino mette il bodino in terra, ed entra in camera di Beatrice.

FLORINDO

(esce dalla sua camera)

Truffaldino

(chiama).

Dove diavolo è costui?

TRUFFALDINO

(esce dalla camera di Beatrice)

L\'è qua

(vedendo Florindo).

FLORINDO

Dove sei? Dove ti perdi?

TRUFFALDINO

Era andà a tor dei piatti, signor.

FLORINDO

Vi è altro da mangiare?

TRUFFALDINO

Anderò a veder.

FLORINDO

Spicciati, ti dico, che ho bisogno di riposare

(torna nella sua camera).

TRUFFALDINO

Subito. Camerieri, gh\'è altro?

(chiama).

Sto bodin me lo metto via per mi

(lo nasconde).

CAMERIERE

Eccovi l\'arrosto

(porta un piatto con l\'arrosto).

TRUFFALDINO

Presto i frutti

(prende l\'arrosto).

CAMERIERE

Gran furie! Subito

(parte).

TRUFFALDINO

L\'arrosto lo porterò a questo

(entra da Florindo).

CAMERIERE

Ecco le frutta, dove siete?

(con un piatto di frutta).

TRUFFALDINO

Son qua

(di camera di Florindo).

CAMERIERE

Tenete

(gli dà le frutta).

Volete altro?

TRUFFALDINO

Aspettè

(porta le frutta a Beatrice).

CAMERIERE

Salta di qua, salta di là; è un diavolo costui.

TRUFFALDINO

Non occorr\'altro. Nissun vol altro.

CAMERIERE

Ho piacere.

TRUFFALDINO

Parecchiè per mi.

CAMERIERE

Subito

(parte).

TRUFFALDINO

Togo su el me bodin; evviva, l\'ho superada, tutti i è contenti, no i
vol alter, i è stadi servidi. Ho servido a tavola do padroni, e un non
ha savudo dell\'altro. Ma se ho servido per do, adess voio andar a
magnar per quattro

(parte).

SCENA SEDICESIMA {#Section0008.xhtml#sigil_toc_id_38}
----------------

Strada con veduta della locanda\
Smeraldina, poi il Cameriere della locanda.

SMERALDINA

Oh, guardate che discretezza della mia padrona! Mandarmi con un
viglietto ad una locanda, una giovane come me! Servire una donna
innamorata è una cosa molto cattiva. Fa mille stravaganze questa mia
padrona; e quel che non so capire si è, che è innamorata del signor
Silvio a segno di sbudellarsi per amor suo, e pur manda i viglietti ad
un altro. Quando non fosse che ne volesse uno per la state e l\'altro
per l\'inverno. Basta\... Io nella locanda non entro certo. Chiamerò;
qualcheduno uscirà. O di casa! o della locanda!

CAMERIERE

Che cosa volete, quella giovine?

SMERALDINA

(Mi vergogno davvero, davvero). Ditemi.. Un certo signor Federigo
Rasponi è alloggiato in questa locanda?

CAMERIERE

Sì, certo. Ha finito di pranzare che è poco.

SMERALDINA

Avrei da dargli una cosa.

CAMERIERE

Qualche ambasciata? Potete passare.

SMERALDINA

Ehi, chi vi credete ch\'io sia? Sono la cameriera della sua sposa.

CAMERIERE

Bene, passate.

SMERALDINA

Oh, non ci vengo io là dentro.

CAMERIERE

Volete ch\'io lo faccia venire sulla strada? Non mi pare cosa ben fatta;
tanto più ch\'egli è in compagnia col signor Pantalone de\' Bisognosi.

SMERALDINA

Il mio padrone? Peggio! Oh, non ci vengo.

CAMERIERE

Manderò il suo servitore, se volete.

SMERALDINA

Quel moretto?

CAMERIERE

Per l\'appunto.

SMERALDINA

Si, mandatelo.

CAMERIERE

(Ho inteso. Il moretto le piace. Si vergogna a venir dentro. Non si
vergognerà a farsi scorgere in mezzo alla strada)

(entra).

SCENA DICIASSETTESIMA {#Section0008.xhtml#sigil_toc_id_39}
---------------------

Smeraldina, poi Truffaldino.

SMERALDINA

Se il padrone mi vede, che cosa gli dirò? Dirò che venivo in traccia di
lui; eccola bella e accomodata. Oh, non mi mancano ripieghi.

TRUFFALDINO

(con un fiasco in mano, ed un bicchiere, ed un tovagliolino)

Chi è che me domanda?

SMERALDINA

Sono io, signore. Mi dispiace avervi incomodato.

TRUFFALDINO

Niente; son qua a ricever i so comandi.

SMERALDINA

M\'immagino che foste a tavola, per quel ch\'io vedo.

TRUFFALDINO

Era a tavola, ma ghe tornerò.

SMERALDINA

Davvero me ne dispiace.

TRUFFALDINO

E mi gh\'ho gusto. Per dirvela, ho la panza piena, e quei bei occhietti
i è giusto a proposito per farme digerir.

SMERALDINA

(Egli è pure grazioso!).

TRUFFALDINO

Metto zo el fiaschetto e son qua da vu, cara.

SMERALDINA

(Mi ha detto cara). La mia padrona manda questo viglietto al signor
Federigo Rasponi; io nella locanda non voglio entrare, onde ho pensato
di dar a voi quest\'incomodo, che siete il suo servitore.

TRUFFALDINO

Volentiera, ghe lo porterò; ma prima sappiè che anca mi v\'ho da far
un\'imbassada.

SMERALDINA

Per parte di chi?

TRUFFALDINO

Per parte de un galantomo. Disime, conossive vu un certo Truffaldin
Battocchio?

SMERALDINA

Mi pare averlo sentito nominare una volta, ma non me ne ricordo.
(Avrebbe a esser lui questo).

TRUFFALDINO

L\'è un bell\'omo: bassotto, traccagnotto, spiritoso, che parla ben.
Maestro de cerimonie\...

SMERALDINA

Io non lo conosco assolutamente.

TRUFFALDINO

E pur lu el ve cognosse, e l\'è innamorado de vu.

SMERALDINA

Oh! mi burlate.

TRUFFALDINO

E se el podesse sperar un tantin de corrispondenza, el se daria da
cognosser.

SMERALDINA

Dirò, signore; se lo vedessi e mi desse nel genio, sarebbe facile ch\'io
gli corrispondessi.

TRUFFALDINO

Vorla che ghe lo fazza veder?

SMERALDINA

Lo vedrò volentieri.

TRUFFALDINO

Adesso subito

(entra nella locanda).

SMERALDINA

Non è lui dunque.

(Truffaldino esce dalla locanda, fa delle riverenze a Smeraldina, le
passa vicino; poi sospira ed entra nella locanda).

Quest\'istoria non la capisco.

TRUFFALDINO

L\'ala visto?

(tornando a uscir fuori).

SMERALDINA

Chi?

TRUFFALDINO

Quello che è innamorato delle so bellezze.

SMERALDINA

Io non ho veduto altri che voi.

TRUFFALDINO

Mah!

(sospirando).

SMERALDINA

Siete voi forse quello che dice di volermi bene?

TRUFFALDINO

Son mi

(sospirando).

SMERALDINA

Perché non me l\'avete detto alla prima?

TRUFFALDINO

Perché son un poco vergognosetto.

SMERALDINA

(Farebbe innamorare i sassi).

TRUFFALDINO

E cusì, cossa me disela?

SMERALDINA

Dico che\...

TRUFFALDINO

Via, la diga.

SMERALDINA

Oh, anch\'io sono vergognosetta.

TRUFFALDINO

Se se unissimo insieme, faressimo el matrimonio de do persone
vergognose.

SMERALDINA

In verità, voi mi date nel genio.

TRUFFALDINO

Èla putta ella?

SMERALDINA

Oh, non si domanda nemmeno.

TRUFFALDINO

Che vol dir, no certo.

SMERALDINA

Anzi vuol dir, sì certissimo.

TRUFFALDINO

Anca mi son putto.

SMERALDINA

Io mi sarei maritata cinquanta volte, ma non ho mai trovato una persona
che mi dia nel genio.

TRUFFALDINO

Mi possio sperar de urtarghe in tela simpatia?

SMERALDINA

In verità, bisogna che io lo dica, voi avete un non so che\... Basta,
non dico altro.

TRUFFALDINO

Uno che la volesse per muier, come averielo da far?

SMERALDINA

Io non ho né padre, né madre. Bisognerebbe dirlo al mio padrone, o alla
mia padrona.

TRUFFALDINO

Benissimo, se ghel dirò, cossa dirali?

SMERALDINA

Diranno, che se sono contenta io\...

TRUFFALDINO

E ella cossa dirala?

SMERALDINA

Dirò\... che se sono contenti loro\...

TRUFFALDINO

Non occorr\'altro. Saremo tutti contenti. Deme la lettera, e co ve
porterò la risposta, discorreremo.

SMERALDINA

Ecco la lettera.

TRUFFALDINO

Saviu mo cossa che la diga sta lettera?

SMERALDINA

Non lo so, e se sapeste che curiosità che avrei di saperlo!

TRUFFALDINO

No voria che la fuss una qualche lettera de sdegno, e che m\'avess da
far romper el muso.

SMERALDINA

Chi sa? D\'amore non dovrebbe essere.

TRUFFALDINO

Mi no vòi impegni. Se no so cossa che la diga, mi no ghe la porto.

SMERALDINA

Si potrebbe aprirla\... ma poi a serrarla ti voglio.

TRUFFALDINO

Eh, lassè far a mi; per serrar le lettere son fatto a posta; no se
cognosserà gnente affatto.

SMERALDINA

Apriamola dunque.

TRUFFALDINO

Saviu lezer vu?

SMERALDINA

Un poco. Ma voi saprete legger bene.

TRUFFALDINO

Anca mi un pochettin.

SMERALDINA

Sentiamo dunque.

TRUFFALDINO

Averzimola con pulizia

(ne straccia una parte).

SMERALDINA

Oh! che avete fatto?

TRUFFALDINO

Niente. Ho el segreto d\'accomodarla. Eccola qua, l\'è averta.

SMERALDINA

Via, leggetela.

TRUFFALDINO

Lezila vu. El carattere della vostra padrona l\'intenderè meio de mi.

SMERALDINA

Per dirla, io non capisco niente

(osservando la lettera).

TRUFFALDINO

E mi gnanca una parola

(fa lo stesso).

SMERALDINA

Che serviva dunque aprirla?

TRUFFALDINO

Aspettè; inzegnemose; qualcossa capisso

(tiene egli la lettera).

SMERALDINA

Anch\'io intendo qualche lettera.

TRUFFALDINO

Provemose un po\'per un. Questo non elo un emme?

SMERALDINA

Oibò; questo è un [erre]{.testo_corsivo}.

TRUFFALDINO

Dall\'erre all\'emme gh\'è poca differenza.

SMERALDINA

[Ri, ri, a, ria]{.testo_corsivo}. No, no, state cheto, che credo sia un
[emme, mi, mi, a, mia]{.testo_corsivo}.

TRUFFALDINO

No dirà [mia,]{.testo_corsivo} dirà [mio]{.testo_corsivo}.

SMERALDINA

No, che vi è la codetta.

TRUFFALDINO

Giusto per questo: [mio]{.testo_corsivo}.

SCENA DICIOTTESIMA {#Section0008.xhtml#sigil_toc_id_40}
------------------

Beatrice e Pantalone dalla locanda, e detti.

PANTALONE

Cossa feu qua?

(a Smeraldina).

SMERALDINA

Niente, signore, venivo in traccia di voi

(intimorita).

PANTALONE

Cossa voleu da mi?

(a Smeraldina).

SMERALDINA

La padrona vi cerca

BEATRICE

Che foglio è quello?

(a Truffaldino).

TRUFFALDINO

Niente, l\'è una carta\...

(intimorito).

BEATRICE

Lascia vedere

(a Truffaldino).

TRUFFALDINO

Signor sì

(gli dà il foglio tremando).

BEATRICE

Come! Questo è un viglietto che viene a me. Indegno! Sempre si aprono le
mie lettere?

TRUFFALDINO

Mi no so niente, signor\...

BEATRICE

Osservate, signor Pantalone, un viglietto della signora Clarice, in cui
mi avvisa delle pazze gelosie di Silvio; e questo briccone me l\'apre.

PANTALONE

E ti, ti ghe tien terzo?

(a Smeraldina).

SMERALDINA

Io non so niente, signore.

BEATRICE

Chi l\'ha aperto questo viglietto?

TRUFFALDINO

Mi no.

SMERALDINA

Nemmen io.

PANTALONE

Mo chi l\'ha portà?

SMERALDINA

Truffaldino lo portava al suo padrone.

TRUFFALDINO

E Smeraldina l\'ha portà a Truffaldin.

SMERALDINA

(Chiacchierone, non ti voglio più bene).

PANTALONE

Ti, pettegola desgraziada, ti ha fatto sta bell\'azion? Non so chi me
tegna che no te daga una man in tel muso.

SMERALDINA

Le mani nel viso non me le ha date nessuno; e mi maraviglio di voi.

PANTALONE

Cusì ti me rispondi?

(le va da vicino).

SMERALDINA

Eh, non mi pigliate. Avete degli impedimenti che non potete correre

(parte correndo).

PANTALONE

Desgraziada, te farò veder se posso correr; te chiaperò

(parte correndo dietro a Smeraldina).

SCENA DICIANNOVESIMA {#Section0008.xhtml#sigil_toc_id_41}
--------------------

Beatrice, Truffaldino, poi Florindo alla finestra della locanda.

TRUFFALDINO

(Se savess come far a cavarme).

BEATRICE

(Povera Clarice, ella è disperata per la gelosia di Silvio; converrà
ch\'io mi scopra, e che la consoli)

(osservando il viglietto).

TRUFFALDINO

(Par che nol me veda. Voi provar de andar via)

(pian piano se ne vorrebbe andare).

BEATRICE

Dove vai?

TRUFFALDINO

Son qua

(si ferma).

BEATRICE

Perché hai aperta questa lettera?

TRUFFALDINO

L\'è stada Smeraldina. Signor, mi no so gnente.

BEATRICE

Che Smeraldina? Tu sei stato, briccone. Una, e una due. Due lettere mi
hai aperte in un giorno. Vieni qui.

TRUFFALDINO

Per carità, signor

(accostandosi con paura).

BEATRICE

Vien qui, dico.

TRUFFALDINO

Per misericordia

(s\'accosta tremando).\
Beatrice leva dal fianco di Truffaldino il bastone, e lo bastona ben
bene, essendo voltata colla schiena alla locanda.

FLORINDO

(alla finestra della locanda)

Come! Si bastona il mio servitore?

(parte dalla finestra).

TRUFFALDINO

No più, per carità.

BEATRICE

Tieni, briccone. Imparerai a aprir le lettere

(getta il bastone per terra e parte).

SCENA VENTESIMA {#Section0008.xhtml#sigil_toc_id_42}
---------------

Truffaldino, poi Florindo dalla locanda.

TRUFFALDINO

(dopo partita Beatrice)

Sangue de mi! Corpo de mi! Cusì se tratta coi omeni della me sorte?
Bastonar un par mio? I servitori, co no i serve, i se manda via, no i se
bastona.

FLORINDO

Che cosa dici?

(uscito dalla locanda non veduto da Truffaldino).

TRUFFALDINO

(Oh!)

(avvedendosi di Florindo).

No se bastona i servitori dei altri in sta maniera. Quest\'l\'è un
affronto, che ha ricevudo el me padron

(verso la parte per dove è andata Beatrice).

FLORINDO

Sì, è un affronto che ricevo io. Chi è colui che ti ha bastonato?

TRUFFALDINO

Mi no lo so, signor: nol conosso.

FLORINDO

Perché ti ha battuto?

TRUFFALDINO

Perché\... perché gh\'ho spudà su una scarpa.

FLORINDO

E ti lasci bastonare così? E non ti muovi, e non ti difendi nemmeno? Ed
esponi il tuo padrone ad un affronto, ad un precipizio? Asino,
poltronaccio che sei

(prende il bastone di terra).

Se hai piacere a essere bastonato, ti darò gusto, ti bastonerò ancora io

(lo bastona, e poi entra nella locanda).

TRUFFALDINO

Adesso posso dir che son servitor de do padroni. Ho tirà el salario da
tutti do

(entra nella locanda).

[]{#Section0009.xhtml}

ATTO TERZO
==========

SCENA PRIMA {#Section0009.xhtml#sigil_toc_id_43}
-----------

Sala della locanda con varie porte\
Truffaldino solo, poi due Camerieri.

TRUFFALDINO

Con una scorladina ho mandà via tutto el dolor delle bastonade; ma ho
magnà ben, ho disnà ben, e sta sera cenerò meio, e fin che posso vòi
servir do padroni, tanto almanco che podesse tirar do salari. Adess mo
coss\'òia da far? El primo patron l\'è fora de casa, el segondo dorme;
poderia giust adesso dar un poco de aria ai abiti; tirarli fora dei
bauli, e vardar se i ha bisogno de gnente. Ho giusto le chiavi. Sta sala
l\'è giusto a proposito. Tirerò fora i bauli, e farò pulito. Bisogna che
me fazza aiutar. Camerieri

(chiama).

CAMERIERE

(viene in compagnia d\'un garzone)

Che volete?

TRUFFALDINO

Voria che me dessi una man a tirar fora certi bauli da quelle camere,
per dar un poco de aria ai vestidi.

CAMERIERE

Andate: aiutategli

(al garzone).

TRUFFALDINO

Andemo, che ve darò de bona man una porzion de quel regalo che m\'ha
fatto i me padroni

(entra in una camera col garzone).

CAMERIERE

Costui pare sia un buon servitore. È lesto, pronto, attentissimo; però
qualche difetto anch\'egli avrà. Ho servito anch\'io, e so come la va.
Per amore non si fa niente. Tutto si fa o per pelar il padrone, o per
fidarlo.

TRUFFALDINO

(dalla suddetta camera col garzone, portando fuori un baule)

A pian; mettemolo qua

(lo posano in mezzo alla sala).

Andemo a tor st\'altro. Ma femo a pian, che el padron l\'è in
quell\'altra stanza, che el dorme (entra col garzone nella camera di
Florindo).

CAMERIERE

Costui o è un grand\'uomo di garbo, o è un gran furbo: servir due
persone in questa maniera non ho più veduto. Davvero voglio stare un
po\' attento; non vorrei che un giorno o l\'altro, col pretesto di
servir due padroni, tutti due li spogliasse.

TRUFFALDINO

(dalla suddetta camera col garzone con l\'altro baule)

E questo mettemolo qua

(lo posano in poca distanza da quell\'altro).

Adesso, se volè andar, andè, che no me occorre altro

(al garzone).

CAMERIERE

Via, andate in cucina.

(al garzone che se ne va)

Avete bisogno di nulla?

(a Truffaldino).

TRUFFALDINO

Gnente affatto. I fatti mii li fazzo da per mi.

CAMERIERE

Oh va, che sei un omone; se la duri, ti stimo

(parte).

TRUFFALDINO

Adesso farò le cosse pulito, con quiete, e senza che nissun me disturba

(tira fuori di tasca una chiave)

Qual èla mo sta chiave? Qual averzela de sti do bauli? Proverò

(apre un baule).

L\'ho indovinada subito. Son el primo omo del mondo. E st\'altra
averzirà quell\'altro

(tira fuori di tasca l\'altra chiave, e apre l\'altro baule).

Eccoli averti tutti do. Tiremo fora ogni cossa

(leva gli abiti da tutti due i bauli e li posa sul tavolino, avvertendo
che in ciaschedun baule vi sia un abito di panno nero, dei libri e delle
scritture, e altre cose a piacere).

Voio un po veder, se gh\'è niente in te le scarselle. Delle volte i ghe
mette dei buzzolai, dei confetti

(visita le tasche del vestito nero di Beatrice, e vi trova un ritratto).

Oh bello! Che bel ritratto! Che bell\'omo! De chi saral sto ritratto?
L\'è un\'idea, che me par de cognosser, e no me l\'arrecordo. El ghe
someia un tantinin all\'alter me padron; ma no, nol gh\'ha né sto abito,
nè sta perrucca.

SCENA SECONDA {#Section0009.xhtml#sigil_toc_id_44}
-------------

Florindo nella sua camera, e detto.

FLORINDO

Truffaldino

(chiamandolo dalla camera).

TRUFFALDINO

O sia maledetto! El s\'ha sveià. Se el diavol fa che el vegna fora, e el
veda st\'alter baul, el vorrà saver\... Presto, presto, lo serrerò, e
dirò che no so de chi el sia

(va riponendo le robe).

FLORINDO

Truffaldino

(come sopra).

TRUFFALDINO

La servo

(risponde forte).

Che metta via la roba. Ma! No me recordo ben sto abito dove che el vada.
E ste carte no me recordo dove che le fusse.

FLORINDO

Vieni, o vengo a prenderti con un bastone?

(come sopra).

TRUFFALDINO

Vengo subito

(forte, come sopra).

Presto, avanti che el vegna. Co l\'anderà fora de casa, giusterò tutto

(mette le robe a caso nei due bauli, e li serra).

FLORINDO

(esce dalla sua stanza in veste da camera)

Che cosa diavolo fai?

(a Truffaldino).

TRUFFALDINO

Caro signor, no m\'ala dito che repulissa i panni? Era qua che fava
l\'obbligo mio.

FLORINDO

E quell\'altro baule di chi è?

TRUFFALDINO

No so gnente; el sarà d\'un altro forestier.

FLORINDO

Dammi il vestito nero.

TRUFFALDINO

La servo

(apre il baule di Florindo, e gli dà il suo vestito nero).\
Florindo si fa levare la veste da camera, e si pone il vestito; poi,
mettendo le mani in tasca, trova il ritratto.

FLORINDO

Che è questo?

(maravigliandosi del ritratto).

TRUFFALDINO

(Oh diavolo! Ho fallà. In vece de metterlo in tel vestido de quel alter,
l\'ho mess in questo. El color m\'ha fatto fallar).

FLORINDO

(Oh cieli! Non m\'inganno io già. Questo è il mio ritratto; il mio
ritratto che donai io medesimo alla mia cara Beatrice). Dimmi, tu, come
è entrato nelle tasche del mio vestito questo ritratto, che non vi era?

TRUFFALDINO

(Adesso mo no so come covrirla. Me inzegnerò).

FLORINDO

Animo, dico; parla, rispondi. Questo ritratto, come nelle mie tasche?

TRUFFALDINO

Caro sior padron, la compatissa la confidenza che me son tolto. Quel
ritratt l\'è roba mia; per no perderlo, l\'aveva nascosto là drento. Per
amor del ciel, la me compatissa.

FLORINDO

Dove hai avuto questo ritratto?

TRUFFALDINO

L\'ho eredità dal me padron.

FLORINDO

Ereditato?

TRUFFALDINO

Sior Sì, ho servido un padron, l\'è morto, el m\'ha lassa delle
bagattelle che le ho vendude, e m\'è resta sto ritratt.

FLORINDO

Oimè! Quanto tempo è che è morto questo tuo padrone?

TRUFFALDINO

Sarà una settimana. (Digo quel che me vien alla bocca).

FLORINDO

Come chiamavasi questo tuo padrone?

TRUFFALDINO

Nol so, signor; el viveva incognito.

FLORINDO

Incognito? Quanto tempo lo hai tu servito?

TRUFFALDINO

Poco: diese o dodese zorni.

FLORINDO

(Oh cieli! Sempre più tremo, che non sia stata Beatrice! Fuggi in abito
d\'uomo\... viveva incognita\... Oh me infelice, se fosse vero!).

TRUFFALDINO

(Col crede tutto, ghe ne racconterò delle belle).

FLORINDO

Dimmi, era giovine il tuo padrone?

(con affanno).

TRUFFALDINO

Sior si, zovene.

FLORINDO

Senza barba?

TRUFFALDINO

Senza barba.

FLORINDO

(Era ella senz\'altro)

(sospirando).

TRUFFALDINO

(Bastonade spereria de no ghe n\'aver).

FLORINDO

Sai la patria almeno del tuo defonto padrone?

TRUFFALDINO

La patria la saveva, e no me l\'arrecordo.

FLORINDO

Turinese forse?

TRUFFALDINO

Sior si, turinese.

FLORINDO

(Ogni accento di costui è una stoccata al mio cuore). Ma dimmi: è egli
veramente morto questo giovine torinese?

TRUFFALDINO

L\'è morto siguro.

FLORINDO

Di qual male è egli morto?

TRUFFALDINO

Gh\'è vegnù un accidente, e l\'è andà. (Cusì me destrigo).

FLORINDO

Dove è stato sepolto?

TRUFFALDINO

(Un altro imbroio). No l\'è stà sepolto, signor; perché un alter
servitor, so patrioto, l\'ha avù la licenza de metterlo in t\'una cassa,
e mandarlo al so paese.

FLORINDO

Questo servitore era forse quello che ti fece stamane ritirar dalla
Posta quella lettera?

TRUFFALDINO

Sior sì, giusto Pasqual.

FLORINDO

(Non vi è più speranza. Beatrice è morta. Misera Beatrice! i disagi del
viaggio, i tormenti del cuore l\'avranno uccisa. Oimè! non posso reggere
all\'eccesso del mio dolore

(entra nella sua camera).

SCENA TERZA {#Section0009.xhtml#sigil_toc_id_45}
-----------

Truffaldino, poi Beatrice e Pantalone.

TRUFFALDINO

Coss\'è st\'imbroio? L\'è addolorà, el pianze, el se despera. No voria
mi co sta favola averghe sveià l\'ippocondria. Mi l\'ho fatto per
schivar el complimento delle bastonade, e per no scovrir l\'imbroio dei
do bauli. Quel ritratto gh\'ha fatto mover i vermi. Bisogna che el lo
conossa. Orsù, l\'è mei che torna a portar sti bauli in camera, e che me
libera da un\'altra seccatura compagna. Ecco qua quell\'alter padron.
Sta volta se divide la servitù, e se me fa el ben servido

(accennando le bastonate).

BEATRICE

Credetemi, signor Pantalone, che l\'ultima partita di specchi e cere è
duplicata.

PANTALONE

Poderia esser che i zoveni avesse fallà. Faremo passar i conti un\'altra
volta col scrittural; incontreremo e vederemo la verità.

BEATRICE

Ho fatto anch\'io un estratto di diverse partite cavate dai nostri
libri. Ora lo riscontreremo. Può darsi che si dilucidi o per voi, o per
me. Truffaldino?

TRUFFALDINO

Signor.

BEATRICE

Hai tu le chiavi del mio baule?

TRUFFALDINO

Sior sì; eccole qua.

BEATRICE

Perché l\'hai portato in sala il mio baule?

TRUFFALDINO

Per dar un poco de aria ai vestidi.

BEATRICE

Hai fatto?

TRUFFALDINO

Ho fatto.

BEATRICE

Apri e dammi\... Quell\'altro baule di chi è?

TRUFFALDINO

L\'è d\'un altro forestier, che è arrivado.

BEATRICE

Dammi un libro di memorie, che troverai nel baule.

TRUFFALDINO

Sior sì. (El ciel me la manda bona)

(apre e cerca il libro).

PANTALONE

Pol esser, come ghe digo, che i abbia fallà. In sto caso, error no fa
pagamento.

BEATRICE

E può essere che così vada bene; lo riscontreremo.

TRUFFALDINO

Elo questo?

(presenta un libro di scritture a Beatrice).

BEATRICE

Sarà questo

(lo prende senza molto osservarlo, e lo apre).

No, non è questo\... Di chi è questo libro?

TRUFFALDINO

(L\'ho fatta).

BEATRICE

(Queste sono due lettere da me scritte a Florindo. Oimè! Queste memorie,
questi conti appartengono a lui. Sudo, tremo, non so in che mondo mi
sia).

PANTALONE

Cossa gh\'è, sior Federigo? Se sentelo gnente

BEATRICE

Niente. (Truffaldino, come nel mio baule evvi questo libro che non è
mio?)

(piano a Truffaldino).

TRUFFALDINO

Mi no saveria..

BEATRICE

Presto, non ti confondere, dimmi la verità.

TRUFFALDINO

Ghe domando scusa dell\'ardir che ho avudo de metter quel libro in tel
so baul. L\'è roba mia, e per non perderlo, l\'ho messo là. (L\'è andada
ben con quell\'alter, pol esser che la vada ben anca con questo).

BEATRICE

Questo libro è tuo, e non lo conosci, e me lo dai in vece del mio?

TRUFFALDINO

(Oh, questo l\'è ancora più fin). Ghe dirò: l\'è poc tempo che l\'è mio,
e cusì subito no lo conosso.

BEATRICE

E dove hai avuto tu questo libro?

TRUFFALDINO

Ho servido un padron a Venezia, che l\'è morto, e ho eredità sto libro.

BEATRICE

Quanto tempo è?

TRUFFALDINO

Che soia mi? Dies o dodese zorni.

BEATRICE

Come può darsi, se io ti ho ritrovato a Verona?

TRUFFALDINO

Giust allora vegniva via da Venezia per la morte del me padron.

BEATRICE

(Misera me!). Questo tuo padrone aveva nome Florindo?

TRUFFALDINO

Sior sì, Florindo.

BEATRICE

Di famiglia Aretusi?

TRUFFALDINO

Giusto, Aretusi.

BEATRICE

Ed è morto sicuramente?

TRUFFALDINO

Sicurissimamente.

BEATRICE

Di che male è egli morto? Dove è stato sepolto?

TRUFFALDINO

L\'è cascà in canal, el s\'ha negà, e nol s\'ha più visto.

BEATRICE

Oh me infelice! Morto è Florindo, morto è il mio bene, morta è l\'unica
mia speranza. A che ora mi serve questa inutile vita, se morto è quello
per cui unicamente viveva? Oh vane lusinghe! Oh cure gettate al vento!
Infelici strattagemmi d\'amore! Lascio la patria, abbandono i parenti,
vesto spoglie virili, mi avventuro ai pericoli, azzardo la vita istessa,
tutto fo per Florindo e il mio Florindo è morto. Sventurata Beatrice!
Era poco la perdita del fratello, se non ti si aggiungeva quella ancor
dello sposo? Alla morte di Federigo volle il cielo che succedesse quella
ancor di Florindo. Ma se io fui la cagione delle morti loro, se io sono
la rea, perchè contro di me non s\'arma il Cielo a vendetta? Inutile è
il pianto, vane son le querele, Florindo è morto. Oimè! Il dolore mi
opprime. Più non veggo la luce. Idolo mio, caro sposo, ti seguirò
disperata

(parte smaniosa, ed entra nella sua camera).

PANTALONE

(inteso con ammirazione tutto il discorso, e la disperazione di
Beatrice)

Truffaldino!

TRUFFALDINO

Sior Pantalon!

PANTALONE

Donna!

TRUFFALDINO

Femmena!

PANTALONE

Oh che caso!

TRUFFALDINO

Oh che maraveia!

PANTALONE

Mi resto confuso.

TRUFFALDINO

Mi son incanta.

PANTALONE

Ghe lo vago a dir a mia fia

(parte).

TRUFFALDINO

No so più servitor de do padroni, ma de un padron e de una padrona

(parte).

SCENA QUARTA {#Section0009.xhtml#sigil_toc_id_46}
------------

Strada colla locanda\
Dottore, poi Pantalone dalla locanda.

DOTTORE

Non mi posso dar pace di questo vecchiaccio di Pantalone. Più che ci
penso, più mi salta la bile.

PANTALONE

Dottor caro, ve reverisso

(con allegria).

DOTTORE

Mi maraviglio che abbiate anche tanto ardire di salutarmi.

PANTALONE

V\'ho da dar una nova. Sappiè\...

DOTTORE

Volete forse dirmi che avete fatto le nozze? Non me n\'importa un fico.

PANTALONE

No xè vero gnente. Lassème parlar, in vostra malora.

DOTTORE

Parlate, che il canchero vi mangi.

PANTALONE

(Adessadesso me vien voggia de dottorarlo a pugni). Mia fia, se volè, la
sarà muggier de vostro fio.

DOTTORE

Obbligatissimo, non v\'incomodate. Mio figlio non è di sì buono stomaco.
Datela al signor turinese.

PANTALONE

Co saverè chi xè quel turinese, no dirè cusì.

DOTTORE

Sia chi esser si voglia. Vostra figlia è stata veduta con lui, *et hoc
sufficit.*

PANTALONE

Ma no xè vero che el sia\...

DOTTORE

Non voglio sentir altro.

PANTALONE

Se no me ascolterè, sarà pezo per vu.

DOTTORE

Lo vedremo per chi sarà peggio.

PANTALONE

Mia fia la xè una putta onorata; e quella\...

DOTTORE

Il diavolo che vi porti.

PANTALONE

Che ve strascina.

DOTTORE

Vecchio senza parola e senza riputazione

(parte).

SCENA QUINTA {#Section0009.xhtml#sigil_toc_id_47}
------------

Pantalone e poi Silvio.

PANTALONE

Siestu maledetto. El xè una bestia vestio da omo costù. Gh\'oggio mai
podesto dir che quella xè una donna? Mo, sior no, nol vol lassar parlar.
Ma xè qua quel spuzzetta de so fio; m\'aspetto qualche altra insolenza.

SILVIO

(Ecco Pantalone. Mi sento tentato di cacciargli la spada nel petto).

PANTALONE

Sior Silvio, con so bona grazia, averave da darghe una bona niova, se la
se degnasse de lassarme parlar, e che no la fusse come quella masena de
molin de so sior pare.

SILVIO

Che avete a dirmi? Parlate.

PANTALONE

La sappia che el matrimonio de mia fia co sior Federigo xè andà a monte.

SILVIO

È vero? Non m\'ingannate?

PANTALONE

Ghe digo la verità, e se la xè più de quell\'umor, mia fia xè pronta a
darghe la man.

SILVIO

Oh cielo! Voi mi ritornate da morte a vita.

PANTALONE

(Via, via, nol xè tanto bestia, come so pare).

SILVIO

Ma! oh cieli! Come potrò stringere al seno colei che con un altro sposo
ha lungamente parlato?

PANTALONE

Alle curte. Federigo Rasponi xè deventà Beatrice, so sorella.

SILVIO

Come! Io non vi capisco.

PANTALONE

S\'è ben duro de legname. Quel che se credeva Federigo, s\'ha scoverto
per Beatrice.

SILVIO

Vestita da uomo?

PANTALONE

Vestia da omo.

SILVIO

Ora la capisco.

PANTALONE

Alle tante.

SILVIO

Come andò? Raccontatemi.

PANTALONE

Andemo in casa. Mia fia non sa gnente. Con un racconto solo soddisfarò
tutti do.

SILVIO

Vi seguo, e vi domando umilmente perdono, se trasportato dalla
passione\...

PANTALONE

A monte; ve compatisso. So cossa che xè amor. Andemo, fio mio, vengì con
mi

(parte).

SILVIO

Chi più felice è di me? Qual cuore può essere più contento del mio?

(parte con Pantalone).

SCENA SESTA {#Section0009.xhtml#sigil_toc_id_48}
-----------

Sala della locanda con varie porte\
Beatrice e Florindo escono ambidue dalle loro camere con un ferro alla
mano, in atto di volersi uccidere: trattenuti quella da Brighella, e
questi dal Cameriere della locanda; e s\'avanzano in modo che i due
amanti non si vedono fra di loro.

BRIGHELLA

La se fermi

(afferrando la mano a Beatrice).

BEATRICE

Lasciatemi per carità

(si sforza per liberarsi da Brighella).

CAMERIERE

Questa è una disperazione

(a Florindo, trattenendolo).

FLORINDO

Andate al diavolo

(si scioglie dal Cameriere).

BEATRICE

Non vi riuscirà d\'impedirmi

(si allontana da Brighella).\
Tutti due s\'avanzano, determinati di volersi uccidere, e vedendosi e
riconoscendosi, rimangono istupiditi.

FLORINDO

Che vedo!

BEATRICE

Florindo!

FLORINDO

Beatrice!

BEATRICE

Siete in vita?

FLORINDO

Voi pur vivete?

BEATRICE

Oh sorte!

FLORINDO

Oh anima mia! Si lasciano cadere i ferri, e si abbracciano.

BRIGHELLA

Tolè su quel sangue, che nol vada de mal

(al Cameriere scherzando, e parte).

CAMERIERE

(Almeno voglio avanzare questi coltelli. Non glieli do più)

(prende i coltelli da terra, e parte).

SCENA SETTIMA {#Section0009.xhtml#sigil_toc_id_49}
-------------

Beatrice, Florindo, poi Brighella.

FLORINDO

Qual motivo vi aveva ridotta a tale disperazione?

BEATRICE

Una falsa novella della vostra morte.

FLORINDO

Chi fu che vi fece credere la mia morte?

BEATRICE

Il mio servitore.

FLORINDO

Ed il mio parimente mi fece credere voi estinta, e trasportato da egual
dolore volea privarmi di vita.

BEATRICE

Questo libro fu cagion ch\'io gli prestai fede.

FLORINDO

Questo libro era nel mio baule. Come passò nelle vostre mani? Ah si, vi
sarà pervenuto, come nelle tasche del mio vestito ritrovai il mio
ritratto; ecco il mio ritratto, ch\'io diedi a voi in Torino.

BEATRICE

Quei ribaldi dei nostri servi, sa il cielo che cosa avranno fatto. Essi
sono stati la causa del nostro dolore e della nostra disperazione.

FLORINDO

Cento favole il mio mi ha raccontato di voi.

BEATRICE

Ed altrettante ne ho io di voi dal servo mio tollerate.

FLORINDO

E dove sono costoro?

BEATRICE

Più non si vedono.

FLORINDO

Cerchiamo di loro e confrontiamo la verità. Chi è di là? Non vi è
nessuno?

(chiama).

BRIGHELLA

La comandi.

FLORINDO

I nostri servidori dove son eglino?

BRIGHELLA

Mi no lo so, signor. I se pol cercar.

FLORINDO

Procurate di ritrovarli, e mandateli qui da noi.

BRIGHELLA

Mi no ghe ne conosso altro che uno; lo dirò ai camerieri; lori li
cognosserà tutti do. Me rallegro con lori che i abbia fatt una morte
cussi dolce; se i se volesse far seppelir, che i vada in un altro logo,
che qua no i stà ben. Servitor de lor signori

(parte).

SCENA OTTAVA {#Section0009.xhtml#sigil_toc_id_50}
------------

Florindo e Beatrice.

FLORINDO

Voi pure siete in questa locanda alloggiata?

BEATRICE

Ci sono giunta stamane.

FLORINDO

Ed io stamane ancora. E non ci siamo prima veduti?

BEATRICE

La fortuna ci ha voluto un po\' tormentare.

FLORINDO

Ditemi: Federigo, vostro fratello, è egli morto?

BEATRICE

Ne dubitate? Spirò sul colpo.

FLORINDO

Eppure mi veniva fatto credere ch\'ei fosse vivo, e in Venezia.

BEATRICE

Quest\'è un inganno di chi sinora mi ha preso per Federigo. Partii di
Turino con questi abiti e questo nome sol per seguire\...

FLORINDO

Lo so, per seguir me, o cara; una lettera, scrittavi dal vostro servitor
di Turino, mi assicurò di un tal fatto.

BEATRICE

Come giunse nelle vostre mani?

FLORINDO

Un servitore, che credo sia stato il vostro, pregò il mio che ne
ricercasse alla Posta. La vidi, e trovandola a voi diretta, non potei a
meno di non aprirla.

BEATRICE

Giustissima curiosità di un amante.

FLORINDO

Che dirà mai Turino della vostra partenza?

BEATRICE

Se tornerò colà vostra sposa, ogni discorso sarà finito.

FLORINDO

Come posso io lusingarmi di ritornarvi sì presto, se della morte di
vostro fratello sono io caricato?

BEATRICE

I capitali ch\'io porterò di Venezia, vi potranno liberare dal bando.

FLORINDO

Ma questi servi ancor non si vedono.

BEATRICE

Che mai li ha indotti a darci sì gran dolore?

FLORINDO

Per saper tutto non conviene usar con essi il rigore. Convien prenderli
colle buone.

BEATRICE

Mi sforzerò di dissimulare.

FLORINDO

Eccone uno

(vedendo venir Truffaldino).

BEATRICE

Ha cera di essere il più briccone.

FLORINDO

Credo che non diciate male.

SCENA NONA {#Section0009.xhtml#sigil_toc_id_51}
----------

Truffaldino, condotto per forza da Brighella e dal Cameriere, e detti.

FLORINDO

Vieni, vieni, non aver paura.

BEATRICE

Non ti vogliamo fare alcun male.

TRUFFALDINO

(Eh! me recordo ancora delle bastonade)

(parte).

BRIGHELLA

Questo l\'avemo trovà; se troveremo quell\'altro, lo faremo vegnir.

FLORINDO

Sì, è necessario che ci sieno tutti due in una volta.

BRIGHELLA

(Lo conosseu vu quell\'altro?)

(piano al Cameriere).

CAMERIERE

(Io no)

(a Brighella).

BRIGHELLA

(Domanderemo in cusina. Qualchedun lo cognosserà)

(al Cameriere, e parte).

CAMERIERE

(Se ci fosse, l\'avrei da conoscere ancora io)

(parte).

FLORINDO

Orsù, narraci un poco come andò la faccenda del cambio del ritratto e
del libro, e perché tanto tu che quell\'altro briccone vi uniste a farci
disperare.

TRUFFALDINO

(fa cenno col dito a tutti due che stiano cheti)

Zitto [(a tutti due).]{.testo_indicazioni_scena}

La favorissa, una parola in disparte [(a Florindo, allontanandolo da
Beatrice).]{.testo_indicazioni_scena}

(Adessadesso ghe racconterò tutto) [(a Beatrice, nell\'atto che si
scosta per parlare a Florindo).]{.testo_indicazioni_scena}

(La sappia, signor [(parla a Florindo)]{.testo_indicazioni_scena} che mi
de tutt sto negozi no ghe n\'ho colpa, ma chi è stà causa l\'è stà
Pasqual, servitor de quella signora ch\'è là [(accennando cautamente
Beatrice).]{.testo_indicazioni_scena}

Lu l\'è sta quello che ha confuso la roba, e quel che andava in t\'un
baul el l\'ha mess in quell\'alter, senza che mi me ne accorza. El
poveromo s\'ha raccomandà a mi che lo tegna coverto, acciò che el so
padron no lo cazza via, e mi che son de bon cor, che per i amici me
faria sbudellar, ho trovà tutte quelle belle invenzion per veder
d\'accomodarla. No me saria mo mai stimà, che quel ritratt fosse voster,
e che tant v\'avess da despiaser che fusse morto quel che l\'aveva.
Eccove contà l\'istoria come che l\'è, da quell\'omo sincero, da quel
servitor fedel che ve ne son).

BEATRICE

(Gran discorso lungo gli fa colui. Son curiosa di saperne il mistero).

FLORINDO

(Dunque colui che ti fece pigliar alla Posta la nota lettera, era
servitore della signora Beatrice?)

(piano a Truffaldino).

TRUFFALDINO

(Sior Sì, el giera Pasqual)

(piano a Florindo).

FLORINDO

(Perché tenermi nascosta una cosa, di cui con tanta premura ti aveva
ricercato?)

(piano a Truffaldino).

TRUFFALDINO

(El m\'aveva pregà che no lo disesse)

(piano a Florindo).

FLORINDO

(Chi?)

(come sopra).

TRUFFALDINO

(Pasqual)

(come sopra).

FLORINDO

(Perché non obbedire al tuo padrone?)

(come sopra).

TRUFFALDINO

(Per amor de Pasqual)

(come sopra).

FLORINDO

(Converrebbe che io bastonassi Pasquale e te nello stesso tempo)

come sopra).

TRUFFALDINO

(In quel caso me toccherave a mi le mie e anca quelle de Pasqual).

BEATRICE

Ê ancor finito questo lungo esame?

FLORINDO

Costui mi va dicendo\...

TRUFFALDINO

(Per amor del cielo, sior padron, no la descoverza Pasqual. Piuttosto la
diga che son stà mi, la me bastona anca, se la vol, ma no la me ruvina
Pasqual)

(piano a Florindo).

FLORINDO

(Sei così amoroso per il tuo Pasquale?)

(piano a Truffaldino).

TRUFFALDINO

(Ghe voi ben, come s el fuss me fradel. Adess voi andar da quella
signora, voi dirghe che son sta mi, che ho fallà; vai che i me grida,
che i me strapazza, ma che se salva Pasqual)

(come sopra, e si scosta da Florindo).

FLORINDO

(Costui è di un carattere molto amoroso).

TRUFFALDINO

Son qua da ella

(accostandosi a Beatrice).

BEATRICE

(Che lungo discorso hai tenuto col signor Florindo?)

(piano a Truffaldino).

TRUFFALDINO

(La sappia che quel signor el gh\'ha un servidor che gh\'ha nome
Pasqual; l\'è el più gran mamalucco del mondo; l\'è stà lu che ha fatt
quei zavai della roba, e perchè el poveromo l\'aveva paura che el so
patron lo cazzasse via, ho trovà mi quella scusa del libro, del padron
morto, nega, etecetera. E anca adess a sior Florindo gh\'ho ditt che mi
son stà causa de tutto)

(piano sempre a Beatrice).

BEATRICE

(Perchè accusarti di una colpa che asserisci di non avere?)

(a Truffaldino, come sopra).

TRUFFALDINO

(Per l\'amor che porto a Pasqual)

(come sopra).

FLORINDO

(La cosa va un poco in lungo).

TRUFFALDINO

(Cara ella, la prego, no la lo precipita)

(piano a Beatrice).

BEATRICE

(Chi?)

(come sopra).

TRUFFALDINO

(Pasqual)

(come sopra).

BEATRICE

(Pasquale e voi siete due bricconi)

(come sopra).

TRUFFALDINO

(Eh, sarò mi solo).

FLORINDO

Non cerchiamo altro, signora Beatrice, i nostri servitori non l\'hanno
fatto a malizia; meritano essere corretti, ma in grazia delle nostre
consolazioni, si può loro perdonare il trascorso.

BEATRICE

È vero, ma il vostro servitore\...

TRUFFALDINO

(Per amor del cielo, no la nomina Pasqual)

(piano a Beatrice).

BEATRICE

Orsù, io andar dovrei dal signor Pantalone de\' Bisognosi; vi sentireste
voi di venir con me?

(a Florindo).

FLORINDO

Ci verrei volentieri, ma devo attendere un banchiere a casa. Ci verrò
più tardi, se avete premura.

BEATRICE

Si, voglio andarvi subito. Vi aspetterò dal signor Pantalone; di là non
parto, se non venite.

FLORINDO

Io non so dove stia di casa.

TRUFFALDINO

Lo so mi, signor, lo compagnerò mi.

BEATRICE

Bene, vado in camera a terminar di vestirmi.

TRUFFALDINO

(La vada, che la servo subito)

(piano a Beatrice).

BEATRICE

Caro Florindo, gran pene che ho provate per voi

(entra in camera).

SCENA DECIMA {#Section0009.xhtml#sigil_toc_id_52}
------------

Florindo e Truffaldino.

FLORINDO

Le mie non sono state minori

(dietro a Beatrice).

TRUFFALDINO

La diga, sior patron, no gh\'è Pasqual; siora Beatrice no gh\'ha nissun
che l\'aiuta a vestir; se contentelo che vada mi a servirla in vece de
Pasqual?

FLORINDO

Si, vanne pure; servila con attenzione, avrò piacere.

TRUFFALDINO

(A invenzion, a prontezza, a cabale, sfido el primo sollicitador de
Palazzo)

(entra nella camera di Beatrice).

SCENA UNDICESIMA {#Section0009.xhtml#sigil_toc_id_53}
----------------

Florindo, poi Beatrice e Truffaldino.

FLORINDO

Grandi accidenti accaduti sono in questa giornata! Pianti, lamenti,
disperazioni, e all\'ultimo consolazione e allegrezza. Passar dal pianto
al riso è un dolce salto che fa scordare gli affanni, ma quando dal
piacere si passa al duolo, è più sensibile la mutazione.

BEATRICE

Eccomi lesta.

FLORINDO

Quando cambierete voi quelle vesti?

BEATRICE

Non istò bene vestita così?

FLORINDO

Non vedo l\'ora di vedervi colla gonnella e col busto. La vostra
bellezza non ha da essere soverchiamente coperta.

BEATRICE

Orsù, vi aspetto dal signor Pantalone; fatevi accompagnare da
Truffaldino.

FLORINDO

L\'attendo ancora un poco; e se il banchiere non viene, ritornerà
un\'altra volta.

BEATRICE

Mostratemi l\'amor vostro nella vostra sollecitudine

(s\'avvia per partire).

TRUFFALDINO

(Comandela che resta a servir sto signor?)

(piano a Beatrice, accennando Florindo).

BEATRICE

(Si, lo accompagnerai dal signor Pantalone)

(a Truffaldino).

TRUFFALDINO

(E da quella strada lo servirò, perché no gh\'è Pasqual)

(come sopra).

BEATRICE

Servilo, mi farai cosa grata. (Lo amo più di me stessa)

(parte).

SCENA DODICESIMA {#Section0009.xhtml#sigil_toc_id_54}
----------------

Florindo e Truffaldino.

TRUFFALDINO

Tolì, nol se vede. El padron se veste, el va fora de casa, e nol se
vede.

FLORINDO

Di chi parli?

TRUFFALDINO

De Pasqual. Ghe voio ben, l\'è me amigo, ma l\'è un poltron. Mi son un
servitor che valo per do.

FLORINDO

Vienmi a vestire. Frattanto verrà il banchiere.

TRUFFALDINO

Sior padron, sento che vussioria ha d\'andar in casa de sior Pantalon.

FLORINDO

Ebbene, che vorresti tu dire?

TRUFFALDINO

Vorria pregarlo de una grazia.

FLORINDO

Sì, te lo meriti davvero per i tuoi buoni portamenti.

TRUFFALDINO

Se è nato qualcossa, la sa che l\'è stà Pasqual.

FLORINDO

Ma dov\'è questo maledetto Pasquale? Non si può vedere?

TRUFFALDINO

El vegnirà sto baron. E cusì, sior padron, voria domandarghe sta grazia.

FLORINDO

Che cosa vuoi?

TRUFFALDINO

Anca mi, poverin, son innamorado.

FLORINDO

Sei innamorato?

TRUFFALDINO

Signor sì; e la me morosa l\'è la serva de sior Pantalon; e voria mo che
vussioria\...

FLORINDO

Come c\'entro io?

TRUFFALDINO

Oh, no digo che la ghe intra; ma essendo mi el so servitor, che la
disess una parola per mi al sior Pantalon.

FLORINDO

Bisogna vedere se la ragazza ti vuole.

TRUFFALDINO

La ragazza me vol. Basta una parola al sior Pantalon; la prego de sta
carità.

FLORINDO

Si, lo farò; ma come la manterrai la moglie?

TRUFFALDINO

Farò quel che poderò. Me raccomanderò a Pasqual.

FLORINDO

Raccomandati a un poco più di giudizio

(entra in camera).

TRUFFALDINO

Se non fazzo giudizio sta volta, no lo fazzo mai più

(entra in camera, dietro a Florindo).

SCENA TREDICESIMA {#Section0009.xhtml#sigil_toc_id_55}
-----------------

Camera in casa di Pantalone\
Pantalone, il Dottore, Clarice, Silvio, Smeraldina.

PANTALONE

Via, Clarice, non esser cusì ustinada. Ti vedi che l\'è pentio sior
Silvio, che el te domanda perdon; se l\'ha dà in qualche debolezza, el
l\'ha fatto per amor; anca mi gh\'ho perdonà i strambezzi, ti ghe li ha
da perdonar anca ti.

SILVIO

Misurate dalla vostra pena la mia, signora Clarice, e tanto più
assicuratevi che vi amo davvero, quanto più il timore di perdervi mi
aveva reso furioso. Il Cielo ci vuol felici, non vi rendete ingrata alle
beneficenze del Cielo. Coll\'immagine della vendetta non funestate il
più bel giorno di nostra vita.

DOTTORE

Alle preghiere di mio figliuolo aggiungo le mie. Signora Clarice, mia
cara nuora, compatitelo il poverino; è stato lì lì per diventar pazzo.

SMERALDINA

Via, signora padrona, che cosa volete fare? Gli uomini, poco più, poco
meno, con noi sono tutti crudeli. Pretendono un\'esattissima fedeltà, e
per ogni leggiero sospetto ci strapazzano, ci maltrattano, ci vorrebbero
veder morire. Già con uno o con l\'altro avete da maritarvi; dirò, come
si dice agli ammalati, giacché avete da prender la medicina, prendetela.

PANTALONE

Via, sentistu? Smeraldina al matrimonio la ghe dise medicamento. No far
che el te para tossego. (Bisogna veder de devertirla)

(piano al Dottore).

DOTTORE

Non è ne veleno, né medicamento, no. Il matrimonio è una confezione, un
giulebbe, un candito.

SILVIO

Ma, cara Clarice mia, possibile che un accento non abbia a uscire dalle
vostre labbra? So che merito da voi essere punito, ma per pietà,
punitemi colle vostre parole, non con il vostro silenzio. Eccomi ai
vostri piedi; movetevi a compassione di me

(s\'inginocchia).

CLARICE

Crudele!

(sospirando verso Silvio).

PANTALONE

(Aveu sentio quella sospiradina? Bon segno)

(piano al Dottore).

DOTTORE

(Incalza l\'argomento)

(piano a Silvio).

SMERALDINA

(Il sospiro è come il lampo: foriero di pioggia).

SILVIO

Se credessi che pretendeste il mio sangue in vendetta della supposta mia
crudeltà, ve lo esibisco di buon animo. Ma oh Dio! in luogo del sangue
delle mie vene, prendetevi quello che mi sgorga dagli occhi

(piange).

PANTALONE

(Bravo!).

CLARICE

Crudele!

(come sopra, e con maggior tenerezza).

DOTTORE

(È cotta)

(piano a Pantalone).

PANTALONE

Animo, leveve su

(a Silvio, alzandolo).

Vegni qua

(al medesimo, prendendolo per la mano).

Vegni qua anca vu, siora

(prende la mano di Clarice).

Animo, torneve a toccar la man; fe pase, no pianzè più, consoleve,
fenila, tolè; el cielo ve benediga

(unisce le mani d\'ambidue).

DOTTORE

Via, è fatta.

SMERALDINA

Fatta, fatta.

SILVIO

Deh, signora Clarice, per carità

(tenendola per la mano).

CLARICE

Ingrato!

SILVIO

Cara.

CLARICE

Inumano!

SILVIO

Anima mia.

CLARICE

Cane!

SILVIO

Viscere mie.

CLARICE

Ah!

(sospira).

PANTALONE

(La va).

SILVIO

Perdonatemi, per amor del cielo.

CLARICE

Ah! vi ho perdonato

(sospirando).

PANTALONE

(La xè andada).

DOTTORE

Via, Silvio, ti ha perdonato.

SMERALDINA

L\'ammalato è disposto, dategli il medicamento.

SCENA QUATTORDICESIMA {#Section0009.xhtml#sigil_toc_id_56}
---------------------

Brighella e detti.

BRIGHELLA

Con bona grazia, se pol vegnir?

(entra).

PANTALONE

Vegni qua mo, sior compare Brighella. Vu sè quello che m\'ha dà da
intender ste belle fandonie, che m\'ha assicurà che sior Federigo gera
quello, ah?

BRIGHELLA

Caro signor, chi non s\'averave ingannà? I era do fradelli che se
somegiava come un pomo spartido. Con quei abiti averia zogà la testa che
el giera lu.

PANTALONE

Basta; la xè passada. Cossa gh\'è da niovo?

BRIGHELLA

La signora Beatrice l\'è qua, che la li vorria reverir.

PANTALONE

Che la vegna pur, che la xè parona.

CLARICE

Povera signora Beatrice, mi consolo che sia in buono stato.

SILVIO

Avete compassione di lei?

CLARICE

Si, moltissima.

SILVIO

E di me?

CLARICE

Ah crudele!

PANTALONE

Sentiu che parole amorose?

(al Dottore).

DOTTORE

Mio figliuolo poi ha maniera

(a Pantalone).

PANTALONE

Mia fia, poverazza, la xè de bon cuor

(al Dottore).

SMERALDINA

(Eh, tutti due sanno fare la loro parte).

SCENA QUINDICESIMA {#Section0009.xhtml#sigil_toc_id_57}
------------------

Beatrice e detti.

BEATRICE

Signori, eccomi qui a chiedervi scusa, a domandarvi perdono, se per
cagione mia aveste dei disturbi\...

CLARICE

Niente, amica, venite qui

(l\'abbraccia).

SILVIO

Ehi?

(mostrando dispiacere di quell\'abbraccio).

BEATRICE

Come! Nemmeno una donna?

(verso Silvio).

SILVIO

(Quegli abiti ancora mi fanno specie).

PANTALONE

Andè là, siora Beatrice, che per esser donna e per esser zovene, gh\'avè
un bel coraggio.

DOTTORE

Troppo spirito, padrona mia

(a Beatrice).

BEATRICE

Amore fa fare delle gran cose.

PANTALONE

I s\'ha trovà, né vero, col so moroso? Me xè stà conta.

BEATRICE

Si, il cielo mi ha consolata.

DOTTORE

Bella riputazione!

(a Beatrice).

BEATRICE

Signore, voi non c\'entrate nei fatti miei

(al Dottore).

SILVIO

Caro signor padre, lasciate che tutti facciano il fatto loro non vi
prendete di tai fastidi. Ora che sono contento io, vorrei che tutto il
mondo godesse. Vi sono altri matrimoni da fare? Si facciano.

SMERALDINA

Ehi, signore, vi sarebbe il mio

(a Silvio).

SILVIO

Con chi?

SMERALDINA

Col primo che viene.

SILVIO

Trovalo, e son qua io.

CLARICE

Voi? Per far che?

(a Silvio).

SILVIO

Per un poco di dote.

CLARICE

Non vi è bisogno di voi.

SMERALDINA

(Ha paura che glielo mangino. Ci ha preso gusto).

SCENA SEDICESIMA {#Section0009.xhtml#sigil_toc_id_58}
----------------

Truffaldino e detti.

TRUFFALDINO

Fazz reverenza a sti signori.

BEATRICE

Il signor Florindo dov\'è?

(a Truffaldino).

TRUFFALDINO

L\'è qua, che el voria vegnir avanti, se i se contenta.

BEATRICE

Vi contentate, signor Pantalone, che passi il signor Florindo?

PANTALONE

Xèlo l\'amigo sì fatto?

(a Beatrice).

BEATRICE

Sì, il mio sposo.

PANTALONE

Che el resta servido.

BEATRICE

Fa che passi

(a Truffaldino).

TRUFFALDINO

Zovenotta, ve reverisso

(a Smeraldina, piano).

SMERALDINA

Addio, morettino

(piano a Truffaldino).

TRUFFALDINO

Parleremo

(come sopra).

SMERALDINA

Di che?

(come sopra).

TRUFFALDINO

Se volessi

(fa cenno di darle l\'anello, come sopra).

SMERALDINA

Perchè no?

(come sopra).

TRUFFALDINO

Parleremo

(come sopra, e parte).

SMERALDINA

Signora padrona, con licenza di questi signori, vorrei pregarla di una
carità

(a Clarice).

CLARICE

Che cosa vuoi?

(tirandosi in disparte per ascoltarla).

SMERALDINA

(Anch\'io sono una povera giovine, che cerco di collocarmi: vi è il
servitore della signora Beatrice che mi vorrebbe; s\'ella dicesse una
parola alla sua padrona, che si contentasse ch\'ei mi prendesse,
spererei di fare la mia fortuna)

(piano a Clarice).

CLARICE

(Sì, cara Smeraldina, lo farò volentieri: subito che potrò parlare a
Beatrice con libertà, lo farò certamente)

(torna al suo posto).

PANTALONE

Cossa xè sti gran secreti

(a Clarice).

CLARICE

Niente, signore. Mi diceva una cosa.

SILVIO

(Posso saperla io?)

(piano a Clarice).

CLARICE

(Gran curiosità! E poi diranno di noi altre donne).

SCENA ULTIMA {#Section0009.xhtml#sigil_toc_id_59}
------------

Florindo, Truffaldino e detti.

FLORINDO

Servitor umilissimo di lor signori.

(Tutti lo salutano).

È ella il padrone di casa?

(a Pantalone).

PANTALONE

Per servirla.

FLORINDO

Permetta ch\'io abbia l\'onore di dedicarle la mia servitù, scortato a
farlo dalla signora Beatrice di cui, siccome di me, note gli saranno le
vicende passate.

PANTALONE

Me consolo de conoscerla e de reverirla, e me consolo de cuor delle so
contentezze.

FLORINDO

La signora Beatrice deve esser mia sposa, e se voi non isdegnate
onorarci, sarete pronubo delle nostre nozze.

PANTALONE

Quel che s\'ha da far, che el se fazza subito. Le se daga la man.

FLORINDO

Son pronto, signora Beatrice.

BEATRICE

Eccola, signor Florindo.

SMERALDINA

(Eh, non si fanno pregare).

PANTALONE

Faremo po el saldo dei nostri conti. Le giusta le so partie, che po
giusteremo le nostre.

CLARICE

Amica, me ne consolo

(a Beatrice).

BEATRICE

Ed io di cuore con voi

(a Clarice).

SILVIO

Signore, mi riconoscete voi?

(a Florindo).

FLORINDO

Si, Vi riconosco; siete quello che voleva fare un duello.

SILVIO

Anzi l\'ho fatto per mio malanno. Ecco chi mi ha disarmato e poco meno
che ucciso

(accennando Beatrice).

BEATRICE

Potete dire chi vi ha donato la vita

(a Silvio).

SILVIO

Si, è vero.

CLARICE

In grazia mia però

(a Silvio).

SILVIO

È verissimo.

PANTALONE

Tutto xè giustà, tutto xè fenio.

TRUFFALDINO

Manca el meggio, signori.

PANTALONE

Cossa manca?

TRUFFALDINO

Con so bona grazia, una parola

(a Florindo, tirandolo in disparte).

FLORINDO

(Che cosa vuoi?)

(piano a Truffaldino).

TRUFFALDINO

(S\'arrecordel cossa ch\'el m\'ha promesso?)

(piano a Florindo).

FLORINDO

(Che cosa? Io non me ne ricordo)

(piano a Truffaldino).

TRUFFALDINO

(De domandar a sior Pantalon Smeraldina per me muier?)

(come sopra).

FLORINDO

(Sì, ora me ne sovviene. Lo faccio subito)

(come sopra).

TRUFFALDINO

(Anca mi, poveromo, che me metta all\'onor del mondo).

FLORINDO

Signor Pantalone, benché sia questa la prima volta sola ch\'io abbia
l\'onore di conoscervi, mi fo ardito di domandarvi una grazia.

PANTALONE

La comandi pur. In quel che posso, la servirò.

FLORINDO

Il mio servitore bramerebbe per moglie la vostra cameriera; avreste voi
difficoltà di accordargliela?

SMERALDINA

(Oh bella! Un altro che mi vuole. Chi diavolo è? Almeno che lo
conoscessi).

PANTALONE

Per mi son contento. Cossa disela ella, patrona?

(a Smeraldina).

SMERALDINA

Se potessi credere d\'avere a star bene\...

PANTALONE

Xèlo omo da qualcossa sto so servitor?

(a Florindo).

FLORINDO

Per quel poco tempo ch\'io l\'ho meco, è fidato certo, e mi pare di
abilita.

CLARICE

Signor Florindo, voi mi avete prevenuta in una cosa che dovevo far io.
Dovevo io proporre le nozze della mia cameriera per il servitore della
signora Beatrice. Voi l\'avete chiesta per il vostro; non occorr\'altro.

FLORINDO

No, no; quando voi avete questa premura, mi ritiro affatto e vi lascio
in pienissima libertà.

CLARICE

Non sarà mai vero che voglia io permettere che le mie premure sieno
preferite alle vostre. E poi non ho, per dirvela, certo impegno.
Proseguite pure nel vostro.

FLORINDO

Voi lo fate per complimento. Signor Pantalone, quel che ho detto, sia
per non detto. Per il mio servitore non vi parlo più, anzi non voglio
che la sposi assolutamente.

CLARICE

Se non la sposa il vostro, non l\'ha da sposare nemmeno quell\'altro. La
cosa ha da essere per lo meno del pari.

TRUFFALDINO

(Oh bella! Lori fa i complimenti, e mi resto senza muier).

SMERALDINA

(Sto a vedere che di due non ne avrò nessuno).

PANTALONE

Eh via, che i se giusta; sta povera putta gh\'ha voggia de maridarse,
dèmola o all\'uno, o all\'altro.

FLORINDO

Al mio no. Non voglio certo far torto alla signora Clarice.

CLARICE

Né io permetterò mai che sia fatto al signor Florindo.

TRUFFALDINO

Siori, sta faccenda l\'aggiusterò mi. Sior Florindo, non ala domandà
Smeraldina per el so servitor?

FLORINDO

Sì, non l\'hai sentito tu stesso?

TRUFFALDINO

E ella, siora Clarice, non àla destinà Smeraldina per el servidor de
siora Beatrice?

CLARICE

Dovevo parlarne sicuramente.

TRUFFALDINO

Ben, co l\'è cusì, Smeraldina, deme la man.

PANTALONE

Mo per cossa voleu che a vu la ve daga la man?

(a Truffaldino).

TRUFFALDINO

Perché mi, mi son servitor de sior Florindo e de siora Beatrice.

FLORINDO

Come?

BEATRICE

Che dici?

TRUFFALDINO

Un pochetto de flemma. Sior Florindo, chi v\'ha pregado de domandar
Smeraldina al sior Pantalon?

FLORINDO

Tu mi hai pregato.

TRUFFALDINO

E ella, siora Clarice, de chi intendevela che l\'avesse da esser
Smeraldina?

CLARICE

Di te.

TRUFFALDINO

Ergo Smeraldina l\'è mia.

FLORINDO

Signora Beatrice, il vostro servitore dov\'è?

BEATRICE

Eccolo qui. Non è Truffaldino?

FLORINDO

Truffaldino? Questi è il mio servitore.

BEATRICE

Il vostro non è Pasquale?

FLORINDO

Pasquale? Doveva essere il vostro.

BEATRICE

Come va la faccenda?

(verso Truffaldino).\
(Truffaldino con lazzi muti domanda scusa).

FLORINDO

Ah briccone!

BEATRICE

Ah galeotto!

FLORINDO

Tu hai servito due padroni nel medesimo tempo?

TRUFFALDINO

Sior si, mi ho fatto sta bravura. Son intrà in sto impegno senza
pensarghe; m\'ho volesto provar. Ho durà poco, è vero, ma almanco ho la
gloria che nissun m\'aveva ancora scoverto, se da per mi no me
descovriva per l\'amor de quella ragazza. Ho fatto una gran fadiga, ho
fatto anca dei mancamenti, ma spero che, per rason della stravaganza,
tutti sti siori me perdonerà.

Fine della commedia.
