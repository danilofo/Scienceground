#! /bin/bash


# Download the texts
curl https://www.liberliber.it/mediateca/libri/m/manzoni/i_promessi_sposi/epub/manzoni_i_promessi_sposi.epub -o i_promessi_sposi.epub
curl https://www.liberliber.it/mediateca/libri/m/manzoni/storia_della_colonna_infame/epub/manzoni_storia_della_colonna_infame.epub -o storia_della_colonna_infame.epub
curl https://www.liberliber.it/mediateca/libri/v/verga/tutte_le_novelle/epub/verga_tutte_le_novelle.epub -o tutte_le_novelle.epub
curl https://www.liberliber.it/mediateca/libri/v/verga/i_malavoglia/epub/verga_i_malavoglia.epub -o i_malavoglia.epub
curl https://www.liberliber.it/mediateca/libri/g/goldoni/la_locandiera/epub/goldoni_la_locandiera.epub -o la_locandiera.epub
curl https://www.liberliber.it/mediateca/libri/g/goldoni/il_servitore_di_due_padroni/epub/goldoni_il_servitore_di_due_padroni.epub -o il_servitore_di_due_padroni.epub

# Convert to Markdown
FILE_LIST="il_servitore_di_due_padroni la_locandiera i_malavoglia tutte_le_novelle storia_della_colonna_infame i_promessi_sposi"

for file in ${FILE_LIST}; do
    echo "Converting file ${file}.epub to markdown..."
    pandoc ${file}.epub -f epub -t markdown -o ${file}.md
    echo "Done"
done

# Remove unused epub
rm *.epub

#
#tail -n 2000 giulio_cesare.md | head -n 700 > gc_sample.md


# Concatenate
echo "Generating the samples"
cat i_promessi_sposi.md storia_della_colonna_infame.md> ps+sdci.md
cat tutte_le_novelle.md storia_della_colonna_infame.md > tln+sdci.md
cat i_promessi_sposi.md i_malavoglia.md > ps+m.md
cat tutte_le_novelle.md i_malavoglia.md > tln+m.md
cat la_locandiera.md i_malavoglia.md > ll+m.md
cat la_locandiera.md il_servitore_di_due_padroni.md > ll+sddp.md

# gzip all
echo "Applying the zip algorithm"
gzip *.md --verbose --keep



