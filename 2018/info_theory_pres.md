---
title: L'Informazione è fisica o la Fisica è un'opinione?
author: Danilo Forastiere & Emanuele Penocchio
---
# Che cos'è l'informazione?

## Un messaggio segreto?

aq xlohqclr, lq dssdwhqcd ajadoh d aq doywr xlohqclr, srywheeh hxswlphwh fhqyr lqyhqclrql glbhwxh; dqfkh aq ilxfklr, g’doywrqgh; sdwodwxl ydfhqgr, r ilxfkldqgr, è xhpswh srxxleloh; lo swreohpd è fdslwxl. rssawh qhxxaqr saò fdslwh qhxxaqr: rjql phwor fwhgh g’dbhw phxxr qho ilxfklr aq xljqlilfdyr irqgdphqydoh shw oal, pd fkh xror oal lqyhqgh; o’doywr jol wledyyh tadofrxd fkh qrq kd qhxxaqd whodclrqh frq tahoor fkh oal kd ghyyr; è aq gldorjr ywd xrwgl, aqd frqbhwxdclrqh xhqcd fdsr qè frgd.

## Analisi della frequenza

<img src="images/palomar.png" width=100%>

## Perché leggere i classici?

<img src="images/promessi_sposi_hist.png" width=100%> 

## Istogrammi a confronto

<img src="images/palomar.png" width=45%> <img src="images/promessi_sposi_hist.png" width=45%> 

## _Palomar_, Italo Calvino

Un silenzio, in apparenza uguale a un altro silenzio, potrebbe esprimere cento intenzioni diverse; anche un fischio, d’altronde; parlarsi tacendo, o fischiando, è sempre possibile; il problema è capirsi. Oppure nessuno può capire nessuno: ogni merlo crede d’aver messo nel fischio un significato fondamentale per lui, ma che solo lui intende; l’altro gli ribatte qualcosa che non ha nessuna relazione con quello che lui ha detto; è un dialogo tra sordi, una conversazione senza capo nè coda.

## Gli ingredienti del ragionamento
- Il dado (la distribuzione uniforme): ogni lancio dà la stessa informazione
- L'istogramma: non tutti i simboli danno la stessa informazione perché non appaiono con la stessa probabilità

## La teoria dell'informazione 
<img src="images/ClaudeShannon.jpg" width=40% align="left">

C. E. Shannon, _A Mathematical Theory of Communication_, 1948.

Entropia di Shannon:
$$ H = - \sum_n p_n \, \log_2 p_n $$

È una misura della **sorpresa** che un osservatore prova quando legge un certo simbolo nella sequenza.

## L'unità minima dell'informazione

Un bit è l'entropia di Shannon di un messaggio che può avere solo due valori (0 e 1 per esempio), con la stessa probabilità.
$$ H(bit) = - 2 \times 1/2 \, \log_2 \, 1/2 = 1 $$
Bit deriva da "binary digit", che è l'unità fisica di base per rappresentare l'informazione nel mondo digitale.

## Entropia e informazione

In modo equivalente, l'entropia di Shannon è il numero medio di domande binarie necessario per identificare un simbolo se questo può apparire con diverse probabilità.

##
$$ P(a) = 1/2$$
$$ P(b) = 1/4$$
$$ P(c) = 1/8$$
$$ P(d) = 1/8$$

$$H = 1 \times  1/2 + 2 \times 1/4 + 3 \times 1/8 = 7/4 $$

## Codici e compressione delle informazioni

L'ultima definizione che abbiamo dato significa che un messaggio con entropia H si può rappresentare con $$ 2^H $$ _binary digits_. Questa idea è alla base di tutti i codici di compressione dell'informazione.

## Ridondanza
<img src="images/foga-maxw.jpg" width=75%>


## Algoritmi di compressione
Lempel-Welsh-Ziv (1958): Usato da `gzip` e altri software per comprimere i file 

Sviluppi "recenti": Uso degli algoritmi di compressione per attribuire un testo ad un autore
(Benedetto et al. PRL 2002)


# Message in a (lab) bottle

## DNA 
<img src="images/dna.svg" width=75%>

## Come sono fatte le "parole"?

## Mutua informazione

$$ I(k) = H(X_0) + H(X_k) - H(X_0, X_k)$$

Se I(k) = 0, la conoscenza di uno dei due simboli non aggiunge nulla sull'altro.

## Mutua informazione della sequenza del DNA
<img src="images/dna_mutual_info.png" width=70% align=left> Grosse et al., PRE 2002

# Il costo termodinamico della conoscenza

## Il secondo principio della termodinamica

$$ \Sigma = \Delta S + Q / T \geq 0 $$

## Il principio di Landauer
 
Identificando l'entropia di Shannon con quella termodinamica, otteniamo che "cancellare" un bit di informazione (in unità fisiche) significa che $$ \Delta S = -k_b \ln 2 $$
Il calore dissipato nel processo deve essere $$ Q \geq k_b T \, \ln 2 $$


## Problema?
L'entropia di Shannon dipende dall'osservatore, quella della teoria termodinamica classica no...

# Grazie per l'attenzione! Domande?

## Per approfondire
Testi divulgativi
---
- Seth Lloyd, _Il programma dell'universo_, Einaudi 2006
- Erwin Schroedinger, _Che cos'è la vita_, Adelphi 1995
- James Stone, _Information Theory, A Tutorial Introduction_, Sebtel Press 2015
- Massimiliano Esposito, _Landauer Principle Stands up to Quantum Test_, Physics 21 May 2018

Articoli scientifici citati
---
- C. E. Shannon, _A Mathematical Theory of Communication_, Bell System Technical Journal 1948
- Grosse, Herzel, Buldyrev, Stanley, _Species independence of mutual information in coding and noncoding DNA_, Physical Review E 2000
- Benedetto Caglioti Loreto _Language Trees and Zipping_, Physical Review Letters 2002

## Contatti

- Danilo Forastiere: danilo.forastiere@uni.lu
- Emanuele Penocchio: emanuele.penocchio@uni.lu 
