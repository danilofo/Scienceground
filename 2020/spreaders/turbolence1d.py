# simple 1d model of turbolence to explore the difference with a simple thermal bath

import numpy as np
from numpy.fft import fft, ifft, fftshift, ifftshift
from numpy.random import randn

# noise generation

## amplitude SD for various correlated
def gaussian_filter(f, a = 100):
    # squared FT is exp(-1/2 a * x**2) 
    return np.sqrt(np.sqrt(1/ (2 * a)) * np.exp(-1/2 * f**2 / a))

gaussian_filter = np.vectorize(gaussian_filter)


def exp_filter(f, a = 10):
    # squared FT is exp(-a|x|)
    return np.sqrt(np.sqrt(2 * a/ np.pi) / (a**2 + f**2))

exp_filter = np.vectorize(exp_filter)

def pink_filter(f, a=100):
    freq = np.abs(f)
    return 1 / np.sqrt(1 + a * freq)

pinkNoiseFilter = np.vectorize(pink_filter)

## correlated noise generators

def corr_noise(L, eps, type='exp'):
    ''' 
    spectral generator based on fft, taken from:
    kmdouglass.github.io/posts/correlated-noise-and-the-fft/
    
    Notes
    -----
    
    Normalization is computed such that 

    Parameters
    ----------
    eps: sampling period
    L: physical length of the grid
    '''
    sampling_f = 1 / eps
    df = 1 / L # minimum resolved freq
    f = np.linspace(-sampling_f/2, sampling_f/2, num=L, endpoint=False)
    norm = 2 * np.pi  / (np.sqrt(df)) # TODO: missing 1/eps, compute correct N
    
    filter = globals()[type + '_filter'] # get a method from string
    shifted_ampl_spectrum = fftshift(filter(f))
    freq_noise = randn(f.size) + 1j * randn(f.size)
    return np.real(ifftshift(ifft(freq_noise * shifted_ampl_spectrum * norm)))


def random_field(sites, C='delta', eps=0):
    '''
    generate a 1d velocity field
    N sites at distance eps
    '''
    u = []
    available_correlation = ['delta', 'exp', 'pink', 'gaussian']
    
    if C in available_correlation:
        if C == 'delta':
            u = randn(sites) # std gaussian vector of shape (sites,)
        else:
            u = corr_noise(sites, eps, type=C)
    else:
        print('Correlation function not yet implemented')
    return u

# integrators

def euler_step(x, v, u, dt, tau, eps, L):
    '''
    Euler algorithm, 1o algorithm, not symplectic
    v vector of tracers' speed
    u fluid velocity field
    tau inverse mobility
    x position of the tracers (discretized)
    eps lattice spacing
    '''
    nT = len(v)
    dx = [ int((dt * v[i]) / eps) for i in range(nT)]
    for i in range(nT):
        x[i] = np.mod(x[i] + dx[i], L)
    dv = [ dt * (1 / tau) * (u[x[i]] -  v[i]) for i in range(nT)]
    for i in range(nT):
        v[i] = v[i] + dv[i]
    return x, v

def leapfrog_step(x, v, u, dt, tau, eps, L):
    ''' 
    A symplectic, angular-momentum-conserving 2o algorithm for 2o ODEs,
    similar to midpoint for acceleration.
    '''
    nT = len(v)
    # current accelerations
    a = [ u[x[i]] - (1 / tau) * v[i] for i in range(nT)]
    # update positions using current velocity and acceleration (discretized)
    dx = [int( (v[i] * dt + 1./2. * a[i] * dt**2) / eps) for i in range(nT)]
    for i in range(nT):
        x[i] = np.mod(x[i] + dx[i], L)
    # new accelerations
    anew = [ (1 / tau) * (u[x[i]] - v[i]) for i in range(nT)]
    # compute new velocities
    for i in range(nT):
        v[i] = v[i] + 1./2. * (a[i] + anew[i]) * dt
    return x, v
