---
title: Microbi e dati
---

[danilo]: # (L'idea è di raccogliere in una presentazione immagini e citazioni da includere, e portare avanti il discorso a voce, usando magari degli appunti personali)
 
## Cosa c'entra l'influenza con le statistiche di Wikipedia?
### Seguendo le tracce batteriche
Cosa rivelano le ricerche degli utenti sul loro stato di salute

### Predire il presente: nowcasting

### 
![](./images/journal.pcbi.1003892.png)
(da Plos CompBio, Nicholas Generous et al. 2014 )

### Letture in rete
* Historical predictions of GFT https://ai.googleblog.com/2015/08/the-next-chapter-for-flu-trends.html
* What can we learn from Google Flu Trend epic fail? https://www.wired.com/2015/10/can-learn-epic-failure-google-flu-trends/

## Serve davvero un bravo medico per una diagnosi accurata?
### DeepMind: Health

### Resistenze antibiotiche e machine learning
Ultima risorsa o mito positivista?

## Come si una calcolatrice con E. Coli? 

### Bacterial computing

## Fin qui tutto bene?

### Fairness e salute pubblica
![](./images/robespierre.jpeg)

### Sicurezza nel trattamento de dati biomedici
* Rischi dell'anonimizzazione dei dati
* Società del controllo e identificazione biometrica

## Consigli di lettura

### Libri sull'intelligenza artificiale
* _Computer e cervello_, John Von Neumann
* _Macchine che pensano_, a cura di Douglas Heaven, Edizioni Dedalo 2018

### Articoli su rivista
The Lab's Quarterly, 2018,  n. 4 (ottobre-dicembre):

* Mattia Galeotti, _Discriminazione e algoritmi. Incontri e scontri tra diverse idee di fairness_
* Chiara Visentin, _Il potere razionale degli algoritmi tra burocrazia e nuovi idealtipi_
