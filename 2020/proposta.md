#Scienceground 1.666...

È solo una bozza.
**In particolare in grassetto le parti da elaborare (tra parentesi proposte assolutamente non vincolanti di persone che potrebbero farlo)**

##Motivazione

Anche quest'anno, la _Piccola Comunità Scientifica ExTemporanea_ propone a Festivaletteratura il progetto _Scienceground_, giunto alla sua quarta edizione -- la 1.6666...!

Partiti proprio dal Festival, siamo ora un gruppo costituito perlopiù da studenti e ricercatori universitari sparpagliati per l’Italia e l’Europa.
Si tratta di un gruppo eterogeneo, inclusivo ed in costante mutazione, mosso da interessi comuni e da forti legami personali e professionali, oltre che dall'affetto per Mantova e Festivaletteratura.

Il nostro gruppo è animato dal desiderio e dall’urgenza di offrire un’alternativa alla comunicazione scientifica di massa, sempre più volta alla narrazione metaforica e alla spettacolarizzazione. 
Pensiamo infatti che per dare alla scienza il ruolo che merita nello sviluppo della società moderna ci sia bisogno di comunicarla in modo appropriato, senza cercare per forza il sensazionalismo.

Siamo inoltre convinti che attraverso la sperimentazione di nuove forme e spazi di dialogo, l’attenzione ai contenuti e la passione e l’impegno individuali sia possibile contrastare la moderna opposizione tra scienza/tecnologia e società/cultura. 
La visione positivista della scienza come portatrice di verità assoluta e fredda razionalità, oltre che essere parecchio opinabile, a lungo andare è deleteria per l’immagine stessa della scienza. 
Una narrazione appiattita offre il fianco a tifoserie, radicalizzazioni e strumentalizzazioni sia da parte di chi sostiene ciecamente un “futuro” a trazione scientifico/tecnologica, sia da parte di chi pensa di poter tornare ad un “passato” che non sarà più. 
Queste dinamiche inevitabilmente incrinano l’immagine della scienza agli occhi dell’opinione pubblica mettendone a rischio gli antichi valori comunitari incarnati dal metodo scientifico, che non è un criterio di certezza, ma al contrario l’esercizio sistematico del dubbio.

Restituire spessore e complessità al racconto scientifico non è quindi solo un modo per evolverci come individui, imparando a riflettere in modo critico, ma in ultima istanza diventa un modo -- forse l’unico? -- per salvaguardare la scienza.
Per dirlo con le parole usate da Bruno Latour in un’intervista che ci ha rilasciato durante l'edizione del 2018 di Scienceground: “_It’s not science communication, It’s science defence_”.

In occasione di Festivaletteratura le nostre attività confluiscono in un momento di incontro e di restituzione pubblica dei percorsi di autoformazione, in cui la comunità offre gli strumenti di analisi critica che ha appreso e si apre all’ascolto delle preoccupazioni delle persone sul rapporto tra scienza e società.
L’obiettivo di Scienceground non è né di intrattenere né di sensibilizzare, ma di dar vita ad un dibattito pubblico che problematizzi la scienza mostrando la complessità intrinseca del processo scientifico, senza far ricorso a verità assolute o autoevidenti.
La sfida è partire da una fruizione libera e conviviale di contenuti avanzati, per stimolare riflessioni trasversali su scienza, società, politica, etica, cultura, nella convinzione che esista una sottilissima rete di connessioni tra tutti questi aspetti della convivenza civile. 
L’esperienza di Scienceground vuole fornire strumenti critici per aiutare a dipanare la matassa, rivelando le profonde relazioni tra l’incedere scientifico – con le sue regole, i suoi miti e le sue controversie – ed il contesto sociale in cui si la scienza si muove.
È lo stesso spirito con cui Latour, nel suo libro I Microbi, descrive le imprese di laboratorio dello scienziato Louis Pasteur: studiandone i progressi scientifici ci si ritrova inevitabilmente nel mondo degli uomini, in quel caso la società francese del XIX secolo. 
Ecco allora che il formato di Scienceground si articola intorno alla scelta di una singola parola chiave (bit, dati, microbi…) da cui prendere il via per sviluppare un dialogo che si ramifica in mille direzioni, senza l’ambizione di giungere a risposte definitive.
In questo senso la collocazione all’interno di un festival letterario acquista un significato e una valenza ben precisi.

Festivaletteratura ha giocato un ruolo chiave nella nascita del progetto, dapprima come centro gravitazionale attorno cui le persone di questa piccola comunità scientifica hanno orbitato per diverso tempo (anche molti anni); poi come terreno fertile e contaminato per sperimentare nuove forme creative di conversazione scientifica: la mostra, la biblioteca, i gruppi di lettura, i laboratori, il podcast, le discussioni alla lavagna.
In questo, Festivaletteratura si è dimostrato un fondamentale catalizzatore di idee dove un dialogo aperto e non-mediato può ancora svilupparsi, più di quanto non sia oggi possibile in ambienti accademici o attraverso canali tradizionali di comunicazione mediatica.

##Proposta

Gli obiettivi di Scienceground 1.6666 continuano quelli sviluppati nelle scorse edizioni: raccontare e problematizzare la scienza nella società e il sociale nella scienza, a partire dalla letteratura (scientifica e di divulgazione, ma non solo); migliorare la qualità del discorso pubblico su scienza e tecnologia portando un pubblico variegato a sperimentare i meccanismi con cui si costruisce la scienza; offrire ai giovani situazioni non solo di ricezione dei contenuti, ma di elaborazione creativa e di creazione di comunità.
A questi intenti generali si aggiungono obiettivi che fanno tesoro dell’esperienza dell'esperienza fin qui maturata: aprire la proposta di Scienceground a un pubblico sempre più ampio, cercando di creare un maggior coinvolgimento nelle attività proposte, rafforzando il legame e le connessioni con gli altri eventi e gli assi tematici che attraversano Festivaletteratura, portando autori presenti ad altri eventi a intervenire alle attività e ai laboratori di Scienceground e viceversa.
**In quest'ottica inclusiva, forti delle positive interazioni avute lo scorso anno con alcune scuole superiori mantovane e vista la sovrapposizione tra l'inizio del Festival e dell'anno scolastico in Lombardia, vorremmo potenziare il dialogo con la struttura ospitante -- speranzosi rimanga il bellissimo spazio del liceo Isabella d'Este -- proponendo percorsi "di inizio anno" agli studenti.**

###Il percorso

Scienceground sta alle attività di ExTemporanea come la sagra sta al raccolto: è il momento in cui i contenuti raccolti vengono rielaborati e condivisi.

Nei mesi trascorsi dall'ultimo Festival sono già numerose le attività con le quali stiamo coltivando il terreno:

- **convegno adi** (accademia)

- **Passaporto Nansen** (contatti con realtà giovani e sperimentali, riflessione sui Festival)

- **riproposizione laboratori (Riccardo, Maria Elena, Matteo, Marco)** ("esportazione" di attività del Festival in altri contesti)

- **Fanzine** (produzione materiale, editoria, arte)

- **Science before Christmas** (realtà sociali, dibattiti, arte, tentativo autofinanziamento)

E tante quelle che ci aspettano:

- **Science-Teen (Matteo, Pino)** (ricerca in didattica delle scienze, appoggio Unilu)

- **Genova (Maria Elena)** (scienza, società, politica, industria)

- **Siena** (accademia, fantascienza)

- **Calibro** (fantascienza, bella realtà legata comunque a FestLett tramite il presidente Andrea Tafini)
 
Da qui germineranno le attività di Scienceground 1.666..., che verranno elaborate collegialmente nei mesi di avvicinamento al Festival tramite vari canali di conversazione.
Quello che segue è un canovaccio flessibile con le principali idee.

### Lo spazio

Festivaletteratura mette a disposizione del progetto un luogo dedicato, aperto al pubblico per tutta la durata del festival, in cui si terranno attività permanenti e attività occasionali. 
Le attività saranno supervisionate dai membri della comunità e condotte dai volontari di Festivaletteratura, ai quali verrà fatto un percorso di formazione che è parte integrante delle attività.

### Biblioteca

Verrà curata una bibliografia ragionata sulle tematiche affrontate, con libri per tutte le età e di tutti i livelli, a partire dai libri educativi per bambini fino alle monografie scientifiche. 
I volumi saranno liberamente consultabili, ad ogni libro sarà associata la lista dei membri della comunità che lo hanno letto e che sono disponibili a chiacchierarne. 
Sui tavoli e nelle scansie si troveranno articoli scientifici specializzati.

### Le _Libere Letture_

Questo il nuovo nome scelto per quelli che l'anno scorso sono stati i "gruppi di lettura".
La modifica risponde a una critica sollevataci da qualche persona tra il pubblico rimasta confusa dal nome precendente, che pareva sottintendere la necessità di aver letto il libro in oggetto per partecipare.
Le _Libere Letture_ vogliono essere momenti di lettura e discussione collettive di libri o loro estratti che mettano in luce vari aspetti della scienza e del suo essere tuttuno con la società.
Percorsi di avvicinamento verranno avviati da ExTemporanea nei giorni precedenti il Festival, ma chiunque tra il pubblico presente potrà contribuire alla discussione.
Quando possibile si cercherà un'interazione con l'autore tramite un'intervista (Dame Sally Davies e César Giraldo Herrera l'anno scorso) o una videointervista (Carlo Rovelli nel 2018) in cui sarà il pubblico a farsi soggetto attivo nella scelta delle domande.
Interlocutrice per questo progetto sarà senz'altro Simona Micali, professoressa associata di Letteratura Comparata presso l'università di Siena, con la quale abbiamo avviato un percorso sulla fantascienza.

Tra i testi che stiamo valutando: _L'invenzione della natura_ (Andrea Wulf), _Il paese delle maree_ (Amitav Gosh), _Stella rossa_ (Aleksandr Bogdanov), _Il pugno chiuso_ (Arrigo Boito), _Auto da fé_ (Elias Canetti), _A Moving Border: Alpine Cartographies of Climate Change_ (collettivo Italian Limes, contributi di Bruno Latour e WM1)

### Laboratori

- **Mappa di Humboldt del Rio/Parco del Mincio (Danilo, Fulvio, Marco)** (più giorni, dallo studio alla produzione di un risultato - una mappa dell'ecosistema considerato)

- **Ecosistemi (Danilo, Pino)** (concetto di sistema, termodinamica, ecologia, rapporti di dipendenza e autoregolazione tra specie)

- **Lattice? (Maria Elena)**

- **Misura?** Proposta da discutere (Danilo): è possibile misurare la fitness evolutiva di una specie? Definizioni e simulazioni dinamiche

### I Temi

La crisi climatica globale è stata al centro dell'attenzione del Festival negli ultimi anni; ricordiamo con piacere, per esempio, l'intervista fatta dalla redazione de Il Tascabile a Caspar Henderson e Telmo Pievani. Proprio dalla discussione avvenuta con quest'ultimo, durante la programmazione di Scienceground, è sorta la necessità dare il nostro contributo, prendendoci i nostri tempi e usando le nostre parole. La perdita di biodiversità è uno dei problemi urgenti della crisi attuale, come ci ricorda il report EPBES del maggio 2019, e per comprenderla e comunicarla appieno bisogna descrivere ecosistemi complessi e fragili attingendo agli strumenti forniti dall'ecologia, della geografia e, noi ci scommettiamo, della letteratura e della riflessione sociologica e antropologica. 
Seguendo la lezione latouriana, un altro percorso tematico vorrebbe ritrovare, a partire dal racconto e dallo studio di un materiale apparentemente ordinario, il lattice, la complessa rete di relazioni scientifiche, ambientali e socioeconomiche che ne determinano la disponibilità e gli usi nella nostra vita quotidiana. Vorremmo anche ritornare sul tema della costruzione del dato scientifico, già affrontato nella prima edizione (2018), questa volta dalla prospettiva dell'atto della misura, sia in relazione agli altri filoni tematici già descritti, sia con delle riflessioni ad hoc.

### I nomi
* Amitav Gosh
* Collettivo Ulyanov o Wu Ming per Stella rossa e la storia di Aleksandr Bogdanov
(anche in relazione al progetto di WM1 "Blues per le terre nuove" relativo all'ecosistema del Delta del Po)
* Telmo Pievani
