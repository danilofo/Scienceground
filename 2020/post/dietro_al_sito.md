Dietro al sito
--------------

Ovvero di come le nostre eroine stanno organizzando questo blog, del perché hanno fatto questo e cosa resta ancora da fare (spoiler: un sacco di roba, se avete tempo da perdere e tre lauree in informatica VI PREGO AIUTATECI). 

# Perché un blog

# Perché self-hosted
Il blog scienceground è ospitato su un server virtuale privato (VPS) su Linode (linode.com, datacenter di Francoforte), su cui è installato un LAMP stack (GNU/Linux distro Debian stable, Apache 2, MySQL e PHP) e Wordpress per i contenuti. La seconda funzione del VPS è di essere un archivio per i file prodotti durante i vari incontri di Scienceground e ExTemporanea. Entrambe queste funzioni sono in costante sviluppo e maggiori informazioni su ciascun aspetto si trovano nelle guide tematiche (work in progress). 

Questo ci impone dei costi.

# Perché WordPress

Comunque il self-hosting ci permette di non avere pubblicità sul sito. 

# Perché non usare MailChimp, e come sembra il mondo senza
O Feedburner, che è lo stesso. Perché vogliamo farci le cose da soli, per quanto possibile e pur non essendo degli smanettoni, se non si era già capito dal paragrafo sul self-hosting.

# Perché fate le cose a caso?

Perché non abbiamo studiato abbastanza per farle meglio, oppure perché abbiamo studiato un sacco solo che ci interessavano i micobatteri, il minimalismo americano, i superfluidi, come fare il pane buono con la pasta madre, gli ideali sinistri, quanti ingegneri cishet servono per fare un chilo di cervello (ops, scusate)... Quindi, no, non abbiamo informatiche (e assimilate) fra noi. Con questo post speriamo di commuoverne almeno qualcuna.

# Quali altre tecnologie collegate al sito stiamo sperimentando
Le redattrici di questo sito vivono in vari e lugubri anfratti dell'Europa. Qualcuna, più fortunata, si aggira anche per Bologna. Per coordinare e documentare le modifiche al sito usiamo una stanza su [matrix.org](matrix.org), a cui accediamo con [Riot][riot], che usiamo un po' come sostituto di Slack, per intenderci. La strada per l'autonomia e la crittografia end-to-end totale è ancora un po' accidentata, ma confidiamo nell'avvenire.


[wp]:https://wordpress.org/
[riot]:about.riot.im
