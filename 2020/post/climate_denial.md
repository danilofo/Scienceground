# Negazionismi climatici hard e soft

# Perché non serve blastare la gente

# "Esiste uno studio che smentisce tutto"

# Biblio(sito)grafia ragionata
Qui siti, video, libri di facile lettura e impatto immediato se c'è un minimo di buona fede. Unica regola: tutti citano le fonti, e non si basano su singoli studi di singoli gruppi di ricerca.

* [The last time the globe warmed (PBS Eons)](https://www.youtube.com/watch?v=ldLBoErAhz4)
Un effetto serra estremo ha già cambiato il corso della storia durante il picco climatico tra Paleocene e Eocene (PETM): in questo video sono analizzate le cause e gli effetti sulla biodiversità. Il confronto grafico con il tasso di emissione corrente di gas serra è molto efficace, e viene spiegato bene che il picco fu raggiunto dopo milioni di anni di rilascio naturale di gas serra, e non pochi decenni.

* [How volcano froze the Earth (twice) (PBS Eons)](https://www.youtube.com/watch?v=4ONwQV26L-k&t=111s)
Le grandi glaciazioni globali del Criogeniano furono causate da uno sbilanciamento eccessivo del ciclo del carbonio a causa della spaccatura del supercontinente Rodinia. Questo racconto geologico fa capire quanto sia ben compreso il meccanismo dell'effetto serra, e illustra benissimo anche il ruolo dell'albedo nella termoregolazione del pianeta. 

# TODO
* Rimuovere i link a youtube usando invidio.us
